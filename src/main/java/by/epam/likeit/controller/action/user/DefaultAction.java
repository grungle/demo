package by.epam.likeit.controller.action.user;

import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;

/**
 * This action implementation does nothing. It is using as a stub
 * to render pages with no dynamic content.
 */
public class DefaultAction extends AbstractAction {

    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        return context.getExecutedResult();
    }

    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        return context.getExecutedResult();
    }

}
