package by.epam.likeit.controller.action.user;


import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.bean.view.AnswerView;
import by.epam.likeit.bean.view.QuestionView;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.dao.BaseDAO.ComparatorMode;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.service.IAnswerService;
import by.epam.likeit.service.IQuestionService;
import by.epam.likeit.util.StringUtils;

import java.util.List;

import static by.epam.likeit.controller.action.AbstractAction.RENDER_ROWS_PARAM_NAME;
import static by.epam.likeit.controller.action.AbstractAction.USER_PARAM_NAME;

/**
 * This interface extracts common methods to render the list of answers of the question.
 */
public interface QuestionUpdatable {

    String SHOW_COUNT_PARAM_NAME = "count";
    String SORTBY_PARAM_NAME = "sortBy";
    String ORDERBY_PARAM_NAME = "orderBy";

    String PAGE_ATTR_NAME = "page";
    String QUESTION_ATTR_NAME = "question";
    String MAX_PAGE_ATTR_NAME = "maxPage";
    String CUR_PAGE_ATTR_NAME = "curPage";
    String RATING = "rating";

    /**
     * Validates the given parameters to retrieve date from data source.
     * @see by.epam.likeit.controller.action.AbstractAction
     */
    default void validate(RequestContext context, RequestContext.ExecutedResult res) {
        Long qId = StringUtils.parseLong(context.getPathInfo().substring(context.getPathInfo().lastIndexOf("/") + 1));
        Long page = StringUtils.parseLong(context.getParameter(PAGE_ATTR_NAME));
        Long length = StringUtils.parseLong(context.getParameter(SHOW_COUNT_PARAM_NAME));
        String sortBy = context.getParameter(SORTBY_PARAM_NAME);

        Validator.validateSQLKey(sortBy, res);
        Validator.validateNotNegativeLong(qId, Validator.QUESTION_ID, res);
        Validator.validateNotNegativeNullLong(page, PAGE_ATTR_NAME, res);
        Validator.validateNotNegativeNullLong(length, SHOW_COUNT_PARAM_NAME, res);
    }


    /**
     * Sets set of attributes that are related to the rendering the question to the request scope.
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested question does not exist
     */
    default void update (RequestContext context, IQuestionService questionService, IAnswerService answerService)
            throws DAOSystemException, NoSuchEntityException{

        long qId = Long.valueOf(context.getPathInfo().substring(context.getPathInfo().lastIndexOf("/") + 1));

        long page = Long.valueOf(context.getParameter(PAGE_ATTR_NAME) == null ?
                "1" : context.getParameter(PAGE_ATTR_NAME));

        long length = Long.valueOf(context.getParameter(SHOW_COUNT_PARAM_NAME) == null ?
                context.findAttribute(RENDER_ROWS_PARAM_NAME).toString() : context.getParameter(SHOW_COUNT_PARAM_NAME));

        ComparatorMode orderBy = ComparatorMode.valueOf(context.getParameter(ORDERBY_PARAM_NAME) == null ?
                ComparatorMode.DESC.toString() : context.getParameter(ORDERBY_PARAM_NAME));

        String sortBy = context.getParameter(SORTBY_PARAM_NAME) == null ?
                RATING : context.getParameter(SORTBY_PARAM_NAME);

        QuestionView question = questionService.findView(qId);

        String userLogin = context.findAttribute(USER_PARAM_NAME) == null ? null : ((User)context.findAttribute(USER_PARAM_NAME)).getLogin();

        List<AnswerView> answers = answerService.getAnswers(userLogin, qId, sortBy, orderBy, (page - 1) * length, length);
        long maxPage = (long)Math.ceil(answerService.getAnswersCount(question.getQuestion()) * 1. / length);

        question.setAnswers(answers);

        context.setAttribute(QUESTION_ATTR_NAME, question);
        context.setAttribute(MAX_PAGE_ATTR_NAME, maxPage);
        context.setAttribute(CUR_PAGE_ATTR_NAME, page);
    }

}
