package by.epam.likeit.controller.action.admin;


import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.bean.view.UserView;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;

import java.util.Set;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.controller.dispatcher.JSONDispatcher.JSON_ATTR_NAME;
import static by.epam.likeit.util.DAOUtils.mapRights;


/**
 * This action is invoked if an user is requesting changing rights of another user
 * @see User.UserRights
 * @see by.epam.likeit.service.IUserService#updateRights(User, User, Set)
 */
public class ChangeRightsAction extends AbstractAction {

    /**
     * Checks If the current user requested to change rights of the valid user.
     * Target user's login must be retrieved by {@literal targetLogin} parameter name.
     * @param context facade over Servlet API to manipulate data
     * @return the result of the validation.
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        String targetLogin = context.getParameter(TARGET_USER_PARAM_NAME);

        Validator.validateNotEmptyString(targetLogin, Validator.USER_LOGIN, res);

        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     * @see by.epam.likeit.service.IUserService#updateRights(User, User, Set)
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);
        String targetLogin = context.getParameter(TARGET_USER_PARAM_NAME);
        Set<User.UserRights> rights = mapRights(context);

        RequestContext.ExecutedResult res = context.getExecutedResult();
        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            User target = USER_SERVICE.find(targetLogin);

            USER_SERVICE.updateRights(curUser, target, rights);
            UserView uv = USER_SERVICE.getInfo(target, Long.valueOf(context.findAttribute(RENDER_ROWS_PARAM_NAME).toString()));

            context.setAttribute(JSON_ATTR_NAME, uv);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);
            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] changed right of " + targetLogin + " to "  + rights);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.addErrorMessage(e.getErrorLabel());
            res.setState(State.valueOf(e.getTextStatus()));
        }

        return res;
    }



}

