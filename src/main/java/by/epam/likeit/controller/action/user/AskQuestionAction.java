package by.epam.likeit.controller.action.user;

import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.DAOUtils;
import by.epam.likeit.util.StringUtils;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;

/**
 * This action is invoked if an user is creating a new question.
 * @see by.epam.likeit.service.IQuestionService#newQuestion(Question, User)
 */
public class AskQuestionAction extends AbstractAction {

    /**
     * Validates header and body parameters.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();

        Long topicId = StringUtils.parseLong(context.getParameter(TOPIC_ID_PARAM_NAME));
        String qHeader = context.getParameter(QUESTION_HEADER_PARAM_NAME);
        String qBody = context.getParameter(QUESTION_BODY_PARAM_NAME);

        Validator.validateNotEmptyString(qHeader, Validator.QUESTION_HEADER, res);
        Validator.validateNotEmptyString(qBody, Validator.QUESTION_BODY, res);
        Validator.validateNotNegativeLong(topicId, Validator.TOPIC_ID, res);

        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);
        RequestContext.ExecutedResult result = context.getExecutedResult();

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            Question question = DAOUtils.mapNewQuestion(context, curUser);
            long generatedId = QUESTION_SERVICE.newQuestion(question, curUser);
            result.setDestination(getJspUri() + generatedId);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] asked a question " + generatedId);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            result.setState(State.valueOf(e.getTextStatus()));
            result.addErrorMessage(e.getErrorLabel());
        }

        return result;
    }

}
