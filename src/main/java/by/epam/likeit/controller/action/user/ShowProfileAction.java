package by.epam.likeit.controller.action.user;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.bean.view.UserView;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;

/**
 * The action is invoked if an user is requesting to render the page of another user or himself
 * @see by.epam.likeit.service.IUserService#showProfile(User, User, long)
 */
public class ShowProfileAction extends AbstractAction {

    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        return context.getExecutedResult();
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User) context.findAttribute(USER_PARAM_NAME);
        String path = context.getPathInfo();
        String targetName = path.substring(path.lastIndexOf("/") + 1);

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            UserView target = USER_SERVICE.showProfile(new UserBuilder().login(targetName).createNewUser(), curUser,
                    Long.valueOf(context.findAttribute(RENDER_ROWS_PARAM_NAME).toString()));
            context.setAttribute(_TARGET_USER_PARAM_NAME, target);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.setState(State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }

        return res;
    }
}
