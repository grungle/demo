package by.epam.likeit.controller.action;
import by.epam.likeit.controller.HttpMethod;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Factory that collect all possible actions available to a client.
 * For registration new action use {@link ActionFactory#registerAction(AbstractAction)}
 * For finding appropriate action use {@link ActionFactory#getAction(String, HttpMethod)}
 */

public class ActionFactory {

    // Vars --------------------------------------------------------------------------------------

    private static Set<AbstractAction> actions = ConcurrentHashMap.newKeySet();

    // Constructors ------------------------------------------------------------------------------

    private ActionFactory(){}

    // Methods -----------------------------------------------------------------------------------

    /**
     * Finds and returns appropriate action depending on requested URI and HTTP method.
     * If there is more then one appropriate action, method returns first appropriate
     * action to be found in foreach loop by {@link ActionFactory#actions} set.
     *
     * NOTE!!! Method does not create new instance or copy of appropriate action, just returns
     * link to it. So each action is storing in one instance and modification of any action can
     * be cause of some issues in the future.
     *
     * @param uri request URI relative to the context path of the application
     * @param method {@linkplain HttpMethod HTTP method} using by client to request
     * @return appropriate action depending on requested URI and HTTP method
     * @throws ActionNotSupportedException if there is no registered action
     * by provided URI with using HTTP method
     * @see AbstractAction#accept(String, HttpMethod)
     */
    public static AbstractAction getAction(final String uri,
                                           final HttpMethod method) throws ActionNotSupportedException {
        for (AbstractAction action : actions) {
            if (action.accept(uri, method)) {
                return action;
            }
        }
        throw new ActionNotSupportedException(uri);
    }

    /**
     * Put an action in {@link ActionFactory#actions}.
     * If two different actions have different overlapping patterns,
     * the {@link Set} cannot determine it and replace action storing in
     * collection to new.
     * Action can be added at runtime if its compiled class is present in
     * classpath of the application
     * @param action action to be registered
     */
    public static void registerAction(AbstractAction action) {
        actions.add(action);
    }

    public static Set<AbstractAction> getAll() {
        return actions;
    }

}
