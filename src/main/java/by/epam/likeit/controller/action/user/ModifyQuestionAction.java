package by.epam.likeit.controller.action.user;

import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.DAOUtils;

import java.util.Date;

import static by.epam.likeit.bean.Question.QuestionState.MODIFIED;
import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.util.DAOUtils.*;

/**
 * This action is invoked if an user is modifying the question.
 * @see by.epam.likeit.service.IQuestionService#modify(Question, Question, User, User)
 */
public class ModifyQuestionAction extends AbstractAction implements QuestionUpdatable{

    /**
     * Validates the id that is retrieved from parameters.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        validate(context, res);
        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * Also this method sets the time of the modifying current date
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        Long qId = Long.valueOf(context.getParameter(QUESTION_ID_PARAM_NAME));
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            String ownerLogin = QUESTION_SERVICE.getOwnerLogin(mapQuestionId(qId));
            User ownerUser = USER_SERVICE.find(ownerLogin);
            curUser = USER_SERVICE.find(curUser.getLogin());

            context.setAttribute(MSG_OWNER_ATTR_NAME, ownerLogin);
            context.setAttribute(MSG_STATE_ATTR_NAME, MODIFIED.toString());

            Question oldQuestion = QUESTION_SERVICE.find(qId);
            Question newQuestion = DAOUtils.mapQuestion(context);
            newQuestion.setLastUpdate(new Date());

            QUESTION_SERVICE.modify(oldQuestion, newQuestion, curUser, ownerUser);
            update(context, QUESTION_SERVICE, ANSWER_SERVICE);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] modified the question " + qId);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.addErrorMessage(e.getErrorLabel());
            res.setState(State.valueOf(e.getTextStatus()));
        }
        return res;
    }
}
