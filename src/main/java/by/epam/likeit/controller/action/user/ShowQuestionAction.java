package by.epam.likeit.controller.action.user;

import by.epam.likeit.bean.User;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;

import static by.epam.likeit.util.DAOUtils.mapQuestionId;

/**
 * This method is invoked if an user is requesting to read the question
 * @see by.epam.likeit.service.IQuestionService#find(long)
 */
public class ShowQuestionAction extends AbstractAction implements QuestionUpdatable {

    /**
     * Validates the parameters.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     * @see QuestionUpdatable#validate(RequestContext, RequestContext.ExecutedResult)
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        validate(context, res);
        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        RequestContext.ExecutedResult result = context.getExecutedResult();
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);
        Long qId = Long.valueOf(context.getPathInfo().substring(context.getPathInfo().lastIndexOf("/") + 1));

        try {

            USER_SERVICE.showQuestion(curUser, mapQuestionId(qId));
            update(context, QUESTION_SERVICE, ANSWER_SERVICE);

        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            result.setState(State.valueOf(e.getTextStatus()));
            result.addErrorMessage(e.getErrorLabel());
        }

        return result;
    }
}