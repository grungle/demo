package by.epam.likeit.controller.action.admin;

import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.action.user.TopicUpdatable;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.StringUtils;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;

/**
 * This action is invoked if an user is deleting a topic
 * @see by.epam.likeit.service.ITopicService#remove(long, User)
 */
public class DeleteTopicAction extends AbstractAction implements TopicUpdatable{

    /**
     * Checks if the topic  to be deleted is specified in the parameters and could be converted to id.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        Long tId = StringUtils.parseLong(context.getParameter(TOPIC_ID_PARAM_NAME));
        Validator.validateNotNegativeLong(tId, Validator.TOPIC_ID, res);
        validate(context, res);
        return res;
    }

    /**
     * Checks if the current user is allowed to delete a topic and executes it.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     * @see by.epam.likeit.service.ITopicService#remove(long, User)
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);
        Long tId = Long.valueOf(context.getParameter(TOPIC_ID_PARAM_NAME));

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            Topic t = new Topic();
            t.setId(tId);

            TOPIC_SERVICE.remove(tId, curUser);
            update(context, TOPIC_SERVICE);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);
            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] deleted topic with id " + tId);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.setState(State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }

        return res;
    }
}
