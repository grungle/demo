package by.epam.likeit.controller.action.user;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.ImageUtils;

import java.io.File;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;

/**
 * This action is invoked if an user has requested changing of his avatar image.
 * After successful execution, user will be able to see the new avatar.
 * @see by.epam.likeit.service.IUserService#updateAvatar(User)
 */
public class EditAvatarAction extends AbstractAction {

    /**
     * Validates the given avatar file extension.
     * NOTE!!! This method does not validate file size!
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        User user = new User();
        user.setAvatar((context.getParameter(USER_AVA_PARAM_NAME)));
        Validator.validateBeanAvatar(user, Validator.AVATAR_FIELD, res, true);
        return res;
    }

    /**
     * Changes the avatar of the current user.
     * There can be one of four possible situations:
     * <ul>
     *     <li> User has default avatar:
     *         <ul>
     *              <li>An attempt to set default avatar. In this case method does nothing.</li>
     *         </ul>
     *         <ul>
     *             <li>
     *             An attempt to set new avatar. In this case method replaces image
     *             to avatar storage directory and renames it to userlogin.fileextension template.
     *             by <code>context</code> and
     *             </li>
     *         </ul>
     *     </li>
     *     <li> User has custom avatar:
     *         <ul>
     *              User has not specified new avatar name(null). In this case old avatar will be deleted, avatar location
     *              will set to default value.
     *         </ul>
     *         <ul>
     *              User has specified new avatar. In this case old avatar file will be replaced to new.
     *         </ul>
     *     </li>
     * </ul>
     *
     * NOTE! File must be already retrieved from request and be ready to work with it.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User) context.findAttribute(USER_PARAM_NAME);

        String location = (String) context.findAttribute(AVATAR_STORAGE_PATH_PARAM_NAME);
        String def_loc = (String) context.findAttribute(AVATAR_DEFAULT_PARAM_NAME);
        String address = context.getRealPath(location);

        RequestContext.ExecutedResult result = context.getExecutedResult();

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            File file = context.getFile(USER_AVA_PARAM_NAME);
            String path = curUser.getAvatar();

            ImageUtils.updateAvatar(curUser, location, def_loc, address, file, path);

            USER_SERVICE.updateAvatar(curUser);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] edited avatar");
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            result.addErrorMessage(e.getErrorLabel());
            result.setState(RequestContext.ExecutedResult.State.valueOf(e.getTextStatus()));
        }

        return result;

    }


}
