package by.epam.likeit.controller.action;


import by.epam.likeit.controller.HttpMethod;
import by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.ReflectionUtils;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.management.ReflectionException;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

/**
 * This class configures set of actions and register them to {@link ActionFactory} after
 * reading them from XML file.
 */
public class ActionStAXConfigurator extends DefaultHandler {

    // Vars -----------------------------------------------------------------------------------------

    private XMLEventReader eventReader;
    private ActionXmlTag currentTag;

    private AbstractAction tempAction;
    private ArrayList<HttpMethod> methods;
    private String pattern;
    private DispatcherType dispatcher;
    private boolean encodedURL;

    // Constructors ----------------------------------------------------------------------------------

    /**
     * Configures the new parser using reader
     * @param src The reader associated to the the XML file
     * @throws ParserConfigurationException If configurator cannot be inititalized
     * @throws SAXException If something fails at parsing level
     * @throws FileNotFoundException If the file does not found
     * @throws XMLStreamException If some unregistered processing error occurred
     */
    public ActionStAXConfigurator(Reader src) throws ParserConfigurationException, SAXException,
            FileNotFoundException, XMLStreamException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        eventReader = factory.createXMLEventReader(src);
        clear();
    }

    /**
     * Clears the fields to its default values
     */
    private void clear(){
        tempAction = null;
        methods = new ArrayList<>(7);
        currentTag = null;
        dispatcher = null;
        pattern = null;
        encodedURL = false;
    }

    /**
     * Parses the given XML file and invokes related listeners to set parameters of the actions
     * and registers them after end of each action.
     * @throws IOException If something fails at I/O level
     * @throws SAXException If something fails at parsing level
     */
    public void parse() throws IOException, SAXException {
        try {
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                switch (event.getEventType()) {
                    case XMLStreamConstants.START_ELEMENT:
                        startElementEvent(event);
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        characters(event);
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        endElement(event);
                        break;
                }
            }
        } catch (XMLStreamException e) {
            LikeLogger.err("XML Stream exception has occurred while reading next event.", e);
        }
    }

    /**
     * Validates the given XML file using given XSD scheme
     * @param schemeFile The XSD scheme file location
     * @param xmlFile The XML to be validated file location
     * @return true if the given XML is valid, otherwise false
     */
    public boolean validateSource(File schemeFile, File xmlFile){
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        try {
            SchemaFactory factory = SchemaFactory.newInstance(language);
            Schema schema = factory.newSchema(schemeFile);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(xmlFile);
            validator.validate(source);
            return true;
        } catch (SAXException e) {
            LikeLogger.err("XML file is not valid.", e);
        } catch (IOException e) {
            LikeLogger.err("Can't read file:", e);
        }
        return false;
    }

    /**
     * @see XMLStreamConstants#START_ELEMENT
     */
    private void startElementEvent(XMLEvent event){
        StartElement element = event.asStartElement();
        String name = element.getName().getLocalPart();

        switch (ActionXmlTag.valueOf(name.toUpperCase())){
            case ACTION:
                Attribute attrName = element.getAttributeByName(QName.valueOf("urlPattern"));
                pattern = attrName.getValue();
                attrName = element.getAttributeByName(QName.valueOf("dispatcher"));
                dispatcher = DispatcherType.valueOf(attrName.getValue());
                attrName = element.getAttributeByName(QName.valueOf("encodeURL"));
                encodedURL = Boolean.valueOf(attrName == null ? "false" : attrName.getValue());
                currentTag = ActionXmlTag.ACTION;
                break;
            case CLASS:
                currentTag = ActionXmlTag.CLASS;
                break;
            case ACTIONS:
                currentTag = ActionXmlTag.ACTIONS;
                break;
            case METHOD:
                currentTag = ActionXmlTag.METHOD;
                break;
            case REF:
                currentTag = ActionXmlTag.REF;
                break;

        }
    }

    /**
     * @see XMLStreamConstants#CHARACTERS
     */
    private void characters(XMLEvent event) {
        String value = event.asCharacters().getData().trim();
        if (value.isEmpty())
            return;

        switch (currentTag){
            case ACTION:
                break;
            case ACTIONS:
                break;
            case CLASS:
                try {
                    tempAction = ReflectionUtils.newInstance(value);
                } catch (ReflectionException e) {
                    LikeLogger.warn("Cannot create an instance of class", e);
                }
                break;
            case METHOD:
                HttpMethod method = HttpMethod.valueOf(value.toUpperCase());
                methods.add(method);
                break;
            case REF:
                tempAction.setJspUri(value);
                break;
        }
    }

    /**
     * @see XMLStreamConstants#END_ELEMENT
     */
    private void endElement(XMLEvent event) {
        EndElement element = event.asEndElement();
        String elname = element.getName().getLocalPart();

        switch (ActionXmlTag.valueOf(elname.toUpperCase())){
            case ACTION:
                HttpMethod[] arr = new HttpMethod[methods.size()];
                tempAction.setMethods(methods.toArray(arr));
                tempAction.setPattern(pattern);
                tempAction.setDispatcherType(dispatcher);
                tempAction.setEncodedURL(encodedURL);
                ActionFactory.registerAction(tempAction);
                clear();
                break;
            case ACTIONS:
                break;
            case METHOD:
                break;
            case REF:
                break;
        }
        currentTag = null;
    }

    // Inner classes --------------------------------------------------------------------------------

    /**
     *
     */
    private enum ActionXmlTag{
        ACTION, ACTIONS, METHOD, REF, CLASS
    }
}
