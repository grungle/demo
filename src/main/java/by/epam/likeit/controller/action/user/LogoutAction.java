package by.epam.likeit.controller.action.user;


import by.epam.likeit.bean.User;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.logger.LikeLogger;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;

/**
 * This action is invoked if a user is going to logout.
 *
 * After successful execution, user who requested this operation
 * will no longer be able to use his personal information that related
 * to the current session.
 * If user can be logged in  in more than one web browsers, only one browser
 * that send this request will loose relation to the session.
 */
public class LogoutAction extends AbstractAction {

    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        return context.getExecutedResult();
    }

    /**
     * Clear session parameters and invalidates the session related to the current thread.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User user = (User)context.findAttribute(USER_PARAM_NAME);
        context.setAttribute(USER_PARAM_NAME, null, SESSION);
        context.invalidate();
        LikeLogger.trace("User " + user.getLogin() +
                "[" + user.getRights()  + "] logouted");
        return context.getExecutedResult();
    }
}
