package by.epam.likeit.controller.action.user;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.PropertiesLoader;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.bean.view.UserView;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.controller.dispatcher.JSONDispatcher.JSON_ATTR_NAME;
import static by.epam.likeit.util.DAOUtils.LOGIN_ATTR_NAME;
import static by.epam.likeit.util.DAOUtils.mapUser;

/**
 * This action is invoked if an user has requested changing of his profile details.
 * @see by.epam.likeit.service.IUserService#update(User, User)
 */
public class EditProfileAction extends AbstractAction {

    /**
     * Validates the provided user attributes that is retrieved from the request using regular expressions.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     * @see Validator#validateBeanProperty(Object, String, RequestContext.ExecutedResult, boolean)
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();

        User user = mapUser(context);

        Validator.validateBeanProperty(user, Validator.LOGIN_FIELD, res, false);
        Validator.validateBeanProperty(user, Validator.FNAME_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.LNAME_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.EMAIL_FIELD, res, false);
        Validator.validateBeanProperty(user, Validator.COUNTRY_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.CITY_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.COMPANY_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.POSITION_FIELD, res, true);

        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * Additionally, as some of user attributes must be unique, this method checks if
     * the given attributes is unique and sets special error messages if the given
     * sensitive data is duplicated.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User oldUser = (User)context.findAttribute(USER_PARAM_NAME);
        context.setAttribute(LOGIN_ATTR_NAME, oldUser.getLogin());
        RequestContext.ExecutedResult res = context.getExecutedResult();

        User newUser = mapUser(context);

        try {
            boolean emailIsUsed = USER_SERVICE.checkUniqueEmail(newUser);
            User curUser = USER_SERVICE.find(oldUser.getLogin());

            if (!emailIsUsed && !curUser.getEmail().equals(newUser.getEmail())){
                res.setState(State.INVALID);
                res.addErrorMessage(PropertiesLoader.get(PropertiesLoader.VALIDATION_EMAIL_WRONG));
                return res;
            }

            User updatedUser = USER_SERVICE.find(curUser.getLogin());

            if (res.getState() == State.SUCCESSFUL)
                context.setAttribute(USER_PARAM_NAME, updatedUser);

            UserView uv = USER_SERVICE.update(curUser, newUser);
            context.setAttribute(JSON_ATTR_NAME, uv);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] edited profile");
        } catch (DAOException e) {
            LikeLogger.warn(e, e);
            res.setState(State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }

        return res;
    }

}
