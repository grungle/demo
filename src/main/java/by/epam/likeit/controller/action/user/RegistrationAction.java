package by.epam.likeit.controller.action.user;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.logger.LikeLogger;
import org.mindrot.jbcrypt.BCrypt;

import java.io.File;
import java.util.concurrent.locks.ReentrantLock;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType.JSON;
import static by.epam.likeit.util.DAOUtils.mapUser;
import static by.epam.likeit.util.ImageUtils.replaceAvatar;

/**
 * This action is invoked if an user is registering.
 * If user has specified his own avatar, it will be set, otherwise user
 * will be has the default avatar image.
 * @see by.epam.likeit.service.IUserService#register(User)
 */
public class RegistrationAction extends AbstractAction {

    private static final String EMAIL_SPAN = "email_span";
    private static final String LOGIN_SPAN = "login_span";
    /**
     * This lock helps to prevent concurrent registration of users
     * with the same sensitive data that not allow duplicates.
     */
    private ReentrantLock lock = new ReentrantLock();

    /**
     * Validates the provided user attributes that is retrieved from the request using regular expressions.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        User user = mapUser(context);

        Validator.validateBeanProperty(user, Validator.LOGIN_FIELD, res, false);
        Validator.validateBeanProperty(user, Validator.FNAME_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.LNAME_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.EMAIL_FIELD, res, false);
        Validator.validateBeanProperty(user, Validator.PASSWORD_FIELD, res, false);
        Validator.validateBeanProperty(user, Validator.COUNTRY_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.CITY_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.COMPANY_FIELD, res, true);
        Validator.validateBeanProperty(user, Validator.POSITION_FIELD, res, true);
        Validator.validateBeanAvatar(user, Validator.AVATAR_FIELD, res, true);

        if (res.getState() == State.INVALID) res.setDispatcher(JSON);
        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * Besides of that, method checks if the user sensitive data has no duplicates
     * in the database.
     * If user data is not unique, the dispatcher type is set to JSON
     * and returns an error with appropriate message.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution.
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        RequestContext.ExecutedResult result = context.getExecutedResult();
        User newUser = mapUser(context);
        String location = (String) context.findAttribute(AVATAR_STORAGE_PATH_PARAM_NAME);
        String def_loc = (String) context.findAttribute(AVATAR_DEFAULT_PARAM_NAME);
        String address = context.getRealPath(location);

        try {
            lock.lock();

            if (!checkUnique(result, newUser)) return result;

            File file = context.getFile(USER_AVA_PARAM_NAME);

            if (file == null){
                newUser.setAvatar(def_loc);
            } else {
                replaceAvatar(newUser, location, def_loc, address, file);
            }

            newUser.setPassword(BCrypt.hashpw(newUser.getPassword(), BCrypt.gensalt()));
            USER_SERVICE.register(newUser);
            User curUser = USER_SERVICE.find(newUser.getLogin());

            result.setDestination(getJspUri() + newUser.getLogin());
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] has been registered.");
        }  catch (DAOException e) {
            //if record wasn't created with no error
            result.setState(State.valueOf(e.getTextStatus()));
            result.addErrorMessage(e.getErrorLabel());
        } finally {
            lock.unlock();
        }
        return result;
    }

    /**
     * Checks if the data has no duplicates in the database.
     * @param result The result to set status of validation
     * @param newUser The user to be checked
     * @return true if the data has no duplicates, otherwise false
     * @throws DAOSystemException If something fails at DAO layer
     */
    private boolean checkUnique(RequestContext.ExecutedResult result, User newUser)
            throws DAOSystemException {

        boolean emailIsUsed = USER_SERVICE.checkUniqueEmail(newUser);
        User copyUser = USER_SERVICE.find(newUser.getLogin());
        if (!emailIsUsed || copyUser != null) {
            result.setState(State.INVALID);
            result.setDispatcher(JSON);
            if (!emailIsUsed) result.addErrorMessage(EMAIL_SPAN);
            if (copyUser != null) result.addErrorMessage(LOGIN_SPAN);
            return false;
        }
        return true;
    }


}
