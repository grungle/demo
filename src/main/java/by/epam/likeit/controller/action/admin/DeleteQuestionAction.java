package by.epam.likeit.controller.action.admin;

import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.StringUtils;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.util.DAOUtils.mapQuestionId;

/**
 * This action is invoked if user is deleting a question
 * @see by.epam.likeit.service.IQuestionService#remove(Question, User)
 */
public class DeleteQuestionAction extends AbstractAction {

    /**
     * Checks the validity of the given question id and the given topic id.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     * @see Validator#validateNotNegativeLong(Long, String, RequestContext.ExecutedResult)
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();

        Long qId = StringUtils.parseLong(context.getParameter(QUESTION_ID_PARAM_NAME));
        Long tId = StringUtils.parseLong(context.getParameter(TOPIC_ID_PARAM_NAME));

        Validator.validateNotNegativeLong(qId, Validator.QUESTION_ID, res);
        Validator.validateNotNegativeLong(tId, Validator.TOPIC_ID, res);

        return res;
    }

    /**
     * Checks if the current user is allowed to delete the questions that id
     * is specified in the {@link AbstractAction#TOPIC_ID_PARAM_NAME parameter}
     *
     * If the question was deleted, this method supports redirecting to the related topic page.
     * Result of the execution of the method is prepared to be redirected to the new topic page.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     * @see by.epam.likeit.service.IQuestionService#remove(Question, User)
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);
        Long qId = Long.valueOf(context.getParameter(QUESTION_ID_PARAM_NAME));
        Long tId = Long.valueOf(context.getParameter(TOPIC_ID_PARAM_NAME));

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            QUESTION_SERVICE.remove(mapQuestionId(qId), curUser);
            res.setDestination(getJspUri() + tId);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);
            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] deleted question " + qId);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.addErrorMessage(e.getErrorLabel());
            res.setState(State.valueOf(e.getTextStatus()));
        }

        return res;
    }
}

