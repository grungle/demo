package by.epam.likeit.controller.action.user;


import by.epam.likeit.bean.User;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.BaseDAO;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;

/**
 * This action is invoked if the current user is requesting list of all topics
 * @see by.epam.likeit.service.ITopicService#getAll(String, BaseDAO.ComparatorMode, long, long)
 */
public class ShowAllTopicAction extends AbstractAction implements TopicUpdatable {

    /**
     * Validates if the requested parameters are valid
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        validate(context, res);
        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        try {
            User curUser = (User)context.findAttribute(USER_PARAM_NAME);
            update(context, TOPIC_SERVICE);
            if (curUser != null){
                curUser = USER_SERVICE.find(curUser.getLogin());
                context.setAttribute(USER_PARAM_NAME, curUser, SESSION);
            }
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.setState(State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }
        return res;
    }

}
