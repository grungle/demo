package by.epam.likeit.controller.action.user;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.DAOUtils;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;

/**
 * This action is invoked if an user is writing the answer
 * @see by.epam.likeit.service.IAnswerService#newAnswer(Answer, User)
 */
public class WriteAnswerAction extends AbstractAction implements QuestionUpdatable {

    /**
     * Validates the answer using regular expression.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation.
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        String aBody = context.getParameter(ANSWER_BODY_PARAM_NAME);
        Validator.validateNotEmptyString(aBody, Validator.ANSWER_BODY, res);
        validate(context, res);
        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            Answer answer = DAOUtils.mapNewAnswer(context, curUser);
            ANSWER_SERVICE.newAnswer(answer, curUser);
            update(context, QUESTION_SERVICE, ANSWER_SERVICE);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] has wrote new answer ");
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.setState(RequestContext.ExecutedResult.State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }

        return res;
    }
}
