package by.epam.likeit.controller.action.user;


import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.dao.BaseDAO.ComparatorMode;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.service.ITopicService;
import by.epam.likeit.util.StringUtils;

import java.util.List;

import static by.epam.likeit.controller.action.AbstractAction.RENDER_ROWS_PARAM_NAME;
import static by.epam.likeit.controller.action.user.QuestionUpdatable.*;
import static by.epam.likeit.dao.BaseDAO.ComparatorMode.ASC;

/**
 * Interface extracts common methods that are used to render a list of topics list.
 * @see by.epam.likeit.controller.action.AbstractAction
 */
public interface TopicUpdatable {

    String NAME_PARAM_NAME = "name";
    String TOPICS_ATTR_NAME = "topics";

    /**
     * Validates the parameters to render topic.
     * @param context The facade over Servlet API to manipulate data
     * @param result The result of the validation
     */
    default void validate(RequestContext context, RequestContext.ExecutedResult result){
        Long page = StringUtils.parseLong(context.getParameter(PAGE_ATTR_NAME));
        Long count = StringUtils.parseLong(context.getParameter(SHOW_COUNT_PARAM_NAME));
        String sortBy = context.getParameter(SORTBY_PARAM_NAME);

        Validator.validateSQLKey(sortBy, result);
        Validator.validateNotNegativeNullLong(count, Validator.COUNT_FIELD, result);
        Validator.validateNotNegativeNullLong(page, Validator.PAGE_FIELD, result);

    }

    /**
     * Sets set of attributes that are related to the rendering the topic to the request scope.
     * @param context The facade over Servlet API to manipulate data
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested question does not exist
     */
    default void update(RequestContext context, ITopicService topicService)
            throws DAOSystemException, NoSuchEntityException {

        String sortBy = context.getParameter(SORTBY_PARAM_NAME) == null ? NAME_PARAM_NAME : context.getParameter(SORTBY_PARAM_NAME);
        String orderBy = context.getParameter(ORDERBY_PARAM_NAME) == null ? ASC.toString() : context.getParameter(ORDERBY_PARAM_NAME);
        long page = context.getParameter(PAGE_ATTR_NAME) == null ? 1 : Long.valueOf(context.getParameter(PAGE_ATTR_NAME));
        long length = context.getParameter(SHOW_COUNT_PARAM_NAME) == null ?
                Integer.valueOf(context.findAttribute(RENDER_ROWS_PARAM_NAME).toString()) :
                Integer.valueOf(context.getParameter(SHOW_COUNT_PARAM_NAME));

        List<Topic> topics = topicService.getAll(sortBy, ComparatorMode.valueOf(orderBy.toUpperCase()),
                (page - 1) * length, length);

        long maxPage = (long) Math.ceil(topicService.getTopicsCount() * 1./ length);

        context.setAttribute(TOPICS_ATTR_NAME, topics);
        context.setAttribute(CUR_PAGE_ATTR_NAME, page);
        context.setAttribute(MAX_PAGE_ATTR_NAME, maxPage);
    }

}
