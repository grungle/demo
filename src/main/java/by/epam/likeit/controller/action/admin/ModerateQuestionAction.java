package by.epam.likeit.controller.action.admin;


import by.epam.likeit.bean.AbstractMessage.Type;
import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.action.user.QuestionUpdatable;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.DAOUtils;
import by.epam.likeit.util.StringUtils;

import java.util.Date;

import static by.epam.likeit.bean.Question.QuestionState.MODIFIED;
import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.util.DAOUtils.MSG_OWNER_ATTR_NAME;
import static by.epam.likeit.util.DAOUtils.MSG_STATE_ATTR_NAME;

/**
 * This action is invoked if an user is moderating a question
 * @see by.epam.likeit.service.IQuestionService#moderate(Question, Question, ModerationInfo, User)
 */
public class ModerateQuestionAction extends AbstractAction implements QuestionUpdatable {

    /**
     * Validates the answer id retrieved from parameters if it could be converted to id.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        validate(context, res);
        Long aId = StringUtils.parseLong(context.getParameter(QUESTION_ID_PARAM_NAME));
        Validator.validateNotNegativeLong(aId, Validator.ANSWER_ID, res);
        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User) context.findAttribute(USER_PARAM_NAME);
        long qId = Long.valueOf(context.getParameter(QUESTION_ID_PARAM_NAME));

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            Question oldQuestion = QUESTION_SERVICE.find(qId);
            context.setAttribute(MSG_OWNER_ATTR_NAME, oldQuestion.getUser().getLogin());
            context.setAttribute(MSG_STATE_ATTR_NAME, MODIFIED.toString());
            ModerationInfo info = DAOUtils.mapModeration(context, Type.QUESTION);
            Question newQuestion = DAOUtils.mapQuestion(context);
            newQuestion.setLastUpdate(new Date());
            QUESTION_SERVICE.moderate(oldQuestion, newQuestion, info, curUser);

            update(context, QUESTION_SERVICE, ANSWER_SERVICE);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] moderated question " + qId);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.setState(State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }
        return res;

    }
}
