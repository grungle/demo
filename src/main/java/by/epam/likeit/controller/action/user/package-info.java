/**
 * Package contains set of {@link by.epam.likeit.controller.action.AbstractAction}
 * implementing classes that can be executed by every user.
 * @see by.epam.likeit.bean.User.UserRights
 * @see by.epam.likeit.controller.action.AbstractAction
 */
package by.epam.likeit.controller.action.user;