package by.epam.likeit.controller.action.user;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.StringUtils;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.util.DAOUtils.mapAnswerId;

/**
 * This action is invoked if an user is deleting the answer
 * @see by.epam.likeit.service.IAnswerService#remove(Answer, User, User)
 */
public class DeleteAnswerAction extends AbstractAction implements QuestionUpdatable {

    /**
     * Validates the answer id that retrieved from parameters
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        Long aId = StringUtils.parseLong(context.getParameter(ANSWER_ID_PARAM_NAME));
        RequestContext.ExecutedResult res = context.getExecutedResult();
        Validator.validateNotNegativeLong(aId, Validator.ANSWER_ID, res);
        validate(context, res);

        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);
        Long aId = Long.valueOf(context.getParameter(ANSWER_ID_PARAM_NAME));

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            String owner = ANSWER_SERVICE.getOwner(mapAnswerId(aId));
            User ownerUser = USER_SERVICE.find(owner);

            ANSWER_SERVICE.remove(mapAnswerId(aId), curUser, ownerUser);
            update(context, QUESTION_SERVICE, ANSWER_SERVICE);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] deleted the answer " + aId);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.setState(RequestContext.ExecutedResult.State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }
        return res;
    }

}
