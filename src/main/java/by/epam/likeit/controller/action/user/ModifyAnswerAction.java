package by.epam.likeit.controller.action.user;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.DAOUtils;
import by.epam.likeit.util.StringUtils;

import java.util.Date;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.util.DAOUtils.mapAnswerId;

/**
 * This action is invoked if an user is trying to modify his answer.
 * @see by.epam.likeit.service.IAnswerService#modify(Answer, Answer, User, User)
 */
public class ModifyAnswerAction extends AbstractAction implements QuestionUpdatable {

    /**
     * Validates the id that is retrieved from parameters.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        Long aId = StringUtils.parseLong(context.getParameter(ANSWER_ID_PARAM_NAME));
        validate(context, res);
        Validator.validateNotNegativeLong(aId, Validator.ANSWER_ID, res);
        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * Also this method sets the time of the modifying current date
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        Long aId = Long.valueOf(context.getParameter(ANSWER_ID_PARAM_NAME));
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            String ownerLogin = ANSWER_SERVICE.getOwner(mapAnswerId(aId));
            User ownerUser = USER_SERVICE.find(ownerLogin);
            curUser = USER_SERVICE.find(curUser.getLogin());

            Answer oldAnswer = ANSWER_SERVICE.find(aId);
            Answer newAnswer = DAOUtils.mapAnswer(context, ownerUser);
            newAnswer.setLastUpdate(new Date());

            ANSWER_SERVICE.modify(oldAnswer, newAnswer, ownerUser, curUser);
            update(context, QUESTION_SERVICE, ANSWER_SERVICE);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] modified the answer " + aId);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.setState(RequestContext.ExecutedResult.State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }

        return res;
    }

}
