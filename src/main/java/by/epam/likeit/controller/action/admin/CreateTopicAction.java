package by.epam.likeit.controller.action.admin;


import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;

/**
 * This action is invoked if an user is creating a new topic
 * @see by.epam.likeit.service.ITopicService#newTopic(Topic, User)
 */
public class CreateTopicAction extends AbstractAction {

    /**
     * Validates if the new topic name is not empty and can be created.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        String tName = context.getParameter(TOPIC_NAME_PARAM_NAME);

        Validator.validateNotEmptyString(tName, Validator.TOPIC_NAME, res);

        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * If the validation is successful, created the topic with the given name.
     * Result of the execution of the method is prepared to be redirected to the new topic page.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     * @see by.epam.likeit.controller.context.RequestContext.ExecutedResult
     * @see by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType
     * @see by.epam.likeit.service.ITopicService#newTopic(Topic, User)
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);
        String tName = context.getParameter(TOPIC_NAME_PARAM_NAME);

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            Topic topic = new Topic();
            topic.setName(tName);
            long generatedId = TOPIC_SERVICE.newTopic(topic, curUser);
            res.setDestination(getJspUri() + generatedId);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);
            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] created new topic with id " + generatedId);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.addErrorMessage(e.getErrorLabel());
            res.setState(State.valueOf(e.getTextStatus()));
        }

        return res;
    }
}
