package by.epam.likeit.controller.action.user;


import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.PropertiesLoader;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.HttpThreadLocalContext;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.logger.LikeLogger;

import static by.epam.likeit.controller.dispatcher.JSONDispatcher.JSON_ATTR_NAME;
import static by.epam.likeit.util.DAOUtils.mapUser;


/**
 * This action is invoked if an user is authorizing
 * After successful execution user instance is set to the session scope.
 * @see by.epam.likeit.service.IUserService#login(User)
 */
public class AuthorizationAction extends AbstractAction {

    /**
     * Validates the given login using associated regexp.
     * Validation of the password is intentionally omitted because of
     * difference in database task requirements and js task requirements
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        User user = mapUser(context);

        if (!Validator.validateBeanProperty(user, Validator.LOGIN_FIELD, res, false)) {
            res.setDispatcher(DispatcherType.JSON);
        }

        //validation of the password is intentionally omitted

        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * If an authorization has failed, method changes dispatcher type to {@linkplain DispatcherType#JSON}
     * otherwise an instance of the current user is set to the request scope.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution.
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            User user = mapUser(context);
            User loggedInUser;

            if ((loggedInUser = USER_SERVICE.login(user)) != null){
                res.setDestination(getJspUri() + loggedInUser.getLogin());
                res.setDispatcher(getDispatcherType());
                context.setAttribute(USER_PARAM_NAME, loggedInUser, HttpThreadLocalContext.Scope.SESSION);
                context.setAttribute(JSON_ATTR_NAME, getJspUri() + loggedInUser.getLogin());
                LikeLogger.trace("User " + loggedInUser.getLogin() +
                        "[" + loggedInUser.getRights()  + "] logged in.");
            } else {
                res.addErrorMessage(PropertiesLoader.get("validation.authorization.wrong"));
                res.setState(State.INVALID);
                res.setDispatcher(DispatcherType.JSON);
            }
        } catch (DAOSystemException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.addErrorMessage(e.getErrorLabel());
            res.setState(State.valueOf(e.getTextStatus()));
        }

        return res;
    }


}
