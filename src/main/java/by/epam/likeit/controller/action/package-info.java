/**
 * Package contains classes and packages that
 * determine possible actions and its lifecycle:
 * initialization, using, handling exceptions and
 * sending response to the client.
 * @see by.epam.likeit.controller.action.AbstractAction
 * @see by.epam.likeit.controller.action.ActionFactory
 * @see by.epam.likeit.controller.action.ActionStAXConfigurator
 */
package by.epam.likeit.controller.action;