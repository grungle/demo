package by.epam.likeit.controller.action;

import by.epam.likeit.controller.HttpMethod;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.jdbc.transaction.TransactionManager;
import by.epam.likeit.jdbc.transaction.UnitOfWork;
import by.epam.likeit.service.*;
import by.epam.likeit.service.impl.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Class represents base action that can be executed by client
 * using {@link HttpMethod supported HTTP} method if the request is valid.
 *
 * @see ActionFactory
 */
public abstract class AbstractAction {

    // Constants ----------------------------------------------------------------------------------

    public static final String USER_PARAM_NAME = "user";
    public static final String TARGET_USER_PARAM_NAME = "targetLogin";
    public static final String USER_AVA_PARAM_NAME = "avatar_location";
    public static final String QUESTION_HEADER_PARAM_NAME = "qHeader";
    public static final String QUESTION_BODY_PARAM_NAME = "qBody";
    public static final String QUESTION_ID_PARAM_NAME = "qId";
    public static final String ANSWER_BODY_PARAM_NAME = "aBody";
    public static final String ANSWER_ID_PARAM_NAME = "aId";
    public static final String TOPIC_ID_PARAM_NAME = "tId";
    public static final String TOPIC_NAME_PARAM_NAME = "tName";
    public static final String MARK_VALUE_PARAM_NAME = "val";
    public static final String AVATAR_STORAGE_PATH_PARAM_NAME = "avatar_storage";
    public static final String AVATAR_DEFAULT_PARAM_NAME = "default_avatar_name";
    public static final String RENDER_ROWS_PARAM_NAME = "render_rows";
    public static final String _TARGET_USER_PARAM_NAME = "targetUser";
    public static final String TOPIC_ATTR_NAME = "topic";
    public static final String PUBLICATION_DATE = "publication_date";

    // Services -----------------------------------------------------------------------------------

    private final TransactionManager txManager = ThreadLocalTransactionManagerImpl.getInstance();

    protected static final IUserService USER_SERVICE = new UserServiceImpl();
    protected static final IQuestionService QUESTION_SERVICE = new QuestionServiceImpl();
    protected static final IAnswerService ANSWER_SERVICE = new AnswerServiceImpl();
    protected static final ITopicService TOPIC_SERVICE = new TopicServiceImpl();
    protected static final IMarkService MARK_SERVICE = new MarkServiceImpl();

    // Vars ---------------------------------------------------------------------------------------

    private DispatcherType dispatcherType;
    private Pattern pattern;
    private List<HttpMethod> methods; //only read operations. List is immutable.
    private boolean encodedURL;
    private String jspUri;

    // Constructors --------------------------------------------------------------------------------

    public AbstractAction() {
    }

    // Methods -------------------------------------------------------------------------------------

    /**
     * Checks input parameters to define if request is valid.
     * If some attribute is invalid, method must change {@linkplain RequestContext.ExecutedResult.State state}
     * to {@link RequestContext.ExecutedResult.State#INVALID} and add error keys for sending it to the client.
     * Referring to it's logic method can also change {@linkplain DispatcherType dispatcher type}
     * to appropriate for proper displaying of an error.
     * <p>
     * Implementations of this method do not have to perform validation of the business logic.
     *
     * @param context facade over Servlet API to manipulate data
     * @return the result of the validation
     * @see RequestContext.ExecutedResult.State
     * @see DispatcherType
     */
    public abstract RequestContext.ExecutedResult validate(RequestContext context);

    /**
     * Performs necessary set of actions to execute request.
     * This method must be performed in a transaction and it does not
     * define the edges of the start the and end of the transaction.
     * If it is necessary, implementation can put some attributes in any possible scope.
     * <p>
     * Implementations of this method should be able to prevent situations if the current user
     * is trying to execute operation that is not forbidden or the given arguments is not valid.
     * In this case, the error message must be set using
     * {@link by.epam.likeit.controller.context.RequestContext.ExecutedResult#addErrorMessage(String)} method and
     * must have the appropriate {@linkplain by.epam.likeit.controller.context.RequestContext.ExecutedResult.State status}
     * <p>
     * {@link RequestContext.ExecutedResult.State}
     *
     * @param context facade over Servlet API to manipulate data
     * @return result of execution
     * @see RequestContext.ExecutedResult.State
     */
    public abstract RequestContext.ExecutedResult executeAction(RequestContext context);

    /**
     * Wraps action to be executed in {@link UnitOfWork<AbstractAction>} and delegates execution to
     * a transaction using {@link AbstractAction#executeAction(RequestContext)} method
     *
     * @param context facade over Servlet API to manipulate data
     * @return result of execution
     * @throws Exception if some error occurs during execution of the action
     * @see TransactionManager#doInTransaction(UnitOfWork)
     * @see UnitOfWork
     * @see #executeAction(RequestContext)
     */
    public final RequestContext.ExecutedResult execute(final RequestContext context) throws Exception {
        UnitOfWork<RequestContext.ExecutedResult> unitOfWork = () -> executeAction(context);
        return txManager.doInTransaction(unitOfWork);
    }

    /**
     * Defines if current class can handle requested action checking requested URI
     * and {@link HttpMethod}. Request URI must not contain context path.
     * URI format must look like <code>/profile/UserName</code>
     *
     * @param reqURI requested URI relative to the context path
     * @param method requested HTTP method
     * @return true if this class object pattern matches requested URI and requested method is supported,
     * otherwise false.
     * @see HttpMethod
     */
    public boolean accept(String reqURI, HttpMethod method) {
        return methods.contains(method) && pattern.matcher(reqURI).matches();
    }

    // Getters & Setters ---------------------------------------------------------------------------

    public String getJspUri() {
        return jspUri;
    }

    void setJspUri(String jspUri) {
        this.jspUri = jspUri;
    }

    public DispatcherType getDispatcherType() {
        return dispatcherType;
    }

    void setDispatcherType(DispatcherType dispatcherType) {
        this.dispatcherType = dispatcherType;
    }

    void setMethods(HttpMethod... methods) {
        this.methods = Arrays.asList(methods);
    }

    public Pattern getPattern() {
        return pattern;
    }

    void setPattern(String regex) {
        this.pattern = Pattern.compile(regex);
    }

    protected boolean isEncodedURL() {
        return encodedURL;
    }

    void setEncodedURL(boolean encodedURL) {
        this.encodedURL = encodedURL;
    }

    // Object overriding ----------------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractAction action = (AbstractAction) o;
        return Objects.equals(pattern, action.pattern) &&
                Objects.equals(methods, action.methods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pattern, methods);
    }

    @Override
    public String toString() {
        return "\n{" + this.getClass().getSimpleName() +
                " pattern=" + pattern +
                ", methods=" + methods +
                ", type=" + dispatcherType +
                ", jspUri='" + jspUri + '\'' +
                '}';
    }

}
