package by.epam.likeit.controller.action.user;

import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.Mark;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.PropertiesLoader;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.StringUtils;

import static by.epam.likeit.bean.Mark.MAX_MARK;
import static by.epam.likeit.bean.Mark.MIN_MARK;
import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.controller.dispatcher.JSONDispatcher.JSON_ATTR_NAME;
import static by.epam.likeit.util.DAOUtils.mapAnswerId;
import static by.epam.likeit.util.DAOUtils.mapMark;


/**
 * This action is invoked if one user is going to estimate the answer of another user.
 * After successful execution, the target user's reputation must be changed.
 * @see by.epam.likeit.service.IMarkService#estimate(Mark, Answer, Mark, User)
 */
public class EstimateAnswerAction extends AbstractAction {

    /**
     * Validates the given answer id and mark. Mark value must not
     * be greater than {@link Mark#MAX_MARK}
     * and lower than {@link Mark#MIN_MARK}
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     * @see Mark#MIN_MARK
     * @see Mark#MAX_MARK
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();
        Long val = StringUtils.parseLong(context.getParameter(MARK_VALUE_PARAM_NAME));
        Long aId = StringUtils.parseLong(context.getParameter(ANSWER_ID_PARAM_NAME));

        if (val == null || val > MAX_MARK || val < MIN_MARK){
            res.setState(RequestContext.ExecutedResult.State.INVALID);
            res.addErrorMessage(PropertiesLoader.get(PropertiesLoader.VALIDATION_ANSWER_ID_INVALID));
        }

        Validator.validateNotNegativeLong(aId, Validator.ANSWER_ID, res);

        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);
        Long aId = Long.valueOf(context.getParameter(ANSWER_ID_PARAM_NAME));

        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            Mark oldMark = MARK_SERVICE.getMark(mapAnswerId(aId), curUser);
            curUser = USER_SERVICE.find(curUser.getLogin());
            Answer answer = ANSWER_SERVICE.find(aId);
            Mark newMark = mapMark(context, curUser, oldMark);

            MARK_SERVICE.estimate(oldMark, answer, newMark, curUser);

            answer = ANSWER_SERVICE.find(aId);
            context.setAttribute(JSON_ATTR_NAME, answer.getRating());
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] estimated the answer with id " + aId + ", mark = " + newMark.getMark());
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.setState(RequestContext.ExecutedResult.State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }

        return res;
    }
}
