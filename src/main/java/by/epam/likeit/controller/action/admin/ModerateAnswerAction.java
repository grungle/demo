package by.epam.likeit.controller.action.admin;

import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.action.user.QuestionUpdatable;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.DAOUtils;
import by.epam.likeit.util.StringUtils;

import java.util.Date;

import static by.epam.likeit.bean.AbstractMessage.Type.ANSWER;
import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;

/**
 * This action is invoked if an user moderating an answer
 * @see by.epam.likeit.service.IAnswerService#moderate(Answer, Answer, ModerationInfo, User)
 */
public class ModerateAnswerAction extends AbstractAction implements QuestionUpdatable {

    /**
     * Checks if the value of answer id that was received from parameters is valid.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation.
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();

        Long aId = StringUtils.parseLong(context.getParameter(ANSWER_ID_PARAM_NAME));
        Validator.validateNotNegativeLong(aId, Validator.ANSWER_ID, res);
        validate(context, res);
        return res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     * @see by.epam.likeit.service.IAnswerService#moderate(Answer, Answer, ModerationInfo, User)
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);
        Long aId = Long.valueOf(context.getParameter(ANSWER_ID_PARAM_NAME));
        RequestContext.ExecutedResult res = context.getExecutedResult();

        try {
            curUser = USER_SERVICE.find(curUser.getLogin());
            ModerationInfo mi = DAOUtils.mapModeration(context, ANSWER);
            Answer oldAnswer = ANSWER_SERVICE.find(aId);

            Answer newAnswer = DAOUtils.mapAnswer(context);
            newAnswer.setLastUpdate(new Date());

            ANSWER_SERVICE.moderate(oldAnswer, newAnswer, mi, curUser);
            update(context, QUESTION_SERVICE, ANSWER_SERVICE);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);
            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] moderated answer " + aId);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.addErrorMessage(e.getErrorLabel());
            res.setState(State.valueOf(e.getTextStatus()));
        }

        return res;
    }


}