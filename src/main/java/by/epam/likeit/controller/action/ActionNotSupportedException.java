package by.epam.likeit.controller.action;

import by.epam.likeit.controller.context.RequestContext;

/**
 * This exception is thrown if {@linkplain ActionFactory factory}
 * cannot find appropriate {@link AbstractAction} implementation.
 * @see by.epam.likeit.controller.HttpMethod
 * @see RequestContext#getPathInfo()
 */
public class ActionNotSupportedException extends Exception {

    private static final long serialVersionUID = -2638868907151007738L;

    public ActionNotSupportedException(String message) {
        super(message);
    }

}
