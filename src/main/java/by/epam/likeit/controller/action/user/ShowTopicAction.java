package by.epam.likeit.controller.action.user;

import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.bean.view.TopicView;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.BaseDAO.ComparatorMode;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;

import java.util.List;

import static by.epam.likeit.controller.action.user.QuestionUpdatable.*;
import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.dao.BaseDAO.ComparatorMode.DESC;
import static by.epam.likeit.util.StringUtils.parseLong;

//import static by.epam.likeit.util.LikeConstants.DEFAULT_RENDER_COUNT;

/**
 * This action is invoked if an user is requesting to show a topic
 */
public class ShowTopicAction extends AbstractAction {

    /**
     * Validates parameters to render the topic.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {

        Long page = parseLong(context.getParameter(PAGE_ATTR_NAME));
        Long count = parseLong(context.getParameter(SHOW_COUNT_PARAM_NAME));
        Long topicID = parseLong(context.getPathInfo().substring(context.getPathInfo().lastIndexOf("/") + 1));
        String sortBy = context.getParameter(SORTBY_PARAM_NAME);

        RequestContext.ExecutedResult res = context.getExecutedResult();

        Validator.validateSQLKey(sortBy, res);
        Validator.validateNotNegativeNullLong(page, Validator.PAGE_FIELD, res);
        Validator.validateNotNegativeNullLong(count, Validator.COUNT_FIELD, res);
        Validator.validateNotNegativeLong(topicID, Validator.TOPIC_ID, res);

        return  res;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        RequestContext.ExecutedResult res = context.getExecutedResult();

        Long topicID = Long.valueOf(context.getPathInfo().substring(context.getPathInfo().lastIndexOf("/") + 1));

        Long page = Long.valueOf(context.getParameter(PAGE_ATTR_NAME) == null ?
                "1" : context.getParameter(PAGE_ATTR_NAME));

        Long length = Long.valueOf(context.getParameter(SHOW_COUNT_PARAM_NAME) == null ?
                context.findAttribute(RENDER_ROWS_PARAM_NAME).toString() : context.getParameter(SHOW_COUNT_PARAM_NAME));

        ComparatorMode orderBy = ComparatorMode.valueOf(context.getParameter(ORDERBY_PARAM_NAME) == null ?
                DESC.toString() : context.getParameter(ORDERBY_PARAM_NAME));

        String sortBy = context.getParameter(SORTBY_PARAM_NAME) == null ?
                PUBLICATION_DATE : context.getParameter(SORTBY_PARAM_NAME);

        User curUser = (User)context.findAttribute(USER_PARAM_NAME);

        try {

            Topic topic = TOPIC_SERVICE.getTopic(topicID);
            List<Question> questions = QUESTION_SERVICE.getQuestions(topic, (page - 1) * length, length, sortBy, orderBy);
            long maxPage = (long)Math.ceil(TOPIC_SERVICE.getQuestionsCountByTopic(topic) *1./ length);

            TopicView tv = new TopicView();
            tv.setQuestions(questions);
            tv.setTopic(topic);

            context.setAttribute(TOPIC_ATTR_NAME, tv);
            context.setAttribute(MAX_PAGE_ATTR_NAME, maxPage);
            context.setAttribute(CUR_PAGE_ATTR_NAME, page);

            if (curUser != null){
                curUser = USER_SERVICE.find(curUser.getLogin());
                context.setAttribute(USER_PARAM_NAME, curUser, SESSION);
            }
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            res.setState(State.valueOf(e.getTextStatus()));
            res.addErrorMessage(e.getErrorLabel());
        }
        return res;
    }

}
