package by.epam.likeit.controller.action.user;


import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.dao.exception.DAOException;
import by.epam.likeit.logger.LikeLogger;

import static by.epam.likeit.controller.context.RequestContext.Scope.SESSION;
import static by.epam.likeit.util.DAOUtils.mapQuestionId;

/**
 * This action is invoked if an user is closing the question
 * @see by.epam.likeit.service.IQuestionService#close(Question, User, User).
 */
public class CloseQuestionAction extends AbstractAction implements QuestionUpdatable{

    /***
     * Validates the question id that retrieved from parameters.
     * @param context facade over Servlet API to manipulate data
     * @return The result of the validation
     */
    @Override
    public RequestContext.ExecutedResult validate(RequestContext context) {
        RequestContext.ExecutedResult result = context.getExecutedResult();
        validate(context, result);
        return result;
    }

    /**
     * Delegates the execution of the current action to an appropriate business logic
     * and sets the received result to the context attributes.
     *
     * @param context facade over Servlet API to manipulate data
     * @return The result of the execution
     */
    @Override
    public RequestContext.ExecutedResult executeAction(RequestContext context) {
        long questionID = Long.valueOf(context.getParameter(QUESTION_ID_PARAM_NAME));
        User curUser = (User)context.findAttribute(USER_PARAM_NAME);

        RequestContext.ExecutedResult result = context.getExecutedResult();
        try {
            String ownerLogin = QUESTION_SERVICE.getOwnerLogin(mapQuestionId(questionID));
            User ownerUser = USER_SERVICE.find(ownerLogin);
            curUser = USER_SERVICE.find(curUser.getLogin());
            QUESTION_SERVICE.close(mapQuestionId(questionID), ownerUser, curUser);
            update(context, QUESTION_SERVICE, ANSWER_SERVICE);
            context.setAttribute(USER_PARAM_NAME, curUser, SESSION);

            LikeLogger.trace("User " + curUser.getLogin() +
                    "[" + curUser.getRights()  + "] closed a question " + questionID);
        } catch (DAOException e) {
            LikeLogger.warn(e.getMessage(), e);
            result.setState(State.valueOf(e.getTextStatus()));
            result.addErrorMessage(e.getErrorLabel());
        }

        return result;
    }
}
