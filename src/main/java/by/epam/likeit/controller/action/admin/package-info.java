/**
 * Package contain set of {@link by.epam.likeit.controller.action.AbstractAction}
 * implementing classes that can be executed only by admin/moderator
 * @see by.epam.likeit.bean.User.UserRights
 * @see by.epam.likeit.controller.action.AbstractAction
 */
package by.epam.likeit.controller.action.admin;