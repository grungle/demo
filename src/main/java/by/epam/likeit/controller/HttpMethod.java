package by.epam.likeit.controller;

import by.epam.likeit.controller.action.AbstractAction;

/**
 * Enum contains all possible HTTP methods.
 * @see AbstractAction
 */
public enum HttpMethod {
    GET, POST, TRACE, DELETE, OPTIONS, PUT, HEAD
}
