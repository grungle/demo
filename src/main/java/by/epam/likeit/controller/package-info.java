/**
 * Package contains classes and other packages
 * associated to Controller role in
 * standard Mode-View-Controller design pattern(MVC)
 */
package by.epam.likeit.controller;