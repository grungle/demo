package by.epam.likeit.controller.dispatcher;


import by.epam.likeit.bean.User;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.epam.likeit.controller.action.AbstractAction.USER_PARAM_NAME;
import static by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType.JSON;
import static by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType.valueOf;
import static by.epam.likeit.controller.dispatcher.JSONDispatcher.JSON_ATTR_NAME;

/**
 * Defines API for behavior to all possible dispatchers.
 *
 * Dispatcher should has it's own type mention in {@link DispatcherType}
 * Creation two instance of the same dispatcher is not prohibited,
 * but does not recommended has no point.
 *
 * Every {@linkplain by.epam.likeit.controller.action.AbstractAction action} implies some result
 * that must be sent to the client. This class provides necessary API for all possible
 * types of returned result as JSON, RENDER, REDIRECT, ect...
 * @see RequestContext.ExecutedResult
 * @see by.epam.likeit.controller.action.AbstractAction
 */
public abstract class AbstractDispatcher {

    private DispatcherType dispatcherType;

    private static final String LOCATION_HEADER_NAME = "Location";
    static final String REDIRECT_TO_LOGIN_URL = "/signin";
    static final String REDIRECT_TO_PROFILE_URL = "/profile/";

    // Constructors -------------------------------------------------------------------------------

    /**
     * Creates new dispatcher base on {@link DispatcherType}
     * @param type Type of dispatcher
     */
    AbstractDispatcher(String type) {
        dispatcherType = valueOf(type);
    }

    // Methods ------------------------------------------------------------------------------------

    /**
     * Checks state of the result, adds necessary headers/attributes
     * and sends to implementation if needed.
     * Method can overlap some attributes if {@linkplain State state}
     * is not {@link State#SUCCESSFUL}
     * Method also encodes all URL. It is needed to support session if
     * a client disabled cookie in the browser.
     * @param result result of execution {@link by.epam.likeit.controller.action.AbstractAction}
     * @param req HTTP Servlet Request
     * @param resp HTTP Servlet Response
     * @throws ServletException if the target resource throws this exception
     * @throws IOException if the target resource throws this exception
     */
    public void invoke(final RequestContext.ExecutedResult result,
                       final HttpServletRequest req,
                       final HttpServletResponse resp) throws ServletException, IOException {

        String destination = result.getDestination();

        if (result.isEncodedURL() && destination != null) {
            result.setDestination(resp.encodeURL(destination));
            req.setAttribute(JSON_ATTR_NAME, result.getDestination());
        }

        State state = result.getState();
        User user = (User)req.getSession().getAttribute(USER_PARAM_NAME);


        String redirectTo = resp.encodeURL(req.getContextPath() + (user == null ?
                REDIRECT_TO_LOGIN_URL :
                REDIRECT_TO_PROFILE_URL + user.getLogin()));

        if ((state == State.FORBIDDEN || state == State.INVALID) && result.getDispatcher() == JSON)
            resp.addHeader("Location", redirectTo);

        switch (state) {
            case SUCCESSFUL:
                invokeDefault(result, req, resp);
                break;
            case NOT_FOUND:
                resp.addHeader(LOCATION_HEADER_NAME, redirectTo);
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                break;
            case INVALID:
                resp.addHeader(LOCATION_HEADER_NAME, redirectTo);
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                req.setAttribute(JSON_ATTR_NAME, result.getErrorKeySet());
                invokeDefault(result, req, resp);
                break;
            case ERROR:
                resp.addHeader(LOCATION_HEADER_NAME, redirectTo);
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                break;
            case FORBIDDEN:
                resp.addHeader(LOCATION_HEADER_NAME, redirectTo);
                resp.sendError(HttpServletResponse.SC_FORBIDDEN);
                break;
        }

    }

    /**
     * Invoke internal mechanism to send data to the client.
     * Any header or additional attributes may be added if needed.
     * @param result result of execution of the {@link by.epam.likeit.controller.action.AbstractAction}
     * @param req HTTP Servlet Request
     * @param resp HTTP Servlet Response
     * @throws ServletException if the target resource throws this exception
     * @throws IOException if the target resource throws this exception
     */
    public abstract void invokeDefault(final RequestContext.ExecutedResult result,
                                       final HttpServletRequest req,
                                       final HttpServletResponse resp) throws ServletException, IOException;

    DispatcherType getDispatcherType() {
        return dispatcherType;
    }

    public void setType(DispatcherType type) {
        this.dispatcherType = type;
    }

    @Override
    public String toString() {
        return "AbstractDispatcher{" + this.getClass().getSimpleName() +
                ", dispatcherType=" + dispatcherType +
                '}';
    }

    // Inner classes -------------------------------------------------------------------------------

    /**
     * Contains all allowed dispatcher types.
     */
    public enum DispatcherType {
        REDIRECT, RENDER, TEXT, JSON
    }

}
