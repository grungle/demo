package by.epam.likeit.controller.dispatcher;

import by.epam.likeit.controller.context.RequestContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Abstract dispatcher implementation based on AJAX redirects.
 * Referring fo jQuery.ajax() specification, all HTTP codes 3XX
 * must be transparently handle, therefore, this implementation based
 * SENDS HTTP code 200, but adds "Location" header to the response.
 *
 * Client must check if this header is exists regardless HTTP code is 200
 * and change actual browser location to mentioned in this header.
 */
public class RedirectDispatcher extends AbstractDispatcher {

    public RedirectDispatcher(String type) {
        super(type);
    }

    /**
     * Adds {@linkplain RequestContext.ExecutedResult#getDestination()} value
     * to the "Location" header of HTTP Response
     * {@inheritDoc}
     */
    @Override
    public void invokeDefault(final RequestContext.ExecutedResult result,
                              final HttpServletRequest req,
                              final HttpServletResponse resp) throws ServletException, IOException {

        String res = result.getDestination();
        resp.addHeader("Location", res);
        resp.setStatus(200);
    }

}
