package by.epam.likeit.controller.dispatcher;


import by.epam.likeit.controller.context.RequestContext.ExecutedResult;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/***
 * JSON base dispatcher.
 * {@inheritDoc}
 */
public class JSONDispatcher extends AbstractDispatcher{

    public static final String JSON_ATTR_NAME = "json";

    JSONDispatcher(String type) {
        super(type);
    }

    /**
     * Sends response to the client in JSON format
     * Retrieves attribute from request by name
     * {@link JSONDispatcher#JSON_ATTR_NAME}
     * and sends it in JSON format.
     * {@inheritDoc}
     * @see Gson
     */
    @Override
    public void invokeDefault(final ExecutedResult result,
                              final HttpServletRequest req,
                              final HttpServletResponse resp) throws ServletException, IOException {



        Object target = req.getAttribute(JSON_ATTR_NAME);
        Gson gson = new Gson();
        String json = gson.toJson(target);
        gson.toJson(json, resp.getWriter());
    }
}
