package by.epam.likeit.controller.dispatcher;


import by.epam.likeit.controller.context.RequestContext;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * {@linkplain javax.servlet.RequestDispatcher#forward(ServletRequest, ServletResponse)}
 * based request dispatcher implementation sends answer to the client
 * using jsp page located at {@link RequestContext.ExecutedResult#getDestination()}
 * {@inheritDoc}
 */
public class RenderJspDispatcher extends AbstractDispatcher {

    RenderJspDispatcher(String type) {
        super(type);
    }

    /**
     * @see RequestContext.ExecutedResult
     * @see javax.servlet.RequestDispatcher#forward(ServletRequest, ServletResponse)
     * {@inheritDoc}
     */
    @Override
    public void invokeDefault(final RequestContext.ExecutedResult result,
                              final HttpServletRequest req,
                              final HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher(result.getDestination()).forward(req, resp);
    }

}
