/**
 * Package contains classes over Servlet API
 * to extract dispatching logic from Servlet
 * to set of classes that use standard {@link javax.servlet.RequestDispatcher}
 * and extends it opportunities.
 *
 * Decision of using dispatcher type is taken
 * from {@link by.epam.likeit.controller.action.AbstractAction}
 * and {@link by.epam.likeit.controller.context.RequestContext.ExecutedResult}
 */
package by.epam.likeit.controller.dispatcher;