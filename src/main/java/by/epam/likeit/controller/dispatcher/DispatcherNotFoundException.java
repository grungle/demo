package by.epam.likeit.controller.dispatcher;

/**
 * Check exception thrown when unsupported dispatcher is requested.
 * @see AbstractDispatcher
 * @see DispatcherHolder
 * @see by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType
 */
public class DispatcherNotFoundException extends Throwable {

    public DispatcherNotFoundException(AbstractDispatcher.DispatcherType type) {
        super("dispatcher with type = " + type + " wasn't found");
    }

}
