package by.epam.likeit.controller.dispatcher;


import static by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType;

/**
 * Contains all possible dispatchers.
 * @see AbstractDispatcher
 * @see DispatcherType
 */
public enum DispatcherHolder {
    //there is no point in changing literals without compilation

    // Values -------------------------------------------------------------------------------------

    REDIRECT(new RedirectDispatcher("REDIRECT")),
    RENDER(new RenderJspDispatcher("RENDER")),
    JSON(new JSONDispatcher("JSON"));

    // Vars ---------------------------------------------------------------------------------------

    private AbstractDispatcher dispatcher;

    // Constructors -------------------------------------------------------------------------------

    DispatcherHolder(AbstractDispatcher dispatcher){
        this.dispatcher = dispatcher;
    }

    // Methods -------------------------------------------------------------------------------------

    public AbstractDispatcher getDispatcher() {
        return dispatcher;
    }

    /**
     * Gets appropriate dispatcher by its type.
     * @param type type of searching dispatcher.
     * @return dispatcher having requested type.
     * @throws DispatcherNotFoundException If dispatcher with requested type does not exist
     */
    public static AbstractDispatcher getDispatcher(DispatcherType type) throws DispatcherNotFoundException {

        for (DispatcherHolder holder : DispatcherHolder.values()) {
            AbstractDispatcher dispatcher = holder.getDispatcher();
            if (dispatcher.getDispatcherType() == type) {
                return dispatcher;
            }
        }
        throw new DispatcherNotFoundException(type);
    }

}
