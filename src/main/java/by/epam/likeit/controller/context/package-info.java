/**
 * Package contains classes over Servlet API 3.0
 * to ease its API and provide extra level
 * of abstraction over it implementing Facade pattern
 * and provides classes for multipart request processing.
 */
package by.epam.likeit.controller.context;