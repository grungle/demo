package by.epam.likeit.controller.context;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;

import static by.epam.likeit.controller.context.RequestContext.Scope.*;

/**
 * Http ThreadLocal implementation of {@link RequestContext} interface.
 * Client context is binded to the current thread.
 * NOTE!!! This implementation work only if request is executed in one thread.
 * If there will be any other created thread, they won't be able to retrieve data
 * from this implementation.
 * @see by.epam.likeit.filter.ContextFilter
 */
@SuppressWarnings("deprecation")
public class HttpThreadLocalContext implements RequestContext, AutoCloseable {

    private static final long serialVersionUID = 1901780287373570922L;

    // Vars -----------------------------------------------------------------------------------------

    private static ThreadLocal<HttpThreadLocalContext> instance = new ThreadLocal<>();
    private ExecutedResult executedResult;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private String pathInfo;

    // Constructors ----------------------------------------------------------------------------------

    /**
     * Creates an instance of the class and sets requested URI related to the context path
     * This path does not include any data like jsession id if client disabled cookie in
     * browser or parameters.
     * @param request HTTP Servlet Request
     * @param response HTTP Servlet Response
     */
    private HttpThreadLocalContext(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        if (request.isRequestedSessionIdFromURL()){
            String url = request.getRequestURL().toString();
            pathInfo = url.substring(url.lastIndexOf(request.getContextPath()) + request.getContextPath().length());
            pathInfo = pathInfo.substring(0, pathInfo.indexOf(";"));
        } else {
            pathInfo = request.getRequestURI();
        }
    }

    /**
     * Creates an instance of the class and sets it to the {@link HttpThreadLocalContext#instance}
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return created instance
     * @see by.epam.likeit.filter.ContextFilter
     */
    public static HttpThreadLocalContext create(HttpServletRequest request, HttpServletResponse response) {
        HttpThreadLocalContext context = new HttpThreadLocalContext(request, response);
        instance.set(context);
        return context;
    }

    // Methods -----------------------------------------------------------------------------------------

    /**
     * Gets current {@link HttpThreadLocalContext} instance related to {@link Thread#currentThread()}
     * @return related instance.
     * @see ThreadLocal
     */
    public static HttpThreadLocalContext lookup() {
        return instance.get();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object findAttribute(String name) {
        Object res;
        if ((res = getAttribute(name, REQUEST)) != null)
            return res;
        else if ((res = getAttribute(name, SESSION)) != null)
            return res;
        else return getAttribute(name, APPLICATION);
    }

    /**
     * @see MultipartMap#getFile(String)
     * @throws IllegalArgumentException If request is not instance of {@link MultipartRequest}
     * @see by.epam.likeit.filter.MultipartFilter
     */
    @Override
    public File getFile(String name) {
        if (!(request instanceof MultipartRequest)) throw new IllegalStateException();
        return ((MultipartRequest) request).getFile(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getParameter(String param) {
        return request.getParameter(param);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(String name, Object obj) {
        setAttribute(name, obj, REQUEST);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(String name, Object obj, Scope scope) {
        switch (scope) {

            case REQUEST:
                request.setAttribute(name, obj);
                break;
            case SESSION:
                HttpSession session = request.getSession();
                session.setAttribute(name, obj);
                break;
            case APPLICATION:
                request.getServletContext().setAttribute(name, obj);
                break;
            case COOKIE:
                response.addCookie(new Cookie(name, obj.toString()));
                break;
        }
    }

    /**
     * {@inheritDoc}
     */
    public void setExecutedResult(ExecutedResult res){
        this.executedResult = res;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getAttribute(String name) {
        return getAttribute(name, REQUEST);
    }

    /**
     * Gets and returns attribute from the given SCOPE.
     * If attribute does not exist at given {@link by.epam.likeit.controller.context.RequestContext.Scope}
     * <i>null</i> null is returned.
     * Unlike {@linkplain RequestContext#findAttribute(String) this method}, does not seek automatically attribute in all possible scopes.
     * @param name Name of the attribute
     * @param scope Place where to fetch attribute.
     * @return found attribute value associated to its name
     */
    public Object getAttribute(String name, Scope scope) {
        switch (scope) {
            case REQUEST:
                return request.getAttribute(name);
            case SESSION:
                return request.getSession(false).getAttribute(name);
            case APPLICATION:
                return request.getServletContext().getInitParameter(name);
            case COOKIE:
                Cookie[] cookies = request.getCookies();
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(name))
                        return cookie.getValue();
                }
                return null;
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     * Gets all cookies
     * @return cookies
     * @see Cookie
     */
    public Cookie[] getCookies() {
        return request.getCookies();
    }

    /**
     * Gets cookie by name.
     * @param name name of cookie to be found
     * @return found cookie if exists, otherwise null
     */
    public Cookie getCookieByName(String name) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(name))
                return cookie;
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void invalidate() {
        request.getSession().invalidate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPathInfo() {
        return pathInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRealPath(String path) {
        return request.getServletContext().getRealPath(path);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExecutedResult getExecutedResult() {
        return executedResult;
    }

    // AutoClosable implementation --------------------------------------------------------------------

    /**
     * Removes instance of this class from {@link HttpThreadLocalContext#instance}
     * related to {@link Thread#currentThread()}
     * @see ThreadLocal
     * @see AutoCloseable
     */
    @Override
    public void close() {
        instance.remove();
    }

}
