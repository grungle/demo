package by.epam.likeit.controller.context;


import by.epam.likeit.controller.dispatcher.AbstractDispatcher;

import java.io.File;
import java.io.Serializable;
import java.util.Set;

/**
 * Interface provides API to implement
 * Facade pattern over {@link javax.servlet.http.HttpServletResponse}
 * and {@link javax.servlet.http.HttpServletRequest}
 */
public interface RequestContext extends Serializable, AutoCloseable {

    /**
     * @see javax.servlet.http.HttpServletRequest#getParameter(String)
     */
    String getParameter(String name);

    /**
     * Sets attribute to the {@link HttpThreadLocalContext.Scope#REQUEST} scope.
     * @see RequestContext#setAttribute(String, Object, Scope)
     */
    void setAttribute(String name, Object obj);

    /**
     * Sets the attribute to given scope.
     * If the attribute with this name is already exist at this scope
     * old attribute will be replaced to this.
     * @param name name by which this attribute can be found
     * @param obj attribute to be set
     * @param scope describes where to store this attribute.
     * @see Scope
     */
    void setAttribute(String name, Object obj, Scope scope);

    /**
     * Gets attribute from context from
     * {@linkplain by.epam.likeit.controller.context.RequestContext.Scope#REQUEST request} scope
     * @see javax.servlet.http.HttpServletRequest#getAttribute(String)
     */
    Object getAttribute(String name);

    /**
     * Seeking attribute by its name in all possible scopes.
     * Basic order of searching is: Request -> Session -> Application.
     * This order can be changed in subclasses, but this does not encouraged.
     * In this way, request attribute can overlap session and application attributes
     * and so on.
     * @param name name by which attribute will be found.
     * @return found attribute if exists, otherwise null.
     */
    Object findAttribute(String name);

    /**
     * Gets requested path relative to context path.
     * If client disabled cookie in browser,
     * and URL is encoded, jsesssionid is omitted.
     * @return request path
     */
    String getPathInfo();

    /**
     * Invalidates current session.
     * @see javax.servlet.http.HttpSession#invalidate()
     */
    void invalidate();

    /**
     * @see MultipartMap#getFile(String)
     * @throws IllegalArgumentException If request is not instance of {@link MultipartRequest}
     * @see by.epam.likeit.filter.MultipartFilter
     */
    File getFile(String name);

    /**
     * Gets the <i>real</i> path corresponding to the given
     * <i>virtual</i> path.
     *
     * @param path path to be found
     * @return request path
     */
    String getRealPath(String path);


    /**
     * Gets the {@link ExecutedResult} of the execution of the current
     * request
     * @return the result of the execution of the current request
     */
    ExecutedResult getExecutedResult();

    /**
     * Binds the given executedResult to the current context
     * @param res the executedResult to be set.
     */
    void setExecutedResult(ExecutedResult res);


    // Inner classes ------------------------------------------------------------------------------

    /**
     * Defines possible scopes to store and retrieve data.
     */
    enum Scope {
        REQUEST, SESSION, APPLICATION, COOKIE
    }

    /**
     * A result of AbstractAction implementation.
     * It has {@linkplain State state} of execution,
     * destination value, errors set and other parameters
     * to be processed at the {@link AbstractDispatcher}
     * to render result to the client in proper way.
     */
    interface ExecutedResult extends Serializable {

        /**
         * Gets destination of the executed action.
         * Depending of {@link AbstractDispatcher.DispatcherType} it may be location
         * of jsp or address to be sent to redirect.
         * @return destination of the executed action
         */
        String getDestination();

        /**
         * Adds error message to the execution result.
         * Error message can be processed at the client side
         * or at the server side by dispatcher. So, dispatcher can
         * analyze errors and change "way" of sending answer
         * to the client.
         * @param error String error representation
         * @return <tt>true</tt> if this error set did not already contain the specified
         *         error, otherwise false
         *
         * @see java.util.HashSet#add(Object)
         */
        boolean addErrorMessage(String error);

        AbstractDispatcher.DispatcherType getDispatcher();

        Set<String> getErrorKeySet();

        void setState(State state);

        State getState();

        boolean isEncodedURL();

        void setEncodedURL(boolean encodedURL);

        void setDestination(String destination);

        void setDispatcher(AbstractDispatcher.DispatcherType dispatcher);

        /**
         * Represents state of the executed action
         */
        enum State {
            INVALID, SUCCESSFUL, NOT_FOUND, ERROR, FORBIDDEN
        }

    }
}
