package by.epam.likeit.controller.context;

import by.epam.likeit.controller.dispatcher.AbstractDispatcher.DispatcherType;

import java.util.HashSet;
import java.util.Set;

/**
 * Basic implementation of {@link RequestContext.ExecutedResult}
 * {@inheritDoc}
 */
public class ExecutedResultImpl implements RequestContext.ExecutedResult {

    private static final long serialVersionUID = 182535376378340728L;

    // Vars --------------------------------------------------------------------------------------

    private boolean encodedURL = true;
    private String destination;
    private Set<String> errorMessages;
    private DispatcherType dispatcher;
    private State state;

    // Constructors -------------------------------------------------------------------------------

    public ExecutedResultImpl (String destination, DispatcherType type){
        this.state = State.SUCCESSFUL;
        this.destination = destination;
        this.dispatcher = type;
        this.errorMessages = new HashSet<>();
    }

    // Methods ------------------------------------------------------------------------------------

    public String getErrorMessage(final String key){
        return errorMessages.contains(key) ? key : null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addErrorMessage(final String key) {
        return key != null && errorMessages.add(key);
    }

    @Override
    public void setState(State state) {
        this.state = state;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Set<String> getErrorKeySet() {
        return new HashSet<>(errorMessages);
    }

    public DispatcherType getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(DispatcherType dispatcher) {
        this.dispatcher = dispatcher;
    }

    @Override
    public String getDestination() {
        return destination;
    }


    @Override
    public State getState() {
        return state;
    }

    @Override
    public boolean isEncodedURL() {
        return encodedURL;
    }

    @Override
    public void setEncodedURL(boolean encodedURL) {
        this.encodedURL = encodedURL;
    }
}
