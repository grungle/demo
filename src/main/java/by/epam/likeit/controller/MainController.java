package by.epam.likeit.controller;

import by.epam.likeit.controller.action.AbstractAction;
import by.epam.likeit.controller.action.ActionFactory;
import by.epam.likeit.controller.action.ActionNotSupportedException;
import by.epam.likeit.controller.context.ExecutedResultImpl;
import by.epam.likeit.controller.context.HttpThreadLocalContext;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.controller.context.RequestContext.ExecutedResult.State;
import by.epam.likeit.controller.dispatcher.AbstractDispatcher;
import by.epam.likeit.controller.dispatcher.DispatcherHolder;
import by.epam.likeit.controller.dispatcher.DispatcherNotFoundException;
import by.epam.likeit.logger.LikeLogger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Class implements Front controller design pattern to collect all requests
 * at the single servlet. Servlet defines requested uri,
 * finds related action and invokes it using one extra abstraction
 * level mentioned at {@link RequestContext}
 * @see by.epam.likeit.filter.ContextFilter
 * @see by.epam.likeit.controller.context.MultipartRequest
 */
public class MainController extends HttpServlet {

    private static final long serialVersionUID = -5645854096827563219L;

    /**
     * Defines requested HTTP method and action by its {@link HttpServletRequest#getPathInfo()}
     * and executes associated action if request is valid.
     * If the requested action is found, creates {@link AbstractDispatcher} and using it
     * sends answer to the client.
     * @see AbstractDispatcher
     * @see AbstractAction
     * @see HttpMethod
     * @see HttpThreadLocalContext
     * @see RequestContext
     * @see by.epam.likeit.filter.ContextFilter
     */
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LikeLogger.trace(req.getMethod() + " " + req.getPathInfo());

        RequestContext context = HttpThreadLocalContext.lookup();
        HttpMethod method = HttpMethod.valueOf(req.getMethod());

        try {

            AbstractAction action = ActionFactory.getAction(req.getPathInfo(), method);
            RequestContext.ExecutedResult result = new ExecutedResultImpl(action.getJspUri(), action.getDispatcherType());
            context.setExecutedResult(result);

            if ((result = action.validate(context)).getState() == State.SUCCESSFUL)
                action.execute(context);

            AbstractDispatcher dispatcher = DispatcherHolder.getDispatcher(result.getDispatcher());
            dispatcher.invoke(result, req, resp);

        } catch (ActionNotSupportedException e) {
            LikeLogger.debug(req.getMethod() + "Action is not supported " + req.getRequestURI(), e);
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        } catch (DispatcherNotFoundException e) {
            LikeLogger.debug("Dispatcher wasn't found", e);
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        } catch (Exception e) {
            LikeLogger.debug("Uncaught exception occurred in front controller servlet", e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }


}
