/**
 * Interfaces in this package provide business
 * logic contracts to be implemented.
 * Basic implementation are located in package {@link by.epam.likeit.service.impl}
 */
package by.epam.likeit.service;