package by.epam.likeit.service;


import by.epam.likeit.bean.*;
import by.epam.likeit.bean.view.QuestionView;
import by.epam.likeit.dao.BaseDAO;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.util.List;

/**
 * This interface declares contract of the business logic associated to {@link Question}
 */
public interface IQuestionService {

    // Methods ---------------------------------------------------------------------------------------

    /**
     * Creates new question related to topic mention in {@link Question#topic}
     * @param question Question to be created
     * @param owner User who requested creating of the question
     * @return {@link Question#id} generated after
     * successful creating of the new question
     * @throws DAOSystemException If something fails at DAO layer or provided {@link Topic#id) does not exist
     * @throws NoSuchEntityException If question was not created
     * @throws InsufficientPermissionException If user who asked a question
     * s {@linkplain User.UserRights#WRITE not allowed} to perform this action
     * @see Topic
     */
    long newQuestion(final Question question, final User owner) throws DAOSystemException,
            NoSuchEntityException, InsufficientPermissionException;

    /**
     * Returns {@link List <Question>} of questions starting from <code>offset</code>,
     * sorted by <code>sortBy</code> and ordered by <code>orderBy</code>.
     * If {@link Topic#id} does not exist, an empty list is returned
     *
     * @param topic Questions associated to that topic must be found
     * @param offset Position starting from which result will be returned
     * @param length Count of entities to be returned starting from <code>offset</code>
     * @param sortBy Result will be sorted by this field
     * @param orderBy Order of sorting {@linkplain by.epam.likeit.dao.BaseDAO.ComparatorMode see more}
     * @return {@link List<Question>} of questions starting from <code>offset</code>,
     * sorted by <code>sortBy</code> and ordered by <code>orderBy</code>
     * @throws DAOSystemException If something fails at DAO layer
     */
    List<Question> getQuestions(final Topic topic,
                                              final long offset,
                                              final long length,
                                              final String sortBy,
                                              final BaseDAO.ComparatorMode orderBy)
            throws DAOSystemException, NoSuchEntityException ;

    /**
     * Finds and removes question with given id {@link Question#id} if
     * requested user {@linkplain User.UserRights#MODERATE is allowed} to do that.
     *
     * NOTE!!!
     *
     * User, who asked this question  will be affected:
     * <ul>
     *     <li>In recent activity</li>
     *     <li>In count of asked questions</li>
     * </ul>
     *
     * All users having answers related to this questions will be affected:
     * <ul>
     *     <li>In recent activity</li>
     *     <li>Loose/returned {@linkplain User#rating rating} </li>
     *     <li>Loose {@linkplain User#answers statistics} </li>
     * </ul>
     *
     * because of all {@link by.epam.likeit.bean.Answer} and {@link by.epam.likeit.bean.Mark} related to this topic will be deleted.
     *
     * Reputation of all users leaved negative {@link by.epam.likeit.bean.Mark} to the question will be returned.
     *
     * @param question Question to be deleted
     * @param curUser User who requested action
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If question does not exist or operation failed
     * @throws InsufficientPermissionException If <code>user</code> is not allowed to remove this question
     */
     void remove(final Question question, final User curUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Finds and returns Question with given id
     * @param qId Question id {@link Question#id} to be found
     * @return Question associated with given id if exists, otherwise {@link NoSuchEntityException} is thrown
     * @throws DAOSystemException If something fails at DAO layer or operation failed
     * @throws NoSuchEntityException If question with given id does not exist
     */
     Question find(final long qId) throws DAOSystemException, NoSuchEntityException;

    /**
     * Finds and returns QuestionView with given id
     * @param qId Question id {@link Question#id} to be found
     * @return Question associated with given id if exists, otherwise {@link NoSuchEntityException} is thrown
     * @throws DAOSystemException If something fails at DAO layer or operation failed
     * @throws NoSuchEntityException If question with given id does not exist
     * @see QuestionView
     */
    QuestionView findView(final long qId) throws DAOSystemException, NoSuchEntityException;

    /**
     *
     * The time of moderation will be saved. User can moderate a question multiple times - only last
     * date and time of moderation will be saved.
     *
     * This operation is forbidden and {@link InsufficientPermissionException} is thrown if question in state:
     *
     * <ul>
     *     <li><code>user</code> is not allowed to {@link User.UserRights#MODERATE}</li>
     * </ul>
     *
     * This operation can change only {@link Question#body} and {@link Question#title} fields.
     * Also it add additional information like {@link ModerationInfo#reason}.
     *
     * If a question is moderate
     * Moderates the question by its {@link Question#id} field, if <code>user</code> has enough rights:
     * <ul>
     *     <li>If <code>user</code> has {@link User.UserRights#MODERATE} right</li>
     * </ul>d, owner can no longer {@linkplain IQuestionService#modify(Question, Question, User, User) modify it}
     * and question has state {@link Question.QuestionState#MODERATED}
     *
     * @see by.epam.likeit.bean.Mark
     * @param oldQuestion Pre-moderated version of the question
     * @param newQuestion Moderated version of the question
     * @param info information provided with action.
     * @param user user who requested this action
     * @throws DAOSystemException If something fails at DAO layer or <code>user</code> does not exist
     * @throws NoSuchEntityException If question does not exist or operation failed
     * @throws InsufficientPermissionException If <code>user</code> is not allowed to {@link User.UserRights#MODERATE}
     */
     void moderate(final Question oldQuestion,
                                final Question newQuestion,
                                final ModerationInfo info,
                                final User user)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Finds and returns login of {@link User#login} who asked question with given id
     * @param question Question, initiator of which should be found
     * @return User's {@linkplain User#login login} who asked this question
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If question with given {@linkplain Question#id} does not exist
     */
     String getOwnerLogin(final Question question) throws DAOSystemException, NoSuchEntityException;

    /**
     * Close the question by its {@link Question#id} field, if <code>user</code> has enough rights:
     * <ul>
     *     <li>if this user asked this questions</li>
     *     <li>if this user is allowed {@link User.UserRights#MODERATE}</li>
     * </ul>
     *
     * If Question is closed, no one user can {@linkplain IAnswerService#newAnswer(Answer, User)}  write new answer} to it.
     * Question cannot be {@linkplain IQuestionService#modify(Question, Question, User, User) modified}
     * Regardless to above, moderator still have opportunity
     * to {@link IQuestionService#moderate(Question, Question, ModerationInfo, User)} this question.
     * This action does not change any questions {@linkplain Question#viewed statistics},
     * so if any user will watch this question, statistic of views will be updated.
     * Also {@linkplain User#questions statistics} of <code>owner</code> won't be changed.
     *
     * To check, if the question is closed, use {@link Question#getState()}
     * @see Question.QuestionState
     *
     * @param question Question to be closed
     * @param ownerUser User who asked this question
     * @param curUser User who requested closing the question
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If question does not exist or operation failed
     * @throws InsufficientPermissionException If <code>curUser</code> has no enough rights
     */
     void close(final Question question,
                             final User ownerUser,
                             final User curUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Modifies the question by its {@link Question#id} field, if <code>user</code> has enough rights:
     * <ul>
     *     <li>If <code>curUser</code> asked this question</li>
     *     <li>If <code>curUser</code> has {@link User.UserRights#MODERATE} right</li>
     * </ul>
     *
     * The time of modifying will be saved. User can modify a question multiple times - only last
     * date and time of moderation will be saved.
     *
     * This operation is forbidden and {@link InsufficientPermissionException} is thrown if question in state:
     *
     * <ul>
     *     <li>{@link Question.QuestionState#CLOSED}</li>
     *     <li>{@link Question.QuestionState#MODERATED}</li>
     *     <li><code>curUser</code> is not allowed to {@link User.UserRights#WRITE}</li>
     *     <li><code>curUser</code> is not user who asked the question</li>
     * </ul>
     *
     * This operation can change only {@link Question#body} and {@link Question#title} fields.
     *
     * @param oldQuestion Old(current) version of the question
     * @param newQuestion Updated version of the question
     * @param curUser User who requested this operation
     * @param owner User who asked this question
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If questions does not exist  or operation failed
     * @throws InsufficientPermissionException If user has no enough rights or modifying this question is forbidden
     */
     void modify(final Question oldQuestion,
                              final Question newQuestion,
                              final User curUser,
                              final User owner)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

}
