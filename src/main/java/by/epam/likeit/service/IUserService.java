package by.epam.likeit.service;


import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.view.UserView;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.util.Set;

/**
 * This interface declares contract of the business logic associated to {@link User}
 */
public interface IUserService {

    // Methods ---------------------------------------------------------------------------------------

    /**
     * Finds user by combination of login and hashed password.
     *
     * @param user Prospective user with given login and email.
     *             Another information won't affect result of searching.
     *             <i>NOTE!!!</i> <b>{@linkplain User#password password} must not be hashed!</b>
     *             <i>NOTE!!!</i> <b>{@linkplain User#login login} is not case sensitive!</b>
     * @return user with given login and password if exists, otherwise null
     * @throws DAOSystemException If something fails at DAO layer.
     */
    User login(final User user) throws DAOSystemException;

    /**
     * Finds requested user by login and returns advanced information including all best skills and
     * recent activity.
     * @param user User that to be found;
     * @param length The length of the records to be returned [0; length]
     * @return Advanced user information to be displayed with full statistics
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested user does not exist
     */
    UserView getInfo(final User user, final long length) throws DAOSystemException, NoSuchEntityException;


    /**
     * Finds requester information about user by his login and returns it if the client who has requested it
     * is allowed to watch profile of another users.
     * If user requested his own information while he is not allowed <code>WATCH_PROFILE</code>, he will receive
     * information regardless lack of rights.
     *
     * @see User.UserRights#WATCH_PROFILE
     * @see IUserService#getInfo(User, long)  (User)
     * @param target User, whose information is requested
     * @param current User who has requested information.
     * @param length The length of the records to be returned [0; length]
     * @return Requested user information
     * @throws InsufficientPermissionException If client is not allowed to watch profile of another users
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested user does not exist
     */
    UserView showProfile(final User target, final User current, final long length)
            throws InsufficientPermissionException, DAOSystemException, NoSuchEntityException;
    /**
     * Finds user by login.
     * Unlike {@link IUserService#showProfile(User, User, long)} (User, User)} this method:
     * <ul>
     *     <li>Does not check {@linkplain User.UserRights rights}.</li>
     *     <li>Does not return personal information.</li>
     *     <li>Does not return statistics.</li>
     *     <li>Does not return recent activity.</li>
     * </ul>
     *
     * @param login String representation of a user's login, whose information is requested.
     * @return User profile information or null if user with given login does not exist
     * @throws DAOSystemException If something fails at DAO layer
     */
    User find(final String login) throws DAOSystemException;

    /**
     * Finds information about user with login stated in <code>oldUser</code>
     * field {@link User#login} and replace it with information mentioned
     * in <code>newUser</code>.
     * if <code>oldUser</code> is allowed to {@link User.UserRights#WRITE}
     *
     * NOTE!!! Method does not update user login and rights.
     * NOTE!!! <code>newUser</code> must also contain the {@link User#login} of
     * the user.
     * @see IUserService#updateRights(User, User, Set)
     *
     * @param oldUser information to be replaced except login and rights
     * @param newUser information to be placed instead of outdated.
     * @return Updated information of given user.
     * @throws DAOSystemException If something fails at DAO layer or unique information is duplicated
     * @throws NoSuchEntityException If user whose information must be updated does not exist
     * @throws InsufficientPermissionException If <code>oldUser</code> is not allowed to <code>WRITE</code>
     */
    UserView update(final User oldUser, final User newUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;
    /**
     * Updates avatar location of given user to value provided in {@link User#avatar}
     * @param user User whose avatar location must be updated
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If user does not exist
     * @throws InsufficientPermissionException if user does not have enough {@link User.UserRights#WRITE} right.
     */
    void updateAvatar(final User user)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Updates statistics of given question and <code>user</code> who requested it.
     * If user is not defined, only <code>question</code> counter will be updated.
     * @param user User who requested question and whose statistics must be updated.
     * @param question Requested question
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested user or question does not exist
     * @throws InsufficientPermissionException If requested user is not allowed to {@linkplain User.UserRights#READ read}
     */
    void showQuestion(final User user, final Question question)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Updates rights of <code>target</code> user.
     * For successful executing this action <code>current</code> user must have {@link User.UserRights#MODERATE} right
     * and the {@linkplain User#getRightsLevel() rights level} stronger then ker than <code>targetUser</code>
     * @param current User who requested updating of rights.
     * @param target User whose rights are going to be updated
     * @param rights Set of {@link User.UserRights} to be inserted instead of outdated
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If <code>target</code> user does not exist
     * @throws InsufficientPermissionException If <code>current</code> user has not enough permissions to
     * manipulate with rights of <code>target</code> user
     * @see User.UserRights
     */
     void updateRights(final User current,
                                    final User target,
                                    final Set<User.UserRights> rights)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;
    /**
     * Creates a new user with data provided in <code>newUser</code>.
     * @param newUser User to be created
     * @throws DAOSystemException If something fails at DAO layer of some unique data is duplicated
     * @throws NoSuchEntityException If user was not created
     */
    void register(final User newUser) throws DAOSystemException, NoSuchEntityException;

    /**
     * Checks if email provided in <code>newUser</code> {@link User#avatar} field is already exists in database.
     * @param newUser User instance with email to be seeking in the database
     * @return true if email is "free", otherwise false.
     * @throws DAOSystemException If something fails at DAO layer
     */
    boolean checkUniqueEmail(final User newUser) throws DAOSystemException;

}
