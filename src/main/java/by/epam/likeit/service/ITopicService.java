package by.epam.likeit.service;


import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.User;
import by.epam.likeit.dao.BaseDAO;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.util.List;

/**
 * This interface declares contract of the business logic associated to {@link Topic}
 */
public interface ITopicService {

    // Methods ---------------------------------------------------------------------------------------

    /**
     * Finds and returns list of topics that are satisfying to the request.
     * If offset or count is incorrect, empty collection is returned
     * @param sortBy Result will be sorted by this field
     * @param orderBy Order of sorting {@linkplain BaseDAO.ComparatorMode see more}
     * @param offset Position starting from which result will be returned
     * @param count Count of entities to be returned starting from <code>offset</code>
     * @return {@linkplain List <Topic> list} of topics satisfying to the request.
     * @throws DAOSystemException If something fails at DAO layer or <code>sortBy</code> is not exist
     */
    List<Topic> getAll(final String sortBy,
                                     final BaseDAO.ComparatorMode orderBy,
                                     final long offset,
                                     final long count) throws DAOSystemException;

    /**
     * Finds and deletes topic with given id if it is allowed to this user.
     * All question in given topic will be deleted.
     *
     * NOTE!!!
     *
     * All users, who asked questions in the topic will be affected:
     * <ul>
     *     <li>In recent activity</li>
     *     <li>In count of asked questions</li>
     * </ul>
     *
     * All users having answers related to this topic will be affected:
     * <ul>
     *     <li>In recent activity</li>
     *     <li>Loose/returned {@linkplain User#rating rating} </li>
     *     <li>Loose {@linkplain User#answers statistics} </li>
     * </ul>
     *
     * because of all {@link by.epam.likeit.bean.Question}, {@link by.epam.likeit.bean.Answer} and {@link by.epam.likeit.bean.Mark}
     * related to this topic will be deleted.
     *
     * Reputation of all users leaved negative {@link by.epam.likeit.bean.Mark} to the answers in this topic will be returned.
     *
     * @param id Requested topic id to be deleted
     * @param user User who requested deleting given topic
     * @throws DAOSystemException If something fails at DAO layer of if topic was not deleted
     * @throws NoSuchEntityException If requested topic does not exist
     * @throws InsufficientPermissionException If user who requested operation is not allowed
     * to {@linkplain User.UserRights#ADMIN delete} topics
     */
     void remove(final long id, final User user)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Finds and returns requested topic.
     * @param topicId Requested topic id
     * @return Requested topic with all requested data
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested topic does not exist
     */
     Topic getTopic(final Long topicId) throws DAOSystemException, NoSuchEntityException;

    /**
     * Calculates count of questions associated to given topic.
     * If requested topic does not exist <code>0</code> is returned.
     * @param topic Topic which contains {@link Topic#id} which questions will be calculated.
     * @return count of questions associated to given topic if the topic is exists, otherwise 0.
     * @throws DAOSystemException If something fails at DAO layer
     */
     Long getQuestionsCountByTopic(final Topic topic)
            throws DAOSystemException;

    /**
     * Checks if <code>user</code> has enough permission to create new topic, creates it
     * and returns generated id of the new topic
     * @param topic Topic object, that contains {@link Topic#name}
     * @param user User who requested creating new topic
     * @return {@linkplain Topic#id generated id} of created topic
     * @throws DAOSystemException If something fails at database level
     * or topic with given {@link Topic#name} is already exists
     * @throws NoSuchEntityException If topic was not created
     * @throws InsufficientPermissionException If user, who requested creating a new topic has not enough
     * {@linkplain User.UserRights#ADMIN permission for creating new topics}
     */
     long newTopic(final Topic topic, final User user)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Calculate topics count
     * @return topics count
     * @throws DAOSystemException If something fails at database level
     * @throws NoSuchEntityException If result was not calculated
     */
    long getTopicsCount() throws DAOSystemException, NoSuchEntityException ;

}
