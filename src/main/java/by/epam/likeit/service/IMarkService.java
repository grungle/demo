package by.epam.likeit.service;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.Mark;
import by.epam.likeit.bean.User;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

/**
 * This interface declares contract of the business logic associated to {@link Mark}
 */
public interface IMarkService {


    // Methods ---------------------------------------------------------------------------------------

    /**
     * Finds and return {@linkplain Mark mark} of the given {@linkplain User user}
     * associated to given {@linkplain Answer answer}. If <code>user</code> has not estimated
     * this answer yet, returns null.
     * @param answer Answer that has been estimated
     * @param user User who estimated this answer
     * @return Associated mark if exists, otherwise null
     * @throws DAOSystemException If something fails at DAO layer
     */
     Mark getMark(final Answer answer, final User user) throws DAOSystemException;

    /**
     * Replaces <code>oldMark</code> to <code>newMark</code> associated to
     * given {@linkplain by.epam.likeit.bean.Question question}
     *
     * If {@link Mark oldMark} does not exist, it inserts new {@link Mark}
     * {@linkplain Mark mark} must be in interval [-5;5].
     * If it is not, {@link IllegalArgumentException} is thrown.
     *
     * NOTE!!! {@link User} who {@linkplain IAnswerService#newAnswer(Answer, User) write the answer}
     * cannot estimate his own answer.
     * In this case {@link InsufficientPermissionException} is thrown.
     *
     * If {@link User} is not allowed to {@link User.UserRights#WRITE}
     * the {@link InsufficientPermissionException} is thrown
     *
     * @param oldMark Old mark to be replaced
     * @param answer Answer to be estimated
     * @param newMark New mark to be placed instead of old
     * @param curUser User who is estimating the answer
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If new mark wasn't create or if existing mark was not updated.
     * @throws InsufficientPermissionException if <code>curUser</code> is not allowed to perform this action
     */
     void estimate(final Mark oldMark, final Answer answer,
                                final Mark newMark, final User curUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;
}
