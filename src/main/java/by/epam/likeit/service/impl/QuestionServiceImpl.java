package by.epam.likeit.service.impl;


import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.view.QuestionView;
import by.epam.likeit.dao.BaseDAO;
import by.epam.likeit.dao.DAOFactory;
import by.epam.likeit.dao.IQuestionDAO;
import by.epam.likeit.dao.QuestionDaoJdbcImpl;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.service.IQuestionService;

import java.util.List;

import static by.epam.likeit.bean.Question.QuestionState.*;
import static by.epam.likeit.bean.User.UserRights.MODERATE;
import static by.epam.likeit.bean.User.UserRights.WRITE;

/**
 * This class provides basic implementation of {@link IQuestionService} interface
 */
public class QuestionServiceImpl implements IQuestionService {


    // Constructors ----------------------------------------------------------------------------------

    public QuestionServiceImpl(){}

    // Methods ---------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public long newQuestion(final Question question, final User owner) throws DAOSystemException,
            NoSuchEntityException, InsufficientPermissionException {

        IQuestionDAO qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);
        if (owner.isAllowed(WRITE))
            return qDao.askQuestion(question);

        throw new InsufficientPermissionException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Question> getQuestions(final Topic topic,
                                              final long offset,
                                              final long length,
                                              final String sortBy,
                                              final BaseDAO.ComparatorMode orderBy)
            throws DAOSystemException, NoSuchEntityException {

        IQuestionDAO qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);

        return qDao.getQuestions(topic, offset, length, sortBy, orderBy);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final Question question, final User curUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IQuestionDAO qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);
        if (curUser.isAllowed(MODERATE))
            qDao.removeQuestion(question);
        else throw new InsufficientPermissionException();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Question find(final long qId) throws DAOSystemException, NoSuchEntityException {
        IQuestionDAO qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);
        return qDao.getQuestionById(qId);
    }

    @Override
    public QuestionView findView(long qId) throws DAOSystemException, NoSuchEntityException {
        QuestionView qv = new QuestionView();
        Question q = find(qId);
        qv.setQuestion(q);
        return qv;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moderate(final Question oldQuestion,
                                final Question newQuestion,
                                final ModerationInfo info,
                                final User user)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IQuestionDAO qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);
        if (user.isAllowed(MODERATE)){
            if (oldQuestion.getState() == MODERATED ) qDao.updateModerationInfo(info);
            else qDao.moderateInfo(info);
            qDao.moderateQuestion(newQuestion);
        } else throw new InsufficientPermissionException();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getOwnerLogin(final Question question) throws DAOSystemException, NoSuchEntityException {
        IQuestionDAO qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);
        return qDao.getOwnerLogin(question);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close(final Question question,
                             final User ownerUser,
                             final User curUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IQuestionDAO qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);

        if (ownerUser.equals(curUser) || curUser.isAllowed(MODERATE)){
            qDao.closeQuestion(question);
        } else
            throw new InsufficientPermissionException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modify(final Question oldQuestion,
                              final Question newQuestion,
                              final User curUser,
                              final User owner)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IQuestionDAO qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);

        if (curUser.equals(owner) && curUser.isAllowed(WRITE) &&
                (oldQuestion.getState() == OPEN || oldQuestion.getState() == MODIFIED))
            qDao.modifyQuestion(newQuestion);
        else
            throw new InsufficientPermissionException();
    }


}
