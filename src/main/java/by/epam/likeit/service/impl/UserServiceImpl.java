package by.epam.likeit.service.impl;

import by.epam.likeit.bean.AbstractMessage;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.view.UserView;
import by.epam.likeit.dao.*;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.service.IUserService;
import javafx.util.Pair;
import org.mindrot.jbcrypt.BCrypt;

import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static by.epam.likeit.bean.User.UserRights.*;


/**
 * This class provides basic implementation of
 * {@link IUserService} interface
 */
public class UserServiceImpl implements IUserService {


    // Constructors ----------------------------------------------------------------------------------

    public UserServiceImpl(){}

    // Methods ---------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    public User login(final User user) throws DAOSystemException {
        IUserDAO uDao = DAOFactory.newInstance(UserDaoJdbcImpl.class);
        try{
            User target = uDao.getUserByLogin(user.getLogin());
            if (target != null && BCrypt.checkpw(user.getPassword(), target.getPassword())){
                return find(user.getLogin());
            }
        } catch (DAOSystemException e){
            LikeLogger.info(e);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public UserView getInfo(final User user, final long length) throws DAOSystemException, NoSuchEntityException {
        IUserDAO uDao = DAOFactory.newInstance(UserDaoJdbcImpl.class);
        User target = uDao.getUserInfoByLogin(user.getLogin());
        List<Pair<String, Double>> skills = uDao.getTopSkills(user);
        List<AbstractMessage> activity = uDao.getLastUserActivity(user, length);

        UserView uv = new UserView();
        uv.setLastMessages(activity);
        uv.setUser(target);
        uv.setTopSkills(skills);
        return uv;
    }

    /**
     * {@inheritDoc}
     */
    public UserView showProfile(final User target, final User current, final long length)
            throws InsufficientPermissionException, DAOSystemException, NoSuchEntityException {

        if ((current == null || !current.isAllowed(WATCH_PROFILE)) && !Objects.equals(current, target))
            throw new InsufficientPermissionException();
        return getInfo(target, length);
    }

    /**
     * {@inheritDoc}
     */
    public User find(final String login) throws DAOSystemException {
        IUserDAO uDao = DAOFactory.newInstance(UserDaoJdbcImpl.class);
        return uDao.getUserByLogin(login);
    }

    /**
     * {@inheritDoc}
     */
    public UserView update(final User oldUser, final User newUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IUserDAO uDao = DAOFactory.newInstance(UserDaoJdbcImpl.class);

        if (oldUser.isAllowed(WRITE)){
            uDao.updateUser(newUser);
        } else {
            throw new InsufficientPermissionException();
        }

        UserView uv = new UserView();
        uv.setUser(uDao.getUserInfoByLogin(oldUser.getLogin()));
        return uv;
    }

    /**
     * {@inheritDoc}
     */
    public void updateAvatar(final User user)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IUserDAO uDao = DAOFactory.newInstance(UserDaoJdbcImpl.class);

        if (user.isAllowed(WRITE)) {
            uDao.updateAvatar(user.getLogin(),user.getAvatar());
        } else
            throw new InsufficientPermissionException();

    }

    /**
     * {@inheritDoc}
     */
    public void showQuestion(final User user, final Question question)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IUserDAO uDao = DAOFactory.newInstance(UserDaoJdbcImpl.class);
        IQuestionDAO qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);

        if (user != null) {
            if (user.isAllowed(READ)) {
                qDao.incrementViews(question);
                uDao.incrementViews(user);
            }
        } else {
            qDao.incrementViews(question);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void updateRights(final User current,
                                    final User target,
                                    final Set<User.UserRights> rights)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException{

        IUserDAO uDao = DAOFactory.newInstance(UserDaoJdbcImpl.class);

        if (current.isAllowed(MODERATE) && current.getRightsLevel() > target.getRightsLevel()){
            uDao.updateRights(target.getLogin(), rights);
            target.setRights(EnumSet.copyOf(rights));
        } else {
            throw new InsufficientPermissionException();
        }
    }

    /**
     * {@inheritDoc}
     */
    public void register(final User newUser) throws DAOSystemException, NoSuchEntityException {
        UserDaoJdbcImpl uDao = DAOFactory.newInstance(UserDaoJdbcImpl.class);
        uDao.registerUser(newUser);
    }

    /**
     * {@inheritDoc}
     */
    public boolean checkUniqueEmail(final User newUser) throws DAOSystemException {
        IUserDAO uDao = DAOFactory.newInstance(UserDaoJdbcImpl.class);
        return uDao.getUserByEmail(newUser.getEmail()) == null;
    }

}
