/**
 * This package contain classes that operates with by.epam.likeit.bean implementing
 * interfaces from {@link by.epam.likeit.service} package
 * and provides methods to execute business logic actions.
 * This methods does NOT "know" anything about transactions.
 * To perform action in a transaction, use {@link by.epam.likeit.jdbc.transaction.TransactionManager}
 *
 * For Model-View-Controller applications, classes in this
 * package represent Model role.
 *
 * @see by.epam.likeit.bean
 * @see by.epam.likeit.jdbc.transaction.TransactionManager
 */
package by.epam.likeit.service.impl;