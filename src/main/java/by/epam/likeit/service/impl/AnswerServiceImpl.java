package by.epam.likeit.service.impl;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.view.AnswerView;
import by.epam.likeit.dao.AnswerDaoJdbcImpl;
import by.epam.likeit.dao.BaseDAO;
import by.epam.likeit.dao.DAOFactory;
import by.epam.likeit.dao.IAnswerDAO;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.service.IAnswerService;

import java.util.List;

import static by.epam.likeit.bean.Answer.AnswerState.MODERATED;
import static by.epam.likeit.bean.User.UserRights.MODERATE;
import static by.epam.likeit.bean.User.UserRights.WRITE;

/**
 * This class provides basic implementation of the
 * {@link IAnswerService} interface.
 */
public class AnswerServiceImpl implements IAnswerService{

    // Constructors ----------------------------------------------------------------------------------

    public AnswerServiceImpl() {}

    // Methods ---------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void newAnswer(final Answer answer, final User owner)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IAnswerDAO aDao = DAOFactory.newInstance(AnswerDaoJdbcImpl.class);

        if (owner.isAllowed(WRITE)) {
            aDao.writeAnswer(answer);
        } else {
            throw new InsufficientPermissionException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnswerView> getAnswers(final String login, final long questionId,
                                       final String sortBy,
                                       final BaseDAO.ComparatorMode orderBy,
                                       final long offset,
                                       final long length)
            throws DAOSystemException, NoSuchEntityException {

        IAnswerDAO aDao = DAOFactory.newInstance(AnswerDaoJdbcImpl.class);
        return aDao.getAnswersByQuestionId(login, questionId, sortBy, orderBy, offset, length);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moderate(final Answer oldAnswer,
                                final Answer newAnswer,
                                final ModerationInfo moderationInfo,
                                final User currentUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IAnswerDAO aDao = DAOFactory.newInstance(AnswerDaoJdbcImpl.class);

        if (currentUser.isAllowed(MODERATE)) {
            if (oldAnswer.getState() == MODERATED)
                aDao.updateModerationInfo(moderationInfo);
            else aDao.moderateInfo(moderationInfo);

            aDao.moderateAnswer(newAnswer);
        } else
            throw new InsufficientPermissionException();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Answer find(final long aId) throws DAOSystemException, NoSuchEntityException {
        IAnswerDAO aDao = DAOFactory.newInstance(AnswerDaoJdbcImpl.class);
        return aDao.getAnswerById(aId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getOwner(final Answer answer) throws DAOSystemException, NoSuchEntityException {
        IAnswerDAO aDao = DAOFactory.newInstance(AnswerDaoJdbcImpl.class);
        return aDao.getOwnerLoginByAnswer(answer);
    }

    /**
     * {@inheritDoc}
     */
    public void modify(final Answer oldAnswer, final Answer newAnswer,
                              final User ownerUser, final User curUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IAnswerDAO aDao = DAOFactory.newInstance(AnswerDaoJdbcImpl.class);

        if (oldAnswer.getState() != MODERATED && (curUser.equals(ownerUser) && curUser.isAllowed(WRITE))) {
            aDao.modifyAnswer(newAnswer);
        } else throw new InsufficientPermissionException();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final Answer answer, final User curUser, final User ownerUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IAnswerDAO aDao = DAOFactory.newInstance(AnswerDaoJdbcImpl.class);

        if ((curUser.equals(ownerUser) && curUser.isAllowed(WRITE)) || curUser.isAllowed(MODERATE)){
            aDao.removeAnswer(answer);
        } else throw new InsufficientPermissionException();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getAnswersCount(final Question question) throws DAOSystemException {
        IAnswerDAO aDao = DAOFactory.newInstance(AnswerDaoJdbcImpl.class);
        return aDao.getAnswersCount(question);
    }


}
