package by.epam.likeit.service.impl;


import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.User;
import by.epam.likeit.dao.BaseDAO;
import by.epam.likeit.dao.DAOFactory;
import by.epam.likeit.dao.QuestionDaoJdbcImpl;
import by.epam.likeit.dao.TopicDaoJdbcImpl;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.service.ITopicService;

import java.util.List;

import static by.epam.likeit.bean.User.UserRights.ADMIN;

/**
 * This class provides basic implementation of {@link ITopicService} interface
 */
public class TopicServiceImpl implements ITopicService {

    // Constructors ----------------------------------------------------------------------------------

    public TopicServiceImpl(){}

    // Methods ---------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Topic> getAll(final String sortBy,
                                     final BaseDAO.ComparatorMode orderBy,
                                     final long offset,
                                     final long count) throws DAOSystemException {

        TopicDaoJdbcImpl tDao = DAOFactory.newInstance(TopicDaoJdbcImpl.class);
        return tDao.getAllTopics(sortBy, orderBy, offset, count);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(final long id, final User user)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        TopicDaoJdbcImpl tDao = DAOFactory.newInstance(TopicDaoJdbcImpl.class);
        if (user.isAllowed(ADMIN))
            tDao.removeTopic(id);
        else
            throw new InsufficientPermissionException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Topic getTopic(final Long topicId) throws DAOSystemException, NoSuchEntityException {
        TopicDaoJdbcImpl tDao = DAOFactory.newInstance(TopicDaoJdbcImpl.class);
        return tDao.getTopicById(topicId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getQuestionsCountByTopic(final Topic topic)
            throws DAOSystemException{

        QuestionDaoJdbcImpl qDao = DAOFactory.newInstance(QuestionDaoJdbcImpl.class);
        return qDao.getQuestionsCountByTopicId(topic);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long newTopic(final Topic topic, final User user)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        TopicDaoJdbcImpl tDao = DAOFactory.newInstance(TopicDaoJdbcImpl.class);
        if (user.isAllowed(ADMIN)){
            return tDao.createTopic(topic);
        } else
            throw new InsufficientPermissionException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTopicsCount() throws DAOSystemException, NoSuchEntityException {
        TopicDaoJdbcImpl tDao = DAOFactory.newInstance(TopicDaoJdbcImpl.class);
        return tDao.getTopicsCount();
    }

}
