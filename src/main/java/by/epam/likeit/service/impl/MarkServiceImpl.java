package by.epam.likeit.service.impl;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.Mark;
import by.epam.likeit.bean.User;
import by.epam.likeit.dao.DAOFactory;
import by.epam.likeit.dao.IMarkDAO;
import by.epam.likeit.dao.MarkDaoJdbcImpl;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.service.IMarkService;

import static by.epam.likeit.bean.Mark.MAX_MARK;
import static by.epam.likeit.bean.Mark.MIN_MARK;
import static by.epam.likeit.bean.User.UserRights.WRITE;

/**
 * Class provides basic implementation of {@link IMarkService} interface
 */
public class MarkServiceImpl implements IMarkService {

    // Constructors ----------------------------------------------------------------------------------

    public MarkServiceImpl() {}

    // Methods ---------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Mark getMark(final Answer answer, final User user) throws DAOSystemException {
        IMarkDAO mDao = DAOFactory.newInstance(MarkDaoJdbcImpl.class);
        return mDao.getMark(answer, user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void estimate(final Mark oldMark, final Answer answer,
                                final Mark newMark, final User curUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException {

        IMarkDAO mDao = DAOFactory.newInstance(MarkDaoJdbcImpl.class);

        if (curUser.isAllowed(WRITE) && !curUser.equals(answer.getUser())) {

            if (oldMark == null) {
                if (newMark.getMark() <= MAX_MARK && newMark.getMark() >= MIN_MARK)
                    mDao.newMark(newMark);
                else throw new IllegalArgumentException("Invalid mark " + newMark.getMark());

            } else {
                if (newMark.getMark() <= MAX_MARK && newMark.getMark() >= MIN_MARK)
                    mDao.updateMark(newMark);
            }

        } else throw new InsufficientPermissionException();

    }
}
