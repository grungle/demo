package by.epam.likeit.service;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.view.AnswerView;
import by.epam.likeit.dao.BaseDAO;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.util.List;

/**
 * This interface declares contract of the business logic associated to {@link Answer}
 */
public interface IAnswerService {

    // Methods ---------------------------------------------------------------------------------------

    /**
     * Creates new answer to question mentioned in {@link Answer#question}
     *
     * User is not allowed to create answer if :
     * <ul>
     *     <li>User is not allowed to {@link User.UserRights#WRITE}</li>
     * </ul>
     *
     * NOTE!!! User CAN wrote answer to his own question.
     *
     * @param answer Answer to be created
     * @param owner User who wrote the answer and requested this operation
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If answer was not created
     * @throws InsufficientPermissionException If user is not allowed to write the answer
     */
     void newAnswer(final Answer answer, final User owner)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Finds and returns list of answers related to given <code>qId</code>.
     * If given question does not exist, an empty list is returned.
     * @param login the login of the user who requested this operation. If this operation is
     *              requested by guest, this parameter must be null.
     * @param questionId Question to which answers must be found
     * @param sortBy field name for result sorting
     * @param orderBy {@link by.epam.likeit.dao.BaseDAO.ComparatorMode} order of sorting
     * @param offset describes position starting from which result is returned
     * @param length maximal count of records to be returned
     * @return {@link List <Answer>} of answers related to given {@linkplain Question#id questionId}.
     * @throws DAOSystemException If something fails at DAO layer or operation failed
     */
     List<AnswerView> getAnswers(final String login, final long questionId,
                                 final String sortBy,
                                 final BaseDAO.ComparatorMode orderBy,
                                 final long offset,
                                 final long length)
            throws DAOSystemException, NoSuchEntityException;

    /**
     * Finds and moderates given answer.
     *
     * Operation can't be moderated if:
     * <ul>
     *     <li>User who requested it is not allowed to {@link User.UserRights#MODERATE}</li>
     * </ul>
     *
     * Changes answer state to {@link Answer.AnswerState#MODERATED}
     * User can moderate answer multiple times, only last moderation will be saved.
     *
     * @param oldAnswer Pre-moderated version of the answer
     * @param newAnswer Moderated version of the answer
     * @param moderationInfo Extended information describing reasons of this action
     * @param currentUser User who requested moderation
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If given answer does not exist or operation failed
     * @throws InsufficientPermissionException If user who requested this operation is not allowed to moderate
     * @see ModerationInfo
     * @see Answer#lastUpdate
     */
    void moderate(final Answer oldAnswer,
                                final Answer newAnswer,
                                final ModerationInfo moderationInfo,
                                final User currentUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Finds and returns answer by its id.
     * @param aId answer with that {@link Answer#id} must be found
     * @return Answer with given id if exists, otherwise null.
     * @throws DAOSystemException If something fails at DAO layer or operation failed
     * @throws NoSuchEntityException If answer with given <code>id</code> does not exist
     */
     Answer find(final long aId) throws DAOSystemException, NoSuchEntityException;

    /**
     * Finds and return users login who wrote the answer.
     * @param answer User, who wrote this answer must be found
     * @return {@linkplain User#login login} of the user who wrote <code>answer</code>
     * @throws DAOSystemException If something fails at DAO layer or operation failed
     * @throws NoSuchEntityException If <code>answer</code> does not exist
     */
     String getOwner(final Answer answer) throws DAOSystemException, NoSuchEntityException;


    /**
     * Modifies answer using class field {@link Answer#id} mentioned in <code>oldAnswer</code>.
     *
     * Answer cannot be modified in three cases:
     * <ul>
     *     <li>Answer has illegal state: {@link Answer.AnswerState#MODERATED}</li>
     *     <li>User who asked this question is not allowed to {@link User.UserRights#WRITE}</li>
     *     <li>User who requested this operation is {@linkplain User.UserRights#MODERATE moderator}</li>
     * </ul>
     *
     * Changes answer state to {@link Answer.AnswerState#MODIFIED}
     *
     * Answer can be modified multiply times, only last date and time {@link Answer#lastUpdate} will be saved
     *
     * @param oldAnswer Pre-modified answer
     * @param newAnswer Modified answer
     * @param ownerUser User who wrote this answer
     * @param curUser User who requested this operation
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested answer does not exist or operation failed
     * @throws InsufficientPermissionException If user is not allowed to modify this answer
     * or answer has illegal state to be modified
     * @see User.UserRights
     */
    void modify(final Answer oldAnswer, final Answer newAnswer,
                              final User ownerUser, final User curUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Finds and removes given answer if <code>curUser</code> is allowed to remove this answer.
     *
     * User is allowed to remove the given answer if:
     *
     * <ul>
     *     <li>User {@linkplain Answer#user wrote} this answer
     *          and having {@link User.UserRights#WRITE} rights
     *     </li>
     *     <li>User is allowed to {@link User.UserRights#MODERATE}</li>
     * </ul>
     *
     * If user is not mentioned above, an {@link InsufficientPermissionException} is thrown.
     *
     * After successful execution some users will be affected:
     * <ul>
     *     <li>User who wrote the answer - loose/restore {@link User#answers} and {@link User#rating}</li>
     *     <li>Users tho leaved negative mark will restore his own {@link User#rating}</li>
     * </ul>
     *
     * @param answer Answer to be removed
     * @param curUser User who requested this operation
     * @param ownerUser User who wrote the answer
     * @throws DAOSystemException If something fails at DAO layer or operation failed
     * @throws NoSuchEntityException If answer does not exist
     * @throws InsufficientPermissionException If given user is not allowed to remove this answer
     */
     void remove(final Answer answer, final User curUser, final User ownerUser)
            throws DAOSystemException, NoSuchEntityException, InsufficientPermissionException;

    /**
     * Calculates and returns count of answers related to given <code>question</code>.
     * If the given question does not exist, <code>0</code> is returned
     * If there is no answers to the given question or the question does not exist, <code>0</code> is returned.
     * @param question Question to which answers must be found
     * @return count of answers related to given question
     * @throws DAOSystemException If something fails at DAO layer or operation failed
     * @see Question
     */
     long getAnswersCount(final Question question) throws DAOSystemException;


}
