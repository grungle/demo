package by.epam.likeit.logger;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.net.URL;

/**
 * This class delegates some methods to l4j logger instance.
 */
public class LikeLogger {

    private static final Logger logger = Logger.getLogger(LikeLogger.class);
    private static final Logger actionLogger = Logger.getLogger("actionsLogger");

    public static void init(URL location){
        PropertyConfigurator.configure(location);
    }

    public static void err(Object message, Throwable cause){
        logger.error(message, cause);
    }

    public static void info(Object message){
        logger.info(message);
    }

    public static void warn(Object message, Throwable cause){
        logger.warn(message, cause);
    }

    public static void debug(Object message){
        logger.debug(message);
    }

    public static void debug(Object message, Throwable cause){
        logger.debug(message, cause);
    }

    public static void trace(Object message){
        actionLogger.trace(message);
    }

    public static void fatal(Object message, Throwable cause){
        logger.fatal(message, cause);
    }

    public static void info(String message, Exception ex) {
        logger.info(message, ex);
    }
}
