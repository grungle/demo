package by.epam.likeit.util;


import by.epam.likeit.logger.LikeLogger;

import java.sql.*;
import java.util.Date;

/**
 * Class provides methods over JDBC API
 * to make its using even easier.
 */
public class JdbcUtils {

    private JdbcUtils(){}

    /**
     * Returns a PreparedStatement of the given connection, set with the given SQL query and the
     * given parameter values.
     * NOTE!!! If one of parameters may be equal to null,
     * use null-safe method {@link this#prepareNullSafeStatement(Connection, String, boolean, Object[])}
     * @param connection The Connection to create the PreparedStatement from.
     * @param sql The SQL query to construct the PreparedStatement with.
     * @param returnGeneratedKeys Set whether to return generated keys or not.
     * @param values The parameter values to be set in the created PreparedStatement.
     * @throws SQLException If something fails during creating the PreparedStatement.
     */
    public static PreparedStatement prepareStatement
    (Connection connection, String sql, boolean returnGeneratedKeys, Object... values)
            throws SQLException {

        PreparedStatement statement = connection.prepareStatement(sql,
                returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
        setValues(statement, values);

        return statement;
    }

    /**
     * Set the given parameter values in the given PreparedStatement.
     * NOTE!!! If one of parameters may be equal to null,
     * use null-safe method {@link this#setNullSafeValues(PreparedStatement, Object[])}
     * @param statement The PreparedStatement to set the given parameter values in.
     * @param values The parameter values to be set in the created PreparedStatement.
     * @throws SQLException If something fails during setting the PreparedStatement values.
     */
    private static void setValues(PreparedStatement statement, Object... values)
            throws SQLException {
        for (int i = 0; i < values.length; i++) {
            statement.setObject(i + 1, values[i]);
        }
    }


    /**
     * Returns a PreparedStatement of the given connection, set with the given SQL query and the
     * given parameter values.
     * Unlike {@link this#prepareStatement(Connection, String, boolean, Object...)} method allows null
     * values in argument list
     * @param connection The Connection to create the PreparedStatement from.
     * @param sql The SQL query to construct the PreparedStatement with.
     * @param returnGeneratedKeys Set whether to return generated keys or not.
     * @param values The parameter values to be set in the created PreparedStatement.
     * @throws SQLException If something fails during creating the PreparedStatement.
     */
    public static PreparedStatement prepareNullSafeStatement(Connection connection, String sql, boolean returnGeneratedKeys,
                                                             Object[] values) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sql,
                returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
        setNullSafeValues(statement, values);

        return statement;
    }

    /**
     * Set the given parameter values in the given PreparedStatement.
     * @param statement The PreparedStatement to set the given parameter values in.
     * @param values The parameter values to be set in the created PreparedStatement.
     * @throws SQLException If something fails during setting the PreparedStatement values.
     */
    private static void setNullSafeValues(PreparedStatement statement, Object[] values) throws SQLException {
        for (int i = 0; i < values.length; i++){
            if (values[i] == null)
                statement.setNull(i + 1, Types.JAVA_OBJECT);
            else
                statement.setObject(i + 1, values[i]);
        }
    }


    /**
     * Converts the given java.by.epam.likeit.util.Date to java.sql.Date.
     * @param date The java.by.epam.likeit.util.Date to be converted to java.sql.Date.
     * @return The converted java.sql.Date.
     */
    public static Date toSqlTimestamp(java.util.Date date) {
        return (date != null) ? new Timestamp(date.getTime()) : null;
    }

    /**
     * Converts SQL Timestamp to java Date.
     * @param ts timestamp to be converted
     * @return the java.by.epam.likeit.util.date object
     * @see java.util.Date
     * @see java.sql.Timestamp
     */
    public static java.util.Date toJavaDate(Timestamp ts){
        return (ts != null) ? new java.util.Date(ts.getTime()) : null;
    }

    /**
     * Closes autoclosable resource.
     * @param resource the resource to be closed
     */
    public static void closeQuietly(AutoCloseable resource){
        if (resource != null){
            try{
                resource.close();
            } catch (Exception e){
                LikeLogger.warn("Can't close a resource", e);
            }
        }
    }

    /**
     * Rolls back the given connection
     * @param conn connection to be roll backed.
     */
    public static void rollBackQuietly(Connection conn){
        if (conn != null){
            try{
                conn.rollback();
            } catch (SQLException e) {
                LikeLogger.warn("Can't rollback a connection", e);
            }
        }
    }


}
