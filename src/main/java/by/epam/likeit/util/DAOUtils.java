package by.epam.likeit.util;


import by.epam.likeit.bean.*;
import by.epam.likeit.bean.AbstractMessage.Type;
import by.epam.likeit.bean.builder.AnswerBuilder;
import by.epam.likeit.bean.builder.QuestionBuilder;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.controller.context.RequestContext;

import java.util.HashSet;
import java.util.Set;

import static by.epam.likeit.bean.AbstractMessage.Type.ANSWER;
import static by.epam.likeit.bean.AbstractMessage.Type.QUESTION;
import static by.epam.likeit.bean.Question.QuestionState.valueOf;
import static by.epam.likeit.bean.User.UserRights.*;
import static by.epam.likeit.controller.action.AbstractAction.*;

/**
 * Class provides set ща methods to map by.epam.likeit.bean objects
 * using {@link RequestContext} parameters
 * @see by.epam.likeit.bean
 * @see RequestContext
 */
public class DAOUtils {

    // Constants ---------------------------------------------------------------------------------

    public static final String F_NAME_ATTR_NAME = "f_name";
    public static final String L_NAME_ATTR_NAME = "l_name";
    public static final String COUNTRY_ATTR_NAME = "country";
    public static final String CITY_ATTR_NAME = "city";
    public static final String COMPANY_ATTR_NAME = "company";
    public static final String POSITION_ATTR_NAME = "position";
    public static final String LOGIN_ATTR_NAME = "login";
    public static final String EMAIL_ATTR_NAME = "email";
    public static final String PASSWORD_ATTR_NAME = "password";

    public static final String MSG_STATE_ATTR_NAME = "state";
    public static final String MSG_OWNER_ATTR_NAME = "owner";
    public static final String MODERATION_REASON_ATTR_NAME = "reason";
    public static final String RIGHT_READ_ATTR_NAME = "read";
    public static final String RIGHT_WRITE_ATTR_NAME = "write";
    public static final String RIGHT_WATCH_ATTR_NAME = "watch_profile";
    public static final String RIGHT_MODERATE_ATTR_NAME = "moderate";
    public static final String RIGHT_ADMIN_ATTR_NAME = "admin";

    // Constructors ------------------------------------------------------------------------------

    private DAOUtils() {}

    // Methods -----------------------------------------------------------------------------------

    public static User mapUser(RequestContext context) {

        return new UserBuilder()
                .fname(context.getParameter(F_NAME_ATTR_NAME))
                .lname(context.getParameter(L_NAME_ATTR_NAME))
                .country(context.getParameter(COUNTRY_ATTR_NAME))
                .city(context.getParameter(CITY_ATTR_NAME))
                .company(context.getParameter(COMPANY_ATTR_NAME))
                .position(context.getParameter(POSITION_ATTR_NAME))
                .login(context.getParameter(LOGIN_ATTR_NAME))
                .email(context.getParameter(EMAIL_ATTR_NAME))
                .avatar(context.getParameter(USER_AVA_PARAM_NAME))
                .password(context.getParameter(PASSWORD_ATTR_NAME))
                .createNewUser();
    }

    public static Question mapQuestion(RequestContext context) {

        return (Question) new QuestionBuilder()
                .state(valueOf(context.getAttribute(MSG_STATE_ATTR_NAME).toString().toUpperCase()))
                .title(context.getParameter(QUESTION_HEADER_PARAM_NAME))
                .body(context.getParameter(QUESTION_BODY_PARAM_NAME))
                .id(Long.valueOf(context.getParameter(QUESTION_ID_PARAM_NAME)))
                .type(QUESTION)
                .user(new UserBuilder().login(context.getAttribute(MSG_OWNER_ATTR_NAME).toString()).createNewUser())
                .createMessage();
    }

    public static Question mapNewQuestion(RequestContext context, User owner) {

        return (Question) new QuestionBuilder()
                .topic(new Topic(Long.valueOf(context.getParameter(TOPIC_ID_PARAM_NAME)), context.getParameter(TOPIC_NAME_PARAM_NAME)))
                .title(context.getParameter(QUESTION_HEADER_PARAM_NAME))
                .body(context.getParameter(QUESTION_BODY_PARAM_NAME))
                .type(QUESTION)
                .user(owner)
                .createMessage();
    }

    public static Answer mapAnswer(RequestContext context) {

        return (Answer) new AnswerBuilder()
                .question((Question) new QuestionBuilder().id(Long.valueOf(context.getParameter(QUESTION_ID_PARAM_NAME))).createMessage())
                .body(context.getParameter(ANSWER_BODY_PARAM_NAME))
                .type(ANSWER)
                .id(Long.valueOf(context.getParameter(ANSWER_ID_PARAM_NAME)))
                .createMessage();
    }

    public static Answer mapAnswer(RequestContext context, User owner) {

        AnswerBuilder ab = (AnswerBuilder)new AnswerBuilder()
                .question((Question) new QuestionBuilder().id(Long.valueOf(context.getParameter(QUESTION_ID_PARAM_NAME))).createMessage())
                .body(context.getParameter(ANSWER_BODY_PARAM_NAME))
                .id(Long.valueOf(context.getParameter(ANSWER_ID_PARAM_NAME)))
                .type(ANSWER)
                .user(owner);
        return ab.createMessage();
    }

    public static Answer mapNewAnswer(RequestContext context, User owner) {

        AnswerBuilder ab = (AnswerBuilder)new AnswerBuilder()
                .question((Question) new QuestionBuilder().id(Long.valueOf(context.getParameter(QUESTION_ID_PARAM_NAME))).createMessage())
                .body(context.getParameter(ANSWER_BODY_PARAM_NAME))
                .type(ANSWER)
                .user(owner);
        return ab.createMessage();
    }

    public static Answer mapAnswerId(Long id){
        Answer answ = new Answer();
        answ.setId(id);
        return answ;
    }

    public static Question mapQuestionId(Long id){
        Question answ = new Question();
        answ.setId(id);
        return answ;
    }

    public static ModerationInfo mapModeration(RequestContext context, Type targetType) {
        ModerationInfo info = new ModerationInfo();
        AbstractMessage target;
        if (targetType == QUESTION) {
            target = new Question();
            target.setId(Long.valueOf(context.getParameter(QUESTION_ID_PARAM_NAME)));
        } else {
            target = new Answer();
            target.setId(Long.valueOf(context.getParameter(ANSWER_ID_PARAM_NAME)));
        }

        User moderator = (User)context.findAttribute(USER_PARAM_NAME);
        if (!moderator.isAllowed(MODERATE)) throw new IllegalArgumentException("Run this method as an moderator");

        info.setReason(context.getParameter(MODERATION_REASON_ATTR_NAME));
        info.setTarget(target);
        info.setModerator(moderator);
        return info;
    }

    public static Set<User.UserRights> mapRights(RequestContext context) {
        HashSet<User.UserRights> rights = new HashSet<>(8);
        if (Boolean.valueOf(context.getParameter(RIGHT_READ_ATTR_NAME))) rights.add(READ);
        if (Boolean.valueOf(context.getParameter(RIGHT_WATCH_ATTR_NAME))) rights.add(WATCH_PROFILE);
        if (Boolean.valueOf(context.getParameter(RIGHT_WRITE_ATTR_NAME))) rights.add(WRITE);
        if (Boolean.valueOf(context.getParameter(RIGHT_MODERATE_ATTR_NAME))) rights.add(MODERATE);
        if (Boolean.valueOf(context.getParameter(RIGHT_ADMIN_ATTR_NAME))) rights.add(ADMIN);
        return rights;
    }

    public static Mark mapMark(RequestContext context, User curUser, Mark oldMark){
        Mark mark = new Mark();
        mark.setUser(curUser);
        mark.setAnswerId(Long.valueOf(context.getParameter(ANSWER_ID_PARAM_NAME)));
        mark.setMark(Integer.valueOf(context.getParameter(MARK_VALUE_PARAM_NAME)) + (oldMark == null ? 0 : oldMark.getMark()));
        return mark;
    }


}
