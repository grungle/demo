package by.epam.likeit.util;

import java.util.Collection;

/**
 * Class contains helpful methods to work with <code>String</code> class
 */
public class StringUtils {

    /**
     * Joins the given collection to the <code>String</code> using the given delimer.
     * @param collection the collection to be joined
     * @param delimer the delimer to join elements of the collection
     * @return joined collection elements into the string
     */
    public static String join(Collection collection, String delimer){
        StringBuilder sb = new StringBuilder();

        for (Object o : collection)
            sb.append(o).append(delimer);

        return sb.substring(0, sb.toString().isEmpty() ? 0 : sb.length() - delimer.length());
    }

    /**
     * Tries to parse the given string value to the long.
     * @param value the string value
     * @return long representation of the given string if the string can be converted to the long,
     * otherwise null.
     */
    public static Long parseLong(String value){
        try {
            return Long.valueOf(value);
        } catch (NumberFormatException e){
            return null;
        }
    }

}
