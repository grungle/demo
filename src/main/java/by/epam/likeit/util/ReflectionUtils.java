package by.epam.likeit.util;


import javax.management.ReflectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Class provides the set of methods of Reflection API
 * to make its using even easier.
 */
public class ReflectionUtils {

    private ReflectionUtils(){}

    /**
     * Gets setter name of the given class variable name by concatenating set to
     * the property name.
     * NOTE!!! The class of the given variable must be java by.epam.likeit.bean.
     * @param property The property name to find associated setter method to it.
     * @return The name of the setter method
     */
    public static String getSetterName(String property){
        return "set" +
                property.substring(0, 1).toUpperCase() + property.substring(1);
    }

    /**
     * Gets the class variable value of the given object by its name.
     * @param fieldName The field name to read its value
     * @param obj The object where to find the given field name
     * @return returns the given class variable value of the given object
     * @throws NoSuchFieldException If the given object does not contain a field with the given name
     * @throws IllegalAccessException If the given field cannot be accessed
     */
    public static Object getValueOf(String fieldName, Object obj) throws NoSuchFieldException, IllegalAccessException {
        try {
            Field field = obj.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return String.valueOf((field.get(obj)));
        } catch (NoSuchFieldException e) {
            System.out.println("no such field " + fieldName);
            throw e;
        }
    }

    /**
     *
     * @param meth The reference to the method
     * @param value The value to be send to the method
     * @param clazz The argument type of the given method
     * @param instance The instance to invoke the method
     * @throws IllegalAccessException If the given method cannot be invoked from this class
     * @throws InstantiationException If specified class object cannot be instantiated
     * @throws InvocationTargetException See {@link InvocationTargetException}
     * @throws NoSuchMethodException If the given method does not exist
     */
    public static void invokeSetter(Method meth, String value, Class clazz, Object instance) throws IllegalAccessException,
            InstantiationException, InvocationTargetException, NoSuchMethodException {
        if (clazz == int.class) {
            invokeInteger(instance, meth, Integer.valueOf(value));
        } else if (clazz == boolean.class) {
            invokeBoolean(instance, meth, Boolean.valueOf(value));
        } else {
            invokeString(instance, meth, value);
        }
    }

    /**
     * Create the new instance of the given class by its name and casts it
     * to the given return type
     *
     * @param clazz The string representation of the class to be created.
     * @param <T> The type of the given class to be returned
     * @return The new instance of the given class
     * @throws ReflectionException if something fails at Reflection layer
     */
    @SuppressWarnings("unchecked")
    public static <T> T newInstance(String clazz) throws ReflectionException {
        try {
            Object instance = Class.forName(clazz).newInstance();
            return (T) instance;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            throw new ReflectionException(e);
        }
    }

    /**
     * @see Method#invoke(Object, Object...)
     */
    private static void invokeString(Object target, Method meth, String value) throws InvocationTargetException, IllegalAccessException {
        meth.invoke(target, value);
    }

    /**
     * @see Method#invoke(Object, Object...)
     */
    private static void invokeInteger(Object target, Method method, int value) throws InvocationTargetException, IllegalAccessException {
        method.invoke(target, value);
    }

    /**
     * @see Method#invoke(Object, Object...)
     */
    private static void invokeBoolean(Object target, Method method, boolean value) throws InvocationTargetException, IllegalAccessException {
        method.invoke(target, value);
    }
}
