package by.epam.likeit.util;


import by.epam.likeit.bean.User;

import java.io.File;

/**
 * Class provides methods to convenient work with file paths and images.
 */
public class ImageUtils {

    public static String getName(String base, File file, String login) {
        String postfix = file.getName().substring(file.getName().lastIndexOf("."));
        return base + File.separator + login + postfix;
    }

    public static String getName(String base, User user){
        return base.replace(File.separator + "avatar", "") + user.getAvatar();
    }

    public static String getRelativePath(String address,  String login) {
        return address + login;
    }

    public static void replaceAvatar(User user, String location, String def_loc, String address, File file) {
        String path = ImageUtils.getName(address, file, user.getLogin());
        String relative = ImageUtils.getName(location, file, user.getLogin());
        if (!file.renameTo(new File(path))) //file replacement failed
            relative = def_loc;
        user.setAvatar(relative);
    }

    public static void updateAvatar(User curUser, String location, String def_loc, String address, File file, String path) {
        //if user has default avatar
        if (new File(def_loc).equals(new File(path))) {
            if (file != null) {
                replaceAvatar(curUser, location, def_loc, address, file);
            }
        } else {
            //user has custom avatar. define his avatar location and delete it.
            String oldPath = ImageUtils.getName(address, curUser);
            boolean delete = new File(oldPath).delete();
            if (file == null) {
                //load default(standard) avatar if user hasn't specified new avatar
                curUser.setAvatar(def_loc);
            } else {
                replaceAvatar(curUser, location, def_loc, address, file);
            }
        }
    }

}
