package by.epam.likeit.util;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;

/**
 * Class represents implementation of custom JSP pagination tag
 * to split a lot of records to set of pages.
 */
public class PaginationTag extends TagSupport {

    private static final long serialVersionUID = -9075233251503010842L;

    // Vars ----------------------------------------------------------------------------------------------

    /**
     * Page to be displayed
     */
    private long curPage;

    /**
     * MAX possible page to be displayed.
     * This value should be equal to the smallest (closest to negative infinity)
     * floating-point value that is greater than or equal to
     * the argument and is equal to a mathematical integer.
     * If there is no record it may be equal to 0.
     * In this case, pagination will not be displayed.
     */
    private long maxPage;

    /**
     * CSS style to display edges and {@link PaginationTag#curPage}
     */
    private String curStyle = "";

    /**
     * Contains HTML/CSS/JS to represent one unit of pagination tag. In can be button or a tag
     * containing not calculated body(for page numbers, styles, ect.)
     */
    private String item;

    /**
     * Default locale
     */
    private Locale locale = Locale.getDefault();

    /**
     * Explains if it is necessary to display "Go To" first/last page feature
     */
    private boolean showEdge;

    private int leftEdge = 2, rightEdge = 2;

    // Methods -----------------------------------------------------------------------------------------------------

    /**
     * Processes and renders pagination feature.
     */
    @Override
    public int doStartTag() throws JspException {
        if (curPage <= 0 || maxPage <= 0) return SKIP_BODY;
        if (item == null || item.isEmpty() || leftEdge <= 0 || rightEdge <= 0) throw new IllegalArgumentException();

        long start = curPage - leftEdge <= 0 ? 1 : curPage - leftEdge;
        long finish = maxPage - curPage >= rightEdge ? curPage + rightEdge : maxPage;
        JspWriter out = pageContext.getOut();

        try {

            if (showEdge) out.write(String.format(locale, item, curPage == 1 ? curStyle : "", 1, '\u00AB'));

            for (long i = start; i <= finish; i++) {
                String res = String.format(locale, item, i == curPage ? curStyle : "", i, i);
                out.write(res);
            }

            if (showEdge) out.write(String.format(locale, item, curPage == maxPage ? curStyle : "", maxPage, '\u00BB'));

        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }

        return SKIP_BODY;
    }

    public void setCurPage(long curPage) {
        this.curPage = curPage;
    }

    public void setMaxPage(long maxPage) {
        this.maxPage = maxPage;
    }

    public void setCurStyle(String curStyle) {
        this.curStyle = curStyle;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public void setLocale(String locale) {
        this.locale = new Locale(locale);
    }

    public void setShowEdge(boolean showEdge) {
        this.showEdge = showEdge;
    }

    public void setLeftEdge(int leftEdge) {
        this.leftEdge = leftEdge;
    }

    public void setRightEdge(int rightEdge) {
        this.rightEdge = rightEdge;
    }
}
