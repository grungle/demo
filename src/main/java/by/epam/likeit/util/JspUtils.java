package by.epam.likeit.util;
import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.Answer.AnswerState;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.Question.QuestionState;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.User.UserRights;
import org.apache.commons.lang3.StringEscapeUtils;

import static by.epam.likeit.bean.Question.QuestionState.CLOSED;

/**
 * Class provides implementation of JSP custom functions.
 */
public class JspUtils {

    /**
     * Checks if the given user is allowed to execute the given task.
     * @param user The user to be checked
     * @param action The right that must be checked.
     * @return true if the user is allowed to execute action, otherwise false.
     */
    public static boolean allowed(User user, String action){
        return user != null && user.getRights() != null && user.isAllowed(UserRights.valueOf(action));
    }

    /**
     * Checks if the state of the given question is closed.
     * @param question The question to be checked.
     * @return true if the given question is closed.
     */
    public static boolean isClosed(Question question){
        return question != null && question.getState() != null && question.getState() == CLOSED;
    }

    /**
     * Compares the state of the given question to the given state.
     * @param question The question to compare its state.
     * @param state The state to be compared
     * @return true if the given question has the given state, otherwise false.
     */
    public static boolean checkQuestionState(Question question, String state){
        return question != null && question.getState() == QuestionState.valueOf(state);
    }

    /**
     * Compares the state of the given answer to the given state.
     * @param answer The answer to compare its state.
     * @param state The state to be compared
     * @return true if the given answer has the given state, otherwise false.
     */
    public static boolean checkAnswerState(Answer answer, String state){
        return answer != null && answer.getState() == AnswerState.valueOf(state);
    }

    /**
     * @see StringEscapeUtils#escapeEcmaScript(String)
     */
    public static String escapeEcmaScript(String original){
        return StringEscapeUtils.escapeEcmaScript(original);
    }

}
