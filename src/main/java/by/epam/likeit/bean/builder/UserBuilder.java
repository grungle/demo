package by.epam.likeit.bean.builder;


import by.epam.likeit.bean.User;

import java.util.Arrays;
import java.util.EnumSet;

/**
 * Class implements Builder pattern to provide opportunities
 * of convenient creating of the new instances.
 */
public class UserBuilder {

    // Vars -------------------------------------------------------------------------------------------

    private User user;

    // Constructors -----------------------------------------------------------------------------------

    public UserBuilder(){
        clear();
    }

    // Methods ----------------------------------------------------------------------------------------

    /**
     * Sets all class variables to its default value
     */
    public void clear(){
        user = new User();
    }

    /**
     * Created and returns new instance of the user
     * @return created instance of the user
     */
    public User createNewUser(){
        return user;
    }

    public UserBuilder login(String login){
        user.setLogin(login);
        return this;
    }

    public UserBuilder email(String email){
        user.setEmail(email);
        return this;
    }

    public UserBuilder password(String login){
        user.setPassword(login);
        return this;
    }

    public UserBuilder fname(String fname){
        user.setFname(fname);
        return this;
    }

    public UserBuilder lname(String lname){
        user.setLname(lname);
        return this;
    }

    public UserBuilder country(String country){
        user.setCountry(country);
        return this;
    }

    public UserBuilder city(String city){
        user.setCity(city);
        return this;
    }

    public UserBuilder company(String company){
        user.setCompany(company);
        return this;
    }

    public UserBuilder position(String position){
        user.setPosition(position);
        return this;
    }

    public UserBuilder views(long views){
        user.setViews(views);
        return this;
    }

    public UserBuilder rating(long rating){
        user.setRating(rating);
        return this;
    }

    public UserBuilder questions(long questions){
        user.setQuestions(questions);
        return this;
    }

    public UserBuilder answers(long answers){
        user.setAnswers(answers);
        return this;
    }

    public UserBuilder avatar(String ava){
        user.setAvatar(ava);
        return this;
    }

    public UserBuilder rights(User.UserRights... rights){
        user.setRights(EnumSet.copyOf(Arrays.asList(rights)));
        return this;
    }

}
