/**
 * Package contains classes to ease building
 * by.epam.likeit.bean classes implementing Builder pattern.
 * @see by.epam.likeit.bean
 */
package by.epam.likeit.bean.builder;