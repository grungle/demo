package by.epam.likeit.bean.builder;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.Topic;

import java.util.ArrayList;
import java.util.List;

/**
 * Class implements Builder pattern to provide
 * convenient way for creating {@link Question}
 */
public class QuestionBuilder extends MessageBuilder {

    // Vars -----------------------------------------------------------------------------------------

    private Question.QuestionState state;
    private List<Answer> answers;
    private Long answersCount;
    private String title;
    private Long viewed;
    private Topic topic;

    // Constructors ----------------------------------------------------------------------------------

    public QuestionBuilder(){}

    // Methods ---------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    public void clear(){
        super.clear();
        answers = new ArrayList<>();
        title = null;
        state = null;
        viewed = 0L;
        answersCount = 0L;
    }

    /**
     * {@inheritDoc}
     * @return created question instance
     */
    @Override
    public Question createMessage() {
        return new Question(id, user, body, date, moderationInfo, lastUpdate, answers, title, viewed, topic, state, answersCount);
    }

    public QuestionBuilder answers(List<Answer> answers) {
        this.answers = answers;
        return this;
    }

    public QuestionBuilder title(String title) {
        this.title = title;
        return this;
    }

    public QuestionBuilder viewed(Long viewed) {
        this.viewed = viewed;
        return this;
    }

    public QuestionBuilder topic(Topic topic) {
        this.topic = topic;
        return this;
    }

    public QuestionBuilder state(Question.QuestionState state) {
        this.state = state;
        return this;
    }

    public QuestionBuilder answersCount(Long count){
        this.answersCount = count;
        return this;
    }
}
