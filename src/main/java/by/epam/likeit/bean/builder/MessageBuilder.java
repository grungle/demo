package by.epam.likeit.bean.builder;


import by.epam.likeit.bean.AbstractMessage;
import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.User;

import java.util.Date;

/**
 * Class implements Builder pattern to provide
 * convenient way for creating {@link AbstractMessage} implementation
 */
public abstract class MessageBuilder {

    // Vars -----------------------------------------------------------------------------------------

    protected Long id;
    protected User user;
    protected String body;
    protected Date date;
    protected AbstractMessage.Type type;
    protected ModerationInfo moderationInfo;
    protected Date lastUpdate;

    // Constructors ----------------------------------------------------------------------------------

    public MessageBuilder(){clear();}

    // Methods ---------------------------------------------------------------------------------------

    /**
     * Sets all class variables to its default value.
     */
    public void clear(){
        id = null;
        user = new User();
        body = null;
        type = null;
        date = null;
        moderationInfo = new ModerationInfo();
        lastUpdate = null;
    }

    /**
     * Creates message using values that are set in the class variables.
     * @return created instance of the class.
     * @see AbstractMessage
     */
    public abstract AbstractMessage createMessage();

    public MessageBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public MessageBuilder user(User user) {
        this.user = user;
        return this;
    }

    public MessageBuilder body(String body) {
        this.body = body;
        return this;
    }

    public MessageBuilder date(Date date) {
        this.date = date;
        return this;
    }

    public MessageBuilder lastUpdate(Date date) {
        this.lastUpdate = date;
        return this;
    }

    public MessageBuilder type(AbstractMessage.Type type) {
        this.type = type;
        return this;
    }

    public MessageBuilder moderatedInfo(ModerationInfo moderationInfo) {
        this.moderationInfo = moderationInfo;
        return this;
    }
}
