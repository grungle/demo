package by.epam.likeit.bean.builder;

import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.Question;

/**
 * Class implements Builder pattern to provide
 * convenient way for creating {@link Answer}
 */
public class AnswerBuilder extends MessageBuilder {

    // Constructors ----------------------------------------------------------------------------------

    private Answer.AnswerState state;
    private Question question;
    private Integer rating;

    // Constructors  ---------------------------------------------------------------------------------

    public AnswerBuilder(){
        clear();
    }

    // Methods  --------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    public void clear(){
        super.clear();
        question = new Question();
        rating = 0;
        state = null;
    }

    /**
     * {@inheritDoc}
     * @return created answer instance
     */
    @Override
    public Answer createMessage() {
        return new Answer(id, user, body, date, moderationInfo, lastUpdate, question, rating, state);
    }

    public AnswerBuilder question(Question question) {
        this.question = question;
        return this;
    }

    public AnswerBuilder rating(Integer rating) {
        this.rating = rating;
        return this;
    }

    public AnswerBuilder state(Answer.AnswerState state){
        this.state = state;
        return this;
    }
}
