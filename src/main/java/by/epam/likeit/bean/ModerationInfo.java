package by.epam.likeit.bean;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class contains extended information for moderation.
 */
public class ModerationInfo implements Serializable {

    // Constants ------------------------------------------------------------------------------------

    private static final long serialVersionUID = 4568997439575971335L;

    // Vars -----------------------------------------------------------------------------------------

    private AbstractMessage target;
    private User moderator;
    private String reason;

    // Constructor ----------------------------------------------------------------------------------

    public ModerationInfo(){
        moderator = new User();
    }

    // Getters and Setters --------------------------------------------------------------------------

    public AbstractMessage getTarget() {
        return target;
    }

    public void setTarget(AbstractMessage target) {
        this.target = target;
    }

    public User getModerator() {
        return moderator;
    }

    public void setModerator(User moderator) {
        this.moderator = moderator;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    // Object overriding ----------------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModerationInfo that = (ModerationInfo) o;
        return Objects.equals(target, that.target) &&
                Objects.equals(moderator, that.moderator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(target, moderator);
    }

    @Override
    public String toString() {
        return "ModerationInfo{" +
                "target=" + target +
                ", moderator=" + moderator +
                '}';
    }
}
