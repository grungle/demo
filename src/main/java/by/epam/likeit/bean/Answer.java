package by.epam.likeit.bean;

import java.util.Date;

/**
 * Class represent answer.
 */
public class Answer extends AbstractMessage {

    // Constants ------------------------------------------------------------------------------------

    private static final long serialVersionUID = -4486496254326193945L;

    // Vars -----------------------------------------------------------------------------------------

    private Question question;
    private Integer rating;
    private AnswerState state;

    // Constructors ---------------------------------------------------------------------------------

    public Answer(){}

    public Answer(Long id, User user, String body, Date date,
                  ModerationInfo moderationInfo, Date lastUpdate, Question question,
                  Integer rating, AnswerState state) {
        super(id, user, body, date, Type.ANSWER, moderationInfo, lastUpdate);
        this.question = question;
        this.rating = rating;
        this.state = state;

    }

    // Getters and Setters --------------------------------------------------------------------------

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public AnswerState getState() {
        return state;
    }

    public void setState(AnswerState state) {
        this.state = state;
    }

    // Object overriding ---------------------------------------------------------------------------

    @Override
    public String toString() {
        return "Answer{" +
                "rating=" + rating + ", " + super.toString() +
                '}';
    }

    // Inner classes --------------------------------------------------------------------------------

    /**
     * Represents state of the answer.
     * Answer cannot be at multiple state at the same time.
     * That's used to define available action to every group of user.
     */
    public enum AnswerState{
        PUBLICATED, MODIFIED, MODERATED
    }

}
