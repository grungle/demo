package by.epam.likeit.bean.validation;


import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.ReflectionUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import static by.epam.likeit.controller.context.RequestContext.ExecutedResult.State.INVALID;

/**
 * Class provides static method to validate data;
 */
public class Validator {

    // Constants --------------------------------------------------------------------------------------

    private static String BASE_REGEXP_NAME;
    private static String BASE_WRONG_NAME;
    private static String BASE_INVALID_NAME;

    public static final String USER_LOGIN = "user.login";
    public static final String TOPIC_NAME = "topic.name";
    public static final String QUESTION_ID = "question.id";
    public static final String TOPIC_ID = "topic.id";
    public static final String ANSWER_ID = "answer.id";
    public static final String QUESTION_HEADER = "question.header";
    public static final String QUESTION_BODY = "question.body";
    public static final String LOGIN_FIELD = "login";
    public static final String AVATAR_FIELD = "avatar";
    public static final String FNAME_FIELD = "fname";
    public static final String LNAME_FIELD = "lname";
    public static final String EMAIL_FIELD = "email";
    public static final String COUNTRY_FIELD = "country";
    public static final String CITY_FIELD = "city";
    public static final String COMPANY_FIELD = "company";
    public static final String POSITION_FIELD = "position";
    public static final String PASSWORD_FIELD = "password";
    public static final String COUNT_FIELD = "count";
    public static final String PAGE_FIELD = "page";
    public static final String ANSWER_BODY = "answer.body";

    /**
     * Set to store all possible keys to be injected in the prepared statement
     * to provide security before the SQL injections.
     */
    private static Set<String> validSQLKeys;

    // Constructors -----------------------------------------------------------------------------------

    private Validator(){}

    // Methods ----------------------------------------------------------------------------------------

    /**
     * Initializes base of all the keys available in the properties.
     * This is convenient to use. Example:
     * Base regexp name: validation.%s.regexp
     * To easy validate some class variable by regexp, use base regexp name and
     * <code>className.fieldName</code> as 'floating part'
     * So, for checking {@link by.epam.likeit.bean.User#login} use 'user.login' will be replaced with %s
     * Result key is validation.user.login.regexp
     *
     * @param baseRegexpName base key for all related to regexp keys
     * @param baseWrongName base key for all related to 'wrong' keys
     * @param baseInvalidName base key for all related to 'invalid' keys.
     *                        Values of this property contain id of the HTML
     *                        that display message to user
     * @see PropertiesLoader
     */
    public static void init(String baseRegexpName, String baseWrongName, String baseInvalidName){
        BASE_REGEXP_NAME = baseRegexpName;
        BASE_WRONG_NAME = baseWrongName;
        BASE_INVALID_NAME = baseInvalidName;
        validSQLKeys = new HashSet<>(Arrays.asList(PropertiesLoader.get("validation.sql.injection.sort.by").split(",")));
    }

    /**
     * Checks if the beans property matches regexp.
     * If field is not valid, {@link RequestContext.ExecutedResult.State} is changed to
     * {@link RequestContext.ExecutedResult.State#INVALID}
     * @param bean by.epam.likeit.bean storing value in its field
     * @param fieldName field name to be validated
     * @param exResult execution result to be changed if fieldName has wrong format.
     * @param canBeNull if current field can be null
     * @return boolean if fieldName matches pattern
     * @see PropertiesLoader
     * @see RequestContext.ExecutedResult
     * @see Validator#init(String, String, String)
     */
    public static boolean validateBeanProperty(final Object bean,
                                               final String fieldName,
                                               final RequestContext.ExecutedResult exResult,
                                               final boolean canBeNull) {
        if (bean == null) return false;
        try {
            String value = (String)ReflectionUtils.getValueOf(fieldName, bean);
            if ((value == null || value.isEmpty()) && canBeNull) return true;
            else if (value == null || value.isEmpty()) return false;

            boolean result = validateString(value, PropertiesLoader.get(String.format(BASE_REGEXP_NAME, fieldName)));
            if (!result){
                exResult.setState(INVALID);
                exResult.addErrorMessage(PropertiesLoader.get(String.format(BASE_WRONG_NAME, fieldName)));
            }
            return result;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LikeLogger.warn("can't ged access to the class variable " + fieldName + " of the " + bean.getClass(), e);
        }

        return false;
    }

    /**
     * Version of {@link Validator#validateBeanProperty(Object, String, RequestContext.ExecutedResult, boolean)}
     * optimized special for avatar checking.
     * If avatar is not valid, {@link RequestContext.ExecutedResult.State} is changed to
     * {@link RequestContext.ExecutedResult.State#INVALID}.
     * @param bean by.epam.likeit.bean containing value to be validated
     * @param fieldName field name to be validated
     * @param exResult object storing state of validation
     * @param canBeNull if value can be null
     * @return true if field is valid, otherwise false.
     */
    public static boolean validateBeanAvatar(final Object bean,
                                             final String fieldName,
                                             final RequestContext.ExecutedResult exResult,
                                             final boolean canBeNull){
        if (bean == null) return false;
        try {
            String value = (String)ReflectionUtils.getValueOf(fieldName, bean);
            if ((value == null || value.isEmpty() || "null".equals(value) ) && canBeNull) return true;
            else if (value == null || value.isEmpty() || "null".equals(value)) return false;
            boolean result = validateString(value, PropertiesLoader.get(String.format(BASE_REGEXP_NAME, fieldName)));
            if (!result){
                exResult.setState(INVALID);
                exResult.addErrorMessage(PropertiesLoader.get(String.format(BASE_WRONG_NAME, fieldName)));
            }
            return result;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Checks if long value is not null and more or equals to zero.
     * If field is not valid, {@link RequestContext.ExecutedResult.State} is changed to
     * {@link RequestContext.ExecutedResult.State#INVALID}
     * @param value value to be validated
     * @param fieldName field name to be validated
     * @param result object storing state of validation
     * @return true if field is valid, otherwise false.
     */
    public static boolean validateNotNegativeLong(final Long value,
                                                  final String fieldName,
                                                  final RequestContext.ExecutedResult result){
        if (value == null || value < 0){
            result.setState(INVALID);
            String pName = String.format(BASE_INVALID_NAME, fieldName);
            result.addErrorMessage(PropertiesLoader.get(pName));
            return false;
        }
        return true;
    }

    /**
     * If field is not valid, {@link RequestContext.ExecutedResult.State} is changed to
     * {@link RequestContext.ExecutedResult.State#INVALID}
     * @param value value to be validated
     * @param fieldName field name to be validated
     * @param result object storing state of validation
     * @return true if field is valid, otherwise false.
     */
    public static boolean validateNotNegativeNullLong(final Long value,
                                                      final String fieldName,
                                                      final RequestContext.ExecutedResult result){
        if (value != null && value < 0){
            result.setState(INVALID);
            String pName = String.format(BASE_INVALID_NAME, fieldName);
            result.addErrorMessage(PropertiesLoader.get(pName));
            return false;
        }
        return true;
    }

    /**
     * Checks if the string is not null and is not empty.
     * If field is not valid, {@link RequestContext.ExecutedResult.State} is changed to
     * {@link RequestContext.ExecutedResult.State#INVALID}
     * @param value string to be validated
     * @param propertyName value of this property will be extracted from {@link PropertiesLoader}
     * @param result to where set if the string is not valid
     * @return true if this thing is valid, otherwise false.
     */
    public static boolean validateNotEmptyString(final String value,
                                                 final String propertyName,
                                                 final RequestContext.ExecutedResult result){

        if (value == null || value.trim().isEmpty()){
            result.setState(INVALID);
            result.addErrorMessage(PropertiesLoader.get(String.format(BASE_INVALID_NAME, propertyName)));
            return false;
        }
        return true;
    }

    /**
     * Checks if string matches given pattern
     * @param str string to be validated
     * @param regexp regexp
     * @return true if string matches the pattern
     * @see Pattern
     */
    private static boolean validateString(final String str, final String regexp){
        return Pattern.compile(regexp).matcher(str).matches();
    }

    /**
     * Checks if the given value is allowed to be used in not sql-injection-safe query.
     * @param value The string to be checked
     * @param res The result where to set if the given value can be potentially an injection
     * @return true if the given value is present in properties file as safe, otherwise false
     */
    public static boolean validateSQLKey(String value, RequestContext.ExecutedResult res) {
        boolean isValid = value == null || validSQLKeys.contains(value);
        if (!isValid){
            res.setState(INVALID);
            LikeLogger.debug("Possible SQL injection: " + value);
        }
        return isValid;
    }
}
