package by.epam.likeit.bean.validation;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class provides static methods over {@link Properties}
 * to extract data by key.
 */
public class PropertiesLoader {

    public static final String VALIDATION_EMAIL_WRONG = "validation.email.wrong";
    public static final String VALIDATION_ANSWER_ID_INVALID = "validation.answer.id.invalid";
    private static Properties properties = new Properties();

    /**
     * Initializes properties using InputStream
     * @param src input stream source to properties location
     * @throws IOException If something fails at I/O level
     */
    public static void init(InputStream src) throws IOException {
        properties.load(src);
    }

    /**
     * @see Properties#getProperty(String)
     */
    public static String get(String key){
        return properties.getProperty(key);
    }

}
