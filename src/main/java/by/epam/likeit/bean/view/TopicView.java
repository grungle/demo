
package by.epam.likeit.bean.view;


import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.Topic;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Wraps {@link Topic} class.
 */
public class TopicView implements Serializable{

    // Constants -------------------------------------------------------------------------------------

    private static final long serialVersionUID = 5772701014988306921L;

    // Vars ------------------------------------------------------------------------------------------

    private Topic topic;
    private List<Question> questions;

    // Constructors  ---------------------------------------------------------------------------------

    public TopicView() {}

    // Methods ---------------------------------------------------------------------------------------

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    // Object overriding  ----------------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopicView topicView = (TopicView) o;
        return Objects.equals(topic, topicView.topic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topic);
    }
}
