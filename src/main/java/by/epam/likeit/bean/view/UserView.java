
package by.epam.likeit.bean.view;

import by.epam.likeit.bean.AbstractMessage;
import by.epam.likeit.bean.User;
import javafx.util.Pair;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Wraps {@link User} class.
 */
public class UserView implements Serializable {

    // Constants -------------------------------------------------------------------------------------

    private static final long serialVersionUID = 2885568498148668635L;

    // Vars ------------------------------------------------------------------------------------------

    private User user;
    private List<Pair<String, Double>> topSkills;
    private List<AbstractMessage> lastMessages;
    private String message;

    // Constructors ----------------------------------------------------------------------------------

    public UserView(){}

    // Getters & Setters -----------------------------------------------------------------------------

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Pair<String, Double>> getTopSkills() {
        return topSkills;
    }

    public void setTopSkills(List<Pair<String, Double>> topSkills) {
        this.topSkills = topSkills;
    }

    public List<AbstractMessage> getLastMessages() {
        return lastMessages;
    }

    public void setLastMessages(List<AbstractMessage> lastMessages) {
        this.lastMessages = lastMessages;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    // Object overriding  ----------------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserView userView = (UserView) o;
        return Objects.equals(user, userView.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user);
    }

    @Override
    public String toString() {
        return "UserView{" +
                "user=" + user +
                ", topSkills=" + topSkills +
                ", lastMessages=" + lastMessages +
                '}';
    }
}
