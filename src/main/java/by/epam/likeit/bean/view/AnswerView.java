package by.epam.likeit.bean.view;

import by.epam.likeit.bean.Answer;

import java.io.Serializable;
import java.util.Objects;

/**
 * Wraps {@link Answer} class.
 */
public class AnswerView implements Serializable{

    private static final long serialVersionUID = -6194206179856960685L;

    // Vars --------------------------------------------------------------------------------

    private Answer answer;

    /**
     * The {@linkplain by.epam.likeit.bean.Mark#mark mark} of the user who requested
     * an action that got this answer from a data source.
     */
    private String mark;

    // Constructors ------------------------------------------------------------------------

    public AnswerView(){}

    // Getters & Setters -------------------------------------------------------------------

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    // Object overriding -------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnswerView that = (AnswerView) o;
        return Objects.equals(answer, that.answer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(answer);
    }

    @Override
    public String toString() {
        return "AnswerView{" +
                "answer=" + answer +
                ", mark=" + mark +
                '}';
    }
}
