/**
 * Package contains classes that wrap another {@link by.epam.likeit.bean} classes related to it
 * and provides advanced information to be processed at JSP.
 * Comparing classes in this package to its {@link by.epam.likeit.bean} analog, classes
 * in this package can also contain some extra fields that
 * are not related(cannot be an attribute) to bean classes.
 * Classes in this package allow not to place redundant fields to by.epam.likeit.bean classes.
 *
 * @see by.epam.likeit.bean
 */
package by.epam.likeit.bean.view;