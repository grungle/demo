package by.epam.likeit.bean.view;

import by.epam.likeit.bean.Question;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Wraps {@link Question} class
 */
public class QuestionView implements Serializable {

    private static final long serialVersionUID = -7979760407034070607L;

    // Vars ----------------------------------------------------------------------------------

    private Question question;
    private List<AnswerView> answers;

    // Constructors --------------------------------------------------------------------------

    public QuestionView(){}

    // Getters & Setters ---------------------------------------------------------------------

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public List<AnswerView> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerView> answers) {
        this.answers = answers;
    }

    // Object overriding ---------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionView that = (QuestionView) o;
        return Objects.equals(question, that.question);
    }

    @Override
    public int hashCode() {
        return Objects.hash(question);
    }

    @Override
    public String toString() {
        return "QuestionView{" +
                "question=" + question +
                '}';
    }
}
