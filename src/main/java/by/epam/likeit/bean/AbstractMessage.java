package by.epam.likeit.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Class unites mutual date related to messages.
 */
public abstract class AbstractMessage implements Serializable {


    // Constants ------------------------------------------------------------------------------------

    private static final long serialVersionUID = -1196975709401634995L;

    // Vars -----------------------------------------------------------------------------------------

    private Long id;
    private User user;
    private String body;
    private Date date;
    private Type type;
    private ModerationInfo moderationInfo;
    private Date lastUpdate;

    // Constructors ----------------------------------------------------------------------------------

    public AbstractMessage(){}

    public AbstractMessage(Long id, User user, String body, Date date, Type type,
                           ModerationInfo moderationInfo, Date lastUpdate) {
        this.id = id;
        this.user = user;
        this.body = body;
        this.date = date;
        this.type = type;
        this.moderationInfo = moderationInfo;
        this.lastUpdate = lastUpdate;
    }

    // Getters and Setters ----------------------------------------------------------------------------

    public ModerationInfo getModerationInfo() {
        return moderationInfo;
    }

    public void setModerationInfo(ModerationInfo moderationInfo) {
        this.moderationInfo = moderationInfo;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    // Object overriding  -----------------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractMessage that = (AbstractMessage) o;
        return Objects.equals(id, that.id) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type);
    }

    @Override
    public String toString() {
        return "AbstractMessage{" +
                "id=" + id +
                ", body='" + body + '\'' +
                ", date=" + date +
                ", type=" + type +
                '}';
    }

    // Inner classes ----------------------------------------------------------------------------------

    /**
     * Q - if current instance is a Questions
     * A - if current instance is an Answer
     */
    public enum Type {

        QUESTION("Q"), ANSWER("A");

        private String val;

        Type (String v){
            val = v;
        }

        @Override
        public String toString() {
            return val;
        }
    }

}
