package by.epam.likeit.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class represents question
 */
public class Question extends AbstractMessage {

    // Constants ------------------------------------------------------------------------------------

    private static final long serialVersionUID = 4980775463660704837L;

    // Vars -----------------------------------------------------------------------------------------

    private List<Answer> answers;
    private String title;
    private Long viewed;
    private Topic topic;
    private Long answersCount;
    private QuestionState state;

    // Constructors --------------------------------------------------------------------------------

    public Question(){
        answers = new ArrayList<>();
    }


    public Question(Long id, User user, String body, Date date,
                    ModerationInfo moderationInfo, Date lastUpdate,
                    List<Answer> answers, String title, Long viewed, Topic topic, QuestionState state, Long answersCount) {
        super(id, user, body, date, Type.QUESTION, moderationInfo, lastUpdate);
        this.answers = answers;
        this.title = title;
        this.viewed = viewed;
        this.topic = topic;
        this.state = state;
        this.answersCount = answersCount;
    }

    // Getters and Setters -------------------------------------------------------------------------

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getViewed() {
        return viewed;
    }

    public void setViewed(Long viewed) {
        this.viewed = viewed;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public QuestionState getState() {
        return state;
    }

    public void setState(QuestionState state) {
        this.state = state;
    }

    public Long getAnswersCount() {
        return answersCount;
    }

    public void setAnswersCount(Long answersCount) {
        this.answersCount = answersCount;
    }

    // Object overriding -------------------------------------------------------------------------

    @Override
    public String toString() {
        return "Question{" +
                "answers=" + answers +
                ", title='" + title + '\'' +
                ", state=" + state +
                '}' + super.toString();
    }

    // Inner classes -----------------------------------------------------------------------------

    /**
     * Represents state of the question.
     * Question cannot be at multiple state at the same time.
     * That's used to define available action to every group of user.
     */
    public enum QuestionState{
        OPEN, MODIFIED, MODERATED, CLOSED
    }

}
