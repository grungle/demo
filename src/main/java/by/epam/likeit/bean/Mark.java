package by.epam.likeit.bean;


import java.io.Serializable;
import java.util.Objects;

/**
 * Class provides mark.
 */
public class Mark implements Serializable {

    // Constants  -----------------------------------------------------------------------------------

    public final static Integer MAX_MARK = 5;
    public final static Integer MIN_MARK = -5;

    private static final long serialVersionUID = -8092887835563415607L;

    // Vars -----------------------------------------------------------------------------------------

    private Long answerId;
    private User user;
    private Integer mark;

    // Constructors ---------------------------------------------------------------------------------

    public Mark(){}

    public Mark(Long aId, User user, int mark){
        this.answerId = aId;
        this.user = user;
        this.mark = mark;
    }

    // Getters and Setters --------------------------------------------------------------------------

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    // Object overriding ----------------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mark mark = (Mark) o;
        return Objects.equals(answerId, mark.answerId) &&
                Objects.equals(user, mark.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(answerId, user);
    }

}