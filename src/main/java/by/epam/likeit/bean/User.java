package by.epam.likeit.bean;


import java.io.Serializable;
import java.util.*;

/**
 * Class represents user.
 */
public class User implements Serializable {

    // Constants ------------------------------------------------------------------------------------

    private static final long serialVersionUID = -4526545608159721055L;

    // Vars -----------------------------------------------------------------------------------------

    private long views,
                rating,
                questions,
                answers;

    private String  fname,
                    lname,
                    login,
                    country,
                    city,
                    company,
                    position,
                    email,
                    password,
                    avatar;

    private EnumSet<UserRights> rights;

    // Constructors ---------------------------------------------------------------------------------

    public User() {}

    // Getters and Setters --------------------------------------------------------------------------

    public long getQuestions() {
        return questions;
    }

    public void setQuestions(long questions) {
        this.questions = questions;
    }

    public long getAnswers() {
        return answers;
    }

    public void setAnswers(long answers) {
        this.answers = answers;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRights(EnumSet<UserRights> rights) {
        this.rights = rights;
    }

    public void setAvatar(String ava){
        this.avatar = ava;
    }

    public long getViews() {
        return views;
    }

    public long getRating() {
        return rating;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getLogin() {
        return login;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getCompany() {
        return company;
    }

    public String getPosition() {
        return position;
    }

    public String getEmail() {
        return email;
    }

    public Set<UserRights> getRights(){return rights;}

    public boolean isAllowed(UserRights right) {
        return rights.contains(right);
    }

    public String getAvatar() {
        return avatar;
    }

    /**
     * Gets level of rights of the given user
     * @return 'the strongest' right of the current user.
     * @see User.UserRights#level
     */
    public int getRightsLevel() {
        return Collections
                .max(rights, Comparator.comparingInt(UserRights::getLevel))
                .level;
    }

    // Object overriding -------------------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

    @Override
    public String toString() {
        return "User{" +
                "views=" + views +
                ", rating=" + rating +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", login='" + login + '\'' +
                '}';
    }

    // Inner classes ---------------------------------------------------------------------------------

    /**
     * This enum presents possible set of rights. User can have any set of this rights,
     * but only one user can have <code>ADMIN</code> right. <code>ADMIN</code> cannot be deleted
     * or appointed to another user. Also <code>ADMIN</code> has all possible rights.
     * This rights may be used to allow or restrict execution of some actions,
     * but final application of them should be defined at the service layer.
     */
    public enum UserRights{

        /**
         * Having this right allows to the user read
         * questions. It does not matter whether this user or
         * another has written target question.
         */
        READ(0),

        /**
         * Having this right allows to the user watch profiles
         * of other users. If the user does not have this one,
         * he is allowed to watch his own profile.
         */
        WATCH_PROFILE(1),

        /**
         * Having this right allows to the user write answers to the
         * questions and ask new questions. Also this right may be used
         * to allow changing user's profile.
         */
        WRITE(2),

        /**
         * Having this right allows to the user moderate answers
         * and questions and edit right level of other users which
         * have lower lever of rights.
         * This right may be used to allow moderators to remove topics
         * and/or questions.
         */
        MODERATE(3),

        /**
         * Having this right allows to the user manage of other users
         * and/or moderators, deleting questions and topics, creating
         * new topics.
         */
        ADMIN(4);

        private int level;

        UserRights(int level){
            this.level = level;
        }

        /**
         * Gets absolute lever of the current right.
         * Much means stronger.
         * @return level of the current right.
         */
        public int getLevel() {
            return level;
        }

    }
}
