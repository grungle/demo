package by.epam.likeit.bean;


import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Class represents topic
 */
public class Topic implements Serializable {

    // Constants ------------------------------------------------------------------------

    private static final long serialVersionUID = 658184654964062949L;

    // Vars -----------------------------------------------------------------------------

    private Long id;
    private String name;
    private Date updated;
    private long questions;

    // Constructors ---------------------------------------------------------------------

    public Topic(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Topic(){}

    // Getters and Setters --------------------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public long getQuestions() {
        return questions;
    }

    public void setQuestions(long questions) {
        this.questions = questions;
    }

    // Object overriding -----------------------------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Topic topic = (Topic) o;
        return Objects.equals(id, topic.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
