package by.epam.likeit.listener;

import by.epam.likeit.bean.validation.PropertiesLoader;
import by.epam.likeit.bean.validation.Validator;
import by.epam.likeit.controller.action.ActionFactory;
import by.epam.likeit.controller.action.ActionStAXConfigurator;
import by.epam.likeit.dao.DAOFactory;
import by.epam.likeit.jdbc.pool.ConnectionPoolHolder;
import by.epam.likeit.jdbc.pool.PoolConfigurator;
import by.epam.likeit.jdbc.pool.PoolManager;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.JdbcUtils;
import org.xml.sax.SAXException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Implementation of {@link ServletContextListener} interface
 * for receiving notification events about ServletContext lifecycle changes.
 */
public class ServletContextListenerImpl implements ServletContextListener {

    /**
     * Receives notification that the web application initialization
     * process is starting.
     * @param sce the ServletContextEvent containing the ServletContext
     * that is being initialized
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        initLogger(sce);
        initDatabasePool(sce);
        initActions(sce);
        initValidator(sce);
        initDAOFactory(sce);
        LikeLogger.debug(ActionFactory.getAll());
    }

    /**
     * Receives notification that the web application initialization
     * process is starting.
     * Initializes DAO factory
     * @see DAOFactory
     * @param sce the ServletContextEvent containing the ServletContext
     * that is being initialized
     */
    private void initDAOFactory(ServletContextEvent sce) {
        String location = sce.getServletContext().getInitParameter("dao_sql_location");
        FileInputStream fis = null;
        try {
            URL url = this.getClass().getClassLoader().getResource(location);
            fis = new FileInputStream(new File(url.toURI()));
            DAOFactory.init(fis);
        } catch (IOException | URISyntaxException e) {
            LikeLogger.err(e.getMessage(), e);
        } finally {
            JdbcUtils.closeQuietly(fis);
        }
    }

    /**
     * Receives notification that the web application initialization
     * process is starting.
     * Initializes Logger
     * @see LikeLogger
     * @param sce the ServletContextEvent containing the ServletContext
     * that is being initialized
     */
    private void initLogger(ServletContextEvent sce) {
        String location = sce.getServletContext().getInitParameter("logger_prop_location");
        URL url = this.getClass().getClassLoader().getResource(location);
        LikeLogger.init(url);
        LikeLogger.debug("Logger initialized");

    }

    /**
     * Receives notification that the web application initialization
     * process is starting.
     *
     * Init beans validation from properties file
     * @see PropertiesLoader
     * @param sce the ServletContextEvent containing the ServletContext
     * that is being initialized
     */
    private void initValidator(ServletContextEvent sce) {
        FileInputStream fis= null;
        try {
            String location = sce.getServletContext().getInitParameter("validation_regexp_location");
            File file = new File(this.getClass().getClassLoader().getResource(location).toURI());
            fis = new FileInputStream(file);
            PropertiesLoader.init(fis);
            Validator.init(PropertiesLoader.get("base.regexp.name"), PropertiesLoader.get("base.wrong.name"), PropertiesLoader.get("base.validation.name"));
        } catch (IOException | URISyntaxException e) {
            LikeLogger.err(e.getMessage(), e);
        } finally {
            JdbcUtils.closeQuietly(fis);
        }
    }

    /**
     * Receives notification that the web application initialization
     * process is starting.
     *
     * Init actions
     *
     * @see ActionFactory
     * @see ActionStAXConfigurator
     * @see by.epam.likeit.controller.action.AbstractAction
     * @param sce the ServletContextEvent containing the ServletContext
     * that is being destroyed
     */
    private void initActions(ServletContextEvent sce) {
        FileReader reader = null;
        try {
            String schemeLoc = sce.getServletContext().getInitParameter("actionMappingSchemeLocation");
            File schemeFile = new File(this.getClass().getClassLoader().getResource(schemeLoc).toURI());

            String xmlLoc = sce.getServletContext().getInitParameter("actionMappingXmlLocation");
            File xmlFile = new File(this.getClass().getClassLoader().getResource(xmlLoc).toURI());

            reader = new FileReader(xmlFile);
            ActionStAXConfigurator config = new ActionStAXConfigurator(reader);

            if (config.validateSource(schemeFile, xmlFile))
                config.parse();

        } catch (SAXException | IOException | ParserConfigurationException | URISyntaxException | XMLStreamException e) {
            LikeLogger.err(e.getMessage(), e);
        } finally {
            JdbcUtils.closeQuietly(reader);
        }
    }

    /**
     * Receives notification that the web application initialization
     * process is starting.
     * Initialize database pool
     * @see ConnectionPoolHolder
     * @see PoolConfigurator
     * @param sce the ServletContextEvent containing the ServletContext
     * that is being destroyed
     */
    private void initDatabasePool(ServletContextEvent sce) {
        InputStream is = null;
        try {
            Properties props = new Properties();
            String propLocation = sce.getServletContext().getInitParameter("poolPropertiesLocation");
            is = sce.getServletContext().getResourceAsStream(propLocation);
            props.load(is);
            PoolConfigurator configurator = new PoolConfigurator(props);
            ConnectionPoolHolder holder = new ConnectionPoolHolder(configurator);
            PoolManager.setPoolHolder(holder);
            ThreadLocalTransactionManagerImpl.getInstance().setDataSource(PoolManager.getInstance());
        } catch (SQLException | IOException | ClassNotFoundException e) {
            LikeLogger.err(e.getMessage(), e);
        } finally {
            JdbcUtils.closeQuietly(is);
        }
    }

    /**
     * Receives notification that the ServletContext is about to be
     * shut down.
     * Destroys pool connection.
     * @see ConnectionPoolHolder
     * @param sce the ServletContextEvent containing the ServletContext
     * that is being destroyed
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        PoolManager.destroy();
        try {
            Enumeration<Driver> drivers = DriverManager.getDrivers();
            while (drivers.hasMoreElements()){
                DriverManager.deregisterDriver(drivers.nextElement());
            }
        } catch (SQLException e) {
            LikeLogger.err(e.getMessage(), e);
        }
    }
}
