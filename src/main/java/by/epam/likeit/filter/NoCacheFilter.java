package by.epam.likeit.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter prevents caching by browsers by setting special headers
 * to the <code>HttpServletResponse</code>
 * {@inheritDoc}
 */
public class NoCacheFilter implements Filter{

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {/*NOP*/}

    /**
     * Checks HTTP protocol and adds proper headers to the <code>HttpServletResponse</code>
     * to prevent caching pages by browser.
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request.getProtocol().endsWith("1.1"))
            ((HttpServletResponse)response).setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
        else
            ((HttpServletResponse)response).setHeader("Pragma", "no-cache");

        chain.doFilter(request, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {/*NOP*/}
}
