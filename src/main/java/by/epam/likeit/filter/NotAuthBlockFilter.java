package by.epam.likeit.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.likeit.controller.action.AbstractAction.USER_PARAM_NAME;

/**
 * Filter detects if client who is already logged in trying
 * to get access to page that available only for guest-users
 * (login/register) and associated actions and redirects to the
 * profile page.
 * {@inheritDoc}
 */
public class NotAuthBlockFilter implements Filter {

    /**
     * Location to redirect not including context path.
     */
    private String redirectTo;

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.redirectTo = filterConfig.getInitParameter("redirectTo");
    }

    /**
     * Detects if user is trying to get access to illegal page. If user cannon access to it,
     * checks the way <code>JSESSION</code> id was received and performs proper redirection
     * regardless client browser disabled cookies.
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession(false);

        boolean loggedIn = session != null && session.getAttribute(USER_PARAM_NAME) != null;
        if (loggedIn)
            chain.doFilter(request, response);
        else {
            if ("GET".equals(request.getMethod())) {
                if (request.isRequestedSessionIdFromURL())
                    response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + redirectTo));
                else
                    response.sendRedirect(request.getContextPath() + redirectTo);
            } else {
                if (request.isRequestedSessionIdFromURL()) {
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    response.addHeader("Location", response.encodeRedirectURL(request.getContextPath() + redirectTo));
                } else {
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    response.addHeader("Location", request.getContextPath() + redirectTo);
                }
            }

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        redirectTo = null;
    }
}
