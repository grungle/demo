package by.epam.likeit.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Filter separates access to static resources/servlets
 * by adding an extra prefix.
 * {@inheritDoc}
 */
public class ServletMappingHiderFilter implements Filter {

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {/*NOP*/}

    /**
     * Determines the target of the request. If that's request for static resources,
     * method do nothing, otherwise adds an extra prefix to its URI to go to the servlet.
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String path = req.getRequestURI().substring(req.getContextPath().length());

       if (path.startsWith("/css/") || path.startsWith("/avatar/") || path.startsWith("/js/")) {
            chain.doFilter(request, response); // Goes to default servlet.
        } else if ("/".equals(path)){
            request.getRequestDispatcher("/go/topics" ).forward(request, response); // Goes to by.epam.likeit.controller.
        }
        else {
            request.getRequestDispatcher("/go" + path).forward(request, response); // Goes to by.epam.likeit.controller.
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {/*NOP*/}
}
