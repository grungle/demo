package by.epam.likeit.filter;


import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter gets client timezone if client specified it
 * int the response and adds it in cookies.
 * Then, receiving client timezone from cookies, JSP
 * page formats all dates and time.
 * If client does not specify timezone, the server timezone is applied.
 * {@inheritDoc}
 */
public class TimeZoneFilter implements Filter {

    private static final String TIMEZONE_PARAM_NAME = "timezone";

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {/*NOP*/}

    /**
     * Sets selected timezone to the cookies.
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;

        String param = req.getParameter(TIMEZONE_PARAM_NAME);
        String offset = "GMT" + param;

        Cookie cookie = new Cookie(TIMEZONE_PARAM_NAME, offset);
        cookie.setMaxAge(-1);
        cookie.setDomain("localhost");
        cookie.setPath(req.getServletContext().getContextPath());

        resp.addCookie(cookie);

        resp.setStatus(HttpServletResponse.SC_OK);
//        chain.doFilter(req, resp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {/*NOP*/}
}
