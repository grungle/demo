package by.epam.likeit.filter;

import by.epam.likeit.logger.LikeLogger;

import javax.servlet.*;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This filter implements {@link Filter} interface and stores
 * count of last requests splitted to the specified count of intervals
 * Each interval has the same length.
 * Filter provides statistics about count of requests of the each interval.
 */
public class StatsFilter implements Filter {

    /**
     * Deque of the last intervals.
     */
    private static ConcurrentLinkedDeque<Long> copy;

    /**
     * Service that runs thread to remove last interval and insert <code>current</code>
     * to the <code>copy</code>
     */
    private ScheduledExecutorService service;

    /**
     * Count of the requests at the current interval.
     */
    private AtomicLong current;

    /**
     * Calculates milliseconds to next interval.
     * This method is required as the <code>init</code> method may be ran
     * in the middle of the first interval, and to split next intervals
     * related to their real time.
     * @param intervalLength The length of an interval (msec)
     * @return The time(msec) to start of the next interval.
     */
    private static long getDelayToNextInterval(long intervalLength) {
        return intervalLength - (System.currentTimeMillis() % intervalLength);
    }

    /**
     * {@inheritDoc}
     */
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        current.incrementAndGet();
        chain.doFilter(req, res);
    }

    /**
     * Gets snapshot statistics about last requests count splitted by intervals.
     * NOTE! Current interval is not included.
     * @return The statistics about last requests count.
     */
    public static long[] lastStatistics(){
        long [] arr = new long[copy.size()];
        int i = 0;
        for (Long val : copy)
            arr[i++] = val;
        return arr;
    }

    /**
     * Reads the init parameters and creates <code>copy</code>
     * with the given interval counts, runs service thread.
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig config) throws ServletException {
        current = new AtomicLong();
        copy = new ConcurrentLinkedDeque<>();
        final long storingIntervals = Integer.valueOf(config.getInitParameter("storingIntervals"));
        final long intervalLength = Integer.valueOf(config.getInitParameter("intervalLength"));

        service = Executors.newScheduledThreadPool(1);
        service.scheduleWithFixedDelay(() -> {
                    LikeLogger.trace(copy);
                    if (copy.size() == storingIntervals)
                        copy.removeLast();
                    copy.addFirst(current.getAndSet(0L));
                },
                getDelayToNextInterval(intervalLength), intervalLength, TimeUnit.MILLISECONDS);
    }

    /**
     * Shuts down the service thread and clears last history of request count.
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        copy.clear();
        service.shutdown();
    }

}

