package by.epam.likeit.filter;


import by.epam.likeit.bean.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.epam.likeit.controller.action.AbstractAction.USER_PARAM_NAME;

/**
 * Filter to redirect user if he is not allowed to
 * get access some secure page or action.
 * {@inheritDoc}
 */
public class AuthBlockFilter implements Filter {

    private static String redirectUrl;

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        redirectUrl = filterConfig.getInitParameter("redirectTo");
    }

    /**
     * Defines if the given client is allowed to get access to
     * the given page or perform action.
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();

        boolean loggedIn = session != null && session.getAttribute(USER_PARAM_NAME) != null;

        if (!loggedIn)
            chain.doFilter(request, response);
        else {
            blockUser(resp, req, session);
        }
    }


    /**
     * Blocks and sets necessary HTTP headers to redirect user
     * who tried to get access to illegal page.
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    private void blockUser(HttpServletResponse resp, HttpServletRequest req, HttpSession session) throws IOException {
        User user = (User)session.getAttribute(USER_PARAM_NAME);

        if ("GET".equals(req.getMethod())){
            if (req.isRequestedSessionIdFromURL())
                resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + redirectUrl + user.getLogin()));
            else
                resp.sendRedirect(req.getContextPath() + redirectUrl + user.getLogin());
        } else {
            if (req.isRequestedSessionIdFromURL()){
                resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
                resp.addHeader("Location", resp.encodeRedirectURL(req.getContextPath() + redirectUrl + user.getLogin()));
            } else {
                resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
                resp.addHeader("Location", req.getContextPath() + redirectUrl + user.getLogin());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {/*NOP*/}
}
