package by.epam.likeit.filter;

import by.epam.likeit.controller.context.HttpThreadLocalContext;
import by.epam.likeit.controller.context.RequestContext;
import by.epam.likeit.logger.LikeLogger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Provides an extra layer of abstraction over
 * Servlet API to make its further using easier by
 * implementing Facade design pattern.
 * @see by.epam.likeit.controller.context.RequestContext
 * {@inheritDoc}
 */
public class ContextFilter implements Filter {

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig config) throws ServletException {/*NOP*/}

    /**
     * Wraps HttpServletResponse and HttpServletRequest using
     * {@link by.epam.likeit.controller.context.RequestContext} implementation.
     * Creates and closes ThreadLocal based implementation of RequestContext
     * @see HttpThreadLocalContext
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        try (RequestContext context = HttpThreadLocalContext.create(req, resp)){
            chain.doFilter(req, resp);
        } catch (Exception ex){
            LikeLogger.warn("Can't close request context", ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {/* NOP */ }

}
