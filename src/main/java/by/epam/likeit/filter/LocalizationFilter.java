package by.epam.likeit.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Locale;

/**
 * Filter to configuration users locale.
 * {@inheritDoc}
 */
public class LocalizationFilter implements Filter {

    // Constants ---------------------------------------------------------------------------------

    private static final String LANGUAGE_ATTR_NAME = "lang";
    private static final String COUNTRY_ATTR_NAME = "country";
    private static Locale defaultLocale;

    // Filter implementation ----------------------------------------------------------------------

    /**
     * Sets default locale
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        defaultLocale = new Locale(filterConfig.getInitParameter(LANGUAGE_ATTR_NAME),
                filterConfig.getInitParameter(COUNTRY_ATTR_NAME));
        Locale.setDefault(defaultLocale);
    }

    /**
     * Gets locale that was sent by client within request.
     * If client did not send locale, checks locale in session.
     * If session associated to the current client does not exist, sets default locale.
     * Else if the session exist, method does nothing.
     * If client sends locale within request, method updates current session locale.
     * If locale is incorrect or does not supported, it is switched to default.
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest)request;
        Locale locale = defaultLocale;

        if (req.getParameter(LANGUAGE_ATTR_NAME) == null) {
            if (req.getSession().getAttribute(LANGUAGE_ATTR_NAME) == null) {
                req.getSession().setAttribute(LANGUAGE_ATTR_NAME, locale.getLanguage());
            }
        } else {
            locale = new Locale(request.getParameter(LANGUAGE_ATTR_NAME));
            req.getSession().setAttribute(LANGUAGE_ATTR_NAME, locale.getLanguage());
        }

        response.setLocale(locale);
        chain.doFilter(request, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {/*NOP*/}
}
