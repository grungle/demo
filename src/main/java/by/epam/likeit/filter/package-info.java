/**
 * Package contains classes that implement
 * {@link javax.servlet.Filter} interface
 * and performs some actions over servlet/response
 * to make its further using easier.
 */
package by.epam.likeit.filter;