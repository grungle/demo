package by.epam.likeit.filter;


import javax.servlet.*;
import java.io.IOException;

/**
 * Filter checks and sets default encoding to the
 * response associated to the given request, if the
 * client browser does not sent preferred encoding.
 * {@inheritDoc}
 */
public class EncodingFilter implements Filter{

    private static final String DEFAULT_ENCODING_ATTR_NAME = "defaultEncoding";
    private String defaultEncoding;

    /**
     * Sets default encoding using web.xml
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        defaultEncoding = filterConfig.getInitParameter(DEFAULT_ENCODING_ATTR_NAME);
    }

    /**
     * Checks if browser agent sent preferred encoding. If this does not
     * equal to <em>null</em>, sets this encoding to the response
     * associated to the given request, otherwise sets default encoding.
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        if (request.getCharacterEncoding() == null) {
            response.setCharacterEncoding(defaultEncoding);
            request.setCharacterEncoding(defaultEncoding);
        } else
            response.setCharacterEncoding(request.getCharacterEncoding());
        chain.doFilter(request, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {/*NOP*/}

}
