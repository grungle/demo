/**
 * Package contains classes to organize transactions management
 * and classes that provide thread-safe connection pool.
 * @see by.epam.likeit.jdbc.transaction.TransactionManager
 * @see by.epam.likeit.jdbc.pool.PoolManager
 * @see by.epam.likeit.jdbc.pool.PoolConfigurator
 */
package by.epam.likeit.jdbc;