package by.epam.likeit.jdbc.pool;

import com.mysql.jdbc.JDBC4Connection;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Class implement the Proxy design pattern over
 * {@link JDBC4Connection} to override its close method
 * by intercepting the {@link Connection#close()} method
 * and instead of closing the real one connection, return back
 * it to the related connection pool.
 */
public class ConnectionProxy extends JDBC4Connection {

    /**
     * Pool that are associated(that contains) to this connection.
     */
    private ConnectionPoolHolder pool;

    /**
     * {@inheritDoc}
     */
    ConnectionProxy(String hostToConnectTo, int portToConnectTo,
                    Properties info, String databaseToConnectTo, String url, ConnectionPoolHolder pool) throws SQLException {
        super(hostToConnectTo, portToConnectTo, info, databaseToConnectTo, url);
        this.pool = pool;
    }

    /**
     * Returns connection to the {@linkplain ConnectionProxy#pool associated pool}
     * @throws SQLException see {@link ConnectionPoolHolder#returnConnection(ConnectionProxy)}
     */
    @Override
    public void close() throws SQLException {
        pool.returnConnection(this);
    }

    /**
     * Closes the real connection.
     * {@inheritDoc}
     */
    void closeRealConnection() throws SQLException{
        super.close();
    }

}