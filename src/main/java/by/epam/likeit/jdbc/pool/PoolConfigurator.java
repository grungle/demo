package by.epam.likeit.jdbc.pool;

import by.epam.likeit.logger.LikeLogger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static by.epam.likeit.util.ReflectionUtils.getSetterName;
import static by.epam.likeit.util.ReflectionUtils.invokeSetter;

/**
 * Class contains properties to configure connection pool
 * and connection within it by reading them from properties
 * and setting to the class variable via reflection API.
 */
public class PoolConfigurator {

    // Driver setting vars --------------------------------------------------------------------------------------

    private String url;
    private String user;
    private String password;
    private String driver;
    private Properties connectionProp;
    private String hostName;
    private String databaseName;
    private int port;

    // Connection pool setting vars -----------------------------------------------------------------------------

    private int maxPoolSize = 12;
    private int minPoolSize = 5;
    private int initialPoolSize = 5;
    private int acquireIncrement = 2;
    private int maxWaitingTimeout = 500; //msec
    private int connectionIdleDelay = 20; //seconds

    private boolean autoCommit = false;
    private boolean autoCommitOnClose = false;
    private int holdability = ResultSet.CLOSE_CURSORS_AT_COMMIT;
    private int isolationLevel = Connection.TRANSACTION_REPEATABLE_READ;
    private boolean readOnly = false;
    private Map<String, Class<?>> typeMap;

    // Helper vars ----------------------------------------------------------------------------------------------

    /**
     * Map to define the type of the class variable by its name to invoke appropriate
     * setter via reflection API.
     */
    private static final Map<String, Class> paramInfo = new HashMap<String, Class>() {
        {
            put("user", String.class);
            put("password", String.class);
            put("url", String.class);
            put("driver", String.class);
            put("autoCommit", boolean.class);
            put("autoCommitOnClose", boolean.class);
            put("initialPoolSize", int.class);
            put("acquireIncrement", int.class);
            put("maxWaitingTimeout", int.class);
            put("holdability", int.class);
            put("isolationLevel", int.class);
            put("minPoolSize", int.class);
            put("maxPoolSize", int.class);
            put("connectionIdleDelay", int.class);
            put("readOnly", boolean.class);
            put("catalog", String.class);
            put("typeMap", Map.class);
            put("hostName", String.class);
            put("port", int.class);
            put("databaseName", String.class);
        }
    };

    // Constructors ---------------------------------------------------------------------------------------------

    /**
     *  Creates and configurates the connection pool using the given properties.
     * @param props the properties that contains configurations.
     * @throws ClassNotFoundException If the driver of the connection does not found
     */
    public PoolConfigurator(Properties props) throws ClassNotFoundException {
        configure(props);
    }

    // Methods --------------------------------------------------------------------------------------------------

    /**
     * Sets settings of the connection
     * to its default value using class variables.
     * @param connection The connection to be reset to default settings.
     * @throws SQLException If something fails at SQL level
     */
    void configureConnection(Connection connection) throws SQLException {
        connection.setAutoCommit(autoCommit);
        connection.setTransactionIsolation(isolationLevel);
        connection.setHoldability(holdability);
        connection.setTypeMap(typeMap);
        connection.setReadOnly(readOnly);
    }

    /**
     * Configurates new connection pool using the given properties.
     * @param props the properties that contain configuration parameters
     * @throws ClassNotFoundException If the driver of the connection does not found
     */
    private void configure(Properties props) throws ClassNotFoundException {

        Set<String> params = props.stringPropertyNames();
        for (String pName : params) {
            try {
                Class clazz = paramInfo.get(pName);
                Method method = PoolConfigurator.class.getDeclaredMethod(getSetterName(pName), clazz);
                invokeSetter(method, props.getProperty(pName), getClassInstance(pName), this);
            } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
                LikeLogger.err("Cannot initialize connection pool", e);
            }
        }
        Class.forName(driver);
        initProperties();
    }

    /**
     * Sets login and password inside the {@link this#connectionProp}
     * to be used to acquiring new connections
     */
    private void initProperties() {
        connectionProp = new Properties();
        connectionProp.put("user", user);
        connectionProp.put("password", password);
    }

    /**
     * Gets type of the given class variable name
     * @param pName the name of the class variable
     * @return the type of the given class variable
     * @see this#paramInfo
     */
    private static Class getClassInstance(String pName) {
        return paramInfo.get(pName);
    }

    // Getters & Setters -----------------------------------------------------------------------------------

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        if (maxPoolSize <= 0 || maxPoolSize < minPoolSize)
            throw new PoolConfigurationException("Illegal max pool size: " + maxPoolSize);
        this.maxPoolSize = maxPoolSize;
    }

    int getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(int minPoolSize) {
        if (minPoolSize <= 0 || minPoolSize > maxPoolSize)
            throw new PoolConfigurationException("Illegal min pool size: " + minPoolSize);
        this.minPoolSize = minPoolSize;
    }

    int getInitialPoolSize() {
        return initialPoolSize;
    }

    public void setInitialPoolSize(int initialPoolSize) {
        if (initialPoolSize <= 0 || initialPoolSize > maxPoolSize)
            throw new PoolConfigurationException("Illegal initial pool size: " + initialPoolSize);
        this.initialPoolSize = initialPoolSize;
    }

    public int getAcquireIncrement() {
        return acquireIncrement;
    }

    public void setAcquireIncrement(int acquireIncrement) {
        if (acquireIncrement <= 0 || acquireIncrement > maxPoolSize)
            throw new PoolConfigurationException("Illegal acquire increment: " + acquireIncrement);
        this.acquireIncrement = acquireIncrement;
    }

    int getMaxWaitingTimeout() {
        return maxWaitingTimeout;
    }

    public void setMaxWaitingTimeout(int maxWaitingTimeout) {
        if (maxWaitingTimeout <= 0)
            throw new PoolConfigurationException("Illegal max waiting timeout: " + maxWaitingTimeout);
        this.maxWaitingTimeout = maxWaitingTimeout;
    }

    int getConnectionIdleDelay() {
        return connectionIdleDelay;
    }

    public void setConnectionIdleDelay(int connectionIdleDelay) {
        if (maxPoolSize <= 0) throw new PoolConfigurationException("Illegal connection idle delay: " + connectionIdleDelay);
        this.connectionIdleDelay = connectionIdleDelay;
    }

    public boolean getAutoCommit() {
        return autoCommit;
    }

    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    boolean getAutoCommitOnClose() {
        return autoCommitOnClose;
    }

    public void setAutoCommitOnClose(boolean autoCommitOnClose) {
        this.autoCommitOnClose = autoCommitOnClose;
    }

    public int getHoldability() {
        return holdability;
    }

    public void setHoldability(int holdability) {
        this.holdability = holdability;
    }

    public int getIsolationLevel() {
        return isolationLevel;
    }

    public void setIsolationLevel(int isolationLevel) {
        this.isolationLevel = isolationLevel;
    }

    public boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Map<String, Class<?>> getTypeMap() {
        return typeMap;
    }

    public void setTypeMap(Map<String, Class<?>> typeMap) {
        this.typeMap = typeMap;
    }

    Properties getConnectionProp() {
        return connectionProp;
    }

    String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
