package by.epam.likeit.jdbc.pool;

/**
 * This exception is thrown if the {@linkplain PoolConfigurator configurator}
 * cannot set some property. For example, if a property value is invalid, it
 * cannot be set to associated class variable.
 * @see PoolConfigurator
 */
public class PoolConfigurationException extends IllegalArgumentException {

    private static final long serialVersionUID = 1933431565388692679L;

    /**
     * {@inheritDoc}
     */
    public PoolConfigurationException(String s) {
        super(s);
    }

}
