/**
 * Package contains classes that read the configuration
 * to be applied to the connection pool and connection
 * that are using in the pool.
 * Also there is a class that provides API to get access to the
 * connection pool.
 * @see by.epam.likeit.jdbc.pool.PoolManager
 * @see by.epam.likeit.jdbc.pool.PoolConfigurator
 * @see by.epam.likeit.jdbc.pool.ConnectionPoolHolder
 */
package by.epam.likeit.jdbc.pool;