package by.epam.likeit.jdbc.pool;


import by.epam.likeit.jdbc.transaction.BaseDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * This class provides API to use with {@linkplain ConnectionPoolHolder connection pool}
 * by implementing singleton design pattern (on demand holder).
 */
public class PoolManager extends BaseDataSource {

    /**
     * Associated connection pool
     */
    private static ConnectionPoolHolder pool;

    // Constructors ----------------------------------------------------------------

    private PoolManager(){}

    // Methods ---------------------------------------------------------------------

    public static void setPoolHolder(ConnectionPoolHolder holder){
        pool = holder;
    }

    public static void destroy(){
        pool.destroyPool();
    }

    @Override
    public Connection getConnection() throws SQLException {
        return pool.getConnection();
    }

    public static PoolManager getInstance(){
        return Holder.MANAGER;
    }

    // Inner classes ----------------------------------------------------------------

    /**
     * Class for holding the instance of the outer class to implement
     * singleton design pattern (on demand holder idiom)
     */
    public static class Holder{
        static final PoolManager MANAGER = new PoolManager();
    }

}
