package by.epam.likeit.jdbc.pool;

/**
 * This exception is thrown if the connection cannot be obtained
 * in he maximal waiting timeout.
 * @see PoolConfigurator#maxWaitingTimeout
 * @see ConnectionPoolHolder#getConnection()
 */
class TimeoutException extends RuntimeException{

    private static final long serialVersionUID = -1090714439756851566L;

    TimeoutException(String message){
        super(message);
    }


}
