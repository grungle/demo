package by.epam.likeit.jdbc.pool;

import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.JdbcUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class contains set of initialized connections to be repeatable used
 * multiple times (connection pool) and provides necessary functionality to use that.
 * To get connection from the pool, use {@link PoolManager} as {@link this#getConnection()}
 * has package-private access modifier.
 * @see PoolConfigurator
 * @see PoolManager
 */
@SuppressWarnings("deprecation")
public class ConnectionPoolHolder {

    // Vars -----------------------------------------------------------------------------------------------

    private Thread creatorConThr;
    private ScheduledExecutorService killerService;
    private final BlockingQueue<ConnectionProxy> freePool;
    private final BlockingQueue<ConnectionProxy> workingPool;
    private final ReentrantLock lock;
    private final Condition conProducerCondition;
    private final Condition conConsumerCondition;
    private final PoolConfigurator config;

    // Constructors ----------------------------------------------------------------------------------------

    /**
     * Creates a new pool holder with the given configuration.
     * @param configurator The configuration object to configure pool
     * @throws SQLException If something fails at SQL layer
     */
    public ConnectionPoolHolder(PoolConfigurator configurator) throws SQLException {
        lock = new ReentrantLock(true);
        config = configurator;
        freePool = new LinkedBlockingDeque<>(config.getMaxPoolSize());
        workingPool = new LinkedBlockingDeque<>(config.getMaxPoolSize());
        conProducerCondition = lock.newCondition();
        conConsumerCondition = lock.newCondition();
        killerService = Executors.newScheduledThreadPool(1);
        createConnections(config.getInitialPoolSize());
        initServiceThreads();
    }

    // Helpers ---------------------------------------------------------------------------------------------

    /**
     * Initializes service threads: a thread that creates extra connections if the current connections
     * count does not enough to fulfil requests, and the thread that kills idle connections.
     * @see ConnectionCreatorThread
     * @see PoolConfigurator
     */
    private void initServiceThreads() {
        Thread.UncaughtExceptionHandler handler = (t, e) -> LikeLogger.err(e.getMessage(), e);

        creatorConThr = new Thread(new ConnectionCreatorThread());
        creatorConThr.setDaemon(true);
        creatorConThr.setUncaughtExceptionHandler(handler);
        creatorConThr.setName("AsyncCreatorThread-0");
        creatorConThr.start();

        killerService.scheduleWithFixedDelay(() -> {
            lock.lock();
            try{
                while (freePool.size() > config.getMinPoolSize()) {
                    ConnectionProxy con = freePool.poll();
                    LikeLogger.trace("Killer is going to kill connection " + con);
                    closeRealConnection(con);
                }
            } finally {
                lock.unlock();
            }
        }, config.getConnectionIdleDelay(), config.getConnectionIdleDelay(), TimeUnit.SECONDS);
    }

    /**
     * Gets the count of the all not closed connections.
     * @return the sum of <code>freePool.size() + workingPool.size()</code>
     */
    private int getActiveConnectionCount() {
        return freePool.size() + workingPool.size();
    }

    // Methods -----------------------------------------------------------------------------------------

    /**
     * Creates the given count of connections that are configured
     * using {@link this#config}
     * @param count the count of connections to be created
     * @throws SQLException If something fails at SQL layer
     */
    private void createConnections(int count) throws  SQLException{
        if (count > (config.getMaxPoolSize() - getActiveConnectionCount()) ||
                count < 0) throw new IllegalArgumentException("Can't acquire " + count + " connections");

        for (int i = 0; i < count; i++){
            ConnectionProxy con = new ConnectionProxy(config.getHostName(), config.getPort(),
                    config.getConnectionProp(), config.getDatabaseName(), config.getUrl() ,this);
            config.configureConnection(con);
            freePool.add(con);
        }
    }

    /**
     * Attempts to get a connection from the pool.
     * If there is no free connection, the {@link this#waitFreeConnection()}
     * is invoked.
     * Secondly, if connection is successfully obtained, it is returned, if not,
     * a {@link TimeoutException} is thrown.
     * @return the connection from pool
     * @throws SQLException If something fails at SQL level
     * @throws TimeoutException If current thread was interrupted or
     * cannot acquire connection from pool
     */
    Connection getConnection() throws SQLException {
        ConnectionProxy con;

        try {
            if (!lock.tryLock(config.getMaxWaitingTimeout(), TimeUnit.MILLISECONDS))
                throw new TimeoutException("timeout has expired while acquiring a lock");

            con = freePool.isEmpty() ? waitFreeConnection() : freePool.poll();

            if (con == null) throw new TimeoutException("timeout has expired while waiting a connection.");
            workingPool.add(con);
            return con;

        } catch (InterruptedException | TimeoutException e){
            LikeLogger.warn(e.getMessage(), e);
            throw new TimeoutException(e.getMessage());
        }finally {
            lock.unlock();
        }
    }

    /**
     * Sends a signal to the {@link this#creatorConThr} to produce extra connections if it
     * is possible and wait time that configured in {@link PoolConfigurator#maxWaitingTimeout}
     * and returns the new connection or null if creating more connections is not allowed.
     * @return the clear configured connection from the pool or null, if there is no free connection.
     * @throws InterruptedException see {@link Thread#interrupt()}
     */
    private ConnectionProxy waitFreeConnection() throws InterruptedException {
        ConnectionProxy con;
        try {
            conProducerCondition.signal();
            // can't delegate this work to queue poll(int, TU) because of we must free lock for ConnectionCreatorThread
            long deadline = System.currentTimeMillis() + config.getMaxWaitingTimeout();
            Date deadLineDate = new Date(deadline);

            while(System.currentTimeMillis() <= deadline)
                conConsumerCondition.awaitUntil(deadLineDate);

            con = freePool.poll();
        } catch (InterruptedException e) { throw new InterruptedException(e.getMessage());}

        return con;
    }

    /**
     * Atomically loses all connections and shutdowns all service threads.
     */
    void destroyPool(){
        lock.lock();
        try {
            killerService.shutdown();
            creatorConThr.interrupt();

            ConnectionProxy con;
            while ((con = workingPool.poll()) != null)
                closeRealConnection(con);

            while ((con = freePool.poll()) != null)
                closeRealConnection(con);

        } finally {
            lock.unlock();
        }
    }

    /**
     * Atomically returns connection to the connection pool and
     * tries to backup connection configuration if the
     * connection setting was changed after it using.
     *
     * Connection is to be returned into the pool of free
     * connections by replacing it from the {@link ConnectionPoolHolder#workingPool}
     * into the {@link ConnectionPoolHolder#freePool}
     *
     * @param connection the connection to be returned into the pool
     * @throws SQLException @see {@link PoolConfigurator#configureConnection(Connection)}
     * @see PoolConfigurator#configureConnection(Connection)
     */
    void returnConnection(ConnectionProxy connection) throws SQLException {
        lock.lock();
        try {
            if (workingPool.remove(connection)) {
                if (config.getAutoCommitOnClose())
                    connection.commit();
                config.configureConnection(connection);
                freePool.add(connection);
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Closes the real connection by sending the real close method
     * to the helper static method.
     * @param connection The connection to be closed.
     */
    private void closeRealConnection(ConnectionProxy connection) {
        JdbcUtils.closeQuietly(connection::closeRealConnection);
    }

    // Inner classes ---------------------------------------------------------------------------------------

    /**
     * Class implement <code>Runnable</code> interface to produce
     * some extra connection on request if there is no free connections in the pool.
     * Count of creating connections depends on {@link PoolConfigurator#acquireIncrement}
     * class variable and the maximal count of connection is {@link PoolConfigurator#maxPoolSize}
     */
    private class ConnectionCreatorThread implements Runnable {

         private ConnectionCreatorThread() {}

         @Override
         public void run() {
             try {
                 lock.lock();
                 while (true) {
                     conProducerCondition.await();
                     int conCount = config.getAcquireIncrement() > (config.getMaxPoolSize() - getActiveConnectionCount()) ?
                             config.getMaxPoolSize() - getActiveConnectionCount() : config.getAcquireIncrement();
                     createConnections(conCount);
                     conConsumerCondition.signalAll();
                 }
             } catch (InterruptedException | SQLException e) {
                 LikeLogger.err(e.getMessage(), e);
             } finally {
                 lock.unlock();
             }
         }
     }

}




