package by.epam.likeit.jdbc.transaction;

/**
 * Interface provides one single method implementing strategy design pattern
 * by which necessary unit of work can be wrapped inside the lambda-expression
 * or anonymous class and to be handled in the appropriate logic.
 * @param <T> The type of the result to be returned after
 *           successful execution of the current <code>UnitOfWork</code>
 *           implementation.
 */
public interface UnitOfWork<T> {

    /**
     *
     * @return the result of the execution.
     * @throws Exception If something while executing the given unit of work.
     */
    T call() throws Exception;

}
