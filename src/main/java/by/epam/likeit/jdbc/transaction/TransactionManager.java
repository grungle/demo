package by.epam.likeit.jdbc.transaction;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * TransactionManager provides common interface to perform task within
 * one transaction or without transaction. Also interface provides contracts
 * to its subclasses to implement DataSource role for obtaining connections
 * from DAOFactory and any other classes.
 *
 * @see by.epam.likeit.dao.DAOFactory
 */
public interface TransactionManager {

    /**
     * Sets {@link java.sql.Connection#setAutoCommit(boolean)}
     * of the current connection to false and sets some addition
     * configuration to organize execution of the given
     * <code>UnitOfWork</code> in one transaction.
     *
     * NOTE!!!Before start of the execution of the given <code>UnitOfWork</code>
     * the current connection extracted from the {@link TransactionManager#setDataSource(DataSource)}
     * is to be set and must be ready to be used using {@link TransactionManager#getConnection()}.
     * DataSource must be also ready to be used.
     *
     *
     * If something fails while transaction is executing, subclasses do rollback
     * of the current transaction and close the current connection.
     *
     * If the given <code>UnitOfWork</code> was successful executed,
     * subclasses commit changes and the result of the <code>UnitOfWork</code> is returned.
     *
     * @param unitOfWork The unit of work to be executed in the transaction.
     * @param <T> The type of the given unit of work to be returned
     * @return result of the execution of the given <code>UnitOfWork</code>
     * @throws Exception If transaction execution threw an exception or
     * connection cannot be configured to execute the given task
     * in the transaction or does not already set.
     * @see UnitOfWork
     * @see Connection#rollback()
     * @see Connection#commit()
     */
    <T> T doInTransaction(UnitOfWork<T> unitOfWork) throws Exception;

    /**
     * Executes the given <code>UnitOfWork</code> with no extra preparation and
     * configuration of the edges of the transaction. It means if method that invoke
     * this one, must provide transaction management by itself.
     *
     * NOTE!!!
     * Implementation of this method have not to invoke <code>commit</code> or
     * <code>rollback</code> methods regardless its status of execution.
     *
     * If something fails while execution of the given <code>UnitOfWork</code>
     * method does not do a rollback and does not close the current connection.
     *
     * @param unitOfWork The unit of work to be executed
     * @param <T> The type of the given unit of work to be returned
     * @return result of the execution of the given <code>UnitOfWork</code>
     * @throws Exception IF something fails while executing the given <code>UnitOfWork</code>
     * of current connection/datasource does not ready to be used
     */
    <T> T doWithoutTransaction(UnitOfWork<T> unitOfWork) throws Exception;

    /**
     * Sets the given <code>DataSource</code> to the current
     * <code>TransactionManager</code> implementation
     * @param ds the given DataSource to be set.
     */
    void setDataSource(DataSource ds);


    /**
     * @see DataSource#getConnection()
     */
    Connection getConnection() throws SQLException;

}
