package by.epam.likeit.jdbc.transaction;

import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.JdbcUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * ThreadLocal based implementation of the <code>TransactionManager</code>.
 * Also this class is a <code>DataSource</code>.
 * {@linkplain by.epam.likeit.dao.DAOFactory DAO Factory} or any other class can get access
 * to this class via {@link Holder} (Singleton "on demand holder")
 * {@inheritDoc}
 * @see Holder
 */
public class ThreadLocalTransactionManagerImpl extends BaseDataSource implements TransactionManager {

    // Vars -------------------------------------------------------------------------------

    private ThreadLocal<Connection> holder = new ThreadLocal<>();
    private DataSource dataSource;

    // Constructors -----------------------------------------------------------------------

    private ThreadLocalTransactionManagerImpl(){}

    // Methods ----------------------------------------------------------------------------

    /**
     * Makes the connection obtained from the current <code>DataSet</code> available to
     * any method in the current thread removes it after finishing.
     * {@inheritDoc}
     */
    @Override
    public <T> T doInTransaction(UnitOfWork<T> unitOfWork) throws Exception {
        Connection con = null;
        try{
            con = dataSource.getConnection();
            holder.set(con);
            con.setAutoCommit(false);
            T result = unitOfWork.call();
            con.commit();
            return result;
        } catch (Exception ex){
            LikeLogger.info(ex.getMessage(), ex);
            JdbcUtils.rollBackQuietly(con);
            throw ex;
        } finally {
            holder.remove();
            JdbcUtils.closeQuietly(con);
        }
    }

    /**
     * Makes the connection obtained from the current <code>DataSet</code> available to
     * any method in the current thread removes it after finishing.
     * {@inheritDoc}
     */
    @Override
    public <T> T doWithoutTransaction(UnitOfWork<T> unitOfWork) throws Exception {
        Connection con;
        try{
            con = dataSource.getConnection();
            holder.set(con);
            return unitOfWork.call();
        } catch (Exception ex){
            LikeLogger.info(ex.getMessage(), ex);
            throw ex;
        } finally {
            holder.remove();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDataSource(DataSource ds){
        dataSource = ds;
    }

    // DataSource implementation ------------------------------------------------------------

    @Override
    public Connection getConnection() throws SQLException {
        return holder.get();
    }

    // Inner classes ------------------------------------------------------------------------

    /**
     * Helper class to implement on demand holder singleton
     */
    public static class Holder{
        static final ThreadLocalTransactionManagerImpl INSTANCE = new ThreadLocalTransactionManagerImpl();
    }

    public static ThreadLocalTransactionManagerImpl getInstance(){
        return Holder.INSTANCE;
    }
}

