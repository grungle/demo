package by.epam.likeit.dao;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.AnswerBuilder;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.bean.view.AnswerView;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static by.epam.likeit.bean.Answer.AnswerState.*;
import static by.epam.likeit.util.JdbcUtils.*;

/**
 * JDBC based implementation of {@link IAnswerDAO}
 * {@inheritDoc}
 */
@SuppressWarnings("FieldCanBeLocal")
public class AnswerDaoJdbcImpl implements IAnswerDAO {


    // Constants ---------------------------------------------------------------------------------

    private static final String LOGIN = "login";
    private static final String AVATAR_LOCATION = "avatar_location";
    private static final String QUESTION_ID = "question_id";
    private static final String MODERATOR = "moderator";
    private static final String STATE = "state";
    private static final String RATING = "rating";
    private static final String BODY = "body";
    private static final String ID = "id";
    private static final String PUBLICATION_DATE = "publication_date";
    private static final String LAST_UPDATE = "last_update";
    private static final String MY_MARK = "myMark";
    private static final String COUNT = "count";

    private final String ANSWER_EDIT = "answer.edit";
    private final String ANSWER_GET_BY_QUESTION = "answer.get.by.question";
    private final String ANSWER_NEW = "answer.new";
    private final String ANSWER_GET_BY_ID = "answer.get.by.id";
    private final String ANSWER_GET_USER_LOGIN = "answer.get.user.login";
    private final String ANSWER_DELETE = "answer.delete";
    private final String ANSWER_GET_COUNT = "answer.get.count";
    private final String ANSWER_UPDATE_MODERATION_INFO = "answer.update.moderation.info";
    private final String ANSWER_MODERATE = "answer.moderate";

    // Vars --------------------------------------------------------------------------------------

    private Connection connection;
    private Properties sqlSource;

    // Constructors ------------------------------------------------------------------------------

    /**
     * @see DAOFactory
     */
    AnswerDaoJdbcImpl() {}

    // BaseDAO implementation  -------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConnection(final Connection con) {
        connection = con;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSqlSource(final Properties properties) {
        this.sqlSource = properties;
    }

    // Helpers --------------------------------------------------------------------------------------

    /**
     * Maps all rows of the given ResultSet to an List<Answer>.
     * @see AnswerDaoJdbcImpl#map(ResultSet)
     */
    private static List<Answer> mapAll(final ResultSet rs) throws SQLException {
        List<Answer> answers = new ArrayList<>();
        while (rs.next()) answers.add(map(rs));
        return answers;
    }

    /**
     * Map the current row of the given ResultSet to an Answer.
     * @param rs The ResultSet of which the current row is to be mapped to an Answer.
     * @return The mapped Answer from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Answer map(final ResultSet rs) throws SQLException {
        User user = new User();
        user.setLogin(rs.getString(LOGIN));
        user.setAvatar(rs.getString(AVATAR_LOCATION));

        Question quest = new Question();
        quest.setId(rs.getLong(QUESTION_ID));

        ModerationInfo mi = new ModerationInfo();
        mi.setModerator(new UserBuilder().login(rs.getString(MODERATOR)).createNewUser());

        AnswerBuilder ans = (AnswerBuilder) new AnswerBuilder()
                .question(quest)
                .state(valueOf(rs.getString(STATE).toUpperCase()))
                .rating(rs.getInt(RATING))
                .body(rs.getString(BODY))
                .id(rs.getLong(ID))
                .user(user)
                .date(toJavaDate(rs.getTimestamp(PUBLICATION_DATE)))
                .lastUpdate(toJavaDate(rs.getTimestamp(LAST_UPDATE)))
                .moderatedInfo(mi);
        return ans.createMessage();
    }

    /**
     * Maps all rows of the given ResultSet to an List<Answer>.
     * @see AnswerDaoJdbcImpl#map(ResultSet)
     */
    private static List<AnswerView> fullMapAll(final ResultSet rs) throws SQLException {
        List<AnswerView> answers = new ArrayList<>();
        while (rs.next()) answers.add(fullMap(rs));
        return answers;
    }

    /**
     * Map the current row of the given ResultSet to an AnswerView.
     * @param rs The ResultSet of which the current row is to be mapped to an AnswerView.
     * @return The mapped AnswerView instance from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static AnswerView fullMap(final ResultSet rs) throws SQLException {
        AnswerView av = new AnswerView();
        av.setAnswer(map(rs));
        av.setMark(rs.getString(MY_MARK));
        return av;
    }

    // AnswerDAO implementation ------------------------------------------------------------------------


    /**
     * {@inheritDoc}
     */
    @Override
    public void moderateAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException {
        Object[] values = {answer.getBody(), MODERATED.toString().toLowerCase(), toSqlTimestamp(answer.getLastUpdate()), answer.getId()};
        try (PreparedStatement stmt = prepareStatement(connection, sqlSource.getProperty(ANSWER_EDIT), false, values)) {
            if (stmt.executeUpdate() == 0)
                throw new NoSuchEntityException();
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnswerView> getAnswersByQuestionId(final String login, final Long qId,
                                               final String sortBy,
                                               final ComparatorMode orderBy,
                                               final long offset,
                                               final long length) throws DAOSystemException {

        String sql = String.format(sqlSource.getProperty(ANSWER_GET_BY_QUESTION), sortBy, orderBy);
        Object[] values = {login, qId, offset, length};
        try (PreparedStatement stmt = prepareNullSafeStatement(connection, sql, false, values)) {
            ResultSet rs = stmt.executeQuery();
            return fullMapAll(rs);
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException {
        Object[] values = {answer.getBody(), MODIFIED.toString().toLowerCase(), toSqlTimestamp(answer.getLastUpdate()), answer.getId()};
        try (PreparedStatement stmt = prepareStatement(connection, sqlSource.getProperty(ANSWER_EDIT), false, values)) {
            if (stmt.executeUpdate() == 0)
                throw new NoSuchEntityException();
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public long writeAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException {
        Object[] values = {answer.getQuestion().getId(), answer.getUser().getLogin(), answer.getBody()};
        try (PreparedStatement stmt = prepareStatement(connection, sqlSource.getProperty(ANSWER_NEW), true, values)) {
            if (stmt.executeUpdate() == 0) throw new NoSuchEntityException();
            ResultSet genKeys = stmt.getGeneratedKeys();
            if (genKeys.next()) return genKeys.getLong(1);
            throw new NoSuchEntityException();
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Answer getAnswerById(final Long aId) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmt = prepareStatement(connection, sqlSource.getProperty(ANSWER_GET_BY_ID), false, aId)) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) return map(rs);
            throw new NoSuchEntityException();
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getOwnerLoginByAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmt = prepareStatement(connection, sqlSource.getProperty(ANSWER_GET_USER_LOGIN), false, answer.getId())) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getString(LOGIN);
            }
            throw new NoSuchEntityException();
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmt = prepareStatement(connection, sqlSource.getProperty(ANSWER_DELETE), false, answer.getId())) {
            if (stmt.executeUpdate() == 0) throw new NoSuchEntityException();
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moderateInfo(final ModerationInfo mi) throws DAOSystemException, NoSuchEntityException {
        Object[] values = {mi.getTarget().getId(), mi.getModerator().getLogin(), mi.getReason()};
        try (PreparedStatement stmt = prepareStatement(connection, sqlSource.getProperty(ANSWER_MODERATE), false, values)) {
            if (stmt.executeUpdate() == 0) throw new NoSuchEntityException();
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateModerationInfo(final ModerationInfo mi) throws DAOSystemException, NoSuchEntityException {
        Object[] values = {mi.getModerator().getLogin(), mi.getReason(), mi.getTarget().getId()};
        try (PreparedStatement stmt = prepareStatement(connection, sqlSource.getProperty(ANSWER_UPDATE_MODERATION_INFO),
                false, values)) {

            if (stmt.executeUpdate() == 0) throw new NoSuchEntityException();

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getAnswersCount(final Question question) throws DAOSystemException {

        try (PreparedStatement stmt = prepareStatement(connection, sqlSource.getProperty(ANSWER_GET_COUNT),
                false, question.getId())){

            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getLong(COUNT);

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }
}
