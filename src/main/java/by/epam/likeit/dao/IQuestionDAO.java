package by.epam.likeit.dao;


import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.Topic;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.util.List;

/**
 * Base DAO interface defining atomic
 * actions related to the {@link Question}
 * by.epam.likeit.bean/table in the database.
 * @see Question
 * @see by.epam.likeit.service.IQuestionService
 */
public interface IQuestionDAO extends BaseDAO {

    /**
     * Gets list of questions satisfying to arguments list.
     * If topic whose question must be found does not exist
     * or requested offset is more than count of questions
     * related to this topic, an empty list is returned.
     * @param topic the topic id to find related questions
     * @param offset the start offset in the data
     * @param count the limit length of returned data
     * @param comparatorFiled the sort field to sort result
     * @param order the order of sorting
     * @return List of questions satisfying to the arguments list
     * @throws DAOSystemException If something fails at DAO layer
     */
    List<Question> getQuestions(final Topic topic,
                                final long offset,
                                final long count,
                                final String comparatorFiled,
                                final ComparatorMode order)
            throws DAOSystemException;

    /**
     * Modifies the given question with id provided in {@link Question#id}
     * and replaces old message body and/or header to new that must be mentioned
     * in {@link Question#body} class variable.
     *
     * After successful execution of this action, state of the question
     * is changed to {@link Question.QuestionState#MODIFIED}.
     *
     * Question can be modified multiple times, in this case,
     * only last change will be saved.
     *
     * @param question the question to be modified
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested question does not exist or operation failed
     */
    void modifyQuestion(final Question question)
            throws DAOSystemException, NoSuchEntityException;

    /**
     * Changes the state of the given question to
     * {@link Question.QuestionState#CLOSED} in the database.
     * @param question The question to be closed.
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested question does not exist or operation failed.
     */
    void closeQuestion(final Question question)
            throws DAOSystemException, NoSuchEntityException;

    /**
     * Removes the given question from the database.
     * @param question The question to be removed
     * @throws DAOSystemException Of something fails at DAO layer
     * @throws NoSuchEntityException If requested question does no exist or operation failed.
     */
    void removeQuestion(final Question question)
            throws DAOSystemException, NoSuchEntityException;

    /**
     * Inserts new question to the database.
     * @param question The question to be inserted.
     * @return The generated id of the inserted question
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If question was not inserted.
     */
    Long askQuestion(final Question question)
            throws DAOSystemException, NoSuchEntityException;

    /**
     * Increments statistics of view of the given question in the database.
     * @param question The question to be updated
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested question does not exist or operation failed.
     */
    void incrementViews(Question question)
            throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets user who asked the given question.
     * @param question The question
     * @return User's login who wrote this question
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested question does not exist or operation failed.
     */
    String getOwnerLogin(final Question question)
            throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets question by the given id.
     * @param qId The id for seeking question.
     * @return Question object associated with given id
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If the question with given id does not exist.
     */
    Question getQuestionById(final Long qId)
            throws DAOSystemException, NoSuchEntityException;

    /**
     * Updates existing moderation info in the database
     * relate to th given question.
     * This action must be associated with
     * {@link IQuestionDAO#moderateQuestion(Question)}
     * if question is to be modified the second and more times.
     *
     * For successful execution,
     * an {@link ModerationInfo#target#id} is required.
     *
     * Comparing this method to {@link IQuestionDAO#moderateInfo(ModerationInfo)}
     * this one is used for the second and next times, if the question
     * is in {@link Question.QuestionState#MODERATED}
     * state, while <em>moderateInfo()</em> is used
     * when question is NOT been moderated yet.
     * @param info the advanced moderation info.
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If moderation info does not related
     * to existing question and/or existing(old) moderation information
     */
    void updateModerationInfo(final ModerationInfo info) throws DAOSystemException, NoSuchEntityException;

    /**
     * Inserts moderation info related to the given question
     * to the database. For successful execution,
     * an {@link ModerationInfo#target#id} is required.
     *
     * This action must be associated with
     * {@link IQuestionDAO#moderateQuestion(Question)}
     * if question is to be modified for the first time.
     *
     * Comparing this method to {@link IQuestionDAO#updateModerationInfo(ModerationInfo)}
     * this one is used for the first time, if the question
     * is not in {@link Question.QuestionState#MODERATED}
     * state, while <em>updateModerationInfo()</em> is used
     * when question is already been moderated
     *
     * @param info the advanced moderation info
     * @throws DAOSystemException If something fails ad DAO layer
     * @throws NoSuchEntityException If moderation info does not related to existing question or operation failed
     * @see IQuestionDAO#moderateQuestion(Question)
     * @see IQuestionDAO#updateModerationInfo(ModerationInfo)
     */
    void moderateInfo(final ModerationInfo info) throws DAOSystemException, NoSuchEntityException;

    /**
     * Moderates the given question with id provided in {@link Question#id}
     * and replaces old message body and/or header to new that must be mentioned
     * in {@link Question#body} class variable.
     *
     * After successful execution of this action, state of the question
     * is changed to {@link Question.QuestionState#MODERATED}.
     *
     * Question can be moderated multiple times, in this case,
     * only last change will be saved.
     *
     * @param question the question to be moderated
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If question does not exist or operation failed
     */
    void moderateQuestion(final Question question) throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets count of the question related to the given topic.
     * @param topic The topic to find related questions
     * @return count of related questions, if the given topic does not exist, 0 is returned.
     * @throws DAOSystemException If something fails at DAO layer.
     */
    long getQuestionsCountByTopicId(final Topic topic) throws DAOSystemException;


}
