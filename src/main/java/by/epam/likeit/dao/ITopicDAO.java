package by.epam.likeit.dao;


import by.epam.likeit.bean.Topic;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.util.List;

/**
 * Base DAO interface defining atomic
 * actions related to the {@link by.epam.likeit.bean.Topic}
 * by.epam.likeit.bean/table in the database.
 * @see Topic
 * @see by.epam.likeit.service.ITopicService
 */
public interface ITopicDAO extends BaseDAO {

    /**
     * Gets list of topics satisfying to arguments list.
     * If requested offset is more than count of topics,
     * an empty list is returned.
     * @param sortBy the sort field to sort result
     * @param orderBy the order of sorting
     * @param offset the start offset in the data
     * @param count the length of returned data
     * @return List of topics satisfying to the arguments list
     * @throws DAOSystemException If something fails at DAO layer
     */
    List<Topic> getAllTopics(String sortBy, ComparatorMode orderBy, long offset, long count) throws DAOSystemException;

    /**
     * Inserts new topic to the database.
     * @param topic the topic to be inserted
     * @return generated id of the inserted topic
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If topic was not created or operation failed.
     */
    long createTopic(Topic topic) throws DAOSystemException, NoSuchEntityException;

    /**
     * Removes the given topic from the database.
     * @param tId the topic id to be deleted
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If the given topic was not related or operation failed
     */
    void removeTopic(Long tId) throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets topic by the given id.
     * @param topicID the id to find related topic
     * @return The topic associated to the given id.
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If topic related to the given id does not exist
     */
    Topic getTopicById(long topicID) throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets count of all topics
     * @return count of topics
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If result cannot be extract
     */
    long getTopicsCount() throws DAOSystemException, NoSuchEntityException;
}
