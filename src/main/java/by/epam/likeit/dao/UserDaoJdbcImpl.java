package by.epam.likeit.dao;


import by.epam.likeit.bean.*;
import by.epam.likeit.bean.User.UserRights;
import by.epam.likeit.bean.builder.QuestionBuilder;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.util.JdbcUtils;
import by.epam.likeit.util.StringUtils;
import javafx.util.Pair;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static by.epam.likeit.bean.AbstractMessage.Type.ANSWER;
import static by.epam.likeit.bean.AbstractMessage.Type.QUESTION;
import static by.epam.likeit.util.JdbcUtils.prepareNullSafeStatement;
import static by.epam.likeit.util.JdbcUtils.prepareStatement;

/**
 * JDBC based implementation of {@link IUserDAO}
 * {@inheritDoc}
 */
public class UserDaoJdbcImpl implements IUserDAO {

    // Constants --------------------------------------------------------------------------------------

    private static final String F_NAME = "f_name";
    private static final String L_NAME = "l_name";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";
    private static final String COMPANY = "company";
    private static final String POSITION = "position";
    private static final String USER_GET_BY_EMAIL = "user.get.by.email";
    private static final String USER_UPDATE_INFO = "user.update.info";
    private static final String USER_UPDATE_MAIN = "user.update.main";
    private static final String USER_UPDATE_AVATAR = "user.update.avatar";
    private static final String USER_NEW_MAIN = "user.new.main";
    private static final String USER_NEW_INFO = "user.new.info";
    private static final String USER_INCREMENT_VIEWS = "user.increment.views";
    private static final String USER_UPDATE_RIGHTS = "user.update.rights";
    private static final String USER_GET_MAIN = "user.get.main";
    private static final String USER_GET_INFO = "user.get.info";
    private static final String USER_GET_SKILLS_TOP = "user.get.skills.top";
    private static final String AVERAGE_RATING = "Average rating";
    private static final String TOPIC = "Topic";
    private static final String USER_GET_ACTIVITY_LAST = "user.get.activity.last";
    private static final String TYPE = "Type";
    private static final String PUBLICATED = "Publicated";
    private static final String MESSAGE = "Message";
    private static final String ID = "id";
    private static final String CONTEXT = "Context";
    private static final String PARENT_ID = "parent_id";

    // Vars ----------------------------------------------------------------------------------------

    private Connection connection;
    private Properties sqlSource;

    // Constructors --------------------------------------------------------------------------------

    /**
     * @see DAOFactory
     */
    UserDaoJdbcImpl() {}

    // BaseDAO implementation -----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConnection(Connection con) {
        connection = con;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSqlSource(Properties properties) {
        sqlSource = properties;
    }

    // Helpers --------------------------------------------------------------------------------------

    /**
     * Maps the current row of the given ResultSet to an User.
     * @param rs The ResultSet of which the current row is to be mapped to an User.
     * @return The mapped User from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static User map(ResultSet rs) throws SQLException {
        User user = new User();
        user.setEmail(rs.getString("email"));
        user.setLogin(rs.getString("login"));
        user.setPassword(rs.getString("password"));
        user.setQuestions(rs.getInt("questions_count"));
        user.setAnswers(rs.getInt("answers_count"));
        user.setViews(rs.getInt("pages_viewed"));
        user.setRating(rs.getInt("rating"));
        user.setAvatar(rs.getString("avatar_location"));
        user.setRights(EnumSet.copyOf(mapRights(rs.getString("rights"))));
        return user;
    }

    /**
     * Maps full data of the current row of the given ResultSet to an User including personal information.
     *
     * @param rs The ResultSet of which the current row is to be mapped to an User.
     * @return The mapped User from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static User fullMap(ResultSet rs) throws SQLException {
        User user = map(rs);
        user.setFname(rs.getString(F_NAME));
        user.setLname(rs.getString(L_NAME));
        user.setCountry(rs.getString(COUNTRY));
        user.setCity(rs.getString(CITY));
        user.setCompany(rs.getString(COMPANY));
        user.setPosition(rs.getString(POSITION));
        return user;
    }

    /**
     * Maps users rights from the given string to {@link Set<UserRights>}
     * @param src String of user rights separated by comma
     * @return the set of the user's rights if string is not empty, otherwise null.
     */
    private static Set<UserRights> mapRights(String src) {
        if (src.isEmpty()) return Collections.emptySet();
        Set<UserRights> rights = new HashSet<>();
        String[] arr = src.split(",");
        for (String anArr : arr) rights.add(UserRights.valueOf(anArr.toUpperCase()));
        return rights;
    }

    // UserDAO implementation -----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public User getUserByEmail(String email) throws DAOSystemException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(USER_GET_BY_EMAIL), false, email)) {
            ResultSet rs = stmnt.executeQuery();
            if (rs.next()) return map(rs);
            else return null;
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateUser(User user) throws NoSuchEntityException, DAOSystemException {
        Object[] infValues = {user.getLogin(), user.getFname(), user.getLname(), user.getCountry(), user.getCity(),
                user.getCompany(), user.getPosition(), user.getLogin()};
        Object[] values = {user.getEmail(), user.getLogin()};

        try (PreparedStatement infStmnt = prepareStatement(connection, sqlSource.getProperty(USER_UPDATE_INFO), false, infValues);
             PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(USER_UPDATE_MAIN), false, values)) {

            stmnt.executeUpdate();
            infStmnt.executeUpdate();

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateAvatar(String login, String path) throws NoSuchEntityException, DAOSystemException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(USER_UPDATE_AVATAR), false, path, login)) {
            if (stmnt.executeUpdate() == 0) throw new NoSuchEntityException();
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void registerUser(User newUser) throws DAOSystemException, NoSuchEntityException {
        Object[] values = {newUser.getLogin(), newUser.getPassword(), newUser.getEmail(), newUser.getAvatar()};
        Object[] infoValues = {newUser.getLogin(), newUser.getFname(), newUser.getLname(), newUser.getCountry(),
                newUser.getCity(), newUser.getCompany(), newUser.getPosition()};

        try (PreparedStatement usersStmnt = prepareNullSafeStatement(connection, sqlSource.getProperty(USER_NEW_MAIN), false, values);
             PreparedStatement usersInfoStmnt = prepareNullSafeStatement(connection, sqlSource.getProperty(USER_NEW_INFO), false, infoValues)) {

            usersStmnt.executeUpdate();
            if (usersInfoStmnt.executeUpdate() == 0) throw new NoSuchEntityException();

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void incrementViews(User user) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(USER_INCREMENT_VIEWS), false, user.getLogin())) {
            if (stmnt.executeUpdate() == 0) throw new NoSuchEntityException(user.getLogin());
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateRights(String login, Set<UserRights> rights) throws DAOSystemException, NoSuchEntityException {
        Object[] values = {StringUtils.join(rights, ",").toLowerCase(), login};
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(USER_UPDATE_RIGHTS), false, values)) {
            if (stmnt.executeUpdate() == 0) throw new NoSuchEntityException();
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getUserByLogin(String targetUser) throws DAOSystemException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(USER_GET_MAIN), false, targetUser)) {
            ResultSet rs = stmnt.executeQuery();
            if (rs.next()) {
                return map(rs);
            } else return null;
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getUserInfoByLogin(String targetUser) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(USER_GET_INFO), false, targetUser)) {
            ResultSet rs = stmnt.executeQuery();
            if (rs.next()) {
                return fullMap(rs);
            } else throw new NoSuchEntityException();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Pair<String, Double>> getTopSkills(User target) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(USER_GET_SKILLS_TOP), false, target.getLogin())) {
            ResultSet rs = stmnt.executeQuery();
            List<Pair<String, Double>> list = new ArrayList<>(5);
            while (rs.next())
                list.add(new Pair<>(rs.getString(TOPIC), rs.getDouble(AVERAGE_RATING)));
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOSystemException(e);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AbstractMessage> getLastUserActivity(User user, long length) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(USER_GET_ACTIVITY_LAST),
                false, user.getLogin(), length)) {
            ResultSet rs = stmnt.executeQuery();
            List<AbstractMessage> list = new ArrayList<>(5);

            while (rs.next()) {
                AbstractMessage.Type type = AbstractMessage.Type.valueOf(rs.getString(TYPE));
                if (type == ANSWER) {
                    Answer answer = new Answer();
                    answer.setId(rs.getLong(ID));
                    answer.setType(ANSWER);
                    answer.setBody(rs.getString(MESSAGE));
                    answer.setLastUpdate(JdbcUtils.toJavaDate(rs.getTimestamp(PUBLICATED)));
                    answer.setQuestion((Question)new QuestionBuilder()
                            .title(rs.getString(CONTEXT))
                            .id(rs.getLong(PARENT_ID))
                            .createMessage());
                    list.add(answer);
                } else {
                    Question question = new Question();
                    question.setId(rs.getLong(ID));
                    question.setType(QUESTION);
                    question.setBody(rs.getString(MESSAGE));
                    question.setLastUpdate(JdbcUtils.toJavaDate(rs.getTimestamp(PUBLICATED)));
                    question.setTopic(new Topic(rs.getLong(PARENT_ID), (rs.getString(CONTEXT))));
                    list.add(question);
                }
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOSystemException(e);
        }
    }

}
