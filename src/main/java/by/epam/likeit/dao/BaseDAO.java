package by.epam.likeit.dao;


import java.sql.Connection;
import java.util.Properties;

/**
 * Base interface for all DAO subclasses
 * All the created DAO have to implement/extend this interface.
 * All DAO implementing this interface must NOT define
 * edges(begin and end) of the transaction.
 * @see by.epam.likeit.jdbc.transaction.TransactionManager
 */
public interface BaseDAO {

    /**
     * Sets connection to current DAO instance.
     * @see Connection
     * @param con connection to be set.
     */
    void setConnection(final Connection con);

    /**
     * Sets source of queries to be used in all DAO.
     * Necessary query must be extracted using {@link Properties#getProperty(String)}
     * @see Properties
     * @param properties properties containing queries
     */
    void setSqlSource(final Properties properties);

    /**
     * Ordering mode
     * ASC - ascending
     * DESC - descending
     */
    enum ComparatorMode{
        ASC, DESC
    }
}
