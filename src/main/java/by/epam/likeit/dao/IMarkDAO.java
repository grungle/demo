package by.epam.likeit.dao;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.Mark;
import by.epam.likeit.bean.User;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

/**
 * Base DAO interface defining atomic
 * actions related to the Mark by.epam.likeit.bean/table
 * in the database.
 * @see Mark
 * @see by.epam.likeit.service.IMarkService
 */
public interface IMarkDAO extends BaseDAO {

    /**
     * Gets mark that given user leaved to the given answer.
     * If user did not estimate given answer, <em>null</em>
     * is returned.
     * @param answer The answer whose mark is to be searched
     * @param curUser The user who estimated given answer
     * @return Mark to be leaved by current user to the given answer
     * @throws DAOSystemException If something fails at DAO layer
     */
    Mark getMark(final Answer answer, final User curUser)
            throws DAOSystemException;

    /**
     * Inserts new mark in the database.
     * @param mark The mark to be inserted.
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If operation failed
     * @see Mark
     */
    void newMark(final Mark mark)
            throws DAOSystemException, NoSuchEntityException;

    /**
     * Sets to the existing mark its new value.
     * Existing mark is found by its class variable,
     * that must not be equals to null in {@link Mark#answerId}
     * @param mark The mark to be updated.
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If old mark does not exist.
     */
    void updateMark(final Mark mark)
            throws DAOSystemException, NoSuchEntityException;
}
