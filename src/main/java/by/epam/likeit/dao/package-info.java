/**
 * Package contains classes implementing DAO layer
 * of the application. DAO layer are splitted on the
 * two parts: interfaces, describing contracts
 * and subclasses of them.
 * NOTE! All subclasses implementing DAO interfaces
 * have only package-private(default) constructor
 * to be created from {@link by.epam.likeit.dao.DAOFactory}
 */
package by.epam.likeit.dao;