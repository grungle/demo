package by.epam.likeit.dao.exception;

/**
 * Exception is thrown if requested entity does not exist
 * or as reaction on request to perform valid action to create
 * entity that must be related to another entity that already
 * does not exist.
 * Example: ask question in topic that does not exist.
 *
 * Also this exception is thrown if result was expected at DAO
 * layer after executing query, but valid action was not executed
 * because of some mismatch at DAO layer were found.
 */
public class NoSuchEntityException extends DAOException {

    // Constants --------------------------------------------------------------------------------

    private static final String STATUS = "NOT_FOUND";
    private static final long serialVersionUID = -6075417855878616640L;

    // Constructors ------------------------------------------------------------------------------

    public NoSuchEntityException() {
        super();
    }

    public NoSuchEntityException(Throwable cause, String errorLabel) {
        super(cause, errorLabel);
    }

    public NoSuchEntityException(String message, String errorLabel) {
        super(message, errorLabel);
    }

    public NoSuchEntityException(Throwable cause){
        super(cause);
    }

    public NoSuchEntityException(String message){
        super(message);
    }

    // Methods ------------------------------------------------------------------------------------

    @Override
    public String getTextStatus() {
        return STATUS;
    }
}
