package by.epam.likeit.dao.exception;

/**
 * Exception is thrown if requested client does not allowed
 * to execute some Action.
 * @see by.epam.likeit.bean.User.UserRights
 */
public class InsufficientPermissionException extends DAOException {

    private static final String STATUS = "FORBIDDEN";
    private static final long serialVersionUID = -474011339065358579L;

    public InsufficientPermissionException() {
        super();
    }

    @Override
    public String getTextStatus() {
        return STATUS;
    }
}
