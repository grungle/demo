package by.epam.likeit.dao.exception;

/**
 * Exception is thrown if some system errors at DAO layer is occurred
 */
public class DAOSystemException extends DAOException {

    // Constants -----------------------------------------------------------------------------------------

    private static final String STATUS = "ERROR";
    private static final long serialVersionUID = -598642570227970828L;

    // Constructors --------------------------------------------------------------------------------------

    public DAOSystemException() {
        super();
    }

    public DAOSystemException(String message, String errorLabel) {
        super(message, errorLabel);
    }

    public DAOSystemException(Throwable e, String errorLabel) {
        super(e, errorLabel);
    }

    public DAOSystemException(Throwable cause){
        super(cause);
    }

    @Override
    public String getTextStatus() {
        return STATUS;
    }
}
