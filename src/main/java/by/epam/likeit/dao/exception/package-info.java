/**
 * Package contains classes describing
 * exceptions that can be thrown from DAO layer.
 */
package by.epam.likeit.dao.exception;