package by.epam.likeit.dao.exception;

/**
 * Base exception for all exception related to DAO.
 */
public abstract class DAOException extends Exception {

    private static final long serialVersionUID = 1449910384069852040L;

    /**
     * This field may contain extra information about error to be process
     * in proper way at the server or rendered at the client side.
     */
    private String errorLabel;

    // Constructors ------=====----------------------------------------------------------------------

    public DAOException() {
        super();
    }

    public DAOException(String message, String errorLabel) {
        super(message);
        this.errorLabel = errorLabel;
    }

    public DAOException(Throwable cause, String errorLabel){
        super(cause);
        this.errorLabel = errorLabel;
    }

    public DAOException(Throwable cause){
        super(cause);
    }

    public DAOException(String message){
        super(message);
    }

    // Methods --------------------------------------------------------------------------------------

    public abstract String getTextStatus();

    public void setErrorLabel(String label){
        this.errorLabel = label;
    }

    public String getErrorLabel(){
        return errorLabel;
    }
}
