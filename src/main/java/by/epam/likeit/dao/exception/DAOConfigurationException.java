package by.epam.likeit.dao.exception;

/**
 * Check exception is thrown when {@link by.epam.likeit.dao.DAOFactory}
 * cannot be configurated
 */
public class DAOConfigurationException extends RuntimeException {

    private static final long serialVersionUID = 5053157098894141995L;

    public DAOConfigurationException(Throwable cause) {super(cause);}
}
