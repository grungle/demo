package by.epam.likeit.dao;


import by.epam.likeit.bean.AbstractMessage;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.User.UserRights;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import javafx.util.Pair;

import java.util.List;
import java.util.Set;

/**
 * Base DAO interface defining atomic
 * actions related to the {@link by.epam.likeit.bean.User}
 * by.epam.likeit.bean in the database.
 * @see User
 * @see by.epam.likeit.service.IUserService
 */
public interface IUserDAO extends BaseDAO {

    /**
     * Updates avatar location of the given user in the database.
     * @param login The user to be updated
     * @param path The new avatar location
     * @throws NoSuchEntityException If the given user does not exist
     * @throws DAOSystemException If something fails at DAO layer
     */
    void updateAvatar(String login, String path) throws NoSuchEntityException, DAOSystemException;

    /**
     * Inserts new user to the database.
     * For successful execution, the given login
     * and email must be uniques.
     * @param newUser The user to be registered
     * @throws DAOSystemException If something fails at DAO layer or provided login
     * and/or email is not uniques.
     * @throws NoSuchEntityException If user was not registered or operation failed.
     */
    void registerUser(User newUser) throws DAOSystemException, NoSuchEntityException;

    /**
     * Updated the given user by the {@link User#login}.
     * @param user The user to be updated
     * @throws NoSuchEntityException If the given user does not exist
     * @throws DAOSystemException If something fails at DAO layer
     */
    void updateUser(User user) throws NoSuchEntityException, DAOSystemException;

    /**
     * Updates views of the given user.
     * The given user may not contain the new
     * count of views as operation is to be executed
     * completely in the database level
     * @param user The user to be updated
     * @throws NoSuchEntityException If the given user does not exist
     * @throws DAOSystemException If something fails at DAO layer
     * @see User#views
     */
    void incrementViews(User user)  throws NoSuchEntityException, DAOSystemException;

    /**
     * Gets main user information by the login.
     * @param targetUser The user to be found
     * @return the user instance containing main information
     * @throws DAOSystemException If something fails at DAO layerist
     */
    User getUserByLogin(String targetUser) throws DAOSystemException;

    /**
     * Update the rights of the given user in the database.
     * @param login The login to define user whose rights is to be updated
     * @param rights The rights to be inserted instead of old
     * @throws NoSuchEntityException If there is no user with given login in the database
     * @throws DAOSystemException If something fails at DAO layer
     */
    void updateRights(String login, Set<UserRights> rights) throws NoSuchEntityException, DAOSystemException;

    /**
     * Gets all user information by the login.
     * @param targetUser The user to be found
     * @return the user instance containing personal information
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If the given user does not exist
     */
    User getUserInfoByLogin(String targetUser) throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets last actions by user sorted since last.
     * If did not ask questions and did not answer to them,
     * an empty list is returned.
     * @param user The user to find his last questions and answers
     * @param length The count of activities to be returned [0; length]
     * @return the list of last user's questions and answers
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If the given user does not exist
     */
    List<AbstractMessage> getLastUserActivity(User user, long length) throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets the topics where user has greatest reputation.
     * If there are no topics where the given user leaved
     * answers, an empty list is returned.
     * @param target User to find topics
     * @return the list of pairs of
     * topic name -> absolute rating(current reputation on the topic / max reputation )
     * where max reputation is count of marks * max possible mark
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If the given user does not exist
     * @see by.epam.likeit.bean.Mark#MAX_MARK
     */
    List<Pair<String, Double>> getTopSkills(User target) throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets user by the given email.
     * @param email The email to find user
     * @return User having the given email, if the email is free, <em>null</em> is returned
     * @throws DAOSystemException If something fails at DAO layer
     */
    User getUserByEmail(String email) throws DAOSystemException;
}
