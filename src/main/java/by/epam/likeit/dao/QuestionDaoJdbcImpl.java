package by.epam.likeit.dao;

import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.QuestionBuilder;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static by.epam.likeit.bean.Question.QuestionState.*;
import static by.epam.likeit.util.JdbcUtils.*;

/**
 * JDBC based implementation of {@link IQuestionDAO}
 * {@inheritDoc}
 */
public class QuestionDaoJdbcImpl implements IQuestionDAO {

    // Constants ------------------------------------------------------------------------------------

    private static final String LOGIN = "login";
    private static final String AVATAR_LOCATION = "avatar_location";
    private static final String MODERATOR = "moderator";
    private static final String TITLE = "title";
    private static final String VIEWED = "viewed";
    private static final String STATE = "state";
    private static final String T_ID = "tId";
    private static final String NAME = "name";
    private static final String ANSWERS_COUNT = "answers_count";
    private static final String LAST_UPDATE = "last_update";
    private static final String PUBLICATION_DATE = "publication_date";
    private static final String BODY = "body";
    private static final String Q_ID = "qId";
    private static final String TOPIC_ID = "topic_id";
    private static final String ID = "id";
    private static final String QUESTION_GET_ALL_BY_TOPIC = "question.get.all.by.topic";
    private static final String QUESTION_CLOSE = "question.close";
    private static final String QUESTION_DELETE = "question.delete";
    private static final String QUESTION_NEW = "question.new";
    private static final String QUESTION_INCREMENT_VIEWS = "question.increment.views";
    private static final String QUESTION_GET_USER_LOGIN = "question.get.user.login";
    private static final String QUESTION_GET_BY_ID = "question.get.by.id";
    private static final String QUESTION_MODIFY = "question.modify";
    private static final String QUESTION_MODERATION_UPDATE = "question.moderation.update";
    private static final String QUESTION_MODERATION_NEW = "question.moderation.new";
    private static final String QUESTION_MODIFY1 = "question.modify";
    private static final String QUESTION_GET_COUNT_BY_TOPIC = "question.get.count.by.topic";
    private static final String COUNT = "count";

    // Vars -----------------------------------------------------------------------------------------

    private Connection connection;
    private Properties sqlSource;

    // Constructors ---------------------------------------------------------------------------------

    /**
     * @see DAOFactory
     */
    QuestionDaoJdbcImpl() {}

    // BaseDAO implementation -----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSqlSource(Properties properties) {
        sqlSource = properties;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConnection(Connection con) {
        connection = con;
    }

    // Helpers ---------------------------------------------------------------------------------------

    /**
     * Maps the current ResultSet to a List<Question>.
     * @param rs the ResultSet to extract questions.
     * @return The mapped list of questions
     * @throws SQLException If something fails at database level
     */
    private static List<Question> mapAll(ResultSet rs) throws SQLException {
        List<Question> questions = new ArrayList<>();
        while (rs.next()) questions.add(mapToView(rs));
        return questions;
    }

    /**
     * Maps the current row of the given ResultSet to a Question.
     *
     * @param rs The ResultSet of which the current row is to be mapped to a Question.
     * @return The mapped Question from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Question map(ResultSet rs) throws SQLException {
        User user = new User();
        user.setLogin(rs.getString(LOGIN));
        user.setAvatar(rs.getString(AVATAR_LOCATION));

        ModerationInfo mi = new ModerationInfo();
        mi.setModerator(new UserBuilder().login(rs.getString(MODERATOR)).createNewUser());

        QuestionBuilder qst = (QuestionBuilder) new QuestionBuilder()
                .title(rs.getString(TITLE))
                .viewed(rs.getLong(VIEWED))
                .state(valueOf(rs.getString(STATE).toUpperCase()))
                .topic(new Topic(rs.getLong(T_ID), rs.getString(NAME)))
                .answersCount(rs.getLong(ANSWERS_COUNT))
                .lastUpdate(toJavaDate(rs.getTimestamp(LAST_UPDATE)))
                .date(toJavaDate(rs.getTimestamp(PUBLICATION_DATE)))
                .body(rs.getString(BODY))
                .user(user)
                .id(rs.getLong(Q_ID))
                .moderatedInfo(mi);

        return qst.createMessage();
    }

    /**
     * Maps all the information the current row from of the given ResultSet to Question object.
     * @param rs the ResultSet to extract row.
     * @return The mapped question.
     * @throws SQLException If something fails at database level
     */
    private static Question mapToView(ResultSet rs) throws SQLException {
        User user = new User();
        user.setLogin(rs.getString(LOGIN));

        QuestionBuilder qst = (QuestionBuilder) new QuestionBuilder()
                .title(rs.getString(TITLE))
                .viewed(rs.getLong(VIEWED))
                .state(valueOf(rs.getString(STATE).toUpperCase()))
                .topic(new Topic(rs.getLong(TOPIC_ID), null))
                .answersCount(rs.getLong(ANSWERS_COUNT))
                .date(toJavaDate(rs.getTimestamp(PUBLICATION_DATE)))
                .user(user)
                .id(rs.getLong(ID));

        return qst.createMessage();
    }

    // QuestionDAO implementation -------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Question> getQuestions(Topic topic, long offset, long count, String sortBy, ComparatorMode order)
            throws DAOSystemException {

        Object[] values = {topic.getId(), offset, count};
        String sql = String.format(sqlSource.getProperty(QUESTION_GET_ALL_BY_TOPIC), sortBy, order);

        try (PreparedStatement stmnt = prepareStatement(connection, sql, false, values)) {
            ResultSet rs = stmnt.executeQuery();
            return mapAll(rs);

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeQuestion(Question quest) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_CLOSE),
                false, quest.getId())) {
            if (stmnt.executeUpdate() == 0) throw new NoSuchEntityException("Question with id " + quest.getId() + " wasn't found");
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeQuestion(Question quest) throws DAOSystemException, NoSuchEntityException {

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_DELETE),
                false, quest.getId())) {

            if (stmnt.executeUpdate() == 0) throw new NoSuchEntityException("Question with id " +
                    quest.getId() + " wasn't found");

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long askQuestion(Question quest) throws DAOSystemException, NoSuchEntityException {

        Object[] values = {quest.getTopic().getId(), quest.getUser().getLogin(), quest.getTitle(), quest.getBody()};
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_NEW), true, values)) {

            if (stmnt.executeUpdate() == 0) throw new NoSuchEntityException("Question wasn't created");
            ResultSet rs = stmnt.getGeneratedKeys();
            if (!rs.next()) throw new NoSuchEntityException();
            return rs.getLong(1);

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void incrementViews(Question quest) throws DAOSystemException, NoSuchEntityException {

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_INCREMENT_VIEWS),
                false, quest.getId())) {

            int res = stmnt.executeUpdate();
            if (res == 0) throw new NoSuchEntityException("Question with id " + quest.getId() + " wasn't found");

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getOwnerLogin(Question quest) throws DAOSystemException, NoSuchEntityException {

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_GET_USER_LOGIN),
                false, quest.getId())) {

            ResultSet rs = stmnt.executeQuery();
            if (rs.next()) return rs.getString(LOGIN);
            throw new NoSuchEntityException("Question with id " + quest.getId() + " wasn't found");

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Question getQuestionById(Long qId) throws DAOSystemException, NoSuchEntityException {

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_GET_BY_ID),
                false, qId, qId)) {

            ResultSet rs = stmnt.executeQuery();
            if (rs.next()) return map(rs);
            else throw new NoSuchEntityException("Question with id " + qId + " wasn't found");

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyQuestion(Question question)
            throws DAOSystemException, NoSuchEntityException {

        Object[] values = {MODIFIED.toString().toLowerCase(), question.getTitle(), question.getBody(),
                toSqlTimestamp(question.getLastUpdate()), question.getId()};

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_MODIFY),
                false, values)) {
            if (stmnt.executeUpdate() == 0)
                throw new NoSuchEntityException("Question with id " + question.getId() + " wasn't found");
        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateModerationInfo(ModerationInfo info) throws DAOSystemException, NoSuchEntityException {
        Object[] values = {info.getModerator().getLogin(), info.getReason(), info.getTarget().getId()};

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_MODERATION_UPDATE),
                false, values)) {

            if (stmnt.executeUpdate() == 0)
                throw new NoSuchEntityException("Moderation info with id = " + info.getTarget().getId() + " wasn't found");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moderateInfo(ModerationInfo info) throws DAOSystemException, NoSuchEntityException {

        Object[] values = {info.getTarget().getId(), info.getModerator().getLogin(), info.getReason()};

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_MODERATION_NEW),
                false, values)) {

            if (stmnt.executeUpdate() == 0) throw new NoSuchEntityException("Failed to create moderation info");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moderateQuestion(Question question) throws DAOSystemException, NoSuchEntityException {
        Object[] values = {MODERATED.toString().toLowerCase(), question.getTitle(), question.getBody(),
                toSqlTimestamp(question.getLastUpdate()), question.getId()};

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_MODIFY1),
                false, values)) {

            if (stmnt.executeUpdate() == 0) throw new NoSuchEntityException("Moderation of the question with id = " +
                    question.getId() + " failed.");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getQuestionsCountByTopicId(Topic topic) throws DAOSystemException {

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(QUESTION_GET_COUNT_BY_TOPIC),
                false, topic.getId())) {

            ResultSet rs = stmnt.executeQuery();
            rs.next();
            return rs.getLong(COUNT);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOSystemException(e);
        }
    }
}
