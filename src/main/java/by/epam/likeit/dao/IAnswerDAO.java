package by.epam.likeit.dao;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.ModerationInfo;
import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.view.AnswerView;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.util.List;

/**
 * Base DAO interface defining atomic
 * actions related to the {@link Answer}
 * table in the database.
 * @see Answer
 * @see by.epam.likeit.service.IAnswerService
 */
public interface IAnswerDAO extends BaseDAO {


    /**
     * Gets list of answers satisfying to arguments list.
     * If question whose answer must be found does not exist
     * or requested offset is more than count of answers
     * related to this question, an empty list is returned.
     * @param login the login of user who requested this operation. If
     *              user is a guest, parameter must be equal to null
     * @param qId the question id to find related answers
     * @param sortBy the sort field to sort result
     * @param orderBy the order of sorting
     * @param offset the start offset in the data
     * @param length the length of returned data
     * @return List of answers satisfying to the arguments list
     * @throws DAOSystemException If something fails at DAO layer
     */
    List<AnswerView> getAnswersByQuestionId(final String login, final Long qId,
                                            final String sortBy,
                                            final ComparatorMode orderBy,
                                            final long offset,
                                            final long length) throws DAOSystemException;

    /**
     * Modifies the given answer with id provided in {@link Answer#id} and replaces old message
     * body to new that must be mentioned in {@link Answer#body} class variable.
     *
     * After successful execution of this action, state of the answer
     * is changed to {@link Answer.AnswerState#MODIFIED}.
     *
     * Answer can be modified multiple times, in this case,
     * only last change will be saved.
     *
     * @param answer the answer to be modified
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested answer does not exist or operation failed
     */
    void modifyAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException;

    /**
     * Moderates answer with id provided in {@link Answer#id} and replaces old message
     * body to new that must be mentioned in {@link Answer#body} class variable.
     *
     * After successful execution of this action, state of the answer
     * is changed to {@link Answer.AnswerState#MODERATED}.
     *
     * Answer can be moderated multiple times, in this case,
     * only last change will be saved.
     *
     * @param answer the answer to be moderated
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If answer does not exist or operation failed
     */
    void moderateAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException;

    /**
     * Inserts the answer to the database.
     * @param answer the answer to be saved in the database
     * @return generated {@linkplain Answer#id id} of the new answer
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If answer was not created or
     * {@linkplain Answer#question question} does not exist
     * @see Answer
     * @see Question
     * @see by.epam.likeit.bean.User.UserRights
     */
    long writeAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets answer by its id
     * @param aId ID for the answer to be found
     * @return found answer
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested answer does not exist
     * @see Answer
     */
    Answer getAnswerById(final Long aId) throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets user's login who asked the given question
     * @param answer The answer whose owner must be found
     * @return Login of user who asked this question
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested answer does not exist
     * @see by.epam.likeit.bean.User
     * @see Answer
     */
    String getOwnerLoginByAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException;

    /**
     * Removes given answer by its {@link Answer#id}
     * @param answer answer to be deleted
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If requested answer does not exist or operation failed
     */
    void removeAnswer(final Answer answer) throws DAOSystemException, NoSuchEntityException;

    /**
     * Inserts moderation info to the database.
     * This action must be associated with
     * {@link IAnswerDAO#moderateAnswer(Answer)}
     * if answer is to be modified for the first time.
     *
     * For successful execution,
     * an {@link ModerationInfo#target#id} is required.
     *
     * Comparing this method to {@link IAnswerDAO#updateModerationInfo(ModerationInfo)}
     * this one is used for the first time, if the answer
     * is not in {@link Answer.AnswerState#MODERATED}
     * state, while <em>updateModerationInfo()</em> is used
     * when answer is already been moderated
     *
     * @param mi the advanced moderation info
     * @throws DAOSystemException If something fails ad DAO layer
     * @throws NoSuchEntityException If moderation info does not related to existing answer or operation failed
     * @see IAnswerDAO#moderateAnswer(Answer)
     * @see IAnswerDAO#updateModerationInfo(ModerationInfo)
     */
    void moderateInfo(final ModerationInfo mi) throws DAOSystemException, NoSuchEntityException;

    /**
     * Updates existing moderation info in the database.
     * This action must be associated with
     * {@link IAnswerDAO#moderateAnswer(Answer)}
     * if answer is to be modified the second and more times.
     *
     * For successful execution,
     * an {@link ModerationInfo#target#id} is required.
     *
     * Comparing this method to {@link IAnswerDAO#moderateInfo(ModerationInfo)}
     * this one is used for the second and next times, if the answer
     * is in {@link Answer.AnswerState#MODERATED}
     * state, while <em>moderateInfo()</em> is used
     * when answer is NOT been moderated yet.
     * @param mi the advanced moderation info.
     * @throws DAOSystemException If something fails at DAO layer
     * @throws NoSuchEntityException If moderation info does not related
     * to existing answer and or existing(old) moderation information
     */
    void updateModerationInfo(final ModerationInfo mi) throws DAOSystemException, NoSuchEntityException;

    /**
     * Gets count of related answers to given by its
     * class variable {@link Question#id}.
     * If requested question does not exist, 0 is returned.
     * @param question question to find related answers
     * @return count of answers related to the given question.
     * @throws DAOSystemException If something fails at DAO layer.
     */
    long getAnswersCount(final Question question) throws DAOSystemException;
}
