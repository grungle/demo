package by.epam.likeit.dao;


import by.epam.likeit.bean.Answer;
import by.epam.likeit.bean.Mark;
import by.epam.likeit.bean.User;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import static by.epam.likeit.util.JdbcUtils.prepareStatement;

/**
 * JDBC based implementation of {@link IMarkDAO}
 * {@inheritDoc}
 */
public class MarkDaoJdbcImpl implements IMarkDAO {

    // Constants -----------------------------------------------------------------------------------

    private static final String LOGIN = "login";
    private static final String ANSWER_ID = "answer_id";
    private static final String MARK = "mark";
    private static final String MARK_GET_BY_ANSWER = "mark.get.by.answer";
    private static final String MARK_NEW = "mark.new";
    private static final String MARK_UPDATE = "mark.update";

    // Vars ----------------------------------------------------------------------------------------

    private Connection connection;
    private Properties sqlSource;

    // Constructors ---------------------------------------------------------------------------------

    /**
     * @see DAOFactory
     */
    MarkDaoJdbcImpl() {}

    // Helpers --------------------------------------------------------------------------------------

    /**
     * Map the current row of the given ResultSet to a Mark object.
     * @param rs The ResultSet of which the current row is to be mapped to a Mark.
     * @return The mapped Mark from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Mark map(ResultSet rs) throws SQLException {
        Mark mark = new Mark();
        User user = new User();
        user.setLogin(rs.getString(LOGIN));

        mark.setAnswerId(rs.getLong(ANSWER_ID));
        mark.setMark(rs.getInt(MARK));
        mark.setUser(user);
        return mark;
    }

    // BaseDAO implementation -----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConnection(Connection con) {
        connection = con;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSqlSource(Properties properties) {
        sqlSource = properties;
    }

    // MarkDAO implementation -----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Mark getMark(Answer answer, User curUser) throws DAOSystemException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(MARK_GET_BY_ANSWER), false, answer.getId(),
                curUser.getLogin())) {

            ResultSet rs = stmnt.executeQuery();
            if (rs.next()) return map(rs);
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void newMark(Mark mark) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(MARK_NEW), false, mark.getAnswerId(),
                mark.getUser().getLogin(), mark.getMark())) {

            if (stmnt.executeUpdate() == 0) throw new NoSuchEntityException();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateMark(Mark mark) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(MARK_UPDATE), false, mark.getMark(),
                mark.getAnswerId(), mark.getUser().getLogin())) {

            if (stmnt.executeUpdate() == 0)
                throw new NoSuchEntityException("Mark to answer with id = " + mark.getAnswerId()
                    + " and estimator = " + mark.getUser().getLogin() + " wasn't found.");

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOSystemException(e);
        }
    }

}
