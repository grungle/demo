package by.epam.likeit.dao;

import by.epam.likeit.bean.Topic;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.logger.LikeLogger;
import by.epam.likeit.util.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static by.epam.likeit.util.JdbcUtils.prepareStatement;
import static by.epam.likeit.util.JdbcUtils.toJavaDate;

/**
 * JDBC based implementation of {@link ITopicDAO}
 * {@inheritDoc}
 */
public class TopicDaoJdbcImpl implements ITopicDAO {

    // Constants -----------------------------------------------------------------------------------

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String UPDATED = "updated";
    private static final String QUESTIONS = "questions";
    private static final String TOPIC_GET_ALL = "topic.get.all";
    private static final String TOPIC_NEW = "topic.new";
    private static final String TOPIC_DELETE = "topic.delete";
    private static final String TOPIC_GET_BY_ID = "topic.get.by.id";
    private static final String TOPIC_GET_COUNT = "topic.get.count";
    private static final String COUNT = "count";

    // Vars ----------------------------------------------------------------------------------------

    private Connection connection;
    private Properties sqlSource;

    // Constructors ---------------------------------------------------------------------------------

    /**
     * @see DAOFactory
     */
    TopicDaoJdbcImpl() {}

    // Helpers --------------------------------------------------------------------------------------

    /**
     * Maps given Result set to the List<Topic>.
     * @param rs the ResultSet to extract rows
     * @return list of mapped topics
     * @throws SQLException If something fails at database level
     */
    private static List<Topic> mapAll(ResultSet rs) throws SQLException {
        List<Topic> topics = new ArrayList<>();
        while (rs.next()) topics.add(map(rs));
        return topics;
    }

    /**
     * Map the current row of the given ResultSet to a Topic.
     * @param rs The ResultSet of which the current row is to be mapped to a Topic.
     * @return The mapped Topic from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Topic map(ResultSet rs) throws SQLException {
        Topic topic = new Topic();
        topic.setId(rs.getLong(ID));
        topic.setName(rs.getString(NAME));
        topic.setUpdated(toJavaDate(rs.getTimestamp(UPDATED)));
        topic.setQuestions(rs.getLong(QUESTIONS));
        return topic;
    }

    // BaseDAO implementation -----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConnection(Connection con) {
        connection = con;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSqlSource(Properties properties) {
        sqlSource = properties;
    }

    // TopicDAO implementation -----------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Topic> getAllTopics(String sortBy, ComparatorMode orderBy, long offset, long count)
            throws DAOSystemException {

        String sql = String.format(sqlSource.getProperty(TOPIC_GET_ALL), sortBy, orderBy);
        try (PreparedStatement stmnt = prepareStatement(connection, sql, false, offset, count)) {
            ResultSet rs = stmnt.executeQuery();
            return mapAll(rs);
        } catch (SQLException e) {
            LikeLogger.warn(e.getMessage(), e);
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long createTopic(Topic topic) throws DAOSystemException, NoSuchEntityException {
        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(TOPIC_NEW),
                true, topic.getName())) {

            stmnt.executeUpdate();
            ResultSet rs = stmnt.getGeneratedKeys();
            if (rs.next()) return rs.getLong(1);
            throw new NoSuchEntityException();

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeTopic(Long tId) throws DAOSystemException, NoSuchEntityException {

        try (PreparedStatement stmnt = JdbcUtils.prepareStatement(connection, sqlSource.getProperty(TOPIC_DELETE),
                false, tId)) {

            int res = stmnt.executeUpdate();
            if (res == 0) throw new NoSuchEntityException();

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Topic getTopicById(long topicID) throws DAOSystemException, NoSuchEntityException {

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(TOPIC_GET_BY_ID), false, topicID)) {

            ResultSet rs = stmnt.executeQuery();
            if (rs.next()) return new Topic(rs.getLong(ID), rs.getString(NAME));
            throw new NoSuchEntityException();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOSystemException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTopicsCount() throws DAOSystemException, NoSuchEntityException {

        try (PreparedStatement stmnt = prepareStatement(connection, sqlSource.getProperty(TOPIC_GET_COUNT), false)) {

            ResultSet rs = stmnt.executeQuery();
            if (rs.next()) return rs.getLong(COUNT);
            throw new NoSuchEntityException();

        } catch (SQLException e) {
            throw new DAOSystemException(e);
        }
    }

}