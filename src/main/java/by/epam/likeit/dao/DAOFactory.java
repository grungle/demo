package by.epam.likeit.dao;


import by.epam.likeit.dao.exception.DAOConfigurationException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.logger.LikeLogger;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Class implements Factory pattern and creates DAO
 * instances based on the requested DAO type class.
 * Class provides SQL queries to its DAO and Connections and/or
 * DataSources
 * @see DataSource
 * @see Connection
 * @see BaseDAO
 * @see by.epam.likeit.dao
 */
public class DAOFactory  {

    // Vars ----------------------------------------------------------------------------------------

    private static Properties properties;

    // Methods --------------------------------------------------------------------------------------

    /**
     * Initializes properties containing all SQL queries to be user in DAO.
     * @param source InputStream to properties
     * @throws IOException If any error occurred at I/O level
     */
    public static void init(InputStream source) throws IOException {
        properties = new Properties();
        properties.load(source);
    }

    /**
     * Configures and returns requested DAO based on DataSource
     * @param clazz DAO class that implements creating DAO class
     * @param ds DataSource implementation
     * @param <DAO> Subclass of BaseDAO
     * @return created DAO instance
     * @throws DAOConfigurationException If some error occurred during configuration DAO
     */
    public static <DAO extends BaseDAO> DAO newInstance(Class<DAO> clazz, DataSource ds){
        try {
            return newInstance(clazz, ds.getConnection());
        } catch (SQLException e) {
            throw new DAOConfigurationException(e);
        }
    }

    /**
     * Configures and returns requested DAO based on ThreadLocalTransactionManagerImpl class.
     * @param clazz DAO class that implements creating DAO class
     * @param <DAO> Subclass of BaseDAO
     * @return created DAO instance
     * @throws DAOConfigurationException If some error occurred during configuration DAO
     * @see ThreadLocalTransactionManagerImpl
     */
    public static <DAO extends BaseDAO> DAO newInstance(Class<DAO> clazz){
        return newInstance(clazz, ThreadLocalTransactionManagerImpl.getInstance());
    }

    /**
     * Configures and returns requested DAO using Connection
     * @param clazz DAO class that implements creating DAO class
     * @param con connection to be used in created DAO class
     * @param <DAO> Subclass of BaseDAO
     * @return created DAO instance
     * @throws DAOConfigurationException If some error occurred during configuration DAO
     */
    public static <DAO extends BaseDAO> DAO newInstance(Class<DAO> clazz, Connection con){
        try {
            DAO dao = clazz.cast(clazz.newInstance());
            dao.setConnection(con);
            dao.setSqlSource(properties);
            return dao;
        }  catch (IllegalAccessException | InstantiationException e) {
            LikeLogger.warn("Cannot create DAO instance ", e);
            throw new DAOConfigurationException(e);
        }

    }

}
