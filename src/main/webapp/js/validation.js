

var MAX_FILE_SIZE = 2 * 1024 * 1024;


/*Cheks if photo size that users is going to load has appropriate size*/
function testFileSize(errMsg) {

    var input = document.getElementsByName("avatar_location")[0];

    if (typeof input.files[0] === typeof undefined || input.files[0] == null) {
        input.setCustomValidity('');
        input.value = "";
        return true;
    }

    var len = input.files[0].size;

    if (len > MAX_FILE_SIZE) {
        input.setCustomValidity(errMsg);
        return false;
    } else {
        input.setCustomValidity('');
        return true;
    }
}

/*Check if password contains at least 1 char in UPPER, 1 char in DOWN register and 1 digit character.
 The length must be equal to 6 or greater*/
function testStrongPass(errMsg, element) {
    var pattern = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})");
    var res = pattern.test(element.value);
    if (res == true) {
        element.setCustomValidity('');
    } else {
        element.setCustomValidity(errMsg);
    }
}

function validateLogin(login, spanId) {
    var pattern = new RegExp("^[a-zA-Z]{1}\w{4,49}$");
    return validateAll(pattern, login.innerHTML, spanId);
}

function validateEmail(email, spanId) {
    var pattern = new RegExp("^[\w+@\w+\.\w+]{1,200}$");
    return validateAll(pattern, email.innerHTML, spanId);
}

function validateUserName(name, spanId) {
    var pattern = new RegExp("^[a-zA-Z]{1}\w{1,49}$");
    return validateAll(pattern, name.innerHTML, spanId);
}

function validateCountry(country, spanId){
    var pattern = new RegExp("^[A-Z]{1}([a-zA-Z]|\s){1,49}$");
    return validateAll(pattern, country.innerHTML, spanId);
}

function validateCity(city, spanId) {
    var pattern = new RegExp("^[A-Z]{1}[a-zA-Z\-]{1,49}$");
    return validateAll(pattern, city.innerHTML, spanId);
}

function validateUserInfo(info, spanId) {
    var pattern = new RegExp("^.{1,50}$");
    return validateAll(pattern, info.innerHTML, spanId);
}

function validateAll(pattern, value, spanId) {
    var res = pattern.test(value);

    if (res == false){
        ('#' + spanId).removeClass("w3-hide");
        return false;
    }

    return true;

}







