<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="t" uri="/WEB-INF/tools.tld" %>
<%--@elvariable id="lang" type="java.lang.String"--%>
<fmt:setLocale value="${lang}"/>

<fmt:setBundle basename="i18n.loc"/>
<%--@elvariable id="targetUser" type="by.epam.likeit.bean.view.UserView"--%>
<%--@elvariable id="user" type="by.epam.likeit.bean.User"--%>
<c:set scope="request" var="targetUser" value="${targetUser}"/>
<c:set scope="session" var="user" value="${user}"/>

<fmt:setTimeZone value="${cookie eq null or cookie['timezone'] eq null ? 'GMT' : cookie['timezone'].value}" />



<!DOCTYPE html>
<html lang="${lang}">
<head>
    <title>${targetUser.user.login}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <link rel="stylesheet" href="<c:url value="/css/st.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/css/theme-teal.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/font-awesome-4.7.0/css/font-awesome.min.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/w3.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/align.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/profile-font-slyle.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/css/font-open-sans.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/font-Roboto.css"/>" type="text/css">
    <script src='<c:url value="/js/jquery.min.js" />'></script>

    <style>
        .modal-body label {
            display: block;
        }
    </style>
</head>


<body class="w3-light-grey">

<div class="main-container">

<!-- Header -->
<c:choose>
    <c:when test="${user eq null}">
        <c:import url="components/unauth_header.jsp"/>
    </c:when>
    <c:otherwise>
        <c:import url="components/auth_header.jsp" />
    </c:otherwise>
</c:choose>

<br><br><br>

<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">

    <!-- The Grid -->
    <div class="w3-row-padding">

        <!-- Left Column -->
        <div class="w3-third">

            <!-- Avatar -->
            <div class="w3-white w3-text-grey w3-card-4">
                <div class="w3-display-container">
                    <img src="<c:url value="${targetUser.user.avatar}"/>" style="width:100%" alt="Avatar">
                    <div class="w3-display-bottomleft w3-container w3-text-black">
                        <h2 id="f_name" contenteditable="false" style="display: inline"><c:out
                                value="${((targetUser.user.fname eq null or fn:length(targetUser.user.fname) == 0) and targetUser.user eq user) ? '---' : targetUser.user.fname}"/></h2>
                        <p id="f_name_span" class="w3-hide" style="font-size:0.7em; color:red;"><fmt:message
                                key="signup.user.info.validity.error_label"/></p>

                        <h2 id="l_name" contenteditable="false" style="display: inline"><c:out
                                value="${((targetUser.user.lname eq null or fn:length(targetUser.user.lname) == 0) and targetUser.user eq user) ? '---' : targetUser.user.lname}"/></h2>
                        <p id="l_name_span" class="w3-hide" style="font-size:0.7em; color:red;"><fmt:message
                                key="signup.user.info.validity.error_label"/></p>
                    </div>

                    <div class="w3-display-bottomright w3-container w3-padding-0 w3-hover-white">
                        <c:if test="${targetUser.user eq user}">
                            <input type="image" src="<c:url value="/avatar/edit_ava.png"/>"
                                   style="height:16px;width:18px"
                                   alt="edit_ava"
                                   class="image-holder">
                            <input type="file" name="new_avatar" id="my_file" style="display: none;">
                        </c:if>
                    </div>
                </div>

                <!-- Profile info -->
                <div id="personal_info" class="w3-container">
                    <hr>
                    <c:if test="${((targetUser.user.login eq null and user eq targetUser.user) or (targetUser.user.login ne null))}">
                        <p><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i>
                            <span id="login"><c:out
                                    value="${(targetUser.user.login eq null or fn:length(targetUser.user.login) == 0) ? '---' : targetUser.user.login}"/></span>
                            <p id="login_span" class="w3-hide" style="font-size:0.7em; color:red;"><fmt:message key="signup.login.duplicate"/>
                        </p>
                    </c:if>

                    <c:if test="${((targetUser.user.email eq null and user eq targetUser.user) or (targetUser.user.email ne null))}">
                        <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i>
                            <span id="email"><c:out
                                    value="${(targetUser.user.email eq null or fn:length(targetUser.user.email) == 0) ? '---' : targetUser.user.email}"/></span>
                            <p id="email_span" class="w3-hide" style="font-size:0.7em; color:red;"> <fmt:message key="signup.email.duplicate"/>
                    </c:if>

                    <c:if test="${((targetUser.user.country eq null and user eq targetUser.user) or (targetUser.user.country ne null))}">
                        <p><i class="fa fa-globe fa-fw w3-margin-right w3-large w3-text-teal"></i>
                            <span id="country"><c:out
                                    value="${(targetUser.user.country eq null or fn:length(targetUser.user.country) == 0) ? '---' : targetUser.user.country}"/></span>
                            <p id="country_span" class="w3-hide" style="font-size:0.7em; color:red;"><fmt:message
                                    key="signup.user.info.validity.error_label"/>
                    </c:if>

                    <c:if test="${((targetUser.user.city eq null and user eq targetUser.user) or (targetUser.user.city ne null))}">
                        <p><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>
                            <span id="city"><c:out
                                    value="${(targetUser.user.city eq null or fn:length(targetUser.user.city) == 0) ? '---' : targetUser.user.city}"/></span>
                            <p id="city_span" class="w3-hide" style="font-size:0.7em; color:red;"><fmt:message
                                    key="signup.user.info.validity.error_label"/>
                    </c:if>

                    <c:if test="${((targetUser.user.company eq null and user eq targetUser.user) or (targetUser.user.company ne null))}">
                        <p><i class="fa fa-suitcase fa-fw w3-margin-right w3-large w3-text-teal"></i>
                            <span id="company"><c:out
                                    value="${(targetUser.user.company eq null or fn:length(targetUser.user.company) == 0) ? '---' : targetUser.user.company}"/></span>
                            <p id="company_span" class="w3-hide" style="font-size:0.7em; color:red;"><fmt:message
                                    key="signup.user.info.job.error_label"/>
                    </c:if>

                    <c:if test="${((targetUser.user.position eq null and user eq targetUser.user) or (targetUser.user.position ne null))}">
                        <p><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>
                            <span id="position"><c:out
                                    value="${(targetUser.user.position eq null or fn:length(targetUser.user.position) == 0) ? '---' : targetUser.user.position}"/></span>
                            <p id="position_span" class="w3-hide" style="font-size:0.7em; color:red;"><fmt:message
                                    key="signup.user.info.job.error_label"/>
                    </c:if>

                    <hr>

                    <!-- Skills -->
                    <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><fmt:message
                            key="profile.skills"/></b></p>

                    <c:forEach var="skill" items="${targetUser.topSkills}">
                        <p><c:out value="${skill.key}"/></p>
                        <div class="w3-progress-container w3-round-xlarge w3-small">
                            <div class="w3-progressbar w3-round-xlarge ${skill.value gt 0 ? "w3-teal" : "w3-red"}"
                                 style="width:<c:out
                                         value="${skill.value lt 0 ? -skill.value * 100 : skill.value * 100}"/>%">
                                <div class="w3-center w3-text-white">
                                    <fmt:formatNumber
                                            value="${skill.value lt 0 ? -skill.value * 100 : skill.value * 100}"
                                            maxFractionDigits="2" /> %
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                    <br>
                </div>
            </div>
            <br>

            <!-- End Left Column -->
        </div>


        <!-- Right Column -->
        <div class="w3-twothird">

            <div class="w3-container w3-card-2 w3-white w3-margin-bottom">
                <h2 class="w3-text-grey w3-padding-16"><i
                        class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i><fmt:message
                        key="profile.recent_activity"/>  ${timezone}</h2>

                <c:forEach var="message" items="${targetUser.lastMessages}" >
                    <div class="w3-container">
                        <h5 class="w3-opacity">
                            <c:choose>
                                <c:when test="${message.type eq 'ANSWER'}">

                                    <a href='<c:url value="/question/${message.question.id}"/>'>
                                        <b><fmt:message key="profile.answered_to_question"/>
                                            <c:out value="${fn:substring(message.question.title, 0, 100)}..."/></b>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <a href='<c:url value="/question/${message.topic.id}"/>'>
                                        <b><fmt:message key="profile.asked_a_question"/>
                                            <c:out value="${fn:substring(message.topic.name, 0, 100)}..."/></b>
                                    </a>

                                </c:otherwise>
                            </c:choose>
                        </h5>
                        <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i><c:out
                                value="${message.type}"/> <span
                                class="w3-tag w3-teal w3-round"><fmt:formatDate
                                                                                value="${message.lastUpdate}"
                                                                                type="BOTH" dateStyle="MEDIUM"
                                                                                timeStyle="MEDIUM"/> </span></h6>
                        <p><c:out value="${fn:substring(message.body, 0, 300)}..."/></p>
                        <hr>
                    </div>
                </c:forEach>


            </div>


            <div class="w3-container w3-card-2 w3-white">
                <header class="w3-container" style="padding-top:22px">
                    <h5><b><i class="fa fa-dashboard"></i> <fmt:message key="profile.statistics"/></b></h5>
                </header>
                <!-- Statistics -->
                <div class="w3-row-padding w3-margin-bottom">

                    <div class="w3-quarter">
                        <div class="w3-container ${targetUser.user.rating > 0 ? "w3-teal" : "w3-red"} w3-padding-8">
                            <div class="w3-left"><i class="fa fa-star w3-xxxlarge"></i></div>
                            <div class="w3-right">
                                <h3><fmt:formatNumber value="${targetUser.user.rating}"/></h3>
                            </div>
                            <div class="w3-clear"></div>
                            <h4><fmt:message key="profile.rating"/></h4>
                        </div>
                    </div>


                    <div class="w3-quarter">
                        <div class="w3-container w3-teal w3-text-white w3-padding-8">
                            <div class="w3-left"><i class="fa fa-question w3-xxxlarge"></i></div>
                            <div class="w3-right">
                                <h3><fmt:formatNumber value="${targetUser.user.questions}"/></h3>
                            </div>
                            <div class="w3-clear"></div>
                            <h4><fmt:message key="profile.questions"/></h4>
                        </div>
                    </div>

                    <div class="w3-quarter">
                        <div class="w3-container w3-teal w3-padding-8">
                            <div class="w3-left"><i class="fa fa-comment w3-xxxlarge"></i></div>
                            <div class="w3-right">
                                <h3><fmt:formatNumber value="${targetUser.user.answers}"/></h3>
                            </div>
                            <div class="w3-clear"></div>
                            <h4><fmt:message key="profile.answers"/></h4>
                        </div>
                    </div>

                    <div class="w3-quarter">
                        <div class="w3-container w3-teal w3-padding-8">
                            <div class="w3-left"><i class="fa fa-eye w3-xxxlarge"></i></div>
                            <div class="w3-right">
                                <h3><fmt:formatNumber value="${targetUser.user.views}"/></h3>
                            </div>
                            <div class="w3-clear"></div>
                            <h4><fmt:message key="profile.views"/></h4>
                        </div>
                    </div>

                    <!-- End Statistics -->
                </div>

                <!-- End Grid -->

            </div>
            <br>


            <c:if test="${(t:allowed(user, 'MODERATE') and user.rightsLevel gt targetUser.user.rightsLevel )
             or ( targetUser.user eq user and t:allowed(user, 'WRITE'))}">
                <div class="w3-container w3-card-2 w3-white">

                    <div class="w3-twothird">
                        <div class="message-tool line" style="margin-top: 2%; margin-right: 2%;">


                            <c:if test="${t:allowed(user, 'MODERATE') and user.rightsLevel gt targetUser.user.rightsLevel}">
                                <button id="open-m-btn" class="button" style="display: inline;"><fmt:message
                                        key="profile.change_rights.button.label"/></button>
                            </c:if>


                            <c:if test="${user eq targetUser.user}">
                                <button id="edit-btn" class="button" style="display: inline;"><fmt:message
                                        key="profile.edit_profile"/></button>
                            </c:if>

                        </div>
                    </div>

                </div>
            </c:if>


        </div>

        <!-- End Page Container -->
    </div>
    <!-- Footer -->


</div>
</div>
<c:import url="components/footer.jsp"/>

<div id="myModal" class="modal" style="width: 40%">
    <form id="change_rights_form" class="modal-content" method="POST" action="#" accept-charset="UTF-8">
        <div class="modal-header">
            <span class="close" onclick="closeModal()">&times;</span>
            <h2><fmt:message key="profile.edit_profile"/></h2>
        </div>
        <div class="modal-body">
            <div class="cb_wrapper">
                <input id="read_cb" type="checkbox"
                       name="read" ${t:allowed(targetUser.user, 'READ') ? 'checked="checked"' : ""}
                       placeholder="<fmt:message key="profile.modal.read.label"/>"/>
                <label for="read_cb" style="display: inline;  color:white;"><fmt:message
                        key="profile.modal.read.label"/></label>
            </div>

            <div class="cb_wrapper">
                <input id="watch_prof_cb" type="checkbox"
                       name="watch_profile" ${t:allowed(targetUser.user, 'WATCH_PROFILE') ? 'checked="checked"' : ""}
                       placeholder="<fmt:message key="profile.modal.watch_profile.label"/>"/>
                <label for="watch_prof_cb" style="display: inline; color:white;"><fmt:message
                        key="profile.modal.watch_profile.label"/></label>
            </div>

            <input id="write_cb" type="checkbox"
                   name="write" ${t:allowed(targetUser.user, 'WRITE') ? 'checked="checked"' : ""}
                   placeholder="<fmt:message key="profile.modal.write.label"/>"/>
            <label for="write_cb" style=" display: inline; color:white; padding-left: 10px;"><fmt:message
                    key="profile.modal.write.label"/></label>

            <div class="cb_wrapper">
                <input id="moderate_cb" type="checkbox"
                       name="moderate" ${t:allowed(targetUser.user, 'MODERATE') ? 'checked="checked"' : ""}
                       placeholder="<fmt:message key="profile.modal.moderate.label"/>"/>
                <label for="moderate_cb" style="display: inline; color:white; "><fmt:message
                        key="profile.modal.moderate.label"/></label>
            </div>

        </div>
        <div class="modal-footer">
            <div class="action-btn">
                <button type="submit" id="send_answer" class="button"><fmt:message
                        key="profile.modal.save"/></button>
                <button type="reset" id="cancel_answer" class="button" onclick="closeModal()"><fmt:message
                        key="profile.modal.cancel"/></button>
            </div>
        </div>
    </form>
</div>

<script>

    var canRead = $(' input[name=read]').prop('checked');
    var canWatch = $('input[name=watch_profile]').prop('checked');
    var canWrite = $('input[name=write]').prop('checked');
    var canModerate = $('input[name=moderate]').prop('checked');


    function redirectIfForbidden(resp, stat, err) {
        if (resp.status == 400) {
            turnSpan(resp, stat, err);
        } else{
            var redirTo = <c:url value='/' />;
            window.location.href = redirTo + resp.status;
        }
    }

    $(document).ready(function () {

        $('#change_rights_form').submit(function (event) {
            var log = $('#login');
            $.ajax({
                type: 'POST',
                url: '<c:url value="/profile/change_rights"/>',
                dataType: "json",
                data: {
                    'targetLogin': log.html(),
                    'read': $('input[name=read]').prop('checked'),
                    'watch_profile': $('input[name=watch_profile]').prop('checked'),
                    'write': $('input[name=write]').prop('checked'),
                    'moderate': $('input[name=moderate]').prop('checked')
                },
                encode: true,
                error: redirectIfForbidden,
                success: function (dat, textStatus, request) {
                    var jsn = JSON.parse(dat);
                    var rights = jsn['user']['rights'];
                    setCb(rights);
                    closeModal();
                }
            });
            event.preventDefault();
        });
    });

    $("input[type='image']").click(function () {
        $("input[id='my_file']").click();
    });

    $("input[id='my_file']").change(function () {
        //load file
        var dat = $("input[id='my_file']")[0].files[0];
        var login = $('#login').html();

        var form = new FormData();
        form.append('login', login);
        form.append('avatar_location', dat);

        $.ajax({
            type: 'POST',
            cache: false,
            contentType: false,
            url: '<c:url value="/profile/upload_avatar"/>',
            data: form,
            encode: true,
            processData: false,
            success: function (dat, stat, resp) {
                window.location.reload();
            },
            error: redirectIfForbidden
        });
    });

    function setAtr(elem, atr, val) {
        if (val == false) {
            $(elem).prop(atr, false);
        } else
            $(elem).prop(atr, true)
    }

    function setCb(rights) {
        canWrite = $.inArray('WRITE', rights) >= 0;
        canWatch = $.inArray('WATCH_PROFILE', rights) >= 0;
        canModerate = $.inArray('MODERATE', rights) >= 0;
        canRead = $.inArray('READ', rights) >= 0;
    }

    function openModal() {
        var target = document.getElementById("myModal");
        resetCb();
        target.style.display = "block";
    }


    function closeModal() {
        var parent = document.getElementById("myModal");
        parent.style.display = "none";
    }

    function resetCb() {
        var atr = 'checked';
        setAtr('input[name=read]', atr, canRead);
        setAtr('input[name=watch_profile]', atr, canWatch);
        setAtr('input[name=write]', atr, canWrite);
        setAtr('input[name=moderate]', atr, canModerate);
    }


    function setEditable(value) {
        // $('#login').attr('contenteditable', value);
        $('#city').attr('contenteditable', value);
        $('#email').attr('contenteditable', value);
        $('#country').attr('contenteditable', value);
        $('#company').attr('contenteditable', value);
        $('#position').attr('contenteditable', value);
        $('#f_name').attr('contenteditable', value);
        $('#l_name').attr('contenteditable', value);
    }


    function clearValidity() {
        $('#login_span').addClass("w3-hide");
        $('#email_span').addClass("w3-hide");
        $('#f_name_span').addClass("w3-hide");
        $('#l_name_span').addClass("w3-hide");
        $('#country_span').addClass("w3-hide");
        $('#city_span').addClass("w3-hide");
        $('#company_span').addClass("w3-hide");
        $('#position_span').addClass("w3-hide");
    }

    var turnSpan = function (xhr, stat, err) {
        //noinspection JSUnresolvedVariable
        var jsn = JSON.parse(xhr.responseJSON);

        clearValidity();

        for (var i = 0; i < jsn.length; i++) {
            console.log(jsn[i]);
            $('#' + jsn[i]).removeClass("w3-hide");
        }

    };


    function saveProfileChanges() {
        $.ajax({
            type: 'POST',
            url: '<c:url value="/profile/edit"/>',
            dataType: "json",
            data: {
                'login': $('#login').html().replace(/---/, ''),
                'f_name': $('#f_name').html().replace(/---/, ''),
                'l_name': $('#l_name').html().replace(/---/, ''),
                'country': $('#country').html().replace(/---/, ''),
                'city': $('#city').html().replace(/---/, ''),
                'company': $('#company').html().replace(/---/, ''),
                'position': $('#position').html().replace(/---/, ''),
                'email': $('#email').html()
            },
            encode: true,
            success: function (dat, stat, resp) {
                clearValidity();
                var jsn = JSON.parse(dat);
                $("#l_name").innerHTML = jsn["user"]["l_name"];
                $("#f_name").innerHTML = jsn["user"]["f_name"];
                $("#country").innerHTML = jsn["user"]["country"];
                $("#city").innerHTML = jsn["user"]["city"];
                $("#company").innerHTML = jsn["user"]["company"];
                $("#position").innerHTML = jsn["user"]["position"];
                $("#email").innerHTML = jsn["user"]["email"];

            },
            error: redirectIfForbidden
        })
    }

    function switchEdit(invoker) {
        if (invoker.innerHTML == "<fmt:message key="profile.edit_profile"/>") {
            setEditable(true);
            invoker.innerHTML = "<fmt:message key="profile.modal.save"/>";
        } else {
            setEditable(false);
            saveProfileChanges();
            invoker.innerHTML = "<fmt:message key="profile.edit_profile"/>";
        }
    }


    $('#open-m-btn').click(function () {
        openModal();
    });

    $('#edit-btn').click(function () {
        switchEdit(this);
    });


</script>

</body>


</html>
