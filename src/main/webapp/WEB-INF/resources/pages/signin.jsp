<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--@elvariable id="lang" type="java.lang.String"--%>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.loc"/>


<!DOCTYPE html>
<html lang="${lang}">
<head>
    <title><fmt:message key="signup.title"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);
    function hideURLbar() {
        window.scrollTo(0, 1);
    } </script>
    <link rel='stylesheet' href="<c:url value="/css/w3.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/align.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/font-awesome-4.7.0/css/font-awesome.min.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/css/style.css"/>" type="text/css" media="all">
    <link rel="stylesheet" href="<c:url value="/css/font-athiti.css"/>" type="text/css">
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--%>
    <script src='<c:url value="/js/jquery.min.js" />'></script>
    <style>
        input:invalid[name=username] + span::after {
            content: "<fmt:message key="signup.login.validity.wrong_label"/>";
            color: #e89406;
        }

        input:invalid[name=password] + span::after {
            content: "<fmt:message key="signup.password.validity.wrong_label"/>";
            color: #e89406;
        }
    </style>
</head>

<body>
<c:import url="components/unauth_transparent_header.jsp"/>
<!-- main -->
<div class="main-agile">
    <div id="w3ls_form" class="signin-form">
        <!-- Sign In Form -->
        <form id="signin_form" action="#" method="post">
            <div class="ribbon"><a href="<c:url value="/signup"/>" id="flipToRecover" class="flipLink"
                                   title="<fmt:message key="login.signup.hover"/>">
                <fmt:message key="login.signup"/></a>
            </div>
            <h2><fmt:message key="login.title"/></h2>

            <p><fmt:message key="login.login"/></p>
            <input type="text" name="username" placeholder="Login" required maxlength=30
                   pattern="^\w{4,}$"/>
            <span></span>
            <span id="login_span" class="hide"><fmt:message key="signup.login.validity.error_label"/></span>

            <p><fmt:message key="login.password"/></p>
            <input type="password" name="password" maxlength=32 placeholder="Password" required/>
            <span></span>
            <span id="auth_span" class="hide"><fmt:message key="signin.auth.wrong"/></span>

            <br>
            <input type="submit" value="<fmt:message key="login.submit"/>">

            <!-- copyright -->
            <div class="copyright">
                <p> © 2016 Modern Flip Sign In Form . All rights reserved | Design by <a href="http://w3layouts.com/"
                                                                                         target="_blank">W3layouts</a></p>
            </div>
        </form>
        <!-- //Sign In Form -->

    </div>

    <!-- //copyright -->
</div>
<!-- //main -->

<script>
    var password = document.getElementsByName("password")[0];

    /*Check if password contains at least 1 char in UPPER, 1 char in DOWN register and 1 digit character.
     The length must be equal to 6 or greater*/
    function testStrongPass() {
        var pattern = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})");
        var res = pattern.test(password.value);
        if (res == true) {
            password.setCustomValidity('');
        } else {
            password.setCustomValidity('');
        }
    }

    //binding events
    password.onkeyup = testStrongPass;

    function clearValidity() {
        $('#auth_span').addClass("hide");
    }

    var turnSpan = function (xhr, stat, err) {
        //noinspection JSUnresolvedVariable
        var jsn = JSON.parse(xhr.responseJSON);

        clearValidity();

        for (var i = 0; i < jsn.length; i++){
            console.log(jsn[i]);
            $('#' + jsn[i]).removeClass("hide");
        }

    };


    $(document).ready(function () {
        $('#signin_form').submit(function (event) {

            $.ajax({
                type: 'POST',
                url: '<c:url value="auth/login"/>',
                dataType: 'json',
                data: {
                    'login': $('input[name=username]').val(),
                    'password': $('input[name=password]').val(),
                },
                encode: true,
                error: turnSpan,
                success: function (dat, textStatus, request) {
                    $('#signin_form')[0].reset();
                    //noinspection JSUnresolvedVariable
                    var location = JSON.parse(dat);


                    location = location.replace(/"/g, "");
                    window.location.href = location;
                }
            });

            event.preventDefault();
        });
    });


</script>
</body>
</html>