<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="/WEB-INF/tools.tld" %>
<%--@elvariable id="lang" type="java.lang.String"--%>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.loc"/>
<fmt:setTimeZone value="${cookie eq null or cookie['timezone'] eq null ? 'GMT' : cookie['timezone'].value}" />


<%--@elvariable id="topic" type="by.epam.likeit.bean.view.TopicView"--%>
<c:set scope="request" var="topic" value="${topic}"/>
<section id="questions_table" class="message-wrapper">
    <table class="overflow-auto">
        <tr>
            <th width="35%"><fmt:message key="questions.title"/></th>
            <th width="15%"><fmt:message key="questions.user"/></th>
            <th width="15%"><fmt:message key="questions.publicated"/></th>
            <th width="15%"><fmt:message key="questions.answers"/></th>
            <th width="15%"><fmt:message key="questions.views"/></th>
            <th width="5%"><fmt:message key="questions.is_closed"/></th>
        </tr>
        <c:forEach var="question" items="${topic.questions}">
            <tr data-qid="${question.id}">
                <td><a href="<c:url value="/question/${question.id}"/>">${question.title}</a></td>
                <td>
                    <a href="<c:url value="/profile/${question.user.login}"/>">${question.user.login}</a>
                </td>
                <td><fmt:formatDate value="${question.date}" type="BOTH" dateStyle="MEDIUM" timeStyle="SHORT"/></td>
                <td><fmt:formatNumber value="${question.answersCount}"/></td>
                <td><fmt:formatNumber value="${question.viewed}"/></td>
                <td width="2%" title="remove">${t:isClosed(question) ? 'Y' : 'N'}</td>
            </tr>
        </c:forEach>

    </table>
</section>

<%--@elvariable id="topics" type="java.util.List"--%>
<jsp:useBean id="curPage" scope="request" type="java.lang.Long"/>
<jsp:useBean id="maxPage" scope="request" type="java.lang.Long"/>
<c:set var="pagePart" scope="request" value=
            '<li>
                <button class="button %s" style="display: inline-block" onclick="sortQuestions(%s)">%s</button>
            </li>'/>

<div class="pagination">
    <ul class="w3-pagination w3-padding-32 message-tool">

        <t:pagination curPage="${curPage}" maxPage="${maxPage}" curStyle="w3-teal" item="${pagePart}"
                      locale="${lang}" showEdge="${true}" rightEdge="2" leftEdge="2"/>

    </ul>
</div>