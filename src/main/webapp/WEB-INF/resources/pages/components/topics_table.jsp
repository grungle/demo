<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="/WEB-INF/tools.tld" %>

<%--@elvariable id="lang" type="java.lang.String"--%>
<c:set scope="session" var="lang" value="${lang}"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.loc"/>
<fmt:setTimeZone value="${cookie eq null or cookie['timezone'] eq null ? 'GMT' : cookie['timezone'].value}" />
<%--@elvariable id="topics" type="java.util.List"--%>
<c:set scope="request" var="topics" value="${topics}"/>

<%--@elvariable id="user" type="by.epam.likeit.bean.User"--%>
<c:set scope="request" var="user" value="${user}"/>

<section id="topic_wrapper" class="message-wrapper" style="display: block; ">

    <table id="topics_table" style="overflow: auto;">
        <tr>
            <th style="width:44%;"><fmt:message key="topics.sortby_name"/></th>
            <th style="width:24%;"><fmt:message key="topics.sortby_questions"/></th>
            <th width="47%" ><fmt:message key="topics.last_update"/></th>
            <th class="remove"></th>
        </tr>
        <c:forEach var="topic" items="${topics}">
            <tr data-tid="${topic.id}">

                <td width="45%"><a href="<c:url value="/topic/${topic.id}"/>">${topic.name}</a></td>
                <td width="23%"><fmt:formatNumber value="${topic.questions}"/></td>
                <td width="23%"><fmt:formatDate value="${topic.updated}" type="BOTH" dateStyle="MEDIUM"
                                                timeStyle="SHORT"/></td>
                <c:if test="${t:allowed(user, 'ADMIN')}">
                    <td class="remove" title="remove" onclick="deleteTopic(this)">x</td>
                </c:if>
            </tr>
        </c:forEach>
    </table>
</section>

<%--@elvariable id="topics" type="java.util.List"--%>
<c:set scope="request" var="topics" value="${topics}"/>
<jsp:useBean id="curPage" scope="request" type="java.lang.Long"/>
<jsp:useBean id="maxPage" scope="request" type="java.lang.Long"/>
<c:set var="pagePart" scope="request" value=
        '<li>
                <button class="button %s" style="display: inline-block" onclick="sortTopics(%s)">%s</button>
            </li>'/>
<div class="pagination">
    <ul class="w3-pagination w3-padding-32 message-tool">

        <t:pagination curPage="${curPage}" maxPage="${maxPage}" curStyle="w3-teal" item="${pagePart}"
                      locale="${lang}" showEdge="${true}" rightEdge="2" leftEdge="2"/>

    </ul>
</div>

