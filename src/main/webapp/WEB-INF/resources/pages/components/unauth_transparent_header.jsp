<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${lang}" />
<fmt:setBundle basename="i18n.loc" />

<div class="w3-top">
    <ul class="w3-navbar w3-left-align w3-large dropdown-menu-right">

        <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
            <a class="w3-padding-large w3-hover-amber w3-large"><i class="fa fa-globe"></i></a>
        </li>

        <li class="w3-hide-small w3-dropdown-hover w3-right dropdown-hover-left pull-right">
            <a href="#" class="w3-padding-large w3-hover-amber" title="<fmt:message key="header.language"/>"><i class="fa fa-globe"></i></a>
            <div class="dropdown-menu-right">
                <div class="w3-dropdown-content w3-amber pull-right">
                    <a href="?lang=en_US">English</a>
                    <a href="?lang=ru_RU">&#x0420&#x0443&#x0441&#x0441&#x043a&#x0438&#x0439</a>
                    <a href="?lang=fr_FR">&#x0046&#x0072&#x0061&#x006e&#x00e7&#x0061&#x0069&#x0073</a>
                    <a href="?lang=zh_CH">&#x4e2d&#x570b</a>
                    <a href="?lang=de">Deutsch</a>
                </div>
            </div>
        </li>

    </ul>
</div>