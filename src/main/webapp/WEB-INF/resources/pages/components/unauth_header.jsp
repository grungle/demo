<%@ page contentType="text/html; UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${lang}" />
<fmt:setBundle basename="i18n.loc" />

<head lang="${lang}">
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
    <link rel="stylesheet" href="<c:url value="/css/theme-teal.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/font-awesome-4.7.0/css/font-awesome.min.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/w3.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/align.css"/>" type="text/css">
</head>

<c:import url="components/unauth_transparent_header.jsp" />


<!-- Header -->
<header class="w3-container w3-theme w3-padding" id="myHeader" style="height:35%;">
    <div class="w3-center"><br><br>
        <h2><fmt:message key="main.greeting"/></h2><br>
        <p class="w3-opacity">Register or login:</p><br>

        <div class="w3-center">
            <!-- The buttons stick together when resizing window -->
            <button class="w3-btn w3-xlarge w3-theme-dark w3-hover-teal w3-animate-left"
                    style="font-weight:900;width:auto;margin:auto">
                <a href="<c:url value="/signup" />" style="text-decoration:none;"><fmt:message key="main.signup"/></a>
            </button>
            &nbsp;
            <button class="w3-btn w3-xlarge w3-theme-dark w3-hover-teal w3-animate-right"
                    style="font-weight:900;width:auto;margin:auto">
                <a href="<c:url value="/signin" />" style="text-decoration:none;"><fmt:message key="main.signin"/></a>
            </button>
            &nbsp;
        </div>
    </div>
</header>

<%--<jsp:include page="components/footer.jsp"/>--%>
