<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="lang" type="java.lang.String"--%>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.loc"/>
<%--@elvariable id="user" type="by.epam.likeit.bean.User"--%>
<c:set var="user" value="${user}" scope="session"/>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<c:url value="/js/timezones.full.min.js" /> "></script>
</head>

<div class="w3-top">
    <ul class="w3-navbar w3-theme-d2 w3-left-align w3-large">
        <li class="w3-hide-medium w3-hide-large w3-opennav w3-right">
            <a class="w3-padding-large w3-hover-white w3-large w3-theme-d2"><i class="fa fa-bars"></i></a>
        </li>

        <!-- Home -->
        <li><a href="<c:url value="/profile/${user.login}"/>" class="w3-padding-large w3-theme-d4"><i
                class="fa fa-home w3-margin-right"></i><fmt:message key="header.home"/></a></li>

        <!-- Language -->
        <li class="w3-hide-small w3-dropdown-hover">
            <a href="#" class="w3-padding-large w3-hover-white" title="Language"><i class="fa fa-globe"></i></a>
            <div class="w3-dropdown-content w3-white w3-card-4">
                <a href="?lang=en_US">English</a>
                <a href="?lang=ru_RU">&#x0420&#x0443&#x0441&#x0441&#x043a&#x0438&#x0439</a>
                <a href="?lang=fr_FR">&#x0046&#x0072&#x0061&#x006e&#x00e7&#x0061&#x0069&#x0073</a>
                <a href="?lang=zh_CH">&#x4e2d&#x570b</a>
                <a href="?lang=de">Deutsch</a>
            </div>
        </li>

        <!-- Messages -->
        <li class="w3-hide-small"><a href="<c:url value="/topics"/>" class="w3-padding-large w3-hover-white"
                                     title="Messages"><i
                class="fa fa-envelope"></i></a></li>

        <li>
            <label class="w3-right">
                <select id="tz_select" onchange="tzchanged()" class="form-control"></select>
            </label>
        </li>

        <!-- Login/Logout -->
        <li class="w3-hide-small w3-dropdown-hover w3-right">
            <a href="#" class="w3-padding-large w3-hover-white" title="Log out">
                <img src="<c:url value="${user.avatar}"/>" class="w3-circle" style="height:25px;width:35px"
                     alt="small_avatar"></a>
            <div class="w3-dropdown-content w3-white w3-card-4 pull-right">
                <a href="#" id="logout_btn"><fmt:message key="profile.logout"/></a>
            </div>
        </li>

    </ul>
</div>

<script>
    $('select').timezones();

    $(document).ready(function () {
        $('#logout_btn').click(function (event) {
            $.ajax({
                type: 'POST',
                url: '<c:url value="/auth/logout"/>',
                encode: true,
                success: function (dat, textStatus, response) {
                    window.location.href = response.getResponseHeader("Location");
                }

            });

        });

    });

    function tzchanged() {
        var sel = $('#tz_select').find('option:selected');
        var val = sel[0].dataset.offset;
        $.ajax({
            type: 'POST',
            data: {
                'timezone': val
            },
            url: '<c:url value="/config/timezone"/>',
            encode: true,
            success: function () {}

        });

    }

</script>
