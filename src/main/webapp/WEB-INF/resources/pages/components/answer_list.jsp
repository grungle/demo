<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" uri="/WEB-INF/tools.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="lang" type="java.lang.String"--%>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.loc"/>
<fmt:setTimeZone value="${cookie eq null or cookie['timezone'] eq null ? 'GMT' : cookie['timezone'].value}" />
<%--@elvariable id="user" type="by.epam.likeit.bean.User"--%>
<c:set scope="session" var="user" value="${user}"/>
<%--@elvariable id="question" type="by.epam.likeit.bean.view.QuestionView"--%>
<c:set scope="request" var="question" value="${question}"/>
<%--@elvariable id="curPage" type="java.lang.Long"--%>
<c:set scope="request" var="curPage" value="${curPage}"/>
<%--@elvariable id="maxPage" type="java.lang.Long"--%>
<c:set scope="request" var="maxPage" value="${maxPage}"/>

<div id="q_wrapper">
    <section>
        <article class="message-wrapper" data-mid="${question.question.id}" data-type="q">
            <div class="user-info">
                <figure class="circle-avatar-medium">
                    <a href="<c:url value="/profile/${question.question.user.login}"/>">
                        <img src="<c:url value="${question.question.user.avatar}"/>" alt="avatar"/>
                    </a>
                    <figcaption class="ava-caption">
                        <a href="<c:url value="/profile/${question.question.user.login}"/>">${question.question.user.login}</a>
                    </figcaption>
                </figure>
            </div>

            <div class="message">
                <h2 class="message-header" contentEditable="false"> <c:out value="${question.question.title}" escapeXml="true" /></h2>
                <p>
                    <c:out value='${question.question.body}' escapeXml="true"/>
                </p>

                <div class="meta-info left-align">
                    <p><fmt:message key="question.publicated_at"/> <fmt:formatDate value="${question.question.date}" type="BOTH"
                                                                                   dateStyle="SHORT"
                                                                                   timeStyle="MEDIUM"/></p>
                </div>


                <c:choose>
                    <c:when test="${t:checkQuestionState(question.question, 'MODIFIED')}">
                        <div class="meta-info right-align">
                            <p><fmt:message key="question.last_modified_at"/> <fmt:formatDate
                                    value="${question.question.lastUpdate}" type="BOTH" dateStyle="SHORT"
                                    timeStyle="MEDIUM"/>
                            </p>
                        </div>
                    </c:when>
                    <c:when test="${t:checkQuestionState(question.question, 'MODERATED')}">
                        <div class="meta-info right-align">
                            <p><fmt:message key="question.last_moderated_at"/>
                                <fmt:formatDate value="${question.question.lastUpdate}" type="BOTH" dateStyle="SHORT"
                                                timeStyle="SHORT"/> <fmt:message key="question.moderated_by"/>
                                <a style="text-decoration:underline;"
                                   href="<c:url value="/profile/${question.question.moderationInfo.moderator.login}"/>">
                                        ${question.question.moderationInfo.moderator.login}
                                </a>
                            </p>
                        </div>
                    </c:when>
                </c:choose>
            </div>


            <div class="message-tool full-width">
                <c:if test="${question.question.user eq user and t:allowed(user, 'WRITE') and
                        not (t:checkQuestionState(question.question, 'MODERATED') or t:checkQuestionState(question.question, 'CLOSED'))}">
                    <button class="button" onclick="edit(this)"><fmt:message key="question.edit"/></button>
                </c:if>
                <c:if test="${t:allowed(user, 'MODERATE')}">
                    <button class="button" onclick="removeQuestion(this)"><fmt:message key="question.remove"/></button>
                </c:if>
                <c:choose>
                    <c:when test="${t:checkQuestionState(question.question, 'CLOSED')}">
                        <button id="an-btn" class="button" disabled
                                onclick="answer()"><fmt:message key="question.closed"/></button>
                    </c:when>
                    <c:when test="${t:allowed(user, 'WRITE')}">
                        <button id="an-btn" class="button" onclick="answer()"><fmt:message
                                key="question.answer"/></button>
                    </c:when>
                </c:choose>

                <c:if test="${t:allowed(user, 'MODERATE')}">
                    <button class="button" onclick="moderate(this)"><fmt:message key="question.moderate"/></button>
                </c:if>
                <c:if test="${(not t:checkQuestionState(question.question, 'CLOSED')) and (t:allowed(user, 'MODERATE') or question.question.user eq user)}">
                    <button id="close-btn" class="button" onclick="closeQuestion(this)"><fmt:message
                            key="question.close"/></button>
                </c:if>
            </div>

        </article>


        <div id="answers_wrapper">
            <c:forEach var="answer" items="${question.answers}">
                <article class="message-wrapper" data-mid="${answer.answer.id}" data-type="a">

                    <div class="mark">
                        <div class="mark-control">
                            <c:if test="${answer.answer.user ne user}">
                                 <img data-mark-val="1" src="<c:url value="/css/arrow-up.png"/>" alt="upvote"
                                     onclick="vote(this)"/>
                            </c:if>

                            <p <c:if test="${answer.answer.user eq user}">style="padding-top: 80px;"</c:if> >${answer.answer.rating}</p>

                            <c:if test="${user ne answer.answer.user}">
                                <img data-mark-val="-1" src="<c:url value="/css/arrow-down.png"/>" alt="downvote"
                                    onclick="vote(this)"/>
                            </c:if>
                        </div>
                        <div class="mark-number">${answer.mark}</div>
                    </div>

                    <div class="user-info">
                        <figure class="circle-avatar-medium">
                            <a href="<c:url value="/profile/${answer.answer.user.login}"/>">
                                <img src="<c:url value="${answer.answer.user.avatar}"/>" alt="${answer.answer.user.login}'s ava"/>
                            </a>
                            <figcaption class="ava-caption">
                                <a href="<c:url value="/profile/${answer.answer.user.login}"/>">${answer.answer.user.login}</a>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="message">
                        <p>
                            <c:out  value='${answer.answer.body}' escapeXml="true"/>
                        </p>
                        <div class="meta-info left-align">
                            <p><fmt:message key="question.publicated_at"/> <fmt:formatDate value="${answer.answer.date}"
                                                                                           type="BOTH" dateStyle="SHORT"
                                                                                           timeStyle="MEDIUM"/></p>
                        </div>


                        <c:choose>
                            <c:when test="${t:checkAnswerState(answer.answer, 'MODERATED')}">
                                <div class="meta-info right-align">
                                    <p>
                                        <fmt:message key="question.last_moderated_at"/>
                                        <fmt:formatDate value="${answer.answer.lastUpdate}" type="BOTH" dateStyle="SHORT"
                                                        timeStyle="MEDIUM"/>
                                        <fmt:message key="question.moderated_by"/>
                                        <a style="text-decoration:underline;"
                                           href="<c:url value="/profile/${answer.answer.moderationInfo.moderator.login}" />" >
                                                ${answer.answer.moderationInfo.moderator.login}</a>
                                    </p>
                                </div>
                            </c:when>
                            <c:when test="${t:checkAnswerState(answer.answer, 'MODIFIED')}">
                                <div class="meta-info right-align">
                                    <p>
                                        <fmt:message key="question.last_modified_at"/> <fmt:formatDate
                                            value="${answer.answer.lastUpdate}"/>
                                    </p>
                                </div>
                            </c:when>
                        </c:choose>


                    </div>
                    <div class="message-tool full-width">
                        <c:if test="${answer.answer.user eq user and t:allowed(user, 'WRITE') and (not t:checkAnswerState(answer.answer, 'MODERATED'))}">
                            <button class="button" onclick="edit(this)"><fmt:message key="question.edit"/></button>
                        </c:if>
                        <c:if test="${t:allowed(user, 'MODERATE')}">
                            <button class="button" onclick="moderate(this)"><fmt:message
                                    key="question.moderate"/></button>
                        </c:if>
                        <c:if test="${(t:allowed(user, 'WRITE') and answer.answer.user eq user) or (t:allowed(user,'MODERATE'))}">
                            <button class="button" onclick="removeAnswer(this)"><fmt:message
                                    key="question.remove"/></button>
                        </c:if>
                    </div>
                </article>
            </c:forEach>
        </div>

    </section>

    <c:set var="curPage" scope="request" value="${curPage}"/>
    <c:set var="maxPage" scope="request" value="${maxPage}"/>
    <c:set var="pagePart" scope="request" value=
            '<li>
                <button class="button %s" style="display: inline-block" onclick="sortAnswers(%s)">%s</button>
            </li>' />

    <div class="pagination">
        <ul class="w3-pagination w3-padding-32 message-tool">

            <t:pagination curPage="${curPage}" maxPage="${maxPage}" curStyle="w3-teal" item="${pagePart}"
                          locale="${lang}" showEdge="${true}" rightEdge="2" leftEdge="2" />

        </ul>
    </div>
</div>