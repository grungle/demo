<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang}" />
<fmt:setBundle basename="i18n.loc" />

<div class="qwert" style="margin-top: -70px;">
<footer class="w3-container w3-theme-d2 w3-center w3-margin-top" style=" bottom: 0;" >
    <p><fmt:message key="footer.join_us_sm"/></p>
    <a href="https://facebook.com"><i class="fa fa-facebook-official w3-hover-text-indigo w3-large"></i></a>
    <a href="https://instagram.com"><i class="fa fa-instagram w3-hover-text-purple w3-large"></i></a>
    <a href="https://www.snapchat.com/"><i class="fa fa-snapchat w3-hover-text-yellow w3-large"></i></a>
    <a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p w3-hover-text-red w3-large"></i></a>
    <a href="https://www.twitter.com/"><i class="fa fa-twitter w3-hover-text-light-blue w3-large"></i></a>
    <a href="https://www.linkedin.com/"><i class="fa fa-linkedin w3-hover-text-indigo w3-large"></i></a>
</footer>
</div>
