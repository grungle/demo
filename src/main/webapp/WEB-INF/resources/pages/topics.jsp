<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" uri="/WEB-INF/tools.tld" %>
<%--@elvariable id="lang" type="java.lang.String"--%>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.loc"/>
<fmt:setTimeZone value="${cookie eq null or cookie['timezone'] eq null ? 'GMT' : cookie['timezone'].value}" />
<%--@elvariable id="user" type="by.epam.likeit.bean.User"--%>
<c:set scope="session" var="user" value="${user}"/>
<%--@elvariable id="topics" type="java.util.List"--%>
<c:set scope="request" var="topics" value="${topics}"/>

<!DOCTYPE html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<fmt:message key="main.title" />">
    <meta name="author" content="Pavel Bachilo">
    <link rel="stylesheet" href="<c:url value="/css/theme-teal.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/font-awesome-4.7.0/css/font-awesome.min.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/w3.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/css/st.css"/>" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Taviraj:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <script src='<c:url value="/js/jquery.min.js" />'></script>
    <title><fmt:message key="topics.title" /></title>
</head>


<body class="info" style="background-color:whitesmoke; min-height: 100%;">
<div class="main-container">
<c:choose>
    <c:when test="${user eq null}">
        <c:import url="components/unauth_header.jsp"/>
    </c:when>
    <c:otherwise>
        <c:import url="components/auth_header.jsp"/>
    </c:otherwise>
</c:choose>

<div class="topic-manage" style="padding-top:1%;margin-top:0;display:block;">

    <div id="navigation-map" class="line">
        <fmt:message key="topics.all_topics"/>
    </div>

    <c:if test="${user ne null and t:allowed(user, 'ADMIN')}">
        <div id="nav-tool" class="message-tool line" style="padding: 0; margin: 0;">
            <button class="button" onclick="openModal()"><fmt:message key="topics.new"/></button>
        </div>
    </c:if>

    <div class="select-wrapper">
        <span><fmt:message key="topics.sortby"/> </span>
        <label>
            <select id="sort_field" class="select-sort button" onchange="sortTopics(1)">
                <option selected value="name"><fmt:message key="topics.sortby_name"/></option>
                <option value="updated"><fmt:message key="topics.sortby_date"/></option>
                <option value="questions"><fmt:message key="topics.sortby_questions"/></option>
            </select>
        </label>
    </div>

    <div class="select-wrapper">
        <label>
            <select id="sort_order" class="select-sort button" onchange="sortTopics(1)">
                <option selected value="ASC"><fmt:message key="topics.asc_sort"/></option>
                <option value="DESC"><fmt:message key="topics.desc_sort"/></option>
            </select>
        </label>
    </div>

</div>


<div id="wr"  style=" padding-right: -30px; margin-right: -30px;">
    <c:import url="components/topics_table.jsp"/>
</div>



<div id="myModal" class="modal">
    <form id="create_topic_form" class="modal-content" method="post" action="#" accept-charset="UTF-8">
        <div class="modal-header">
            <span class="close" onclick="closeModal()">&times;</span>
            <h2><fmt:message key="topics.modal.new_topic"/></h2>
        </div>
        <div class="modal-body">
            <textarea form="create_topic_form" name="topic_name"
                      placeholder="<fmt:message key="topics.modal.topicname_placeholder"/>" maxlength=250
                      required></textarea>
        </div>
        <div class="modal-footer">
            <div class="action-btn">
                <button type="submit" id="send_answer" class="button"><fmt:message key="topics.modal.create"/></button>
                <button type="reset" id="cancel_answer" class="button" onclick="closeModal()"><fmt:message
                        key="topics.modal.cancel"/></button>
            </div>
        </div>
    </form>
</div>
</div>
<jsp:include page="components/footer.jsp"/>

<script>

    var curPage = 1;

    function redirectIfForbidden(resp, stat, err) {
        var redirTo = <c:url value='/' />;
        window.location.href = redirTo + resp.status;
    }

    function openModal() {
        var target = document.getElementById("myModal");
        target.style.display = "block";
    }

    function closeModal() {
        var parent = document.getElementById("myModal");
        parent.style.display = "none";
    }

    $(document).ready(function () {
        $('#create_topic_form').submit(function (event) {
            event.preventDefault();
            $.ajax({
                type: 'POST',

                url: '<c:url value="/topics/new"/>',
                data: {'tName': $('textarea[name=topic_name]').val()},
                encode: true,
                complete: function () {
                    window.closeModal();
                    $('#create_topic_form')[0].reset();
                },
                success: function (dat, stat, resp) {
                    window.location.href = resp.getResponseHeader("Location");
                }
            });
        });
    });

    function deleteTopic(elem) {
        var topicId = parseInt(elem.parentElement.getAttribute("data-tid"));
        $.ajax({
            type: 'POST',
            url: '<c:url value="/topics/delete"/>',
            data: {
                'tId': topicId,
                'sortBy': $('#sort_field').find('option:selected').val(),
                'orderBy': $('#sort_order').find('option:selected').val(),
                'page': curPage
            },
            success: function (dat) {
                var table = document.getElementById('wr');
                table.innerHTML = dat;
            }
        })
    }

    function sortTopics(page, recordsOnPage) {
        curPage = page;
        $.ajax({
            type: 'GET',
            url: '<c:url value="/topics/reload_all"/>',
            data: {
                'sortBy': $('#sort_field').find('option:selected').val(),
                'orderBy': $('#sort_order').find('option:selected').val(),
                'page': (page === null || typeof page === typeof undefined) ? '1' : page
            },
            success: function (dat) {
                var table = document.getElementById('wr');
                table.innerHTML = dat;
            }
        })
    }

</script>

</body>


</html>
