<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--@elvariable id="lang" type="java.lang.String"--%>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.loc"/>

<!DOCTYPE html>
<html lang="${lang}">


<head>
    <title><fmt:message key="signup.title"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel='stylesheet' href="<c:url value="/css/w3.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/css/style.css"/>" type="text/css" media="all"/>
    <link rel='stylesheet' href="<c:url value="/css/font-athiti.css"/>" type="text/css"/>
    <link rel='stylesheet' href="<c:url value="/css/font-awesome-4.7.0/css/font-awesome.min.css"/>" type="text/css"/>
    <link rel='stylesheet' href="<c:url value="/css/align.css"/>" type="text/css"/>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--%>
    <script src='<c:url value="/js/jquery.min.js" />'></script>
    <script src="<c:url value="/js/validation.js"/>"></script>
    <style>
        input:invalid[name=login] + span::after {
            content: "<fmt:message key="signup.login.validity.error_label"/>";
            color: #e89406;
        }

        input:invalid[type=email] + span::after {
            content: "<fmt:message key="signup.email.validity.error_label"/>";
            color: #e89406;
            display: inline;
        }

        input:invalid[name=password] + span::after {
            content: "<fmt:message key="signup.password.validity.error_weak_label"/>";
            color: #e89406;
        }

        input:invalid[name=confirm_password] + span::after {
            content: "<fmt:message key="signup.password.validity.confirm_label"/>";
            color: #e89406;
        }

        input:optional:invalid[name=country] + span::after,
        input:optional:invalid[name=l_name] + span::after,
        input:optional:invalid[name=f_name] + span::after {
            content: "<fmt:message key="signup.user.info.validity.error_label"/>";
            color: #e89406;
        }

        input:optional:invalid[name=city] + span::after {
            content: "<fmt:message key="signup.city.validity.error_label"/>";
            color: #e89406;
        }

        input:optional:invalid[name=position] + span::after,
        input:optional:invalid[name=company] + span::after {
            content: "<fmt:message key="signup.user.info.job.error_label"/>";
            color: #e89406;
        }
    </style>

</head>

<body>

<%--<c:import url="components/unauth_transparent_header.jsp"/>--%>

<div class="main-agile">
    <c:import url="components/unauth_transparent_header.jsp"/>
    <div id="w3ls_form" class="signin-form signup_frm">
        <form id="signup_form" autocomplete="off" action="#" onsubmit="return false;" method="post">

            <div class="ribbon">
                <a href="<c:url value="/signin"/>" id="flipToRecover1" class="flipLink" title="<fmt:message key="signup.login.hover"/>">
                    <fmt:message key="signup.signin"/>
                </a>
            </div>

            <h3><fmt:message key="signup.regform"/></h3>

            <p><fmt:message key="signup.login"/></p>
            <input type="text" name="login" placeholder="Login(5-30 chars)" required maxlength=30
                   pattern="^[a-zA-Z]{1}\w{4,}$"/>
            <span></span>
            <span id="login_span" class="hide"><fmt:message key="signup.login.duplicate"/></span>

            <p><fmt:message key="signup.email"/></p>
            <input type="email" name="email" placeholder="fname_lname@epam.com" maxlength=200 required
                   pattern="^\w+@\w+\.\w+$"/>
            <span></span>
            <span id="email_span" class="hide"><fmt:message key="signup.email.duplicate"/></span>

            <p><fmt:message key="signup.password"/></p>
            <input type="password" name="password" placeholder="<fmt:message key="signup.password.placeholder"/>"
                   maxlength=32 required/>
            <span></span>
            <span id="password_span" class="hide"><fmt:message
                    key="signup.password.validity.error_weak_label"/></span>

            <p><fmt:message key="signup.password.confirm_label"/></p>
            <input type="password" name="confirm_password" placeholder="" required/>
            <span></span>
            <span id="confirm_passwords_span" class="hide"><fmt:message
                    key="signup.password.validity.confirm_label"/></span>

            <p><fmt:message key="signup.photo.label"/></p>
            <input type="file" name="avatar_location" onchange="testFileSize('<fmt:message key="signup.photo.validity.label"/>')" accept="image/*"/>
            <span></span>
            <span id="avatar_location_span" class="hide"><fmt:message
                    key="signup.avatar.wrong_format"/></span>

            <p><fmt:message key="signup.first_name.label"/></p>
            <input type="text" name="f_name" placeholder="Pavel" maxlength="50" pattern="^[A-Z]{1}[a-z]+$"/>
            <span></span>
            <span id="f_name_span" class="hide"><fmt:message
                    key="signup.user.info.validity.error_label"/></span>

            <p><fmt:message key="signup.last_name.label"/></p>
            <input type="text" name="l_name" placeholder="Bachilo" maxlength="50" pattern="^[A-Z]{1}[a-z]+$"/>
            <span></span>
            <span id="l_name_span" class="hide"><fmt:message
                    key="signup.user.info.validity.error_label"/></span>

            <p><fmt:message key="signup.country.label"/></p>
            <input type="text" name="country" placeholder="United States of America" maxlength="50"
                   pattern="^[A-Z]{1}([a-zA-Z]|\s)+$"/>
            <span></span>
            <span id="country_span" class="hide"><fmt:message
                    key="signup.user.info.validity.error_label"/></span>

            <p><fmt:message key="signup.city.label"/></p>
            <input type="text" name="city" placeholder="Buenos-aires" maxlength="50" pattern="^[A-Z]{1}[a-zA-Z\-]+$"/>
            <span></span>
            <span id="city_span" class="hide"><fmt:message
                    key="signup.user.info.validity.error_label"/></span>

            <p><fmt:message key="signup.company.label"/></p>
            <input type="text" name="company" placeholder="EPAM Systems" maxlength="50" pattern="^.+$"/>
            <span></span>
            <span id="company_span" class="hide"><fmt:message key="signup.user.info.job.error_label"/></span>

            <p><fmt:message key="signup.position.label"/></p>
            <input type="text" name="position" placeholder="Student" maxlength="50" pattern="^.+$"/>
            <span></span>
            <span id="position_span" class="hide"><fmt:message key="signup.user.info.job.error_label"/></span>

            <input type="checkbox" id="acceptTerm" required name="acceptTerms" value="true">
            <label for="acceptTerm"><fmt:message key="signup.acceptTerms"/></label>
            <span></span>
            <span id="acceptTerm_span" class="hide"><fmt:message key="signup.acceptTerm.wrong"/></span>
            <input type="submit" value="<fmt:message key="signup.submit"/>">

            <div class="copyright">
                <p> © 2016 Modern Flip Sign In Form . All rights reserved | Design by <a href="http://w3layouts.com/"
                                                                                         target="_blank">W3layouts</a>
                </p>
            </div>
        </form>

        <!-- Sign up Form-->
    </div>
    <!-- copyright -->


</div>


<script>

    function clearValidity() {
        $('#login_span').addClass("hide");
        $('#email_span').addClass("hide");
        $('#password_span').addClass("hide");
        $('#confirm_passwords_span').addClass("hide");
        $('#avatar_location_span').addClass("hide");
        $('#f_name_span').addClass("hide");
        $('#l_name_span').addClass("hide");
        $('#country_span').addClass("hide");
        $('#city_span').addClass("hide");
        $('#company_span').addClass("hide");
        $('#position_span').addClass("hide");
    }

    var turnSpan = function (xhr, stat, err) {
        var jsn = JSON.parse(xhr.response);

        clearValidity();

        for (var i = 0; i < jsn.length; i++){
            console.log(jsn[i]);
            $('#' + jsn[i]).removeClass("hide");
        }

    };

    function send(event) {
        var forma = document.getElementById('signup_form');
        var formData = new FormData(forma);

        var xhr = new XMLHttpRequest();
        xhr.addEventListener("error", turnSpan, false);
        xhr.responseType = 'json';
        xhr.open("POST", "<c:url value="/auth/register"/>");


        xhr.onload = function () {
            var location = xhr.getResponseHeader("Location");
            if (typeof location === typeof undefined || location === null) {
                turnSpan(xhr);
            } else {
                window.location.href = location;
            }
            event.preventDefault();
        };
        xhr.send(formData);

    }


    $('#signup_form').submit(function fun(event) {
        send(event);
    });


    var password = document.getElementsByName("password")[0],
        confirm_password = document.getElementsByName("confirm_password")[0];


    /*Check if password contains at least 1 char in UPPER, 1 char in DOWN register and 1 digit character.
     The length must be equal to 6 or greater*/
    function testStrongPass() {
        var pattern = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})");
        var res = pattern.test(password.value);
        if (res == true) {
            password.setCustomValidity('');
        } else {
            password.setCustomValidity("<fmt:message key="signup.password.validity.label"/>");
        }
    }

    /* Compares if passford and confirm_password field are the same*/
    function comparePasswords(errMsg) {
        if (password.value === confirm_password.value) {
            confirm_password.setCustomValidity('');
        } else {
            confirm_password.setCustomValidity('<fmt:message key="signup.password.validity.confirm_label" />');
        }
    }
    //binding events
    password.onkeyup = testStrongPass;
    password.onchange = comparePasswords;
    confirm_password.onkeyup = comparePasswords;








</script>

</body>
</html>