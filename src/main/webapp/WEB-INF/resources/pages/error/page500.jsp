<%@ page contentType="text/html;charset=UTF-8" language="java" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang}" />
<fmt:setBundle basename="i18n.loc" />
<!DOCTYPE HTML>

<html lang="${lang}">

<head>
    <title><fmt:message key="error.500.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="<c:url value="/css/err/style.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/err/fenix-font.css"/>" type="text/css">
</head>

<body>

<div class="wrap">
    <div class="main">
        <h3><a href="<c:url value="/topics" />" style="color: white">LikeIT</a></h3>
        <h1><fmt:message key="error.500.advanced"/></h1>
        <p><fmt:message key="error.500.description"/><span class="error"> 500</span>.<br><br>
            <span><fmt:message key="error.500.appeal"/></span></p><br><br><br>
    </div>
    <div class="footer">
        <p>&#x00A9 All rights Reseverd | Design by <a href="http://w3layouts.com/">W3Layouts</a></p>
    </div>
</div>

</body>
</html>

