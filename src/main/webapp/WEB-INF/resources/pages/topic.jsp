<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" uri="/WEB-INF/tools.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--@elvariable id="lang" type="java.lang.string"--%>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.loc"/>
<fmt:setTimeZone value="${cookie eq null or cookie['timezone'] eq null ? 'GMT' : cookie['timezone'].value}" />
<%--@elvariable id="user" type="by.epam.likeit.bean.User"--%>
<c:set scope="session" var="user" value="${user}"/>
<%--@elvariable id="topic" type="by.epam.likeit.bean.view.TopicView"--%>
<c:set scope="request" var="topic" value="${topic}"/>

<!DOCTYPE html>
<html lang=${lang}>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="${topic.topic.name}">
    <meta name="author" content="Pavel Bachilo">
    <link rel="stylesheet" href="<c:url value="/css/theme-teal.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/font-awesome-4.7.0/css/font-awesome.min.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/w3.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/css/st.css"/>" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Taviraj:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <script src='<c:url value="/js/jquery.min.js" />'></script>
    <title>${topic.topic.name}</title>
</head>


<body class="info" style="background-color:whitesmoke; min-height: 100%;">
<div class="main-container">
<c:choose>
    <c:when test="${user eq null}">
        <c:import url="components/unauth_header.jsp"/>
    </c:when>
    <c:otherwise>
        <c:import url="components/auth_header.jsp"/>
    </c:otherwise>
</c:choose>

<div class="topic-manage" style="padding-top:1%;margin-top:0;display:block;">

    <div id="navigation-map" class="line">
        <a href="<c:url value="/topics"/>"><fmt:message key="topics.all_topics"/></a>
        &rarr; ${topic.topic.name}
    </div>

    <div id="nav-tool" class="select-wrapper">
        <c:if test="${user ne null and t:allowed(user,'WRITE')}">
            <div class="message-tool line" style="margin-top: 0; padding-top:0;">
                <button class="button" onclick="openModal()"><fmt:message key="questions.ask"/></button>
            </div>
        </c:if>

        <span><fmt:message key="topics.sortby"/> </span>
        <label>
            <select id="sort_field" class="select-sort button" onchange="sortQuestions()">
                <option value="title"><fmt:message key="questions.sortby_title"/></option>
                <option selected value="publication_date"><fmt:message key="topics.sortby_date"/></option>
                <option value="viewed"><fmt:message key="topics.sortby_views"/></option>
                <option value="answers_count"><fmt:message key="topics.sortby_answers"/></option>
            </select>
        </label>
    </div>

    <div class="select-wrapper">
        <span><fmt:message key="topics.sortby"/> </span>
        <label>
            <select id="sort_order" class="select-sort button" onchange="sortQuestions()">
                <option value="ASC"><fmt:message key="topics.asc_sort"/></option>
                <option selected value="DESC"><fmt:message key="topics.desc_sort"/></option>
            </select>
        </label>

    </div>
</div>

<div id="wr">
    <c:import url="components/questions_table.jsp" charEncoding="UTF-8"/>
</div>



<div id="myModal" class="modal">
    <form id="new_question_form" class="modal-content" method="post" action="#" onsubmit="return false;"
          accept-charset="US-ASCII">
        <div class="modal-header">
            <span class="close" onclick="closeModal()">&times;</span>
            <h2><fmt:message key="questions.modal.new_question"/></h2>
        </div>
        <div class="modal-body">
            <textarea form="new_question_form" name="qHeader" class="header"
                      placeholder="<fmt:message key="questions.modal.title_placeholder"/>" maxlength=250
                      required></textarea>
            <textarea form="new_question_form" name="qBody"
                      placeholder="<fmt:message key="questions.modal.body_placeholder"/>" maxlength=10000
                      required></textarea>
            <input type="hidden" name="tId" value="${topic.topic.id}"/>
        </div>
        <div class="modal-footer">
            <div class="action-btn">
                <button type="submit" id="send_answer" class="button"><fmt:message key="questions.modal.ask"/></button>
                <button type="reset" id="cancel_answer" class="button" onclick="closeModal()"><fmt:message
                        key="questions.modal.cancel"/></button>
            </div>
        </div>
    </form>
</div>
</div>
<c:import url="components/footer.jsp"/>

<script>

    var curPage = 1;

    function redirectIfForbidden(resp, stat, err) {
        var redirTo = <c:url value='/' />;
        window.location.href = redirTo + resp.status;
    }

    function openModal() {
        var target = document.getElementById("myModal");
        target.style.display = "block";
    }

    function closeModal() {
        var parent = document.getElementById("myModal");
        parent.style.display = "none";
    }

    $(document).ready(function () {
        $('#new_question_form').submit(function (event) {

            $.ajax({
                type: 'POST',
                url: "<c:url value="/topic/ask/${topic.topic.id}"/>",
                data: {
                    'qHeader': $('textarea[name=qHeader]').val(),
                    'qBody': $('textarea[name=qBody]').val(),
                    'tId': $('input[name=tId]').val(),
                    'tName': "${t:escapeString(topic.topic.name)}"
                },
                encode: true,
                error: redirectIfForbidden,
                success: function (dat, textStatus, request) {
                    window.closeModal();
                    $('#new_question_form')[0].reset();
                    window.location.href = request.getResponseHeader('Location');
                }
            });
            event.preventDefault();
        });
    });

    function sortQuestions(page, length) {
        curPage = page;
        $.ajax({
            type: 'GET',
            url: "<c:url value="/reload_topic/${topic.topic.id}"/>",
            data: {
                'sortBy': $('#sort_field').find('option:selected').val(),
                'orderBy': $('#sort_order').find('option:selected').val(),
                'page' : curPage
            },
            encode: true,
            error: redirectIfForbidden,
            success: function (dat) {
                var questions_table = document.getElementById("wr");
                questions_table.innerHTML = dat;
            }
        });
    }

</script>

</body>

</html>
