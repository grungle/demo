<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" uri="/WEB-INF/tools.tld" %>
<%--@elvariable id="lang" type="java.lang.String"--%>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="i18n.loc"/>
<fmt:setTimeZone value="${cookie eq null or cookie['timezone'] eq null ? 'GMT' : cookie['timezone'].value}" />
<%--@elvariable id="user" type="by.epam.likeit.bean.User"--%>
<c:set scope="session" var="user" value="${user}"/>

<%--@elvariable id="question" type="by.epam.likeit.bean.view.QuestionView"--%>
<c:set scope="request" var="question" value="${question}"/>
<!DOCTYPE html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="${question.question.title}">
    <link rel='stylesheet' href="<c:url value="/css/font-awesome-4.7.0/css/font-awesome.min.css"/>" type="text/css">
    <link rel='stylesheet' href="<c:url value="/css/w3.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/css/theme-teal.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/css/st.css"/>" type="text/css">
    <script src='<c:url value="/js/jquery.min.js" />'></script>
    <link href="https://fonts.googleapis.com/css?family=Taviraj:700" rel="stylesheet">
    <title>${question.question.title}</title>

</head>


<body class="info" style="min-height: 100%; padding-bottom: 70px;">

<div class="main-container">

<c:choose>
    <c:when test="${user eq null}">
        <c:import url="components/unauth_header.jsp"/>
    </c:when>
    <c:otherwise>
        <c:import url="components/auth_header.jsp"/>
    </c:otherwise>
</c:choose>

<div id="navigation-map" class="line" style="color: #00766a;">
    <a href="<c:url value="/topics"/>"><fmt:message key="topics.all_topics"/></a>
    &rarr;
    <a href="<c:url value="/topic/${question.question.topic.id}"/>">${question.question.topic.name}</a>


    <span><fmt:message key="topics.sortby"/> </span>
    <label>
        <select id="sort_field" class="select-sort button" onchange="sortAnswers()">
            <option selected value="rating"><fmt:message key="answers.sortby_rating"/></option>
            <option value="publication_date"><fmt:message key="topics.sortby_date"/></option>
        </select>
    </label>
</div>

<div class="select-wrapper">
    <span><fmt:message key="topics.sortby"/> </span>
    <label>
        <select id="sort_order" class="select-sort button" onchange="sortAnswers()">
            <option value="ASC"><fmt:message key="topics.asc_sort"/></option>
            <option selected value="DESC"><fmt:message key="topics.desc_sort"/></option>
        </select>
    </label>

</div>


<div id="aq_wrapper">
    <c:import url="components/answer_list.jsp"/>
</div>

<div id="myModal" class="modal">
    <form id="send_answer_form" class="modal-content" method="POST" action="#" onsubmit="return false;"
          accept-charset="US-ASCII">
        <div class="modal-header">
            <span class="close" onclick="closeModal()">&times;</span>
            <h2><fmt:message key="question.modal.title"/></h2>
        </div>
        <div class="modal-body">
            <textarea form="send_answer_form" name="aBody" placeholder="<fmt:message key="question.modal.placeholder"/>"
                      maxlength=10000 required=""></textarea>
            <input id="qId" type="hidden" name="qId" value="${question.question.id}"/>
        </div>
        <div class="modal-footer">
            <div class="action-btn">
                <button type="submit" id="send_answer" class="button"><fmt:message key="question.answer"/></button>
                <button type="reset" id="cancel_answer" class="button" onclick="closeModal()"><fmt:message
                        key="question.cancel"/></button>
            </div>
        </div>
    </form>
</div>
</div>
<c:import url="components/footer.jsp"/>


<script>

    var curPage = 1;
    var sortBy = $('#sort_field').find('option:selected').val();
    var orderBy= $('#sort_order').find('option:selected').val();

    function redirectIfForbidden(resp) {
        var redirTo = <c:url value='/' />;
        window.location.href = redirTo + resp.status;
    }


    function edit(elem) {
        var wrapper = elem.parentElement.parentElement;

        if (elem.innerHTML === "<fmt:message key="question.save"/>") {
            if (wrapper.getAttribute("data-type") === "q") {
                saveQuestionChanges(wrapper);
            } else if (wrapper.getAttribute("data-type") === "a") {
                saveAnswerChanges(wrapper);
            }
        }

        var editTag = elem.parentElement.previousElementSibling.children[0];
        if (wrapper.getAttribute("data-type") === "q") {
            var q_body = elem.parentElement.previousElementSibling.children[1];
            q_body.setAttribute("contentEditable", (q_body.getAttribute("contentEditable") != 'true'));
        }
        editTag.setAttribute("contentEditable", (editTag.getAttribute("contentEditable") != 'true'));
        if (editTag.getAttribute("contentEditable") == 'true') {
            elem.innerHTML = "<fmt:message key="question.save"/>";
        } else {
            elem.innerHTML = "<fmt:message key="question.edit"/>";
        }
    }

    function saveAnswerChanges(wrapper) {
        var aId = wrapper.getAttribute("data-mid");

        var aBody = wrapper.children[2].firstElementChild.innerText.trim();
        aBody = escapeXml(aBody);

        $.ajax({
            type: 'POST',
            url: '<c:url value="/answer/modify/${question.question.id}"/>',
            data: {
                'aId': aId,
                'aBody': aBody,
                'qId' : ${question.question.id},
                "page": curPage,
                "sortBy": sortBy,
                "orderBy": orderBy
            },
            encode: true,
            error: redirectIfForbidden,
            success: function (dat) {
                var questions_table = document.getElementById("q_wrapper");
                questions_table.innerHTML = dat;
            }
        });
    }

    function saveQuestionChanges(wrapper) {
        var qHeader = wrapper.children[1].firstElementChild;
        var qHeaderText = qHeader.innerText.trim();
        var qBody = qHeader.nextElementSibling;
        var qBodyText = qBody.innerText.trim();

        qHeaderText = escapeXml(qHeaderText);
        qBodyText = escapeXml(qBodyText);

        $.ajax({
            type: 'POST',
            url: '<c:url value="/question/modify/${question.question.id}"/>',
            data: {
                'qId': ${question.question.id},
                'qHeader': qHeaderText,
                'qBody': qBodyText,
                "page": curPage,
                "sortBy": sortBy,
                "orderBy": orderBy
            },
            encode: true,
            error: redirectIfForbidden,
            success: function (dat) {
                var questions_table = document.getElementById("q_wrapper");
                questions_table.innerHTML = dat;
            }
        });
    }

    function answer() {
        var target = document.getElementById("myModal");
        target.style.display = "block";
    }

    function closeModal() {
        var parent = document.getElementById("myModal");
        parent.style.display = "none";
    }

    function closeQuestion(elem) {
        var wrapper = elem.parentElement.parentElement;
        var qId = wrapper.getAttribute("data-mid");
        $.ajax({
            type: 'POST',
            url: '<c:url value="/question/close/${question.question.id}"/>',
            data: {
                'qId': qId,
                "page": curPage,
                "sortBy": sortBy,
                "orderBy": orderBy
            },
            encode: true,
            error: redirectIfForbidden,
            success: function (dat) {
                document.getElementById("q_wrapper").innerHTML = dat;
            }
        });
    }

    $(document).ready(function () {
        $('#send_answer_form').submit(function (event) {
            event.preventDefault();
            var formData = {
                'qId': ${question.question.id},
                'aBody': escapeXml($('textarea[name=aBody]').val().trim()),
                "page": curPage,
                "sortBy": sortBy,
                "orderBy": orderBy
            };
            $.ajax({
                type: 'POST',
                url: '<c:url value="/question/answer/${question.question.id}"/>',
                data: formData,
                encode: true,
                error: redirectIfForbidden,
                complete: function () {
                    window.closeModal();
                    $('#send_answer_form')[0].reset();
                },
                success: function (dat, stat, resp) {
                    var questions_table = document.getElementById("q_wrapper");
                    questions_table.innerHTML = dat;
                }
            });
        });
    });

    function removeAnswer(elem) {
        var wrapper = elem.parentElement.parentElement;
        var aId = wrapper.getAttribute("data-mid");
        $.ajax({
            type: "POST",
            url: "<c:url value="/answer/delete/${question.question.id}"/>",
            data: {
                "aId": aId,
                "page": curPage,
                "sortBy": sortBy,
                "orderBy": orderBy
            },
            error: redirectIfForbidden,
            success: function (dat) {
                var questions_table = document.getElementById("q_wrapper");
                questions_table.innerHTML = dat;
            }
        });
    }

    function removeQuestion() {
        $.ajax({
            type: "POST",
            url: '<c:url value="/question/delete/${question.question.id}"/>',
            data: {
                "qId": ${question.question.id},
                "tId": ${question.question.topic.id}
            },
            error: redirectIfForbidden,
            success: function (dat, stat, resp) {
                window.location.href = resp.getResponseHeader("Location");
            }
        });
    }


    function moderate(elem) {
        var wrapper = elem.parentElement.parentElement;

        if (elem.innerHTML === "<fmt:message key="question.save"/>") {
            if (wrapper.getAttribute("data-type") === "q") {
                moderateQuestion(wrapper);
            } else if (wrapper.getAttribute("data-type") === "a") {
                moderateAnswer(wrapper);
            }
        }

        var editTag = elem.parentElement.previousElementSibling.children[0];
        if (wrapper.getAttribute("data-type") === "q") {
            var q_body = elem.parentElement.previousElementSibling.children[1];
            q_body.setAttribute("contentEditable", (q_body.getAttribute("contentEditable") != 'true'));
        }
        editTag.setAttribute("contentEditable", (editTag.getAttribute("contentEditable") != 'true'));
        if (editTag.getAttribute("contentEditable") == 'true') {
            elem.innerHTML = "<fmt:message key="question.save"/>";
        } else {
            elem.innerHTML = "<fmt:message key="question.moderate"/>";
        }
        event.preventDefault();
        return false;
    }

    function moderateAnswer(wrapper) {
        var aId = wrapper.getAttribute("data-mid");
        var aBody = wrapper.children[2].firstElementChild;
        var aBodyText = aBody.innerHTML.trim();
        aBodyText = escapeXml(aBodyText);


        $.ajax({
            type: "POST",
            url: '<c:url value="/answer/moderate/${question.question.id}"/>',
            data: {
                "aId": aId,
                "aBody": aBodyText,
                'qId' : ${question.question.id},
                "page": curPage,
                "sortBy": sortBy,
                "orderBy": orderBy
            },
            error: redirectIfForbidden,
            success: function (dat, stat, resp) {
                var questions_table = document.getElementById("q_wrapper");
                questions_table.innerHTML = dat;
            }
        });
    }

    function escapeXml(plainText) {
        return plainText.replace(/&apos;/g, "'")
            .replace(/&quot;/g, '"')
            .replace(/&gt;/g, '>')
            .replace(/&lt;/g, '<')
            .replace(/&amp;/g, '&')
            .replace(/&nbsp;/g, ' ');
    }


    function moderateQuestion(wrapper) {
        var qHeader = wrapper.children[1].firstElementChild;
        var qHeaderText = qHeader.innerHTML.trim();
        var qBody = qHeader.nextElementSibling;
        var qBodyText = qBody.innerHTML.trim();

        qHeaderText = escapeXml(qHeaderText);
        qBodyText = escapeXml(qBodyText);

        $.ajax({
            type: "POST",
            url: '<c:url value="/question/moderate/${question.question.id}"/>',
            data: {
                "qId": ${question.question.id},
                "qHeader": qHeaderText,
                "qBody": qBodyText,
                "page": curPage,
                "sortBy": sortBy,
                "orderBy": orderBy
            },
            error: redirectIfForbidden,
            success: function (dat, stat, resp) {
                var questions_table = document.getElementById("q_wrapper");
                questions_table.innerHTML = dat;
            }
        });
    }


    function vote(elem) {
        var value = parseInt(elem.getAttribute("data-mark-val"));
        var target = elem.parentElement.firstElementChild.nextElementSibling;
        var aId = elem.parentElement.parentElement.parentElement.getAttribute("data-mid");
        var currentMark = target.parentElement.nextElementSibling;

        $.ajax({
            type: 'POST',
            url: '<c:url value="/answer/estimate/${question.question.id}"/>',
            data: {'aId': aId, 'val': value},
            encode: true,
            dataType: 'html',
            error: redirectIfForbidden,
            success: function (dat) {
                target.innerHTML = dat.replace(/"/g, "").replace(/'/g, "");
                var curMarkValue = currentMark.innerHTML;
                if (curMarkValue !== "-") {
                    var newMarkValue = parseInt(curMarkValue) + value;
                    if (newMarkValue >= -5 && newMarkValue <= 5)
                        currentMark.innerHTML = newMarkValue;
                } else {
                    currentMark.innerHTML = value;
                }
            }
        });
    }

    function getTargetMark(elems, aId) {
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].getAttribute('data-mid') == aId)
                return elems[i];
        }
        return "not found";
    }

    function sortAnswers(page, length) {
        curPage = page;
        sortBy = $('#sort_field').find('option:selected').val();
        orderBy = $('#sort_order').find('option:selected').val();
        $.ajax({
            type: 'GET',
            url: '<c:url value="/reload_question/${question.question.id}"/>',
            data: {
                'sortBy': sortBy,
                'orderBy': orderBy,
                'page': curPage
            },
            encode: true,
            error: redirectIfForbidden,
            success: function (dat) {
                var questions_table = document.getElementById("q_wrapper");
                questions_table.innerHTML = dat;
            }
        });
    }


</script>

</body>

</html>