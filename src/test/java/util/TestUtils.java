package util;

import by.epam.likeit.dao.DAOFactory;
import by.epam.likeit.jdbc.pool.ConnectionPoolHolder;
import by.epam.likeit.jdbc.pool.PoolConfigurator;
import by.epam.likeit.jdbc.pool.PoolManager;
import by.epam.likeit.jdbc.transaction.BaseDataSource;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.logger.LikeLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class TestUtils {

    @SuppressWarnings("all")
    public static Connection prepareConnectionForTest() throws IOException, SQLException, URISyntaxException {
        URL daoUrl = TestUtils.class.getClassLoader().getResource("./init/dao_sql.properties");
        URL poolUri = TestUtils.class.getClassLoader().getResource("./init/pool.properties");

        Connection connection = null;

        try (FileInputStream daoFis = new FileInputStream(new File(daoUrl.toURI()));
             FileInputStream poolFis = new FileInputStream(new File(poolUri.toURI()))) {

            Properties props = new Properties();
            props.load(poolFis);

            DAOFactory.init(daoFis);

            PoolConfigurator configurator = new PoolConfigurator(props);
            ConnectionPoolHolder holder = new ConnectionPoolHolder(configurator);
            PoolManager.setPoolHolder(holder);
            Connection con = PoolManager.getInstance().getConnection();
            connection = con;

            ThreadLocalTransactionManagerImpl.getInstance().setDataSource(new BaseDataSource() {
                @Override
                public Connection getConnection() throws SQLException {
                    return con;
                }
            });
        } catch (ClassNotFoundException e){
            LikeLogger.err(e.getMessage(), e);
        }
        return connection;
    }

}
