package by.epam.likeit.service;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import util.TestUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class RegisterUsersTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();

    public RegisterUsersTest() throws SQLException {}

    // Setting up --------------------------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception {
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception {}

    // Test methods -------------------------------------------------------------------------------------------

    @Test
    public void registerOneValidUser() throws Exception {
        Long before = getUsersCount();
        Long expected = before + 1;

        User user = new UserBuilder().login("qweasdg").password("qgrehg").avatar("/fweg/wegweg").email("wege@g.g").createNewUser();

        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            userService.register(user);
            return null;
        });

        Long actual = getUsersCount();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void registerTwoValidUser() throws Exception {
        Long before = getUsersCount();
        Long expected = before + 2;

        User user1 = new UserBuilder().login("qweasdg").password("qgrehg").avatar("/fweg/wegweg").email("wege@g.g").createNewUser();
        User user2 = new UserBuilder().login("qweasdg22").password("qg125rehg").avatar("/fweg/wegweg").email("1we1ge@g.g").createNewUser();

        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            userService.register(user1);
            userService.register(user2);
            return null;
        });

        Long actual = getUsersCount();
        Assert.assertEquals(actual, expected);
    }

    @Test(expected = DAOSystemException.class)
    public void registerUserWithDuplicatedEmail() throws Exception {
        User user1 = new UserBuilder().login("qweasdg").password("123456").avatar("/fweg/wegweg").email("qq@qq.qq").createNewUser();
        User user2 = new UserBuilder().login("qweasdg22").password("123456").avatar("/fweg/1wegweg").email("qq@qq.qq").createNewUser();

        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            userService.register(user1);
            userService.register(user2);
            return null;
        });

        Assert.fail("No exception in register user with duplicated email");
    }

    @Test(expected = DAOSystemException.class)
    public void registerUserWithDuplicatedLogin() throws Exception {
        User user1 = new UserBuilder().login("LoginLogin").password("1123q").avatar("/fweg/wegweg").email("qq@qq.123qq").createNewUser();
        User user2 = new UserBuilder().login("LoginLogin").password("42516").avatar("/fweg/1wegweg").email("333qq@qq.qq").createNewUser();

        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            userService.register(user1);
            userService.register(user2);
            return null;
        });

        Assert.fail("No exception in register user with duplicated login");
    }

    // Tearing down -------------------------------------------------------------------------------------------

    @After
    public void tearDown() throws Exception {
        connection.rollback();
    }

    @AfterClass
    public static void tearDownAfter() throws Exception{
        connection.close();
    }

    // Helpers -------------------------------------------------------------------------------------------------

    private static Long getUsersCount() throws SQLException {
        String q = "SELECT COUNT(*) AS 'count' FROM users";
        Statement st = connection.createStatement();
        ResultSet resultSet = st.executeQuery(q);
        Long val = null;
        if (resultSet.next()) val = resultSet.getLong("count");
        return val;
    }



}
