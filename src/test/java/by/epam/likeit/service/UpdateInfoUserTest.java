package by.epam.likeit.service;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.bean.view.UserView;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static by.epam.likeit.bean.User.UserRights.*;

@RunWith(Parameterized.class)
public class UpdateInfoUserTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();

    public UpdateInfoUserTest(){}

    // Parameters ---------------------------------------------------------------------------

    @Parameterized.Parameter
    public User oldUser;

    @Parameterized.Parameter(value = 1)
    public User newUser;

    @Parameterized.Parameter(value = 2)
    public Class<? extends Exception> expectedException;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    // Setting up ----------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        if (expectedException != null){
            exception.expect(expectedException);
        }
    }

    // Test method ---------------------------------------------------------------------------

    @Test
    public void updateUserTest() throws Exception{
        UserView uv = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> userService.update(oldUser, newUser));
        Assert.assertNotNull(uv);
    }

    // Tearing down ---------------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Test data ------------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        User normalOldUser = new UserBuilder().login("pro_master").rights(READ, WRITE, WATCH_PROFILE).createNewUser();
        User forbiddenOldUser = new UserBuilder().login("pro_master").rights(READ, WATCH_PROFILE).createNewUser();
        User invalidOldUser = new UserBuilder().login("1pro_master").rights(READ, WRITE, WATCH_PROFILE).createNewUser();

        User duplicatedNewUser = new UserBuilder().login("pro_master").email("instanceofnull@yahoo.com").city("Minsk").createNewUser();
        User noChangesNewUser = new UserBuilder().login("pro_master").email("master_of@yahoo.com").city("Minsk").createNewUser();
        User normalNewUser = new UserBuilder().login("pro_master").email("master22_of@yahoo.com").city("Minsk").createNewUser();

        list.add(new Object[]{normalOldUser, noChangesNewUser, null});
        list.add(new Object[]{normalOldUser, duplicatedNewUser, DAOSystemException.class});
        list.add(new Object[]{normalOldUser, normalNewUser, null});

        list.add(new Object[]{forbiddenOldUser, normalOldUser, InsufficientPermissionException.class});
        list.add(new Object[]{forbiddenOldUser, duplicatedNewUser, InsufficientPermissionException.class});
        list.add(new Object[]{forbiddenOldUser, noChangesNewUser, InsufficientPermissionException.class});

        list.add(new Object[]{invalidOldUser, normalNewUser, NoSuchEntityException.class});

        return list;
    }
}
