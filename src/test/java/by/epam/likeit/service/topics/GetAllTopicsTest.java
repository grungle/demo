package by.epam.likeit.service.topics;

import by.epam.likeit.bean.Topic;
import by.epam.likeit.dao.BaseDAO;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.ITopicService;
import by.epam.likeit.service.impl.TopicServiceImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static by.epam.likeit.dao.BaseDAO.ComparatorMode.ASC;
import static by.epam.likeit.dao.BaseDAO.ComparatorMode.DESC;


@RunWith(Parameterized.class)
public class GetAllTopicsTest {

    private static Connection connection;
    private ITopicService topicService = new TopicServiceImpl();

    public GetAllTopicsTest(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter
    public String sortBy;

    @Parameterized.Parameter(value = 1)
    public BaseDAO.ComparatorMode orderBy;

    @Parameterized.Parameter(value = 2)
    public long offset;

    @Parameterized.Parameter(value = 3)
    public long count;

    @Parameterized.Parameter(value = 4)
    public long expected;

    @Parameterized.Parameter(value = 5)
    public Class<? extends Exception> exception;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        if (exception != null)
            expectedException.expect(exception);
    }

    // Test method -------------------------------------------------------------------------

    @Test
    public void getAllTopicTest() throws Exception{
        List<Topic> actual = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() ->
                topicService.getAll(sortBy, orderBy, offset, count));

        Assert.assertEquals(expected, actual.size());
    }

    // Tearing down -----------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Helpers ----------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        list.add(new Object[]{"name", ASC, 1, 2, 2, null});
        list.add(new Object[]{"name", DESC, 0, 2, 2, null});
        list.add(new Object[]{"name", ASC, 10, 2, 2, null});
        list.add(new Object[]{"name", DESC, 55, 2, 0, null});
        list.add(new Object[]{"name", ASC, 51, 2, 0, null});
        list.add(new Object[]{"name", DESC, 10, 15, 4, null});
        list.add(new Object[]{"name", ASC, 0, 15, 14, null});
        list.add(new Object[]{"name", DESC, 0, 14, 14, null});
        list.add(new Object[]{"name", DESC, 0, -1, 14, DAOSystemException.class});
        list.add(new Object[]{"name", ASC, -1, 0, 2, DAOSystemException.class});
        list.add(new Object[]{"id", ASC, 1, 2, 2, null});
        list.add(new Object[]{"id", DESC, 0, 2, 2, null});
        list.add(new Object[]{"id", ASC, 10, 2, 2, null});
        list.add(new Object[]{"invalid", ASC, 1, 2, 2, DAOSystemException.class});
        list.add(new Object[]{"invalid", DESC, 0, 2, 2, DAOSystemException.class});
        list.add(new Object[]{"invalid", ASC, 10, 2, 2, DAOSystemException.class});

        return list;
    }
}
