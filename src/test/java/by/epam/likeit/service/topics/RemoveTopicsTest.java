package by.epam.likeit.service.topics;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.ITopicService;
import by.epam.likeit.service.impl.TopicServiceImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static by.epam.likeit.bean.User.UserRights.*;

@RunWith(Parameterized.class)
public class RemoveTopicsTest {

    private static Connection connection;
    private ITopicService topicService = new TopicServiceImpl();

    public RemoveTopicsTest(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter
    public long id;

    @Parameterized.Parameter(value = 1)
    public User user;

    @Parameterized.Parameter(value = 2)
    public Class<? extends Exception> exception;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        if (exception != null)
            expectedException.expect(exception);
    }

    // Test method -------------------------------------------------------------------------

    @Test
    public void removeTopicTest() throws Exception{
        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            topicService.remove(id, user);
            return null;
        });

        Assert.assertNull(exception + " was expected, but wasn't thrown.", exception);
    }

    // Tearing down -----------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Test data --------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        User admin = new UserBuilder().login("admin").rights(ADMIN).createNewUser();
        User moderator = new UserBuilder().login("Grungle").rights(WATCH_PROFILE, READ, WRITE, MODERATE).createNewUser();

        list.add(new Object[]{1, admin, null});
        list.add(new Object[]{1, moderator, InsufficientPermissionException.class});
        list.add(new Object[]{0, admin, NoSuchEntityException.class});
        list.add(new Object[]{0, moderator, InsufficientPermissionException.class});

        return list;
    }
}
