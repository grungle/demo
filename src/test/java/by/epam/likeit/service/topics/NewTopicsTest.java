package by.epam.likeit.service.topics;

import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.dao.exception.DAOSystemException;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.ITopicService;
import by.epam.likeit.service.impl.TopicServiceImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static by.epam.likeit.bean.User.UserRights.*;

@RunWith(Parameterized.class)
public class NewTopicsTest {

    private static Connection connection;
    private ITopicService topicService = new TopicServiceImpl();

    public NewTopicsTest(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter
    public Topic topic;

    @Parameterized.Parameter(value = 1)
    public User user;

    @Parameterized.Parameter(value = 2)
    public Class<? extends Exception> exception;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        if (exception != null)
            expectedException.expect(exception);
    }

    // Test method -------------------------------------------------------------------------

    @Test
    public void createNewTopicTest() throws Exception{
        long generatedId = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> topicService.newTopic(topic, user));
        Assert.assertNotNull(generatedId);
    }

    // Tearing down -----------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Test data --------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        User admin = new UserBuilder().login("admin").rights(ADMIN, WRITE).createNewUser();
        User moderator = new UserBuilder().login("Grungle").rights(WATCH_PROFILE, READ, WRITE, MODERATE).createNewUser();

        Topic validTopic = new Topic();
        validTopic.setName("ValidTopicName");

        Topic invalidTopic = new Topic();
        invalidTopic.setName(".NET");

        list.add(new Object[]{validTopic, admin, null});
        list.add(new Object[]{validTopic, moderator, InsufficientPermissionException.class});
        list.add(new Object[]{invalidTopic, admin, DAOSystemException.class});
        list.add(new Object[]{invalidTopic, moderator, InsufficientPermissionException.class});

        return list;
    }
}
