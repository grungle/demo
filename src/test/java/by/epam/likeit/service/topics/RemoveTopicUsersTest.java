package by.epam.likeit.service.topics;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.ITopicService;
import by.epam.likeit.service.IUserService;
import by.epam.likeit.service.impl.TopicServiceImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@RunWith(Parameterized.class)
public class RemoveTopicUsersTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();
    private ITopicService topicService = new TopicServiceImpl();

    public RemoveTopicUsersTest(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter
    public Long topicId;

    @Parameterized.Parameter(value = 1)
    public User expected;

    @Parameterized.Parameter(value = 2)
    public User admin;


    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            topicService.remove(topicId, admin);
            return null;
        });
    }

    // Test methods -------------------------------------------------------------------------

    @Test
    public void compareQuestionsCountTest() throws Exception{
        User actual = ThreadLocalTransactionManagerImpl.getInstance()
                .doWithoutTransaction(() -> userService.find(expected.getLogin()));
        Assert.assertEquals(expected.getQuestions(), actual.getQuestions());
    }

    @Test
    public void compareAnswersCountTest() throws Exception{
        User actual = ThreadLocalTransactionManagerImpl.getInstance()
                .doWithoutTransaction(() -> userService.find(expected.getLogin()));
        Assert.assertEquals(expected.getAnswers(), actual.getAnswers());
    }

    @Test
    public void compareUserRatingTest() throws Exception{
        User actual = ThreadLocalTransactionManagerImpl.getInstance()
                .doWithoutTransaction(() -> userService.find(expected.getLogin()));
        Assert.assertEquals(expected.getRating(), actual.getRating());
    }

    // Tearing down -----------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Test data --------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        User admin = new UserBuilder().login("admin").rights(User.UserRights.ADMIN).createNewUser();
        User alexspz = new UserBuilder().login("alex_spz").questions(0).answers(2).rating(11).createNewUser();
        User jenkins = new UserBuilder().login("Jenkins").questions(1).answers(0).rating(-3).createNewUser();
        User grungle = new UserBuilder().login("Grungle").questions(2).answers(2).rating(1).createNewUser();
        User adminTest = new UserBuilder().login("admin").questions(3).answers(35).rating(33).createNewUser();
        User majestics = new UserBuilder().login("majestics").questions(3).answers(1).rating(-1).createNewUser();
        User promaster = new UserBuilder().login("pro_master").questions(3).answers(4).rating(19).createNewUser();
        User balusc = new UserBuilder().login("BalusC").questions(0).answers(4).rating(59).createNewUser();

        list.add(new Object[]{1L, alexspz, admin});
        list.add(new Object[]{1L, jenkins, admin});
        list.add(new Object[]{1L, grungle, admin});
        list.add(new Object[]{1L, adminTest, admin});
        list.add(new Object[]{1L, majestics, admin});
        list.add(new Object[]{1L, promaster, admin});
        list.add(new Object[]{1L, balusc, admin});


        return list;
    }


}
