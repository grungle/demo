package by.epam.likeit.service.topics;

import by.epam.likeit.bean.Topic;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.ITopicService;
import by.epam.likeit.service.impl.TopicServiceImpl;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@RunWith(Parameterized.class)
public class GetQuestionsCountByTopic {

    private static Connection connection;
    private ITopicService topicService = new TopicServiceImpl();

    public GetQuestionsCountByTopic(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter
    public Topic topic;

    @Parameterized.Parameter(value = 1)
    public long expected;


    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{}

    // Test method -------------------------------------------------------------------------

    @Test
    public void getQuestionsCountByTopic() throws Exception{
        long actual = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> topicService.getQuestionsCountByTopic(topic));
        Assert.assertEquals(expected, actual);
    }

    // Tearing down -----------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Test data --------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        Topic invalid = new Topic(-1L, "invalid");
        Topic _invalid = new Topic(2151124214L, "invalid");
        Topic valid = new Topic(2L, "valid");
        Topic _valid = new Topic(6L, "valid");

        list.add(new Object[]{invalid, 0});
        list.add(new Object[]{_invalid, 0});
        list.add(new Object[]{valid, 6L});
        list.add(new Object[]{_valid, 3L});

        return list;
    }

}
