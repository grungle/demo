package by.epam.likeit.service.topics;

import by.epam.likeit.bean.Topic;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.dao.BaseDAO;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.ITopicService;
import by.epam.likeit.service.IUserService;
import by.epam.likeit.service.impl.TopicServiceImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@RunWith(Parameterized.class)
public class RemoveAllTopicsUsersZeroTest {


    private static Connection connection;
    private IUserService userService = new UserServiceImpl();
    private ITopicService topicService = new TopicServiceImpl();

    public RemoveAllTopicsUsersZeroTest(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter(value = 0)
    public User expected;

    @Parameterized.Parameter(value = 1)
    public User admin;


    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            List<Topic> topics = topicService.getAll("name", BaseDAO.ComparatorMode.ASC, 0, 1000);
            for (Topic topic : topics)
                topicService.remove(topic.getId(), admin);
            return null;
        });
    }

    // Test methods -------------------------------------------------------------------------

    @Test
    public void compareQuestionsCountTest() throws Exception{
        User actual = ThreadLocalTransactionManagerImpl.getInstance()
                .doWithoutTransaction(() -> userService.find(expected.getLogin()));

        Assert.assertEquals(expected.getQuestions(), actual.getQuestions());
    }

    @Test
    public void compareAnswersCountTest() throws Exception{
        User actual = ThreadLocalTransactionManagerImpl.getInstance()
                .doWithoutTransaction(() -> userService.find(expected.getLogin()));
        Assert.assertEquals(expected.getAnswers(), actual.getAnswers());
    }

    @Test
    public void compareUserRatingTest() throws Exception{
        User actual = ThreadLocalTransactionManagerImpl.getInstance()
                .doWithoutTransaction(() -> userService.find(expected.getLogin()));
        Assert.assertEquals(expected.getRating(), actual.getRating());
    }

    // Tearing down -----------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Test data --------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        User admin = new UserBuilder().login("admin").rights(User.UserRights.ADMIN).createNewUser();
        User alexspz = new UserBuilder().login("alex_spz").questions(0).answers(0).rating(0).createNewUser();
        User jenkins = new UserBuilder().login("Jenkins").questions(0).answers(0).rating(0).createNewUser();
        User grungle = new UserBuilder().login("Grungle").questions(0).answers(0).rating(0).createNewUser();
        User adminTest = new UserBuilder().login("admin").questions(0).answers(0).rating(0).createNewUser();
        User majestics = new UserBuilder().login("majestics").questions(0).answers(0).rating(0).createNewUser();
        User promaster = new UserBuilder().login("pro_master").questions(0).answers(0).rating(0).createNewUser();
        User balusc = new UserBuilder().login("BalusC").questions(0).answers(0).rating(0).createNewUser();
        User bigjoe = new UserBuilder().login("BigJoe").questions(0).answers(0).rating(0).createNewUser();
        User fooooo = new UserBuilder().login("Fooooo").questions(0).answers(0).rating(0).createNewUser();
        User itexpert = new UserBuilder().login("it_expert").questions(0).answers(0).rating(0).createNewUser();
        User moder = new UserBuilder().login("moder").questions(0).answers(0).rating(0).createNewUser();
        User moderator = new UserBuilder().login("moderator").questions(0).answers(0).rating(0).createNewUser();
        User someuser = new UserBuilder().login("some_user").questions(0).answers(0).rating(0).createNewUser();
        User vbnmz = new UserBuilder().login("vbnmz").questions(0).answers(0).rating(0).createNewUser();
        User zzzzzzzzzzzzzzzzzzz = new UserBuilder().login("zzzzzzzzzzzzzzzzzzz").questions(0).answers(0).rating(0).createNewUser();

        list.add(new Object[]{alexspz, admin});
        list.add(new Object[]{jenkins, admin});
        list.add(new Object[]{grungle, admin});
        list.add(new Object[]{adminTest, admin});
        list.add(new Object[]{majestics, admin});
        list.add(new Object[]{promaster, admin});
        list.add(new Object[]{balusc, admin});
        list.add(new Object[]{bigjoe, admin});
        list.add(new Object[]{fooooo, admin});
        list.add(new Object[]{itexpert, admin});
        list.add(new Object[]{moder, admin});
        list.add(new Object[]{moderator, admin});
        list.add(new Object[]{someuser, admin});
        list.add(new Object[]{vbnmz, admin});
        list.add(new Object[]{zzzzzzzzzzzzzzzzzzz, admin});

        return list;
    }

}
