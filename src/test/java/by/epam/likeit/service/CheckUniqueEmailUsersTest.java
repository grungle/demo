package by.epam.likeit.service;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@RunWith(Parameterized.class)
public class CheckUniqueEmailUsersTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();

    public CheckUniqueEmailUsersTest(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter
    public String email;

    @Parameterized.Parameter(value = 1)
    public boolean expected;

    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{}

    // Test method -------------------------------------------------------------------------

    @Test
    public void checkUniqueEmailTest() throws Exception{
        User user = new UserBuilder().email(email).createNewUser();
        boolean actual = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> userService.checkUniqueEmail(user));
        Assert.assertEquals(expected, actual);
    }

    // Tearing down -----------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Helpers ----------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();
        list.add(new Object[]{"instanceofnull@yahoo.com", false});
        list.add(new Object[]{"admin@mail.ru", false});
        list.add(new Object[]{"admin123@mail.ru", true});
        list.add(new Object[]{"master_of@yahoo.com", false});
        list.add(new Object[]{"master_of@yahoo.comm", true});
        list.add(new Object[]{"admin@mail.ru,instanceofnull@yahoo.com", true});
        return list;
    }

}
