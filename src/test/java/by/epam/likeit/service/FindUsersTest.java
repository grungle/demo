package by.epam.likeit.service;


import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@RunWith(Parameterized.class)
public class FindUsersTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();

    public FindUsersTest(){}

    // Parameters -----------------------------------------------------------------------------

    @Parameterized.Parameter
    public String login;

    @Parameterized.Parameter(value = 1)
    public User expected;


    // Setting up -----------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{}

    // Test method ----------------------------------------------------------------------------


    @Test
    public void findUserTest() throws Exception{
        User actual = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> userService.find(login));
        Assert.assertEquals(actual, expected);
    }

    // Tearing down ---------------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Test data ------------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();
        list.add(new Object[]{"Grungle", new UserBuilder().login("Grungle").createNewUser()});
        list.add(new Object[]{"admin", new UserBuilder().login("admin").createNewUser()});
        list.add(new Object[]{"some_user", new UserBuilder().login("some_user").createNewUser()});
        list.add(new Object[]{"blablabla_user", null});
        list.add(new Object[]{"Grungleadmin", null});
        list.add(new Object[]{"user", null});
        list.add(new Object[]{"true", null});
        return list;
    }
}
