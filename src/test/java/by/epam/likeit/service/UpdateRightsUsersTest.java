package by.epam.likeit.service;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.*;

import static by.epam.likeit.bean.User.UserRights.*;


@RunWith(Parameterized.class)
public class UpdateRightsUsersTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();

    // Parameters -----------------------------------------------------------------------------

    @Parameterized.Parameter
    public User current;

    @Parameterized.Parameter(value = 1)
    public User target;

    @Parameterized.Parameter(value = 2)
    public Set<User.UserRights> expected;

    @Parameterized.Parameter(value = 3)
    public Class<? extends Exception> expectedException;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    // Setting up -----------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        if (expectedException != null){
            exception.expect(expectedException);
        }
    }

    // Test method ----------------------------------------------------------------------------

    @Test
    public void updateUserRights() throws Exception{
        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            userService.updateRights(current, target, expected);
            return null;
        });
        User actualUser = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> userService.find(target.getLogin()));

        Assert.assertEquals(expected, actualUser.getRights());
    }

    // Tearing down ---------------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Test data ------------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        // "moderators"
        User moderator = new UserBuilder().login("Grungle").rights(READ, WRITE, WATCH_PROFILE, MODERATE).createNewUser();
        User admin = new UserBuilder().login("admin").rights(READ, WRITE, WATCH_PROFILE, MODERATE, ADMIN).createNewUser();
        User user = new UserBuilder().login("BigJoe").rights(READ, WRITE, WATCH_PROFILE).createNewUser();

        // "targets"
        User weakRightsUser = new UserBuilder().login("alex_spz").rights(READ, WATCH_PROFILE).createNewUser();
        User normalUser = new UserBuilder().login("alex_spz").rights(READ, WATCH_PROFILE, WRITE).createNewUser();

        List<Object[]> list = new ArrayList<>();

        // moderator -> normal user. OK
        list.add(new Object[]{moderator, normalUser, getRights(READ, WATCH_PROFILE, WRITE), null});
        // admin -> moderator. OK
        list.add(new Object[]{admin, moderator, getRights(READ, WATCH_PROFILE, WRITE, MODERATE), null});
        // admin -> normal user. OK
        list.add(new Object[]{admin, normalUser, getRights(READ, WATCH_PROFILE, WRITE), null});
        // moderator -> moderator. FAIL
        list.add(new Object[]{moderator, moderator, getRights(READ, WRITE, WATCH_PROFILE), InsufficientPermissionException.class});
        // user -> weak user. FAIL
        list.add(new Object[]{user, weakRightsUser, getRights(READ, WATCH_PROFILE), InsufficientPermissionException.class});
        // weak user -> user. FAIL
        list.add(new Object[]{weakRightsUser, user, getRights(READ, WRITE, WATCH_PROFILE), InsufficientPermissionException.class});
        // moderator -> admin. FAIL
        list.add(new Object[]{moderator, admin, getRights(READ, WRITE, WATCH_PROFILE, MODERATE, ADMIN), InsufficientPermissionException.class});

        return list;
    }

    private static HashSet<User.UserRights> getRights(User.UserRights ... rights){
        return new HashSet<>(Arrays.asList(rights));
    }

}
