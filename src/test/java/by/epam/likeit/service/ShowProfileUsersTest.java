package by.epam.likeit.service;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.bean.view.UserView;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static by.epam.likeit.bean.User.UserRights.*;

@RunWith(Parameterized.class)
public class ShowProfileUsersTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();

    public ShowProfileUsersTest(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter
    public User current;

    @Parameterized.Parameter(value = 1)
    public User target;

    @Parameterized.Parameter(value = 2)
    public Class<? extends Exception> expectedException;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        if (expectedException != null){
            exception.expect(expectedException);
        }
    }

    // Test method -------------------------------------------------------------------------

    @Test
    public void showProfileUsersTest() throws Exception{
        UserView actual = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> userService.showProfile(target, current, 5));
        Assert.assertNotNull(actual);
    }

    // Tearing down -----------------------------------------------------------------------

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    // Test data ---------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        User normalUser = new UserBuilder().login("Grungle").rights(READ, WATCH_PROFILE, WRITE).createNewUser();
        User moderator = new UserBuilder().login("moder").rights(READ, WATCH_PROFILE, WRITE).createNewUser();
        User cantWatchUser = new UserBuilder().login("some_user").rights(READ).createNewUser();
        User _cantWatchUser = new UserBuilder().login("alex_spz").rights(READ).createNewUser();

        list.add(new Object[]{cantWatchUser, cantWatchUser, null});
        list.add(new Object[]{cantWatchUser, _cantWatchUser, InsufficientPermissionException.class});
        list.add(new Object[]{cantWatchUser, normalUser, InsufficientPermissionException.class});
        list.add(new Object[]{normalUser, moderator, null});
        list.add(new Object[]{normalUser, cantWatchUser, null});
        list.add(new Object[]{moderator, normalUser, null});
        list.add(new Object[]{moderator, _cantWatchUser, null});

        return list;
    }

}
