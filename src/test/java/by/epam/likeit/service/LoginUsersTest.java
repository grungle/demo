package by.epam.likeit.service;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RunWith(Parameterized.class)
public class LoginUsersTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();

    public LoginUsersTest(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter
    public User current;

    @Parameterized.Parameter(value = 1)
    public User expected;


    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{}

    // Test method -------------------------------------------------------------------------

    @Test
    public void showProfileUsersTest() throws Exception{
        User actual = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> userService.login(current));
        Assert.assertEquals(expected, actual);
    }

    // Tearing down -----------------------------------------------------------------------

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    // Test data ---------------------------------------------------------------------------

    @Parameterized.Parameters
    public static Collection<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        User grungleValid = new UserBuilder().login("Grungle").password("rootroot").createNewUser();
        User adminValid = new UserBuilder().login("admin").password("admin").createNewUser();
        User adminInvalid = new UserBuilder().login("admin").password("rootroot").createNewUser();
        User noSuchUser = new UserBuilder().login("qqadq").password("41251gw").createNewUser();
        User _adminInvalid = new UserBuilder().login("admin").password("Admin").createNewUser();
        User _grungleInalid = new UserBuilder().login("GRuNgLe").password("rootroot").createNewUser();

        list.add(new Object[]{grungleValid, grungleValid});
        list.add(new Object[]{adminValid, adminValid});
        list.add(new Object[]{adminInvalid, null});
        list.add(new Object[]{noSuchUser, null});
        list.add(new Object[]{_adminInvalid, null});
        list.add(new Object[]{_grungleInalid, null});

        return list;
    }
}
