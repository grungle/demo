package by.epam.likeit.service;

import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.dao.exception.InsufficientPermissionException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static by.epam.likeit.bean.User.UserRights.*;

@RunWith(Parameterized.class)
public class UpdateAvatarUsersTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();

    public UpdateAvatarUsersTest(){}

    // Parameters --------------------------------------------------------------------------

    @Parameterized.Parameter
    public User user;

    @Parameterized.Parameter(value = 1)
    public String expected;

    @Parameterized.Parameter(value = 2)
    public Class<? extends Exception> expectedException;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    // Setting up --------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        if (expectedException != null){
            exception.expect(expectedException);
        }
    }

    // Test method -------------------------------------------------------------------------

    @Test
    public void updateAvatarTest() throws Exception{

        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            userService.updateAvatar(user);
            return null;
        });
        User actual = ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> userService.find(user.getLogin()));
        Assert.assertEquals(expected, actual.getAvatar());
    }

    // Tearing down -----------------------------------------------------------------------

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    // Test data ---------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();
        User user = new UserBuilder().avatar("/avatar/123default.png").login("moder").rights(WRITE).createNewUser();
        User invalidUser = new UserBuilder().avatar("/avatar/123default.png").login("moder").rights(READ, WATCH_PROFILE).createNewUser();

        list.add(new Object[]{user, "/avatar/123default.png", null});
        list.add(new Object[]{invalidUser, "/avatar/moder.png", InsufficientPermissionException.class});

        return list;
    }
}
