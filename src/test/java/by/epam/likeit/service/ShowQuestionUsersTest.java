package by.epam.likeit.service;


import by.epam.likeit.bean.Question;
import by.epam.likeit.bean.User;
import by.epam.likeit.bean.builder.QuestionBuilder;
import by.epam.likeit.bean.builder.UserBuilder;
import by.epam.likeit.dao.exception.NoSuchEntityException;
import by.epam.likeit.jdbc.transaction.ThreadLocalTransactionManagerImpl;
import by.epam.likeit.service.impl.QuestionServiceImpl;
import by.epam.likeit.service.impl.UserServiceImpl;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import util.TestUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static by.epam.likeit.bean.User.UserRights.*;

@RunWith(Parameterized.class)
public class ShowQuestionUsersTest {

    private static Connection connection;
    private IUserService userService = new UserServiceImpl();
    private IQuestionService questionService = new QuestionServiceImpl();

    // Parameters -----------------------------------------------------------------------------

    @Parameterized.Parameter
    public User user;

    @Parameterized.Parameter(value = 1)
    public Question question;

    @Parameterized.Parameter(value = 2)
    public Long expectedUViews;

    @Parameterized.Parameter(value = 3)
    public Long expectedQViews;

    @Parameterized.Parameter(value = 4)
    public Class<? extends Exception> expectedException;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    // Setting up -----------------------------------------------------------------------------

    @BeforeClass
    public static void setUpClass() throws Exception{
        connection = TestUtils.prepareConnectionForTest();
    }

    @Before
    public void setUp() throws Exception{
        if (expectedException != null){
            exception.expect(expectedException);
        }
    }

    // Test method ----------------------------------------------------------------------------

    @Test
    public void showQuestionTestUserValidation() throws Exception{
        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            userService.showQuestion(user, question);
            return null;
        });

        if (user == null) {
            Assert.assertNull(null);
        } else {

            User actualUser = ThreadLocalTransactionManagerImpl.getInstance()
                    .doWithoutTransaction(() -> userService.find(user.getLogin()));

            Assert.assertEquals(expectedUViews, Long.valueOf(actualUser.getViews()));
        }
    }

    @Test
    public void showQuestionTestQuestionValidation() throws Exception{
        ThreadLocalTransactionManagerImpl.getInstance().doWithoutTransaction(() -> {
            userService.showQuestion(user, question);
            return null;
        });
        Question actualQuestion = ThreadLocalTransactionManagerImpl.getInstance()
                .doWithoutTransaction(() -> questionService.find(question.getId()));

        Assert.assertEquals(expectedQViews, actualQuestion.getViewed());
    }

    // Tearing down ---------------------------------------------------------------------------

    @AfterClass
    public static void tearDownClass() throws Exception{
        connection.close();
    }

    @After
    public void tearDown() throws Exception{
        connection.rollback();
    }

    // Test data ------------------------------------------------------------------------------

    @Parameterized.Parameters
    public static List<Object[]> getData(){
        List<Object[]> list = new ArrayList<>();

        Question validQuestion = (Question) new QuestionBuilder().id(1L).createMessage();
        Question invalidQuestion = (Question) new QuestionBuilder().id(532316L).createMessage();

        User user = new UserBuilder().login("pro_master").rights(READ, WRITE, WATCH_PROFILE).createNewUser();

        list.add(new Object[]{user, validQuestion, 79L, 665L, null});
        list.add(new Object[]{user, invalidQuestion, null, null, NoSuchEntityException.class});
        list.add(new Object[]{null, validQuestion, null, 665L, null});
        list.add(new Object[]{null, invalidQuestion, null, null, NoSuchEntityException.class});
        return list;
    }

}
