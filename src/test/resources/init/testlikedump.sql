-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: likeit_community_test
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer_marks`
--

DROP TABLE IF EXISTS `answer_marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer_marks` (
  `answer_id` int(10) unsigned NOT NULL,
  `login` varchar(30) NOT NULL COMMENT 'It contains user''s login who estimated question',
  `mark` tinyint(1) NOT NULL COMMENT 'Mark from user to user must be in range [-5;5]',
  `is_new` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'For notification feature.',
  PRIMARY KEY (`answer_id`,`login`),
  KEY `answers_marks_users_login_fk_idx` (`login`),
  CONSTRAINT `answer_marks_ibfk_1` FOREIGN KEY (`answer_id`) REFERENCES `answers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `answer_marks_ibfk_2` FOREIGN KEY (`login`) REFERENCES `users` (`login`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of all marks user->answer another user. If admin delete question->answer, all related records in this table will be deleted too on cascade.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer_marks`
--

LOCK TABLES `answer_marks` WRITE;
/*!40000 ALTER TABLE `answer_marks` DISABLE KEYS */;
INSERT INTO `answer_marks` VALUES (1,'alex_spz',4,1),(1,'BalusC',2,1),(1,'Grungle',5,1),(1,'majestics',3,1),(1,'pro_master',2,1),(2,'admin',5,1),(2,'alex_spz',4,1),(2,'BalusC',2,1),(2,'Grungle',4,1),(3,'BigJoe',2,1),(3,'Grungle',5,1),(3,'majestics',3,1),(4,'admin',4,1),(4,'majestics',2,1),(5,'admin',0,1),(5,'Jenkins',-2,1),(5,'majestics',0,1),(5,'moder',2,1),(5,'pro_master',1,1),(6,'admin',1,1),(6,'alex_spz',3,1),(6,'Grungle',0,1),(6,'moderator',3,1),(6,'some_user',0,1),(7,'admin',0,1),(7,'alex_spz',5,1),(7,'moderator',5,1),(7,'pro_master',5,1),(7,'some_user',4,1),(8,'alex_spz',3,1),(8,'BalusC',4,1),(8,'BigJoe',5,1),(8,'Grungle',4,1),(9,'admin',2,1),(9,'Grungle',1,1),(9,'Jenkins',3,1),(9,'moder',3,1),(9,'some_user',0,1),(10,'admin',5,1),(10,'Jenkins',1,1),(10,'moderator',3,1),(10,'pro_master',5,1),(11,'admin',1,1),(11,'BalusC',2,1),(11,'it_expert',3,1),(11,'Jenkins',3,1),(12,'Grungle',1,1),(12,'it_expert',3,1),(12,'Jenkins',2,1),(12,'majestics',0,1),(13,'admin',0,1),(13,'alex_spz',1,1),(13,'Grungle',1,1),(13,'Jenkins',-1,1),(13,'pro_master',1,1),(14,'Grungle',2,1),(14,'it_expert',2,1),(15,'admin',5,1),(15,'Grungle',5,1),(15,'Jenkins',5,1),(15,'moder',4,1),(16,'BigJoe',0,1),(16,'it_expert',-3,1),(16,'some_user',2,1),(17,'it_expert',4,1),(17,'moderator',5,1),(17,'pro_master',5,1),(18,'BalusC',3,1),(18,'Grungle',3,1),(18,'it_expert',4,1),(19,'admin',4,1),(19,'alex_spz',5,1),(19,'moderator',4,1),(20,'it_expert',4,1),(20,'pro_master',5,1),(20,'some_user',4,1),(21,'BalusC',3,1),(21,'Jenkins',3,1),(21,'pro_master',4,1),(22,'BalusC',1,1),(22,'Grungle',-3,1),(22,'pro_master',-2,1),(45,'vbnmz',0,1),(46,'Grungle',0,1),(47,'vbnmz',0,1),(49,'Grungle',0,1),(70,'vbnmz',0,1),(72,'vbnmz',0,1),(73,'vbnmz',0,1),(75,'Grungle',1,1),(78,'admin',1,1);
/*!40000 ALTER TABLE `answer_marks` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`answersNewMark` AFTER INSERT ON `answer_marks` FOR EACH ROW
  BEGIN
    DECLARE target_user_login VARCHAR(30);

    SELECT login INTO target_user_login
    FROM answers
    WHERE answers.id = NEW.answer_id;

    UPDATE answers SET
      rating = rating + NEW.mark
    WHERE id = NEW.answer_id;

    UPDATE users SET
      rating = rating + NEW.mark
    WHERE users.login = target_user_login;

    IF NEW.mark < 0 THEN
      UPDATE users SET
        rating = rating + NEW.mark
      WHERE users.login = NEW.login;
    END IF;

  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`answer_marksUpdateMark` AFTER UPDATE ON `answer_marks` FOR EACH ROW
  BEGIN
    DECLARE target_user_login VARCHAR(30);

    SELECT login INTO target_user_login
    FROM answers
    WHERE answers.id = NEW.answer_id;

    UPDATE answers SET
      rating = rating + NEW.mark - OLD.mark
    WHERE id = NEW.answer_id;

    UPDATE users SET
      rating = rating + NEW.mark - OLD.mark
    WHERE users.login = target_user_login;

    IF NEW.mark <= 0 AND OLD.MARK <=0 THEN
      UPDATE users SET
        rating = rating - OLD.mark + NEW.mark
      WHERE users.login = OLD.login;
    END IF;

  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`answer_AD_marksDeleteMark` AFTER DELETE ON `answer_marks` FOR EACH ROW
  BEGIN
    DECLARE target_user_login VARCHAR(30);

    SELECT login INTO target_user_login
    FROM answers
    WHERE answers.id = OLD.answer_id;

    UPDATE users SET
      rating = rating - OLD.mark
    WHERE users.login = target_user_login;

    UPDATE answers SET
      rating = rating - OLD.mark
    WHERE answers.id = OLD.answer_id;

    -- Restore reputation of user who leave negative mark
    IF OLD.mark < 0 THEN
      UPDATE users SET
        rating = rating - OLD.mark
      WHERE users.login = OLD.login;
    END IF;

    -- User can't delete his mark, he can change it to neutral. So we don't need to update answer rating here.
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL,
  `login` varchar(30) NOT NULL,
  `body` varchar(10000) NOT NULL,
  `publication_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of publication',
  `rating` int(11) NOT NULL DEFAULT '0' COMMENT 'Sum of all marks to the answer',
  `is_new` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'If the answer is new for a user who asked question(for notification feature)',
  `state` enum('publicated','modified','moderated') NOT NULL DEFAULT 'publicated' COMMENT 'modified - If the answer was modified by user.\nmoderated - If the answer was moderated by admin/moderator\npublicated - no modification/moderation\n*Only for displaying like "publicated at + publication date and  modified or modificated by...  at .... last_update"',
  `last_update` datetime DEFAULT NULL COMMENT 'This field are common for answers and answers_moderations tables because of user can not modify his answer after moderation by admin/moderator. Also the answer may not be moderated, only modified by user.',
  PRIMARY KEY (`id`),
  KEY `fk_answers_users1_idx` (`login`),
  KEY `fk_answers_quest_id_idx` (`question_id`),
  CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `answers_ibfk_2` FOREIGN KEY (`login`) REFERENCES `users` (`login`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COMMENT='User can''t delete his answer. In this case it''s body will be changed to ''message is deleted''(Implemented in Java). The only way to delete answer is delete question(only for admin/moderator). All answers will be deleted on cascade.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,1,'admin','You are using Ajax incorrectly. The idea is not to have it return anything, but instead hand off the data to something called a callback function, which handles the data.\nReturning anything in the submit handler will not do anything. You must instead either hand off the data, or do what you want with it directly inside the success function.','2017-02-23 13:00:21',16,1,'publicated','2017-04-04 02:43:54'),(2,1,'pro_master','For people who are using AngularJS, can handle this situation using Promises.\nHere it says,\nPromises can be used to unnest asynchronous functions and allows one to chain multiple functions together.\nYou can find a nice explanation here also.','2017-02-23 13:01:21',15,1,'moderated','2017-04-04 13:46:47'),(3,2,'admin','Some of the solutions posted here are inefficient. Repeating the regular expression search every time the script needs to access a parameter is completely unnecessary, one single function to split up the parameters into an associative-array style object is enough. If you\'re not working with the HTML 5 History API, this is only necessary once per page load. The other suggestions here also fail to decode the URL correctly.','2017-02-23 13:05:21',10,1,'modified','2017-04-04 00:42:02'),(4,2,'some_user','Just another recommendation. The plugin Purl allows to retrieve all parts of URL, including anchor, host, etc.\nIt can be used with or without jQuery.','2017-02-23 13:07:21',6,1,'moderated','2017-04-15 01:41:41'),(5,2,'alex_spz','I like some_user\'s solution. But I don\'t see any point of extending jQuery for that? There is no usage of jQuery functionality.\nOn other hand I like the built-in function in Google Chrome: window.location.getParameter.','2017-02-23 13:09:21',1,1,'publicated','2017-04-04 13:21:55'),(6,4,'Jenkins','Note, that a lot of the answers state that Hashtable is synchronised. In practice this buys you very little. The synchronization is on the accessor / mutator methods will stop two threads adding or removing from the map concurrently, but in the real world you will often need additional synchronisation.','2017-02-23 13:23:21',7,1,'modified','2017-04-04 13:45:02'),(7,4,'Grungle','There are several differences between HashMap and Hashtable in Java:\nHashtable is synchronized, whereas HashMap is not. This makes HashMap better for non-threaded applications, as unsynchronized Objects typically perform better than synchronized ones.\nHashtable does not allow null keys or values.  HashMap allows one null key and any number of null values.\nOne of HashMap\'s subclasses is LinkedHashMap, so in the event that you\'d want predictable iteration order (which is insertion order by default), you could easily swap out the HashMap for a LinkedHashMap. This wouldn\'t be as easy if you were using Hashtable.\nSince synchronization is not an issue for you, I\'d recommend HashMap. If synchronization becomes an issue, you may also look at ConcurrentHashMap.','2017-02-23 13:27:21',19,1,'moderated','2017-04-17 01:42:38'),(8,4,'admin','In addition to what izb said, HashMap allows null values, whereas the Hashtable does not.\nAlso note that Hashtable extends the Dictionary class, which as the Javadocs state, is obsolete and has been replaced by the Map interface.','2017-02-23 13:31:21',16,1,'modified','2017-04-17 01:42:50'),(9,5,'majestics','Map<String, String> map = ...\nfor (Map.Entry<String, String> entry : map.entrySet())\n{\n    System.out.println(entry.getKey() + \"/\" + entry.getValue());\n}','2017-02-23 13:44:21',9,1,'modified','2017-04-04 13:27:29'),(10,5,'Grungle','In Java 8 you can do it clean and fast using the new lambdas features:\n Map&lt;String,String&gt; map = new HashMap&lt;&gt;();\n map.put(\"SomeKey\", \"SomeValue\");\n map.forEach( (k,v) -&gt; [do something with key and value] );\n // such as\n map.forEach( (k,v) -&gt; System.out.println(\"Key: \" + k + \": Value: \" + v));\nThe type of k and v will be inferred by the compiler and there is no need to use Map.Entry anymore.','2017-02-23 13:49:21',14,1,'moderated','2017-04-06 23:52:00'),(11,5,'alex_spz','For what it\'s worth, here\'s another way to extract tokens from an input string, relying only on standard library facilities. It\'s an example of the power and elegance behind the design of the STL.\n\n#include <iostream>\n#include <string>\n#include <sstream>\n#include <algorithm>\n#include <iterator>\n\nint main() {\n    using namespace std;\n    string sentence = \"And I feel fine...\";\n    istringstream iss(sentence);\n    copy(istream_iterator<string>(iss),\n         istream_iterator<string>(),\n         ostream_iterator<string>(cout, \"\n\"));\n}\nInstead of copying the extracted tokens to an output stream, one could insert them into a container, using the same generic copy algorithm.\n\nvector<string> tokens;\ncopy(istream_iterator<string>(iss),\n     istream_iterator<string>(),\n     back_inserter(tokens));\n... or create the vector directly:\n\nvector<string> tokens{istream_iterator<string>{iss},\n                      istream_iterator<string>{}};','2017-02-23 13:54:21',9,1,'publicated','2017-04-04 13:20:43'),(12,8,'admin','ALTER TABLE {TABLENAME} \nADD {COLUMNNAME} {TYPE} {NULL|NOT NULL} \nCONSTRAINT {CONSTRAINT_NAME} DEFAULT {DEFAULT_VALUE}\n[WITH VALUES]','2017-02-23 14:35:21',6,1,'modified','2017-04-04 02:44:00'),(13,8,'moderator','ALTER TABLE Protocols\nADD ProtocolTypeID int NOT NULL DEFAULT(1)\nGO\nThe inclusion of the DEFAULT fills the column in existing rows with the default value, so the NOT NULL constraint is not violated.','2017-02-23 14:43:21',2,1,'publicated','2017-04-04 13:35:30'),(14,11,'pro_master','DriverManager is a fairly old way of doing things. The better way is to get a DataSource, either by looking one up that your app server container already configured for you:\n\nContext context = new InitialContext();\nDataSource dataSource = (DataSource) context.lookup(\"java:comp/env/jdbc/myDB\");\nor instantiating and configuring one from your database driver directly:\n\nMysqlDataSource dataSource = new MysqlDataSource();\ndataSource.setUser(\"scott\");\ndataSource.setPassword(\"tiger\");\ndataSource.setServerName(\"myDBHost.example.org\");\nand then obtain connections from it, same as above:\n\nConnection conn = dataSource.getConnection();\nStatement stmt = conn.createStatement();\nResultSet rs = stmt.executeQuery(\"SELECT ID FROM USERS\");\n...\nrs.close();\nstmt.close();\nconn.close();','2017-02-23 15:33:21',4,1,'modified','2017-04-06 17:20:49'),(15,12,'BalusC','It really doesn\'t matter to me or anyone else where exactly on the local disk file system it will be saved, as long as you do not ever use getRealPath() method. Using that method is in any case alarming.\n\nThe path to the storage location can in turn be definied in many ways. You have to do it all by yourself. Perhaps this is where your confusion is caused because you somehow expected that the server does that all automagically. Please note that @MultipartConfig(location) does not specify the final upload destination, but the temporary storage location for the case file size exceeds memory storage threshold.\n\nSo, the path to the final storage location can be definied in either of the following ways:\n\nHardcoded:\nFile uploads = new File(\"/path/to/uploads\");\nEnvironment variable via SET UPLOAD_LOCATION=/path/to/uploads:\nFile uploads = new File(System.getenv(\"UPLOAD_LOCATION\"));\nVM argument during server startup via -Dupload.location=\"/path/to/uploads\":\nFile uploads = new File(System.getProperty(\"upload.location\"));\n*.properties file entry as upload.location=/path/to/uploads:\nFile uploads = new File(properties.getProperty(\"upload.location\"));\nweb.xml <context-param> with name upload.location and value /path/to/uploads:\nFile uploads = new File(getServletContext().getInitParameter(\"upload.location\"));\nIf any, use the server-provided location, e.g. in JBoss AS/WildFly:\nFile uploads = new File(System.getProperty(\"jboss.server.data.dir\"), \"uploads\");\nEither way, you can easily reference and save the file as follows:\n\nFile file = new File(uploads, \"somefilename.ext\");\n\ntry (InputStream input = part.getInputStream()) {\n    Files.copy(input, file.toPath());\n}\nOr, when you want to autogenerate an unique file name to prevent users from overwriting existing files with coincidentally the same name:\n\nFile file = File.createTempFile(\"somefilename-\", \".ext\", uploads);\n\ntry (InputStream input = part.getInputStream()) {\n    Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);\n}\nHow to obtain part in JSP/Servlet is answered in How to upload files to server using JSP/Servlet? and how to obtain part in JSF is answered in How to upload file using JSF 2.2 <h:inputFile>? Where is the saved File?\n\nNote: do not use Part#write() as it interprets the path relative to the temporary storage location defined in @MultipartConfig(location).','2017-02-23 15:59:21',19,1,'modified','2017-04-04 00:19:41'),(16,13,'majestics','From Browser/Client perspective\n\nJSP and JSF both looks same, As Per Application Requirements goes, JSP is more suited for request - response based applications.\n\nJSF is targetted for richer event based Web applications. I see event as much more granular than request/response.\n\nFrom Server Perspective\n\nJSP page is converted to servlet, and it has only minimal behaviour.\n\nJSF page is converted to components tree(by specialized FacesServlet) and it follows component lifecycle defined by spec.','2017-02-23 16:27:21',-1,1,'publicated','2017-04-04 02:31:24'),(17,13,'BalusC','ALTER TABLE Protocols\nADD ProtocolTypeID int NOT NULL DEFAULT(1)JSP (JavaServer Pages)\n\nJSP is a Java view technology running on the server machine which allows you to write template text in client side languages (like HTML, CSS, JavaScript, ect.). JSP supports taglibs, which are backed by pieces of Java code that let you control the page flow or output dynamically. A well-known taglib is JSTL. JSP also supports Expression Language, which can be used to access backend data (via attributes available in the page, request, session and application scopes), mostly in combination with taglibs.\n\nWhen a JSP is requested for the first time or when the web app starts up, the servlet container will compile it into a class extending HttpServlet and use it during the web app\'s lifetime. You can find the generated source code in the server\'s work directory. In for example Tomcat, it\'s the /work directory. On a JSP request, the servlet container will execute the compiled JSP class and send the generated output (usually just HTML/CSS/JS) through the web server over a network to the client side, which in turn displays it in the web browser.\n\nServlets\n\nServlet is a Java application programming interface (API) running on the server machine, which intercepts requests made by the client and generates/sends a response. A well-known example is the  HttpServlet which provides methods to hook on HTTP requests using the popular HTTP methods such as GET and POST. You can configure HttpServlets to listen to a certain HTTP URL pattern, which is configurable in web.xml, or more recently with Java EE 6, with @WebServlet annotation.\n\nWhen a Servlet is first requested or during web app startup, the servlet container will create an instance of it and keep it in memory during the web app\'s lifetime. The same instance will be reused for every incoming request whose URL matches the servlet\'s URL pattern. You can access the request data by HttpServletRequest and handle the response by HttpServletResponse. Both objects are available as method arguments inside any of the overridden methods of HttpServlet, such as doGet() and doPost().\n\nJSF (JavaServer Faces)\n\nJSF is a component based MVC framework which is built on top of the Servlet API and provides components via taglibs which can be used in JSP or any other Java based view technology such as Facelets. Facelets is much more suited to JSF than JSP. It namely provides great templating capabilities such as composite components, while JSP basically only offers the <jsp:include> for templating, so that you\'re forced to create custom components with raw Java code (which is a bit opaque and a lot of tedious work in JSF) when you want to replace a repeated group of components with a single component. Since JSF 2.0, JSP has been deprecated as view technology in favor of Facelets.\n\nAs being a MVC (Model-View-Controller) framework, JSF provides the FacesServlet as the sole request-response Controller. It takes all the standard and tedious HTTP request/response work from your hands, such as gathering user input, validating/converting them, putting them in model objects, invoking actions and rendering the response. This way you end up with basically a JSP or Facelets (XHTML) page for View and a JavaBean class as Model. The JSF components are used to bind the view with the model (such as your ASP.NET web control does) and the FacesServlet uses the JSF component tree to do all the work.','2017-02-23 16:40:21',14,1,'publicated','2017-04-04 13:35:04'),(18,15,'alex_spz','For now you won\'t need an answer on which servlet mapping to use, I think. You would probably be better of with some reading first. I have taken the liberty to find two related questions which might open your eyes a little considering JSF and the file extension.','2017-02-23 17:29:21',10,1,'moderated','2017-04-04 00:10:29'),(19,16,'BalusC','Actually, all of those examples on the web wherein the common content/file type like \"js\", \"css\", \"img\", etc is been used as library name are misleading.\n\nReal world examples\n\nTo start, let\'s look at how existing JSF implementations like Mojarra and MyFaces and JSF component libraries like PrimeFaces and OmniFaces use it. No one of them use resource libraries this way. They use it (under the covers, by @ResourceDependency or UIViewRoot#addComponentResource()) the following way:\n\n<h:outputScript library=\"javax.faces\" name=\"jsf.js\" />\n<h:outputScript library=\"primefaces\" name=\"jquery/jquery.js\" />\n<h:outputScript library=\"omnifaces\" name=\"omnifaces.js\" />\n<h:outputScript library=\"omnifaces\" name=\"fixviewstate.js\" />\n<h:outputScript library=\"omnifaces.combined\" name=\"[dynamicname].js\" />\n<h:outputStylesheet library=\"primefaces\" name=\"primefaces.css\" />\n<h:outputStylesheet library=\"primefaces-aristo\" name=\"theme.css\" />\n<h:outputStylesheet library=\"primefaces-vader\" name=\"theme.css\" />\nIt should become clear that it basically represents the common library/module/theme name where all of those resources commonly belong to.','2017-02-23 18:03:21',13,1,'publicated',NULL),(20,17,'BalusC','I have never used the first, but the second is syntactically valid and should technically work. If it doesn\'t work, then the relative URL in the href attribute is simply wrong.\n\nIn relative URL\'s, the leading slash / points to the domain root. So if the JSF page is for example requested by http://example.com/context/page.jsf, the CSS URL will absolutely point to http://example.com/styles/decoration.css. To know the valid relative URL, you need to know the absolute URL of both the JSF page and the CSS file and extract the one from the other.\n\nLet guess that your CSS file is actually located at http://example.com/context/styles/decoration.css, then you need to remove the leading slash so that it is relative to the current context (the one of the page.jsp):\n\n<link rel=\"stylesheet\" type=\"text/css\" href=\"styles/decoration.css\" />','2017-02-23 18:39:21',13,1,'publicated',NULL),(21,17,'BigJoe','The updated JSF 2.0 method is a bit tidier. Instead of: <link rel=\"stylesheet\" type=\"text/css\" href=\"#{request.contextPath}/css/compass.css\">;\nyou now do this: <h:outputStylesheet library=\"css\" name=\"compass.css\"/>;\nand the stylesheet resource should be placed in resources/css. Where resources is at the same level as the WEB-INF.','2017-02-23 18:56:21',10,1,'moderated','2017-04-17 16:40:39'),(22,5,'admin','Just empty message','2017-02-25 16:28:12',-4,1,'publicated',NULL),(27,14,'pro_master','Pro_master answer)','2017-04-16 23:47:01',0,1,'modified','2017-04-16 23:52:26'),(30,20,'Grungle','jejetyj','2017-04-17 00:55:31',0,1,'publicated',NULL),(43,25,'admin','1','2017-04-17 17:12:03',0,1,'publicated',NULL),(44,25,'admin','2','2017-04-17 17:12:14',0,1,'publicated',NULL),(45,25,'admin','3','2017-04-17 17:12:18',0,1,'publicated',NULL),(46,25,'admin','4','2017-04-17 17:12:22',0,1,'publicated',NULL),(47,25,'admin','5','2017-04-17 17:12:25',0,1,'publicated',NULL),(48,25,'admin','6','2017-04-17 17:12:30',0,1,'publicated',NULL),(49,25,'admin','7','2017-04-17 17:13:32',0,1,'publicated',NULL),(50,25,'admin','8','2017-04-17 17:13:45',0,1,'publicated',NULL),(51,25,'admin','9','2017-04-17 17:13:49',0,1,'publicated',NULL),(52,25,'admin','10','2017-04-17 17:13:53',0,1,'publicated',NULL),(53,25,'admin','11','2017-04-17 17:13:55',0,1,'publicated',NULL),(54,25,'admin','12','2017-04-17 17:13:58',0,1,'publicated',NULL),(55,25,'admin','13','2017-04-17 17:14:01',0,1,'publicated',NULL),(56,25,'admin','14','2017-04-17 17:14:03',0,1,'publicated',NULL),(57,25,'admin','15','2017-04-17 17:14:06',0,1,'publicated',NULL),(58,25,'admin','16','2017-04-17 17:14:13',0,1,'publicated',NULL),(59,25,'admin','17','2017-04-17 17:16:14',0,1,'publicated',NULL),(60,25,'admin','18','2017-04-17 17:16:16',0,1,'publicated',NULL),(61,25,'admin','19','2017-04-17 17:16:19',0,1,'publicated',NULL),(62,25,'admin','20','2017-04-17 17:16:25',0,1,'publicated',NULL),(63,25,'admin','21','2017-04-17 17:16:29',0,1,'publicated',NULL),(64,25,'admin','22','2017-04-17 17:16:31',0,1,'publicated',NULL),(65,25,'admin','23','2017-04-17 17:16:34',0,1,'publicated',NULL),(66,25,'admin','24','2017-04-17 17:16:36',0,1,'publicated',NULL),(67,25,'admin','25','2017-04-17 17:16:38',0,1,'publicated',NULL),(68,25,'admin','26','2017-04-17 17:16:40',0,1,'modified','2017-04-17 17:46:37'),(69,25,'admin','27','2017-04-17 17:16:43',0,1,'publicated',NULL),(70,25,'admin','28','2017-04-17 17:16:46',0,1,'publicated',NULL),(71,25,'admin','29','2017-04-17 17:33:28',0,1,'publicated',NULL),(72,25,'admin','30','2017-04-17 17:40:22',0,1,'publicated',NULL),(73,25,'admin','31','2017-04-17 17:40:33',0,1,'publicated',NULL),(75,28,'admin','aaa','2017-04-17 19:25:38',1,1,'publicated',NULL),(76,14,'pro_master','hi','2017-04-18 03:23:35',0,1,'publicated',NULL),(78,21,'Grungle','Just a message','2017-04-20 20:27:54',1,1,'publicated',NULL);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,ALLOW_INVALID_DATES,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`answers_AI_update_answers_count` AFTER INSERT ON `answers` FOR EACH ROW
  BEGIN
    UPDATE users SET
      answers_count = answers_count + 1
    WHERE login = NEW.login;
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`answers_BEFORE_DELETE` BEFORE DELETE ON `answers` FOR EACH ROW
  BEGIN

    #answers_count and rating
    UPDATE users SET
      answers_count = answers_count - 1,
      users.rating = users.rating - OLD.rating
    WHERE users.login = OLD.login;

    #restore negative marks
    UPDATE users JOIN answer_marks USING(login) JOIN answers ON answers.id = answer_marks.answer_id
    SET users.rating = users.rating -
                       coalesce((SELECT mark FROM answer_marks WHERE answer_id = OLD.id AND login = users.login AND mark < 0), 0)
    WHERE answer_marks.login = users.login;

  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `answers_moderations`
--

DROP TABLE IF EXISTS `answers_moderations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers_moderations` (
  `answer_id` int(10) unsigned NOT NULL,
  `moderator` varchar(30) NOT NULL,
  `reason` varchar(255) DEFAULT NULL COMMENT 'Violation of rules, ect',
  `is_new` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Notify user if admin/moderator has moderate his question',
  PRIMARY KEY (`answer_id`,`moderator`),
  KEY `fk_answer_modifications_users_login_idx` (`moderator`),
  CONSTRAINT `answers_moderations_ibfk_1` FOREIGN KEY (`answer_id`) REFERENCES `answers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `answers_moderations_ibfk_2` FOREIGN KEY (`moderator`) REFERENCES `users` (`login`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='I am not going to store all history of moderation. Moderator/admin can fix answer multiple times and I need store last fix.\nRelation 1-1 because of this information is used only if moderator/admin has modified/closed the answer. I estimate it about 5-7% cases of all answers\nIf admin delete question->answer, all related records in this table will be deleted too on cascade.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers_moderations`
--

LOCK TABLES `answers_moderations` WRITE;
/*!40000 ALTER TABLE `answers_moderations` DISABLE KEYS */;
INSERT INTO `answers_moderations` VALUES (2,'moder','Incorrect reference fix',1),(4,'admin',NULL,1),(7,'admin',NULL,1),(10,'admin',NULL,1),(18,'moderator','Advertising is not permitted',1),(21,'admin',NULL,1);
/*!40000 ALTER TABLE `answers_moderations` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`answers_moderations_AFTER_INSERT` AFTER INSERT ON `answers_moderations` FOR EACH ROW
  BEGIN
    UPDATE answers SET
      state = 'moderated',
      last_update = CURRENT_TIMESTAMP
    WHERE answers.id = NEW.answer_id;
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`answers_moderations_AFTER_UPDATE` AFTER UPDATE ON `answers_moderations` FOR EACH ROW
  BEGIN
    UPDATE answers SET
      state = 'moderated',
      last_update = CURRENT_TIMESTAMP
    WHERE answers.id = NEW.answer_id;
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(10) unsigned NOT NULL,
  `login` varchar(30) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` varchar(10000) NOT NULL,
  `publication_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime of publication',
  `viewed` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Amount of views(not unique views).',
  `state` enum('open','modified','moderated','closed') NOT NULL DEFAULT 'open' COMMENT 'Comumn for displaying the state of the question. As one question can be modified by moderator/admin and user.\nmodified - user updated his question\nmoderated - admin/moderator fixed the question. And the related table(questions moderations) has corresponding record. AFTER MODERATION USER CAN NOT MODIFY HIS QUESTION, but \n(Moderator/admin can)!!\nclosed - admin/moderator/user can close question.\nbecause of I am going to show information about date of publication AND information about last  modification and corresponding word(publicated at ...  last modified/moderated at... by login of user/admin).',
  `last_update` datetime DEFAULT NULL COMMENT 'Is used for displayng last moderation or modification datetime.\nThis field are common for questions and questions_moderations tables because of user can not modify his question after moderation by admin/moderator. Also the question may not be moderated, only modified by user.',
  PRIMARY KEY (`id`),
  KEY `login_fk_idx` (`login`),
  KEY `topics_id_fk_idx` (`topic_id`),
  CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`login`) REFERENCES `users` (`login`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='User can only change status of his question to closed. Admin/Moderator can delete question.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,6,'Grungle','How do I return the response from an asynchronous call?','I have a function foo which makes an Ajax request. How can I return the response from foo?\nI tried to return the value from the success callback as well as assigning the response to a local variable inside the function and return that one, but none of those ways actually return the response.','2017-02-21 17:31:25',664,'open',NULL),(2,6,'majestics','How can I get query string values in JavaScript?','Is there a plugin-less way of retrieving query string values via jQuery (or without)? If so, how? If not, is there a plugin which can do so?','2017-02-21 17:37:25',1443,'open',NULL),(3,6,'majestics','How does the “this” keyword work?','I have noticed that there doesn\'t appear to be a clear explanation of what the this keyword is and how it is correctly (and incorrectly) used in JavaScript on the Stack Overflow site.\nI have witnessed some very strange behaviour with it and have failed to understand why it has occurred. How does this work and when should it be used?','2017-02-21 17:43:25',2601,'closed','2017-02-24 19:58:39'),(4,1,'alex_spz','Differences between HashMap and Hashtable?','What are the differences between a HashMap and a Hashtable in Java? Which is more efficient for non-threaded applications?','2017-02-21 17:29:25',447,'moderated','2017-04-06 19:36:59'),(5,1,'Jenkins','How to efficiently iterate over each Entry in a Map?','If I have an object implementing the Map interface in Java and I wish to iterate over every pair contained within it, what is the most efficient way of going through the map? \nWill the ordering of elements depend on the specific map implementation that I have for the interface?','2017-02-21 17:30:25',576,'open',NULL),(6,5,'Jenkins','Split a string in C++?','What\'s the most elegant way to split a string in C++? The string can be assumed to be composed of words separated by whitespace.\n(Note that I\'m not interested in C string functions or that kind of character manipulation/access. Also, please give precedence to elegance over efficiency in your answer.)','2017-02-21 17:55:25',12153,'open',NULL),(7,5,'some_user','Similar function to java\'s string.split(“ ”) in C++','I am looking for the similar function in C++ to string.split(delimiter).It does return an array of strings cut by specified delimiter','2017-02-21 18:00:25',17442,'moderated','2017-02-24 15:45:22'),(8,3,'majestics','Add a column with a default value to an existing table in SQL Server','How can a column with a default value be added to an existing table in SQL Server 2000 / SQL Server 2005?','2017-02-21 17:49:25',2570,'modified','2017-02-24 15:45:22'),(9,3,'some_user','How can I prevent SQL injection?','If user input is inserted without modification into an SQL query, then the application becomes vulnerable to SQL injection','2017-02-21 17:52:25',2987,'open',NULL),(10,3,'pro_master','Are PDO prepared statements sufficient to prevent SQL injection?','You can assume MySQL if it makes a difference. Also, I\'m really only curious about the use of prepared statements against SQL injection. In this context, I don\'t care about XSS or other possible vulnerabilities.','2017-02-21 17:55:25',3473,'moderated','2017-02-24 20:00:45'),(11,3,'it_expert','Connect Java to a MySQL database','How do you connect to a MySQL database in Java?','2017-02-21 17:58:25',3679,'open',NULL),(12,2,'Grungle','Recommended way to save uploaded files in a servlet application','I read here that one should not save the file in the server anyway as it is not portable, transactional and requires external parameters. However, given that I need a tmp solution for tomcat (7) and that I have (relative) control over the server machine?','2017-02-21 17:49:25',2486,'modified','2017-02-24 15:45:22'),(13,2,'it_expert','What is the difference between JSF, Servlet and JSP?','How are JSP and Servlet related to each other? Is JSP some kind of Servlet? How are JSP and JSF related to each other? Is JSF some kind of prebuild UI based JSP like ASP.NET-MVC?','2017-02-21 17:51:25',2700,'open',NULL),(14,2,'moder','What is the difference between creating JSF pages with .jsp or .xhtml or .jsf extension','I saw some examples creating the JSF pages with .jsp extension, other examples creating them with .xhtml extension, and other examples choose .jsf. I just would like to know what the difference is between above extensions when working with JSF pages, and how to choose the appropriate extension?','2017-02-21 17:53:25',2934,'open',NULL),(15,2,'pro_master','Servlet mapping JSF','I\'m asking myself which way is the best for servlet mapping in jsf? And Why?','2017-02-21 17:55:25',3228,'open',NULL),(16,2,'moderator','What is the JSF resource library for and how should it be used?','The JSF <h:outputStylesheet>, <h:outputScript> and <h:graphicImage> components have a library attribute. What is this and how should this be used? There are a lot of examples on the web which use it as follows with the common content/file type css, js and img (or image) as library name depending on the tag used','2017-02-21 17:57:25',3487,'modified','2017-02-24 15:45:22'),(17,2,'pro_master','External CSS for JSF','What is syntax to add external CSS file to jsf?','2017-02-21 17:59:25',3919,'open',NULL),(20,4,'Fooooo','What is the main advantage of C# over Java?','Not from design perspective, not the usual limited to Microsoft and stuff, but really, except that, is C# in anyway powerful over Java?\n\nI have worked in Java for almost 2 years, but the organisation I have joined needs me to work in C# for sometime. Now, working through the syntax, I feel like I am dealing with some cheap Java ripoff. I can\'t help but think why couldn\'t Microsoft work with friggin java instead of creating a cheap imitation? I know this is some sort of a holy war going on since the inception of C#. But can anyone really tell me its advantages over java so I stop feeling that I am working with crap.','2017-04-12 14:28:57',33,'open',NULL),(21,33,'Fooooo','Test Question of new user','I\'ve decided to check my WRITE right;)','2017-04-12 14:30:59',25,'modified','2017-04-12 14:31:09'),(25,39,'admin','etykeyti','yj567','2017-04-17 01:47:01',218,'open',NULL),(27,41,'admin','Crete <question> with & escape chars','Body <with> & es    cape     Xml c a   h   r  ','2017-04-17 16:50:00',19,'moderated',NULL),(28,42,'admin','HEADER_COOKIES','BODY_COOKIES','2017-04-17 17:51:13',30,'open',NULL);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,ALLOW_INVALID_DATES,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`questions_AI_update_questions_count` AFTER INSERT ON `questions` FOR EACH ROW
  BEGIN
    UPDATE users SET
      questions_count = questions_count + 1
    where login = NEW.login;
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`questions_BEFORE_DELETE` BEFORE DELETE ON `questions` FOR EACH ROW
  BEGIN

    #questions_count
    UPDATE users SET questions_count = questions_count - 1 WHERE users.login = OLD.login;

    #answers_count
    UPDATE users JOIN answers USING(login) SET answers_count = answers_count -
                                                               (SELECT COUNT(1) FROM answers
                                                               WHERE answers.question_id = OLD.id AND users.login = answers.login)
    WHERE answers.login = users.login;

    #restore rating
    UPDATE users JOIN answers USING(login) SET users.rating = users.rating -
                                                              (SELECT res FROM
                                                                (SELECT answers.login, SUM(rating) AS 'res' FROM answers
                                                                GROUP BY answers.question_id, answers.login
                                                                HAVING answers.question_id = OLD.id
                                                                )AS `tbl`
                                                              WHERE login = users.login)
    WHERE answers.login = users.login AND answers.question_id = OLD.id;

    #restore negative rating to estimator
    UPDATE users JOIN answer_marks USING(login) JOIN answers ON answers.id = answer_marks.answer_id
    SET users.rating = users.rating -
                       coalesce((SELECT SUM(res) FROM
                         (SELECT answer_marks.login, SUM(mark) as 'res' FROM answer_marks
                           JOIN answers ON answer_marks.answer_id = answers.id
                           JOIN questions ON answers.question_id = questions.id
                         GROUP BY answer_marks.login, question_id, mark
                         HAVING mark < 0 AND question_id = OLD.id) AS `tbl`
                       WHERE login = users.login), 0)
    WHERE answer_marks.login = users.login;

  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `questions_moderations`
--

DROP TABLE IF EXISTS `questions_moderations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions_moderations` (
  `question_id` int(10) unsigned NOT NULL,
  `moderator` varchar(30) NOT NULL COMMENT 'login of admin/moderator who modified the question',
  `reason` varchar(255) DEFAULT NULL COMMENT 'For example: violation of rules/duplicate',
  `is_new` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'For notification feature',
  PRIMARY KEY (`question_id`,`moderator`),
  KEY `fk_quest_moderations_users_login_idx` (`moderator`),
  CONSTRAINT `questions_moderations_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `questions_moderations_ibfk_2` FOREIGN KEY (`moderator`) REFERENCES `users` (`login`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' I am not going to store all history of moderation. Moderator/admin can fix question multiple times and I need store last fix.\nRelation 1-1 because of this information is used only if moderator/admin has modified/closed the question. I estimate it about 5-7% cases of all questions.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions_moderations`
--

LOCK TABLES `questions_moderations` WRITE;
/*!40000 ALTER TABLE `questions_moderations` DISABLE KEYS */;
INSERT INTO `questions_moderations` VALUES (3,'admin','Question must be clear.',1),(4,'admin',NULL,1),(7,'moderator','Duplicate. See question with id = 6.',1),(10,'moderator','Just rephrase question.',1),(27,'admin',NULL,1);
/*!40000 ALTER TABLE `questions_moderations` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`questions_moderations_AFTER_INSERT` AFTER INSERT ON `questions_moderations` FOR EACH ROW
  BEGIN
    UPDATE questions SET
      state = 'moderated',
      last_update = CURRENT_TIMESTAMP
    WHERE questions.id = new.question_id;
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`questions_moderations_AFTER_UPDATE` AFTER UPDATE ON `questions_moderations` FOR EACH ROW
  BEGIN
    UPDATE questions SET
      state = 'moderated',
      last_update = CURRENT_TIMESTAMP
    WHERE questions.id = new.question_id;
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `testt`
--

DROP TABLE IF EXISTS `testt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uname` varchar(30) DEFAULT NULL,
  `unumeric` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testt`
--

LOCK TABLES `testt` WRITE;
/*!40000 ALTER TABLE `testt` DISABLE KEYS */;
/*!40000 ALTER TABLE `testt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topics`
--

DROP TABLE IF EXISTS `topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL COMMENT 'Name of topic in which users can ask associated questions.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `topic_name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topics`
--

LOCK TABLES `topics` WRITE;
/*!40000 ALTER TABLE `topics` DISABLE KEYS */;
INSERT INTO `topics` VALUES (4,'.NET'),(33,'11th topic'),(5,'C++'),(3,'Databases and SQL'),(37,'ewgwg'),(35,'fwegwegweg'),(40,'hartrrth'),(2,'Java Enterprice Edition'),(1,'Java Standart Edition'),(6,'JavaScript'),(39,'nnnnnnnnnnnnnnnnnnnnnnnnnnnnn'),(7,'Other'),(41,'Test escapeXmlfunction'),(42,'TOPIC_COOKIES');
/*!40000 ALTER TABLE `topics` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`topics_BEFORE_DELETE` BEFORE DELETE ON `topics` FOR EACH ROW
  BEGIN
    #questions_count
    UPDATE users JOIN questions USING(login) SET questions_count = questions_count -
                                                                   (SELECT COUNT(1) FROM questions
                                                                   WHERE topic_id = OLD.id AND users.login = questions.login)
    WHERE users.login = questions.login;

    #answers_count
    UPDATE users JOIN answers USING(login) SET answers_count = answers_count -
                                                               (SELECT COUNT(1) FROM answers
                                                                 JOIN questions ON answers.question_id = questions.id
                                                               WHERE questions.topic_id = OLD.id AND users.login = answers.login)
    WHERE answers.login = users.login;

    #reputation
    UPDATE users JOIN answers USING(login) SET users.rating = users.rating -
                                                              (SELECT res FROM (SELECT answers.login, SUM(rating) AS 'res' FROM answers
                                                                JOIN questions ON answers.question_id = questions.id
                                                              GROUP BY topic_id, answers.login
                                                              HAVING questions.topic_id = OLD.id
                                                                               ) AS `tbl`
                                                              WHERE login = users.login)
    WHERE answers.login = users.login AND answers.question_id IN (SELECT id FROM questions WHERE topic_id = OLD.id);

    #restore negative rating to estimator topics
    UPDATE users JOIN answer_marks USING(login) JOIN answers ON answers.id = answer_marks.answer_id
    SET users.rating = users.rating -
                       coalesce((SELECT SUM(res) FROM
                         (SELECT answer_marks.login, SUM(mark) as 'res' FROM answer_marks
                           JOIN answers ON answer_marks.answer_id = answers.id
                           JOIN questions ON answers.question_id = questions.id
                           JOIN topics ON questions.topic_id = topics.id
                         GROUP BY answer_marks.login, topics.id, mark
                         HAVING mark < 0 AND topics.id = OLD.id) AS `tbl`
                       WHERE login = users.login), 0)
    WHERE answer_marks.login = users.login;


  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `login` varchar(30) NOT NULL COMMENT 'Since user cannot be deleted, so using natural PK is a good idea.',
  `password` char(60) NOT NULL COMMENT 'The field for the password hash. I decided to use BCrypt algorithm. It hashes password to a 60 length string(including salt).',
  `email` varchar(200) NOT NULL,
  `questions_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'questions, answers and pages count will be used too often.',
  `answers_count` int(10) unsigned NOT NULL DEFAULT '0',
  `pages_viewed` int(10) unsigned NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0' COMMENT 'Aside from performance details, rating can be changed with no reference to an assessment of user answer. If the user downrate to another, he loses the same points of reputation too.',
  `avatar_location` varchar(250) NOT NULL DEFAULT 'DEFAULT_LOCATION' COMMENT 'Is not unique because of if user has not specified his avatar, web server will provide standart avatar.',
  `rights` set('read','watch_profile','write','moderate','admin') NOT NULL DEFAULT 'read,watch_profile,write' COMMENT 'Only one person can use  ''admin'' right. Admin can restrict any right.',
  PRIMARY KEY (`login`),
  UNIQUE KEY `users_email_uniq_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('admin','$2a$10$PIRIL3DfVPYg1s5Uzwsj4OYMBynkWdFvjc04hL5tNzF10VM5kfX0C','admin@mail.ru',3,37,331,45,'/avatar\\default.png','read,watch_profile,write,moderate,admin'),('alex_spz','$2a$10$tJwlEs193IQVmZFEDEVovuN1Pemz8aNoNPUPqTi41Cr0Xij7/LTi2','spz_alex@rambler.ru',1,3,0,20,'/avatar/default.png','read,watch_profile,write'),('BalusC','$2a$10$tFDosBCEabn9in96jNJUV.YKPe6DGwDW7ckL/qe6Fi6ZCrySOv6oe','javaee_master@google.com',0,4,0,59,'/avatar/BalusC.png','read,watch_profile,write'),('BigJoe','$2a$10$.ocWM8iQwDX4BpwBfbd96O52t.IL5y58ja65UrYecsFpYUxpvXo2e','big_bro@google.com',0,1,0,10,'/avatar/BigJoe.png','read,watch_profile,write'),('Fooooo','$2a$10$9AtbF.UKj7qjQgx9bdfR.uzPJM1OTaKPETlkkkXrYfW9..mt.jWfO','qwe@ww.w',2,0,7,0,'/avatar/default.png','read,watch_profile,write'),('Grungle','$2a$10$UG1YVaV1AlrgGLxZX6LfyuKVfQf.ofISQ3IpVRtOM.O887nH2wrKC','instanceofnull@yahoo.com',2,4,245,31,'/avatar\\Grungle.jpg','read,watch_profile,write,moderate'),('it_expert','$2a$10$3pvqPvKm3TXlLgVycgiMYOaU.urkCyrInI5Li02N8iNN5OEsHb.92','expert_ao@yandex.ru',2,0,0,-3,'/avatar/default.png','read,watch_profile,write'),('Jenkins','$2a$10$FGzM5LlhZcyXkGJcCSqYm.ZKLTDcFMCjaTsLto4zxX1fnJuqKYR12','jen@google.com',2,1,0,4,'/avatar/Jenkins.png','read,watch_profile,write'),('majestics','$2a$10$oCI1GyJIeWTWkIMoztRWbu6hJ5ELMgH9g1TfonJog598jxHuQpf2y','majesti123@google.com',3,2,0,8,'/avatar/majestics.png','read,watch_profile,write'),('moder','$2a$10$r9mPJpliNyZzE0p/erK.PuQPRdEHjbVNeJJuy2udRNLdJToemEzLu','use_moder@gmail.com',1,0,0,0,'/avatar/moder.png','read,watch_profile,write,moderate'),('moderator','$2a$10$ZHlP9caze1qNuizyefFwueHgcVAmZfGQ6FPqoLFylo4nXMMw1pdVi','moder@mail.ru',1,1,0,2,'/avatar/default.png','read,watch_profile,write,moderate'),('pro_master','$2a$10$AgO6sFuhk6zwBdR74/S3luIJHwQdKfiE/wAj/mR/3fH.3cGhOljKK','master_of@yahoo.com',3,4,78,17,'/avatar\\pro_master.png','read,watch_profile,write'),('some_user','$2a$10$IajfyvE5qR0mGyOHDkHfa.YavrWccns4yDvkECG777VHJXbv8T/KW','useruser@mail.ru',2,1,0,6,'/avatar/default.png','read,watch_profile,write'),('vbnmz','$2a$10$1/UMSUSHPzurhe7M1cPJKuS7QRxBucchuliKGh3ZtVJPHBkBBFTGm','qqasd@gs.g',0,0,25,0,'/avatar/vbnmz.png','read,watch_profile,write'),('zzzzzzzzzzzzzzzzzzz','$2a$10$yUpW8.AxHMvfLmCQslK7quC2C42knCCiUPklfW0KrIMAe3Vpn0ScS','qqwe@qwfg.g',0,0,0,0,'/avatar/zzzzzzzzzzzzzzzzzzz.jpg','read,watch_profile,write');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,ALLOW_INVALID_DATES,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`checkIfAdminExists` BEFORE INSERT ON `users` FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM users WHERE find_in_set('admin', rights) > 0 AND find_in_set('admin', NEW.rights) > 0) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Admin is already exist', MYSQL_ERRNO = 1001;
    END IF;
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`users_constraintUpdating` BEFORE UPDATE ON `users` FOR EACH ROW
  BEGIN
    IF (find_in_set('admin', OLD.rights) > 0 AND NEW.rights != OLD.rights) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Changing admin rights is forbidden!', MYSQL_ERRNO = 1001;
    ELSEIF (find_in_set('admin', OLD.rights) = 0 AND find_in_set('admin', NEW.rights) > 0) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Admin is already exist!', MYSQL_ERRNO = 1001;
    END IF;
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `likeit_community_test`.`users_ForbidDeleting` BEFORE DELETE ON `users` FOR EACH ROW
  BEGIN
    # SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Delete operation is not supported!', MYSQL_ERRNO = 1001;
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `users_info`
--

DROP TABLE IF EXISTS `users_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_info` (
  `login` varchar(30) NOT NULL,
  `f_name` varchar(50) DEFAULT NULL,
  `l_name` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`login`),
  CONSTRAINT `users_info_ibfk_1` FOREIGN KEY (`login`) REFERENCES `users` (`login`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This data will be displayed only in user''s profile. In addition to it, only registered users can watch profiles. It means this data will be used not too often unlike data in users table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_info`
--

LOCK TABLES `users_info` WRITE;
/*!40000 ALTER TABLE `users_info` DISABLE KEYS */;
INSERT INTO `users_info` VALUES ('admin','Alex','Sanchez','Chilie','Ch','LikeIT community','Administrator'),('alex_spz',NULL,NULL,'Russia','Moscow','EPAM',NULL),('BalusC','Bauke','Scholtz','Neatherlands','Amsterdam','JSF','Developer'),('BigJoe','Joe','Anders','USA','Moscow',NULL,'Web Designer'),('Fooooo','Leo','Messi','Belarus','Homel','---','---'),('Grungle','Pavel','Bachilo','Belarus','Minsk','EPAM','Student'),('it_expert',NULL,NULL,'France','Lille',NULL,NULL),('Jenkins','Jerry','Jenkins','USA',NULL,'IBM','Database Administrator'),('majestics','Mark',NULL,NULL,'Tokio','Google','Developer'),('moder','Manuel','Muller','Germany','Berlin','Oracle',NULL),('moderator','George','Smith','UK','London','LikeIT','Moderator'),('pro_master','Nick','','Hungary','','',''),('some_user','Andrey',NULL,'Russia',NULL,'MSU','Student'),('vbnmz','Fqwe','Fqwe','Fqwe','Fqwe','Fqwe','Fqwe'),('zzzzzzzzzzzzzzzzzzz','Qwer','Qwer','Qwer','Qwer-Qwe','Qwer','Qwer');
/*!40000 ALTER TABLE `users_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'likeit_community_test'
--

--
-- Dumping routines for database 'likeit_community_test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-16 18:14:10
